﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp6
{
    public static class Value
    {
        public static double r { get; set; }
        public static double vol { get; set; }
        public static double t { get; set; }
        public static double S { get; set; }
        public static double K { get; set; }
        public static uint step { get; set; }
        public static UInt32 trials { get; set; }
        public static int Thread;
        public static double rebate { get; set; }
        public static double barrier { get; set; }

        public static double Price { get; set; }
        public static double SE { get; set; }
        public static double delta { get; set; }
        public static double gamma { get; set; }
        public static double theta { get; set; }
        public static double vega { get; set; }
        public static double rho { get; set; }
        public static string timer { get; set; }
    }
}
