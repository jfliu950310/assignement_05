﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ConsoleApp6
{
    public partial class Gui : Form
    {
        
        public Gui()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            EuroPresent eu = new EuroPresent();
            AsianPresent asian = new AsianPresent();
            DigitalPresent di = new DigitalPresent();
            BarrierPresent ba = new BarrierPresent();
            LookbackPresent lo = new LookbackPresent();
            RangePresent ra = new RangePresent();
            progressBar1.Value = 0;
            progressBar1.Maximum = 13;
                try
                {
                    Value.S = Convert.ToDouble(UnderlyingPrice.Text);
                    Value.K = Convert.ToDouble(StrikePrice.Text);
                    Value.t = Convert.ToDouble(Tenor.Text);
                    Value.vol= Convert.ToDouble(Volatility.Text);
                    Value.r = Convert.ToDouble(RiskfreeRate.Text);
                    Value.step = Convert.ToUInt16(Steps.Text);
                    Value.trials = Convert.ToUInt32(Trials.Text);
                    Value.rebate = Convert.ToDouble(Rebate.Text);
                    Value.barrier = Convert.ToDouble(Barrier.Text);
                if (checkBox6.Checked == true && checkBox7.Checked == false && checkBox8.Checked == false && checkBox9.Checked == false && checkBox10.Checked == false && checkBox11.Checked == false)
                {
                    if (checkBox3.Checked == false && checkBox4.Checked == false && checkBox5.Checked == false)
                    {
                        if (checkBox1.Checked == true && checkBox2.Checked == false)
                        {
                            Thread r = new Thread(eu.EuroCall);
                            r.Start();
                        }
                        else if (checkBox1.Checked == false && checkBox2.Checked == true)
                        {
                            Thread r = new Thread(eu.EuroPut);
                            r.Start();
                        }
                        else { MessageBox.Show("Invalid options"); }
                    }

                    if (checkBox3.Checked == true && checkBox4.Checked == false && checkBox5.Checked == false)
                    {
                        if (checkBox1.Checked == true && checkBox2.Checked == false)
                        {
                            Thread r = new Thread(eu.EuroCallAnti);
                            r.Start();
                        }
                        else if (checkBox1.Checked == false && checkBox2.Checked == true)
                        {
                            Thread r = new Thread(eu.EuroPutAnti);
                            r.Start();
                        }
                        else { MessageBox.Show("Invalid options"); }
                    }

                    if (checkBox3.Checked == true && checkBox4.Checked == true && checkBox5.Checked == false)
                    {
                        if (checkBox1.Checked == true && checkBox2.Checked == false)
                        {
                            Thread r = new Thread(eu.EuroCallAntiCV);
                            r.Start();
                        }
                        else if (checkBox1.Checked == false && checkBox2.Checked == true)
                        {
                            Thread r = new Thread(eu.EuroPutAntiCV);
                            r.Start();
                        }
                        else { MessageBox.Show("Invalid options"); }
                    }

                    if (checkBox3.Checked == true && checkBox4.Checked == true && checkBox5.Checked == true)
                    {
                        Value.Thread = 4;
                        if (checkBox1.Checked == true && checkBox2.Checked == false)
                        {
                            Thread r = new Thread(eu.EuroCallAntiCVMT);
                            r.Start();
                        }
                        else if (checkBox1.Checked == false && checkBox2.Checked == true)
                        {
                            Thread r = new Thread(eu.EuroPutAntiCVMT);
                            r.Start();
                        }
                        else { MessageBox.Show("Invalid options"); }
                    }

                    if (checkBox3.Checked == false && checkBox4.Checked == true && checkBox5.Checked == false)
                    {
                        if (checkBox1.Checked == true && checkBox2.Checked == false)
                        {
                            Thread r = new Thread(eu.EuroCallCV);
                            r.Start();
                        }
                        else if (checkBox1.Checked == false && checkBox2.Checked == true)
                        {
                            Thread r = new Thread(eu.EuroPutCV);
                            r.Start();
                        }
                        else { MessageBox.Show("Invalid options"); }
                    }

                    if (checkBox3.Checked == false && checkBox4.Checked == true && checkBox5.Checked == true)
                    {
                        Value.Thread = 4;
                        if (checkBox1.Checked == true && checkBox2.Checked == false)
                        {
                            Thread r = new Thread(eu.EuroCallCVMT);
                            r.Start();
                        }
                        else if (checkBox1.Checked == false && checkBox2.Checked == true)
                        {
                            Thread r = new Thread(eu.EuroPutCVMT);
                            r.Start();
                        }
                        else { MessageBox.Show("Invalid options"); }
                    }

                    if (checkBox3.Checked == false && checkBox4.Checked == false && checkBox5.Checked == true)
                    {
                        Value.Thread = 4;
                        if (checkBox1.Checked == true && checkBox2.Checked == false)
                        {
                            Thread r = new Thread(eu.EuroCallMT);
                            r.Start();
                        }
                        else if (checkBox1.Checked == false && checkBox2.Checked == true)
                        {
                            Thread r = new Thread(eu.EuroPutMT);
                            r.Start();
                        }
                        else { MessageBox.Show("Invalid options"); }
                    }

                    if (checkBox3.Checked == true && checkBox4.Checked == false && checkBox5.Checked == true)
                    {
                        Value.Thread = 4;
                        if (checkBox1.Checked == true && checkBox2.Checked == false)
                        {
                            Thread r = new Thread(eu.EuroCallAntiMT);
                            r.Start();
                        }
                        else if (checkBox1.Checked == false && checkBox2.Checked == true)
                        {
                            Thread r = new Thread(eu.EuroPutAntiMT);
                            r.Start();
                        }
                        else { MessageBox.Show("Invalid options"); }
                    }
                }
                else if (checkBox6.Checked == false && checkBox7.Checked == true && checkBox8.Checked == false && checkBox9.Checked == false && checkBox10.Checked == false && checkBox11.Checked == false)
                {
                    if (checkBox3.Checked == false && checkBox4.Checked == false && checkBox5.Checked == false)
                    {
                        if (checkBox1.Checked == true && checkBox2.Checked == false)
                        {
                            Thread r = new Thread(asian.AsianCall);
                            r.Start();
                        }
                        else if (checkBox1.Checked == false && checkBox2.Checked == true)
                        {
                            Thread r = new Thread(asian.AsianPut);
                            r.Start();
                        }
                        else { MessageBox.Show("Invalid options"); }
                    }

                    if (checkBox3.Checked == true && checkBox4.Checked == false && checkBox5.Checked == false)
                    {
                        if (checkBox1.Checked == true && checkBox2.Checked == false)
                        {
                            Thread r = new Thread(asian.AsianCallAnti);
                            r.Start();
                        }
                        else if (checkBox1.Checked == false && checkBox2.Checked == true)
                        {
                            Thread r = new Thread(asian.AsianPutAnti);
                            r.Start();
                        }
                        else { MessageBox.Show("Invalid options"); }
                    }

                    if (checkBox3.Checked == true && checkBox4.Checked == true && checkBox5.Checked == false)
                    {
                        if (checkBox1.Checked == true && checkBox2.Checked == false)
                        {
                            Thread r = new Thread(asian.AsianCallAntiCV);
                            r.Start();
                        }
                        else if (checkBox1.Checked == false && checkBox2.Checked == true)
                        {
                            Thread r = new Thread(asian.AsianPutAntiCV);
                            r.Start();
                        }
                        else { MessageBox.Show("Invalid options"); }
                    }

                    if (checkBox3.Checked == true && checkBox4.Checked == true && checkBox5.Checked == true)
                    {
                        Value.Thread = 4;
                        if (checkBox1.Checked == true && checkBox2.Checked == false)
                        {
                            Thread r = new Thread(asian.AsianCallAntiCVMT);
                            r.Start();
                        }
                        else if (checkBox1.Checked == false && checkBox2.Checked == true)
                        {
                            Thread r = new Thread(asian.AsianPutAntiCVMT);
                            r.Start();
                        }
                        else { MessageBox.Show("Invalid options"); }
                    }

                    if (checkBox3.Checked == false && checkBox4.Checked == true && checkBox5.Checked == false)
                    {
                        if (checkBox1.Checked == true && checkBox2.Checked == false)
                        {
                            Thread r = new Thread(asian.AsianCallCV);
                            r.Start();
                        }
                        else if (checkBox1.Checked == false && checkBox2.Checked == true)
                        {
                            Thread r = new Thread(asian.AsianPutCV);
                            r.Start();
                        }
                        else { MessageBox.Show("Invalid options"); }
                    }

                    if (checkBox3.Checked == false && checkBox4.Checked == true && checkBox5.Checked == true)
                    {
                        Value.Thread = 4;
                        if (checkBox1.Checked == true && checkBox2.Checked == false)
                        {
                            Thread r = new Thread(asian.AsianCallCVMT);
                            r.Start();
                        }
                        else if (checkBox1.Checked == false && checkBox2.Checked == true)
                        {
                            Thread r = new Thread(asian.AsianPutCVMT);
                            r.Start();
                        }
                        else { MessageBox.Show("Invalid options"); }
                    }

                    if (checkBox3.Checked == false && checkBox4.Checked == false && checkBox5.Checked == true)
                    {
                        Value.Thread = 4;
                        if (checkBox1.Checked == true && checkBox2.Checked == false)
                        {
                            Thread r = new Thread(asian.AsianCallMT);
                            r.Start();
                        }
                        else if (checkBox1.Checked == false && checkBox2.Checked == true)
                        {
                            Thread r = new Thread(asian.AsianPutMT);
                            r.Start();
                        }
                        else { MessageBox.Show("Invalid options"); }
                    }

                    if (checkBox3.Checked == true && checkBox4.Checked == false && checkBox5.Checked == true)
                    {
                        Value.Thread = 4;
                        if (checkBox1.Checked == true && checkBox2.Checked == false)
                        {
                            Thread r = new Thread(asian.AsianCallAntiMT);
                            r.Start();
                        }
                        else if (checkBox1.Checked == false && checkBox2.Checked == true)
                        {
                            Thread r = new Thread(asian.AsianPutAntiMT);
                            r.Start();
                        }
                        else { MessageBox.Show("Invalid options"); }
                    }
                }
                else if (checkBox6.Checked == false && checkBox7.Checked == false && checkBox8.Checked == true && checkBox9.Checked == false && checkBox10.Checked == false && checkBox11.Checked == false)
                {
                    if (checkBox3.Checked == false && checkBox4.Checked == false && checkBox5.Checked == false)
                    {
                        if (checkBox1.Checked == true && checkBox2.Checked == false)
                        {
                            Thread r = new Thread(di.DigitalCall);
                            r.Start();
                        }
                        else if (checkBox1.Checked == false && checkBox2.Checked == true)
                        {
                            Thread r = new Thread(di.DigitalPut);
                            r.Start();
                        }
                        else { MessageBox.Show("Invalid options"); }
                    }

                    if (checkBox3.Checked == true && checkBox4.Checked == false && checkBox5.Checked == false)
                    {
                        if (checkBox1.Checked == true && checkBox2.Checked == false)
                        {
                            Thread r = new Thread(di.DigitalCallAnti);
                            r.Start();
                        }
                        else if (checkBox1.Checked == false && checkBox2.Checked == true)
                        {
                            Thread r = new Thread(di.DigitalPutAnti);
                            r.Start();
                        }
                        else { MessageBox.Show("Invalid options"); }
                    }

                    if (checkBox3.Checked == true && checkBox4.Checked == true && checkBox5.Checked == false)
                    {
                        if (checkBox1.Checked == true && checkBox2.Checked == false)
                        {
                            Thread r = new Thread(di.DigitalCallAntiCV);
                            r.Start();
                        }
                        else if (checkBox1.Checked == false && checkBox2.Checked == true)
                        {
                            Thread r = new Thread(di.DigitalPutAntiCV);
                            r.Start();
                        }
                        else { MessageBox.Show("Invalid options"); }
                    }

                    if (checkBox3.Checked == true && checkBox4.Checked == true && checkBox5.Checked == true)
                    {
                        Value.Thread = 4;
                        if (checkBox1.Checked == true && checkBox2.Checked == false)
                        {
                            Thread r = new Thread(di.DigitalCallAntiCVMT);
                            r.Start();
                        }
                        else if (checkBox1.Checked == false && checkBox2.Checked == true)
                        {
                            Thread r = new Thread(di.DigitalPutAntiCVMT);
                            r.Start();
                        }
                        else { MessageBox.Show("Invalid options"); }
                    }

                    if (checkBox3.Checked == false && checkBox4.Checked == true && checkBox5.Checked == false)
                    {
                        if (checkBox1.Checked == true && checkBox2.Checked == false)
                        {
                            Thread r = new Thread(di.DigitalCallCV);
                            r.Start();
                        }
                        else if (checkBox1.Checked == false && checkBox2.Checked == true)
                        {
                            Thread r = new Thread(di.DigitalPutCV);
                            r.Start();
                        }
                        else { MessageBox.Show("Invalid options"); }
                    }

                    if (checkBox3.Checked == false && checkBox4.Checked == true && checkBox5.Checked == true)
                    {
                        Value.Thread = 4;
                        if (checkBox1.Checked == true && checkBox2.Checked == false)
                        {
                            Thread r = new Thread(di.DigitalCallCVMT);
                            r.Start();
                        }
                        else if (checkBox1.Checked == false && checkBox2.Checked == true)
                        {
                            Thread r = new Thread(di.DigitalPutCVMT);
                            r.Start();
                        }
                        else { MessageBox.Show("Invalid options"); }
                    }

                    if (checkBox3.Checked == false && checkBox4.Checked == false && checkBox5.Checked == true)
                    {
                        Value.Thread = 4;
                        if (checkBox1.Checked == true && checkBox2.Checked == false)
                        {
                            Thread r = new Thread(di.DigitalCallMT);
                            r.Start();
                        }
                        else if (checkBox1.Checked == false && checkBox2.Checked == true)
                        {
                            Thread r = new Thread(di.DigitalPutMT);
                            r.Start();
                        }
                        else { MessageBox.Show("Invalid options"); }
                    }

                    if (checkBox3.Checked == true && checkBox4.Checked == false && checkBox5.Checked == true)
                    {
                        Value.Thread = 4;
                        if (checkBox1.Checked == true && checkBox2.Checked == false)
                        {
                            Thread r = new Thread(di.DigitalCallAntiMT);
                            r.Start();
                        }
                        else if (checkBox1.Checked == false && checkBox2.Checked == true)
                        {
                            Thread r = new Thread(di.DigitalPutAntiMT);
                            r.Start();
                        }
                        else { MessageBox.Show("Invalid options"); }
                    }
                }
                else if (checkBox6.Checked == false && checkBox7.Checked == false && checkBox8.Checked == false && checkBox9.Checked == true && checkBox10.Checked == false && checkBox11.Checked == false)
                {
                    if (checkBox12.Checked == true && checkBox13.Checked == false && checkBox14.Checked == false && checkBox15.Checked == false)
                    {
                        if (Value.barrier >= Value.S)
                        {
                            MessageBox.Show("Invalid value");
                        }
                        else
                        {
                            if (checkBox3.Checked == false && checkBox4.Checked == false && checkBox5.Checked == false)
                            {
                                if (checkBox1.Checked == true && checkBox2.Checked == false)
                                {
                                    Thread r = new Thread(ba.BarrierDAOCall);
                                    r.Start();
                                }
                                else if (checkBox1.Checked == false && checkBox2.Checked == true)
                                {
                                    Thread r = new Thread(ba.BarrierDAOPut);
                                    r.Start();
                                }
                                else { MessageBox.Show("Invalid options"); }
                            }

                            if (checkBox3.Checked == true && checkBox4.Checked == false && checkBox5.Checked == false)
                            {
                                if (checkBox1.Checked == true && checkBox2.Checked == false)
                                {
                                    Thread r = new Thread(ba.BarrierDAOCallAnti);
                                    r.Start();
                                }
                                else if (checkBox1.Checked == false && checkBox2.Checked == true)
                                {
                                    Thread r = new Thread(ba.BarrierDAOPutAnti);
                                    r.Start();
                                }
                                else { MessageBox.Show("Invalid options"); }
                            }

                            if (checkBox3.Checked == true && checkBox4.Checked == true && checkBox5.Checked == false)
                            {
                                if (checkBox1.Checked == true && checkBox2.Checked == false)
                                {
                                    Thread r = new Thread(ba.BarrierDAOCallAntiCV);
                                    r.Start();
                                }
                                else if (checkBox1.Checked == false && checkBox2.Checked == true)
                                {
                                    Thread r = new Thread(ba.BarrierDAOPutAntiCV);
                                    r.Start();
                                }
                                else { MessageBox.Show("Invalid options"); }
                            }

                            if (checkBox3.Checked == true && checkBox4.Checked == true && checkBox5.Checked == true)
                            {
                                Value.Thread = 4;
                                if (checkBox1.Checked == true && checkBox2.Checked == false)
                                {
                                    Thread r = new Thread(ba.BarrierDAOCallAntiCVMT);
                                    r.Start();
                                }
                                else if (checkBox1.Checked == false && checkBox2.Checked == true)
                                {
                                    Thread r = new Thread(ba.BarrierDAOPutAntiCVMT);
                                    r.Start();
                                }
                                else { MessageBox.Show("Invalid options"); }
                            }

                            if (checkBox3.Checked == false && checkBox4.Checked == true && checkBox5.Checked == false)
                            {
                                if (checkBox1.Checked == true && checkBox2.Checked == false)
                                {
                                    Thread r = new Thread(ba.BarrierDAOCallCV);
                                    r.Start();
                                }
                                else if (checkBox1.Checked == false && checkBox2.Checked == true)
                                {
                                    Thread r = new Thread(ba.BarrierDAOPutCV);
                                    r.Start();
                                }
                                else { MessageBox.Show("Invalid options"); }
                            }

                            if (checkBox3.Checked == false && checkBox4.Checked == true && checkBox5.Checked == true)
                            {
                                Value.Thread = 4;
                                if (checkBox1.Checked == true && checkBox2.Checked == false)
                                {
                                    Thread r = new Thread(ba.BarrierDAOCallCVMT);
                                    r.Start();
                                }
                                else if (checkBox1.Checked == false && checkBox2.Checked == true)
                                {
                                    Thread r = new Thread(ba.BarrierDAOPutCVMT);
                                    r.Start();
                                }
                                else { MessageBox.Show("Invalid options"); }
                            }

                            if (checkBox3.Checked == false && checkBox4.Checked == false && checkBox5.Checked == true)
                            {
                                Value.Thread = 4;
                                if (checkBox1.Checked == true && checkBox2.Checked == false)
                                {
                                    Thread r = new Thread(ba.BarrierDAOCallMT);
                                    r.Start();
                                }
                                else if (checkBox1.Checked == false && checkBox2.Checked == true)
                                {
                                    Thread r = new Thread(ba.BarrierDAOPutMT);
                                    r.Start();
                                }
                                else { MessageBox.Show("Invalid options"); }
                            }

                            if (checkBox3.Checked == true && checkBox4.Checked == false && checkBox5.Checked == true)
                            {
                                Value.Thread = 4;
                                if (checkBox1.Checked == true && checkBox2.Checked == false)
                                {
                                    Thread r = new Thread(ba.BarrierDAOCallAntiMT);
                                    r.Start();
                                }
                                else if (checkBox1.Checked == false && checkBox2.Checked == true)
                                {
                                    Thread r = new Thread(ba.BarrierDAOPutAntiMT);
                                    r.Start();
                                }
                                else { MessageBox.Show("Invalid options"); }
                            }
                        }
                    }
                    else if (checkBox12.Checked == false && checkBox13.Checked == true && checkBox14.Checked == false && checkBox15.Checked == false)
                    {
                        if (Value.barrier <= Value.S)
                        {
                            MessageBox.Show("Invalid value");
                        }
                        else
                        {
                            if (checkBox3.Checked == false && checkBox4.Checked == false && checkBox5.Checked == false)
                            {
                                if (checkBox1.Checked == true && checkBox2.Checked == false)
                                {
                                    Thread r = new Thread(ba.BarrierUAOCall);
                                    r.Start();
                                }
                                else if (checkBox1.Checked == false && checkBox2.Checked == true)
                                {
                                    Thread r = new Thread(ba.BarrierUAOPut);
                                    r.Start();
                                }
                                else { MessageBox.Show("Invalid options"); }
                            }

                            if (checkBox3.Checked == true && checkBox4.Checked == false && checkBox5.Checked == false)
                            {
                                if (checkBox1.Checked == true && checkBox2.Checked == false)
                                {
                                    Thread r = new Thread(ba.BarrierUAOCallAnti);
                                    r.Start();
                                }
                                else if (checkBox1.Checked == false && checkBox2.Checked == true)
                                {
                                    Thread r = new Thread(ba.BarrierUAOPutAnti);
                                    r.Start();
                                }
                                else { MessageBox.Show("Invalid options"); }
                            }

                            if (checkBox3.Checked == true && checkBox4.Checked == true && checkBox5.Checked == false)
                            {
                                if (checkBox1.Checked == true && checkBox2.Checked == false)
                                {
                                    Thread r = new Thread(ba.BarrierUAOCallAntiCV);
                                    r.Start();
                                }
                                else if (checkBox1.Checked == false && checkBox2.Checked == true)
                                {
                                    Thread r = new Thread(ba.BarrierUAOPutAntiCV);
                                    r.Start();
                                }
                                else { MessageBox.Show("Invalid options"); }
                            }

                            if (checkBox3.Checked == true && checkBox4.Checked == true && checkBox5.Checked == true)
                            {
                                Value.Thread = 4;
                                if (checkBox1.Checked == true && checkBox2.Checked == false)
                                {
                                    Thread r = new Thread(ba.BarrierUAOCallAntiCVMT);
                                    r.Start();
                                }
                                else if (checkBox1.Checked == false && checkBox2.Checked == true)
                                {
                                    Thread r = new Thread(ba.BarrierUAOPutAntiCVMT);
                                    r.Start();
                                }
                                else { MessageBox.Show("Invalid options"); }
                            }

                            if (checkBox3.Checked == false && checkBox4.Checked == true && checkBox5.Checked == false)
                            {
                                if (checkBox1.Checked == true && checkBox2.Checked == false)
                                {
                                    Thread r = new Thread(ba.BarrierUAOCallCV);
                                    r.Start();
                                }
                                else if (checkBox1.Checked == false && checkBox2.Checked == true)
                                {
                                    Thread r = new Thread(ba.BarrierUAOPutCV);
                                    r.Start();
                                }
                                else { MessageBox.Show("Invalid options"); }
                            }

                            if (checkBox3.Checked == false && checkBox4.Checked == true && checkBox5.Checked == true)
                            {
                                Value.Thread = 4;
                                if (checkBox1.Checked == true && checkBox2.Checked == false)
                                {
                                    Thread r = new Thread(ba.BarrierUAOCallCVMT);
                                    r.Start();
                                }
                                else if (checkBox1.Checked == false && checkBox2.Checked == true)
                                {
                                    Thread r = new Thread(ba.BarrierUAOPutCVMT);
                                    r.Start();
                                }
                                else { MessageBox.Show("Invalid options"); }
                            }

                            if (checkBox3.Checked == false && checkBox4.Checked == false && checkBox5.Checked == true)
                            {
                                Value.Thread = 4;
                                if (checkBox1.Checked == true && checkBox2.Checked == false)
                                {
                                    Thread r = new Thread(ba.BarrierUAOCallMT);
                                    r.Start();
                                }
                                else if (checkBox1.Checked == false && checkBox2.Checked == true)
                                {
                                    Thread r = new Thread(ba.BarrierUAOPutMT);
                                    r.Start();
                                }
                                else { MessageBox.Show("Invalid options"); }
                            }

                            if (checkBox3.Checked == true && checkBox4.Checked == false && checkBox5.Checked == true)
                            {
                                Value.Thread = 4;
                                if (checkBox1.Checked == true && checkBox2.Checked == false)
                                {
                                    Thread r = new Thread(ba.BarrierUAOCallAntiMT);
                                    r.Start();
                                }
                                else if (checkBox1.Checked == false && checkBox2.Checked == true)
                                {
                                    Thread r = new Thread(ba.BarrierUAOPutAntiMT);
                                    r.Start();
                                }
                                else { MessageBox.Show("Invalid options"); }
                            }
                        }
                    }
                    else if (checkBox12.Checked == false && checkBox13.Checked == false && checkBox14.Checked == true && checkBox15.Checked == false)
                    {
                        if (Value.barrier >= Value.S)
                        {
                            MessageBox.Show("Invalid value");
                        }
                        else
                        {
                            if (checkBox3.Checked == false && checkBox4.Checked == false && checkBox5.Checked == false)
                            {
                                if (checkBox1.Checked == true && checkBox2.Checked == false)
                                {
                                    Thread r = new Thread(ba.BarrierDAICall);
                                    r.Start();
                                }
                                else if (checkBox1.Checked == false && checkBox2.Checked == true)
                                {
                                    Thread r = new Thread(ba.BarrierDAIPut);
                                    r.Start();
                                }
                                else { MessageBox.Show("Invalid options"); }
                            }

                            if (checkBox3.Checked == true && checkBox4.Checked == false && checkBox5.Checked == false)
                            {
                                if (checkBox1.Checked == true && checkBox2.Checked == false)
                                {
                                    Thread r = new Thread(ba.BarrierDAICallAnti);
                                    r.Start();
                                }
                                else if (checkBox1.Checked == false && checkBox2.Checked == true)
                                {
                                    Thread r = new Thread(ba.BarrierDAIPutAnti);
                                    r.Start();
                                }
                                else { MessageBox.Show("Invalid options"); }
                            }

                            if (checkBox3.Checked == true && checkBox4.Checked == true && checkBox5.Checked == false)
                            {
                                if (checkBox1.Checked == true && checkBox2.Checked == false)
                                {
                                    Thread r = new Thread(ba.BarrierDAICallAntiCV);
                                    r.Start();
                                }
                                else if (checkBox1.Checked == false && checkBox2.Checked == true)
                                {
                                    Thread r = new Thread(ba.BarrierDAIPutAntiCV);
                                    r.Start();
                                }
                                else { MessageBox.Show("Invalid options"); }
                            }

                            if (checkBox3.Checked == true && checkBox4.Checked == true && checkBox5.Checked == true)
                            {
                                Value.Thread = 4;
                                if (checkBox1.Checked == true && checkBox2.Checked == false)
                                {
                                    Thread r = new Thread(ba.BarrierDAICallAntiCVMT);
                                    r.Start();
                                }
                                else if (checkBox1.Checked == false && checkBox2.Checked == true)
                                {
                                    Thread r = new Thread(ba.BarrierDAIPutAntiCVMT);
                                    r.Start();
                                }
                                else { MessageBox.Show("Invalid options"); }
                            }

                            if (checkBox3.Checked == false && checkBox4.Checked == true && checkBox5.Checked == false)
                            {
                                if (checkBox1.Checked == true && checkBox2.Checked == false)
                                {
                                    Thread r = new Thread(ba.BarrierDAICallCV);
                                    r.Start();
                                }
                                else if (checkBox1.Checked == false && checkBox2.Checked == true)
                                {
                                    Thread r = new Thread(ba.BarrierDAIPutCV);
                                    r.Start();
                                }
                                else { MessageBox.Show("Invalid options"); }
                            }

                            if (checkBox3.Checked == false && checkBox4.Checked == true && checkBox5.Checked == true)
                            {
                                Value.Thread = 4;
                                if (checkBox1.Checked == true && checkBox2.Checked == false)
                                {
                                    Thread r = new Thread(ba.BarrierDAICallCVMT);
                                    r.Start();
                                }
                                else if (checkBox1.Checked == false && checkBox2.Checked == true)
                                {
                                    Thread r = new Thread(ba.BarrierDAIPutCVMT);
                                    r.Start();
                                }
                                else { MessageBox.Show("Invalid options"); }
                            }

                            if (checkBox3.Checked == false && checkBox4.Checked == false && checkBox5.Checked == true)
                            {
                                Value.Thread = 4;
                                if (checkBox1.Checked == true && checkBox2.Checked == false)
                                {
                                    Thread r = new Thread(ba.BarrierDAICallMT);
                                    r.Start();
                                }
                                else if (checkBox1.Checked == false && checkBox2.Checked == true)
                                {
                                    Thread r = new Thread(ba.BarrierDAIPutMT);
                                    r.Start();
                                }
                                else { MessageBox.Show("Invalid options"); }
                            }

                            if (checkBox3.Checked == true && checkBox4.Checked == false && checkBox5.Checked == true)
                            {
                                Value.Thread = 4;
                                if (checkBox1.Checked == true && checkBox2.Checked == false)
                                {
                                    Thread r = new Thread(ba.BarrierDAICallAntiMT);
                                    r.Start();
                                }
                                else if (checkBox1.Checked == false && checkBox2.Checked == true)
                                {
                                    Thread r = new Thread(ba.BarrierDAIPutAntiMT);
                                    r.Start();
                                }
                                else { MessageBox.Show("Invalid options"); }
                            }
                        }
                    }
                    else if (checkBox12.Checked == false && checkBox13.Checked == false && checkBox14.Checked == false && checkBox15.Checked == true)
                    {
                        if (Value.barrier <= Value.S)
                        {
                            MessageBox.Show("Invalid value");
                        }
                        else
                        {
                            if (checkBox3.Checked == false && checkBox4.Checked == false && checkBox5.Checked == false)
                            {
                                if (checkBox1.Checked == true && checkBox2.Checked == false)
                                {
                                    Thread r = new Thread(ba.BarrierUAICall);
                                    r.Start();
                                }
                                else if (checkBox1.Checked == false && checkBox2.Checked == true)
                                {
                                    Thread r = new Thread(ba.BarrierUAIPut);
                                    r.Start();
                                }
                                else { MessageBox.Show("Invalid options"); }
                            }

                            if (checkBox3.Checked == true && checkBox4.Checked == false && checkBox5.Checked == false)
                            {
                                if (checkBox1.Checked == true && checkBox2.Checked == false)
                                {
                                    Thread r = new Thread(ba.BarrierUAICallAnti);
                                    r.Start();
                                }
                                else if (checkBox1.Checked == false && checkBox2.Checked == true)
                                {
                                    Thread r = new Thread(ba.BarrierUAIPutAnti);
                                    r.Start();
                                }
                                else { MessageBox.Show("Invalid options"); }
                            }

                            if (checkBox3.Checked == true && checkBox4.Checked == true && checkBox5.Checked == false)
                            {
                                if (checkBox1.Checked == true && checkBox2.Checked == false)
                                {
                                    Thread r = new Thread(ba.BarrierUAICallAntiCV);
                                    r.Start();
                                }
                                else if (checkBox1.Checked == false && checkBox2.Checked == true)
                                {
                                    Thread r = new Thread(ba.BarrierUAIPutAntiCV);
                                    r.Start();
                                }
                                else { MessageBox.Show("Invalid options"); }
                            }

                            if (checkBox3.Checked == true && checkBox4.Checked == true && checkBox5.Checked == true)
                            {
                                Value.Thread = 4;
                                if (checkBox1.Checked == true && checkBox2.Checked == false)
                                {
                                    Thread r = new Thread(ba.BarrierUAICallAntiCVMT);
                                    r.Start();
                                }
                                else if (checkBox1.Checked == false && checkBox2.Checked == true)
                                {
                                    Thread r = new Thread(ba.BarrierUAIPutAntiCVMT);
                                    r.Start();
                                }
                                else { MessageBox.Show("Invalid options"); }
                            }

                            if (checkBox3.Checked == false && checkBox4.Checked == true && checkBox5.Checked == false)
                            {
                                if (checkBox1.Checked == true && checkBox2.Checked == false)
                                {
                                    Thread r = new Thread(ba.BarrierUAICallCV);
                                    r.Start();
                                }
                                else if (checkBox1.Checked == false && checkBox2.Checked == true)
                                {
                                    Thread r = new Thread(ba.BarrierUAIPutCV);
                                    r.Start();
                                }
                                else { MessageBox.Show("Invalid options"); }
                            }

                            if (checkBox3.Checked == false && checkBox4.Checked == true && checkBox5.Checked == true)
                            {
                                Value.Thread = 4;
                                if (checkBox1.Checked == true && checkBox2.Checked == false)
                                {
                                    Thread r = new Thread(ba.BarrierUAICallCVMT);
                                    r.Start();
                                }
                                else if (checkBox1.Checked == false && checkBox2.Checked == true)
                                {
                                    Thread r = new Thread(ba.BarrierUAIPutCVMT);
                                    r.Start();
                                }
                                else { MessageBox.Show("Invalid options"); }
                            }

                            if (checkBox3.Checked == false && checkBox4.Checked == false && checkBox5.Checked == true)
                            {
                                Value.Thread = 4;
                                if (checkBox1.Checked == true && checkBox2.Checked == false)
                                {
                                    Thread r = new Thread(ba.BarrierUAICallMT);
                                    r.Start();
                                }
                                else if (checkBox1.Checked == false && checkBox2.Checked == true)
                                {
                                    Thread r = new Thread(ba.BarrierUAIPutMT);
                                    r.Start();
                                }
                                else { MessageBox.Show("Invalid options"); }
                            }

                            if (checkBox3.Checked == true && checkBox4.Checked == false && checkBox5.Checked == true)
                            {
                                Value.Thread = 4;
                                if (checkBox1.Checked == true && checkBox2.Checked == false)
                                {
                                    Thread r = new Thread(ba.BarrierUAICallAntiMT);
                                    r.Start();
                                }
                                else if (checkBox1.Checked == false && checkBox2.Checked == true)
                                {
                                    Thread r = new Thread(ba.BarrierUAIPutAntiMT);
                                    r.Start();
                                }
                                else { MessageBox.Show("Invalid options"); }
                            }
                        }
                    }
                    else { MessageBox.Show("Invalid options"); }
                }
                else if (checkBox6.Checked == false && checkBox7.Checked == false && checkBox8.Checked == false && checkBox9.Checked == false && checkBox10.Checked == true && checkBox11.Checked == false)
                {
                    if (checkBox3.Checked == false && checkBox4.Checked == false && checkBox5.Checked == false)
                    {
                        if (checkBox1.Checked == true && checkBox2.Checked == false)
                        {
                            Thread r = new Thread(lo.LookbackCall);
                            r.Start();
                        }
                        else if (checkBox1.Checked == false && checkBox2.Checked == true)
                        {
                            Thread r = new Thread(lo.LookbackPut);
                            r.Start();
                        }
                        else { MessageBox.Show("Invalid options"); }
                    }

                    if (checkBox3.Checked == true && checkBox4.Checked == false && checkBox5.Checked == false)
                    {
                        if (checkBox1.Checked == true && checkBox2.Checked == false)
                        {
                            Thread r = new Thread(lo.LookbackCallAnti);
                            r.Start();
                        }
                        else if (checkBox1.Checked == false && checkBox2.Checked == true)
                        {
                            Thread r = new Thread(lo.LookbackPutAnti);
                            r.Start();
                        }
                        else { MessageBox.Show("Invalid options"); }
                    }

                    if (checkBox3.Checked == true && checkBox4.Checked == true && checkBox5.Checked == false)
                    {
                        if (checkBox1.Checked == true && checkBox2.Checked == false)
                        {
                            Thread r = new Thread(lo.LookbackCallAntiCV);
                            r.Start();
                        }
                        else if (checkBox1.Checked == false && checkBox2.Checked == true)
                        {
                            Thread r = new Thread(lo.LookbackPutAntiCV);
                            r.Start();
                        }
                        else { MessageBox.Show("Invalid options"); }
                    }

                    if (checkBox3.Checked == true && checkBox4.Checked == true && checkBox5.Checked == true)
                    {
                        Value.Thread = 4;
                        if (checkBox1.Checked == true && checkBox2.Checked == false)
                        {
                            Thread r = new Thread(lo.LookbackCallAntiCVMT);
                            r.Start();
                        }
                        else if (checkBox1.Checked == false && checkBox2.Checked == true)
                        {
                            Thread r = new Thread(lo.LookbackPutAntiCVMT);
                            r.Start();
                        }
                        else { MessageBox.Show("Invalid options"); }
                    }

                    if (checkBox3.Checked == false && checkBox4.Checked == true && checkBox5.Checked == false)
                    {
                        if (checkBox1.Checked == true && checkBox2.Checked == false)
                        {
                            Thread r = new Thread(lo.LookbackCallCV);
                            r.Start();
                        }
                        else if (checkBox1.Checked == false && checkBox2.Checked == true)
                        {
                            Thread r = new Thread(lo.LookbackPutCV);
                            r.Start();
                        }
                        else { MessageBox.Show("Invalid options"); }
                    }

                    if (checkBox3.Checked == false && checkBox4.Checked == true && checkBox5.Checked == true)
                    {
                        Value.Thread = 4;
                        if (checkBox1.Checked == true && checkBox2.Checked == false)
                        {
                            Thread r = new Thread(lo.LookbackCallCVMT);
                            r.Start();
                        }
                        else if (checkBox1.Checked == false && checkBox2.Checked == true)
                        {
                            Thread r = new Thread(lo.LookbackPutCVMT);
                            r.Start();
                        }
                        else { MessageBox.Show("Invalid options"); }
                    }

                    if (checkBox3.Checked == false && checkBox4.Checked == false && checkBox5.Checked == true)
                    {
                        Value.Thread = 4;
                        if (checkBox1.Checked == true && checkBox2.Checked == false)
                        {
                            Thread r = new Thread(lo.LookbackCallMT);
                            r.Start();
                        }
                        else if (checkBox1.Checked == false && checkBox2.Checked == true)
                        {
                            Thread r = new Thread(lo.LookbackPutMT);
                            r.Start();
                        }
                        else { MessageBox.Show("Invalid options"); }
                    }

                    if (checkBox3.Checked == true && checkBox4.Checked == false && checkBox5.Checked == true)
                    {
                        Value.Thread = 4;
                        if (checkBox1.Checked == true && checkBox2.Checked == false)
                        {
                            Thread r = new Thread(lo.LookbackCallAntiMT);
                            r.Start();
                        }
                        else if (checkBox1.Checked == false && checkBox2.Checked == true)
                        {
                            Thread r = new Thread(lo.LookbackPutAntiMT);
                            r.Start();
                        }
                        else { MessageBox.Show("Invalid options"); }
                    }
                }

                else if (checkBox6.Checked == false && checkBox7.Checked == false && checkBox8.Checked == false && checkBox9.Checked == false && checkBox10.Checked == false && checkBox11.Checked == true)
                {
                    if (checkBox3.Checked == false && checkBox4.Checked == false && checkBox5.Checked == false)
                    {
                        if (checkBox1.Checked == true && checkBox2.Checked == false)
                        {
                            Thread r = new Thread(ra.RangeCall);
                            r.Start();
                        }
                        else if (checkBox1.Checked == false && checkBox2.Checked == true)
                        {
                            Thread r = new Thread(ra.RangePut);
                            r.Start();
                        }
                        else { MessageBox.Show("Invalid options"); }
                    }

                    if (checkBox3.Checked == true && checkBox4.Checked == false && checkBox5.Checked == false)
                    {
                        if (checkBox1.Checked == true && checkBox2.Checked == false)
                        {
                            Thread r = new Thread(ra.RangeCallAnti);
                            r.Start();
                        }
                        else if (checkBox1.Checked == false && checkBox2.Checked == true)
                        {
                            Thread r = new Thread(ra.RangePutAnti);
                            r.Start();
                        }
                        else { MessageBox.Show("Invalid options"); }
                    }

                    if (checkBox3.Checked == true && checkBox4.Checked == true && checkBox5.Checked == false)
                    {
                        if (checkBox1.Checked == true && checkBox2.Checked == false)
                        {
                            Thread r = new Thread(ra.RangeCallAntiCV);
                            r.Start();
                        }
                        else if (checkBox1.Checked == false && checkBox2.Checked == true)
                        {
                            Thread r = new Thread(ra.RangePutAntiCV);
                            r.Start();
                        }
                        else { MessageBox.Show("Invalid options"); }
                    }

                    if (checkBox3.Checked == true && checkBox4.Checked == true && checkBox5.Checked == true)
                    {
                        Value.Thread = 4;
                        if (checkBox1.Checked == true && checkBox2.Checked == false)
                        {
                            Thread r = new Thread(ra.RangeCallAntiCVMT);
                            r.Start();
                        }
                        else if (checkBox1.Checked == false && checkBox2.Checked == true)
                        {
                            Thread r = new Thread(ra.RangePutAntiCVMT);
                            r.Start();
                        }
                        else { MessageBox.Show("Invalid options"); }
                    }

                    if (checkBox3.Checked == false && checkBox4.Checked == true && checkBox5.Checked == false)
                    {
                        if (checkBox1.Checked == true && checkBox2.Checked == false)
                        {
                            Thread r = new Thread(ra.RangeCallCV);
                            r.Start();
                        }
                        else if (checkBox1.Checked == false && checkBox2.Checked == true)
                        {
                            Thread r = new Thread(ra.RangePutCV);
                            r.Start();
                        }
                        else { MessageBox.Show("Invalid options"); }
                    }

                    if (checkBox3.Checked == false && checkBox4.Checked == true && checkBox5.Checked == true)
                    {
                        Value.Thread = 4;
                        if (checkBox1.Checked == true && checkBox2.Checked == false)
                        {
                            Thread r = new Thread(ra.RangeCallCVMT);
                            r.Start();
                        }
                        else if (checkBox1.Checked == false && checkBox2.Checked == true)
                        {
                            Thread r = new Thread(ra.RangePutCVMT);
                            r.Start();
                        }
                        else { MessageBox.Show("Invalid options"); }
                    }

                    if (checkBox3.Checked == false && checkBox4.Checked == false && checkBox5.Checked == true)
                    {
                        Value.Thread = 4;
                        if (checkBox1.Checked == true && checkBox2.Checked == false)
                        {
                            Thread r = new Thread(ra.RangeCallMT);
                            r.Start();
                        }
                        else if (checkBox1.Checked == false && checkBox2.Checked == true)
                        {
                            Thread r = new Thread(ra.RangePutMT);
                            r.Start();
                        }
                        else { MessageBox.Show("Invalid options"); }
                    }

                    if (checkBox3.Checked == true && checkBox4.Checked == false && checkBox5.Checked == true)
                    {
                        Value.Thread = 4;
                        if (checkBox1.Checked == true && checkBox2.Checked == false)
                        {
                            Thread r = new Thread(ra.RangeCallAntiMT);
                            r.Start();
                        }
                        else if (checkBox1.Checked == false && checkBox2.Checked == true)
                        {
                            Thread r = new Thread(ra.RangePutAntiMT);
                            r.Start();
                        }
                        else { MessageBox.Show("Invalid options"); }
                    }
                }
                else { MessageBox.Show("Invalid options"); }
                }
                catch (FormatException)
                {
                    MessageBox.Show("Invalid value");
                }
        }

        public void finish()
        {
            richTextBox1.Text =
                "  Call option price is  " + Value.Price + "\n"
                  + " Standard Error is " + Value.SE + "\n"
                 + " the Delta is  " + Value.delta + "\n"
                 + " the Gamma is  " + Value.gamma + "\n"
                 + " the Theta is  " + Value.theta + "\n"
                 + " the Vega is  " + Value.vega + "\n"
                 + " the rho is  " + Value.rho + "\n"
                 + "Running Time  " + ": " + Value.timer + "\n" +
                 " The number of cores is   " + Value.Thread + ".";
        }

        private void UnderlyingPrice_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void StrikePrice_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void Tenor_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void Volatility_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void RiskfreeRate_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void Steps_TextChanged(object sender, EventArgs e)
        {
            

        }

        private void Trials_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void checkBox3_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void checkBox4_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void label8_Click(object sender, EventArgs e)
        {

        }

        private void progressBar1_Click(object sender, EventArgs e)
        {

        }

        private void checkBox5_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void checkBox6_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void checkBox8_CheckedChanged(object sender, EventArgs e)
        {

        }
    }
}
