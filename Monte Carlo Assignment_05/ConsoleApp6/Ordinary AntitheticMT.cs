﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ConsoleApp6
{
    public class PreparationAntiMT
    {
        public double[,] optionprice { get; set; }
        public double[,] optioninverse { get; set; }
        public List<double> Option1 { get; set; }
        public List<double> Option2 { get; set; }
        public double Price(double S, double r, double vol, double t, double ep)
        {
            double price;
            double deltaT = t / Convert.ToDouble(Value.step);
            price = S * Math.Exp((r - Math.Pow(vol, 2) / 2) * deltaT + vol * Math.Sqrt(deltaT) * ep);
            return price;
        }
    }

    public class EuropeanAntiMT:PreparationAntiMT,CallPrice,StandardError,PutPrice,CallGreek,PutGreek
    {
        public static double[,] RandomList1;
        public static double[,] RandomList2;
        public void Initial()
        {
            optionprice = new double[Value.trials, Value.step + 1];
            optioninverse = new double[Value.trials, Value.step + 1];
            Option1 = new List<double>();
            Option2 = new List<double>();
        }
        public void RandomGenerated()
        {
            RandomList1 = new double[Value.trials, Value.step];
            Parallel.ForEach(Ienum.Step(0, Value.trials, 1), new ParallelOptions { MaxDegreeOfParallelism = Value.Thread }, i =>
            {
                for (int k = 0; k < Value.step; k++)
                {
                    RandomList1[i, k] = GaussianBoxMuller.NextDouble();
                }
            });
        }
        public void RandomAntithetic()
        {
            RandomList2 = new double[Value.trials, Value.step];
            Parallel.ForEach(Ienum.Step(0, Value.trials, 1), new ParallelOptions { MaxDegreeOfParallelism = Value.Thread }, i =>
            {
                for (int k = 0; k < Value.step; k++)
                {
                    RandomList2[i, k] = -RandomList1[i, k];
                }
            });
        }
        public double CallPrice(double S, double r, double vol, double t)
        {
            Initial();
            Parallel.ForEach(Ienum.Step(0, Value.trials, 1), new ParallelOptions { MaxDegreeOfParallelism = Value.Thread }, i =>
            {
                optionprice[i, 0] = S;
                for (int j = 1; j < Value.step + 1; j++)
                {
                    optionprice[i, j] = Price(optionprice[i, j - 1], r, vol, t, RandomList1[i, j - 1]);
                }
            });
            RandomAntithetic();
            Parallel.ForEach(Ienum.Step(0, Value.trials, 1), new ParallelOptions { MaxDegreeOfParallelism = Value.Thread }, i =>
            {
                optioninverse[i, 0] = S;
                for (int j = 1; j < Value.step + 1; j++)
                {
                    optioninverse[i, j] = Price(optioninverse[i, j - 1], r, vol, t, RandomList2[i, j - 1]);
                }
            });
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                Option1.Add(Math.Max(optionprice[i, Value.step] - Value.K, 0));
            }
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                Option2.Add(Math.Max(optioninverse[i, Value.step] - Value.K, 0));
            }
            double price1 = 0;
            price1 = Math.Exp(-r * t) * Option1.Average();
            double price2 = 0;
            price2 = Math.Exp(-r * t) * Option2.Average();
            Program.increase(1);
            return (price1 + price2) / 2;
        }
        public double PutPrice(double S, double r, double vol, double t)
        {
            Initial();
            Parallel.ForEach(Ienum.Step(0, Value.trials, 1), new ParallelOptions { MaxDegreeOfParallelism = Value.Thread }, i =>
            {
                optionprice[i, 0] = S;
                for (uint j = 1; j < Value.step + 1; j++)
                {
                    optionprice[i, j] = Price(optionprice[i, j - 1], r, vol, t, RandomList1[i, j - 1]);
                }
            });
            RandomAntithetic();
            Parallel.ForEach(Ienum.Step(0, Value.trials, 1), new ParallelOptions { MaxDegreeOfParallelism = Value.Thread }, i =>
            {
                optioninverse[i, 0] = S;
                for (int j = 1; j < Value.step + 1; j++)
                {
                    optioninverse[i, j] = Price(optioninverse[i, j - 1], r, vol, t, RandomList2[i, j - 1]);
                }
            });
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                Option1.Add(Math.Max(Value.K - optionprice[i, Value.step], 0));
            }
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                Option2.Add(Math.Max(Value.K - optioninverse[i, Value.step], 0));
            }
            double price1 = 0;
            price1 = Math.Exp(-r * t) * Option1.Average();
            double price2 = 0;
            price2 = Math.Exp(-r * t) * Option2.Average();
            Program.increase(1);
            return (price1 + price2) / 2;
        }
        public double SE()
        {
            List<double> Option = new List<double>();
            for (UInt32 i = 0; i < Option1.Count; i++)
            {
                Option.Add((Option1[(int)i] + Option2[(int)i]) / 2);
            }
            double avg = Option.Average();
            double sum = Option.Sum(v => Math.Pow(v - avg, 2));
            return (Math.Sqrt(sum / (Option.Count - 1))) / (Math.Sqrt(Option.Count));
        }
        internal static class GaussianBoxMuller
        {
            public static double NextDouble()
            {
                double x, y, square;

                do
                {
                    x = 2 * RandomProvider.GetThreadRandom().NextDouble() - 1;
                    y = 2 * RandomProvider.GetThreadRandom().NextDouble() - 1;
                    square = (x * x) + (y * y);
                } while (square >= 1);

                return x * Math.Sqrt(-2 * Math.Log(square) / square);
            }
        }
        internal static class RandomProvider
        {
            private static ThreadLocal<Random> randomWrapper = new ThreadLocal<Random>(() =>
                new Random(Interlocked.Increment(ref seed))
            );

            private static int seed = Environment.TickCount * 10;

            public static Random GetThreadRandom()
            {
                return randomWrapper.Value;
            }
        }
        public double calldelta(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S + 0.0001, r, vol, t);
            double cp2 = CallPrice(S - 0.0001, r, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double callgamma(double S, double r, double vol, double t)
        {
            return (calldelta(S + 0.01, r, vol, t) - calldelta(S, r, vol, t)) / 0.01;
        }
        public double callvega(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r, vol + 0.0001, t);
            double cp2 = CallPrice(S, r, vol - 0.0001, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double calltheta(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r, vol, t + 0.0001);
            double cp2 = CallPrice(S, r, vol, t - 0.0001);
            return (cp1 - cp2) / 0.0002;
        }
        public double callrho(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r + 0.0001, vol, t);
            double cp2 = CallPrice(S, r - 0.0001, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double putdelta(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S + 0.0001, r, vol, t);
            double cp2 = PutPrice(S - 0.0001, r, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double putgamma(double S, double r, double vol, double t)
        {
            return (putdelta(S + 0.01, r, vol, t) - putdelta(S, r, vol, t)) / 0.01;
        }
        public double putvega(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r, vol + 0.0001, t);
            double cp2 = PutPrice(S, r, vol - 0.0001, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double puttheta(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r, vol, t + 0.0001);
            double cp2 = PutPrice(S, r, vol, t - 0.0001);
            return (cp1 - cp2) / 0.0002;
        }
        public double putrho(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r + 0.0001, vol, t);
            double cp2 = PutPrice(S, r - 0.0001, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
    }

    public class AsianAntiMT: PreparationAntiMT, CallPrice, StandardError, PutPrice, CallGreek, PutGreek
    {
        public static double[,] RandomList1;
        public static double[,] RandomList2;
        public void Initial()
        {
            optionprice = new double[Value.trials, Value.step + 1];
            optioninverse = new double[Value.trials, Value.step + 1];
            Option1 = new List<double>();
            Option2 = new List<double>();
        }
        public void RandomGenerated()
        {
            RandomList1 = new double[Value.trials, Value.step];
            Parallel.ForEach(Ienum.Step(0, Value.trials, 1), new ParallelOptions { MaxDegreeOfParallelism = Value.Thread }, i =>
            {
                for (int k = 0; k < Value.step; k++)
                {
                    RandomList1[i, k] = GaussianBoxMuller.NextDouble();
                }
            });
        }
        public void RandomAntithetic()
        {
            RandomList2 = new double[Value.trials, Value.step];
            Parallel.ForEach(Ienum.Step(0, Value.trials, 1), new ParallelOptions { MaxDegreeOfParallelism = Value.Thread }, i =>
            {
                for (int k = 0; k < Value.step; k++)
                {
                    RandomList2[i, k] = -RandomList1[i, k];
                }
            });
        }
        public double CallPrice(double S, double r, double vol, double t)
        {
            Initial();
            Parallel.ForEach(Ienum.Step(0, Value.trials, 1), new ParallelOptions { MaxDegreeOfParallelism = Value.Thread }, i =>
            {
                optionprice[i, 0] = S;
                for (int j = 1; j < Value.step + 1; j++)
                {
                    optionprice[i, j] = Price(optionprice[i, j - 1], r, vol, t, RandomList1[i, j - 1]);
                }
            });
            RandomAntithetic();
            Parallel.ForEach(Ienum.Step(0, Value.trials, 1), new ParallelOptions { MaxDegreeOfParallelism = Value.Thread }, i =>
            {
                optioninverse[i, 0] = S;
                for (int j = 1; j < Value.step + 1; j++)
                {
                    optioninverse[i, j] = Price(optioninverse[i, j - 1], r, vol, t, RandomList2[i, j - 1]);
                }
            });
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                double[] m = new double[Value.step + 1];
                for(uint j=0;j<Value.step+1;j++)
                {
                    m[j] = optionprice[i, j];
                }
                Option1.Add(Math.Max((m.Average() * (Value.step + 1) - m[0]) / (Value.step) - Value.K, 0));
            }
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                double[] m = new double[Value.step + 1];
                for (uint j = 0; j < Value.step + 1; j++)
                {
                    m[j] = optioninverse[i, j];
                }
                Option2.Add(Math.Max((m.Average() * (Value.step + 1) - m[0]) / (Value.step) - Value.K, 0));
            }
            double price1 = 0;
            price1 = Math.Exp(-r * t) * Option1.Average();
            double price2 = 0;
            price2 = Math.Exp(-r * t) * Option2.Average();
            Program.increase(1);
            return (price1 + price2) / 2;
        }
        public double PutPrice(double S, double r, double vol, double t)
        {
            Initial();
            Parallel.ForEach(Ienum.Step(0, Value.trials, 1), new ParallelOptions { MaxDegreeOfParallelism = Value.Thread }, i =>
            {
                optionprice[i, 0] = S;
                for (uint j = 1; j < Value.step + 1; j++)
                {
                    optionprice[i, j] = Price(optionprice[i, j - 1], r, vol, t, RandomList1[i, j - 1]);
                }
            });
            RandomAntithetic();
            Parallel.ForEach(Ienum.Step(0, Value.trials, 1), new ParallelOptions { MaxDegreeOfParallelism = Value.Thread }, i =>
            {
                optioninverse[i, 0] = S;
                for (int j = 1; j < Value.step + 1; j++)
                {
                    optioninverse[i, j] = Price(optioninverse[i, j - 1], r, vol, t, RandomList2[i, j - 1]);
                }
            });
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                double[] m = new double[Value.step + 1];
                for (uint j = 0; j < Value.step + 1; j++)
                {
                    m[j] = optionprice[i, j];
                }
                Option1.Add(Math.Max(Value.K - (m.Average() * (Value.step + 1) - m[0]) / (Value.step), 0));
            }
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                double[] m = new double[Value.step + 1];
                for (uint j = 0; j < Value.step + 1; j++)
                {
                    m[j] = optioninverse[i, j];
                }
                Option2.Add(Math.Max(Value.K - (m.Average() * (Value.step + 1) - m[0]) / (Value.step), 0));
            }
            double price1 = 0;
            price1 = Math.Exp(-r * t) * Option1.Average();
            double price2 = 0;
            price2 = Math.Exp(-r * t) * Option2.Average();
            Program.increase(1);
            return (price1 + price2) / 2;
        }
        public double SE()
        {
            List<double> Option = new List<double>();
            for (UInt32 i = 0; i < Option1.Count; i++)
            {
                Option.Add((Option1[(int)i] + Option2[(int)i]) / 2);
            }
            double avg = Option.Average();
            double sum = Option.Sum(v => Math.Pow(v - avg, 2));
            return (Math.Sqrt(sum / (Option.Count - 1))) / (Math.Sqrt(Option.Count));
        }
        internal static class GaussianBoxMuller
        {
            public static double NextDouble()
            {
                double x, y, square;

                do
                {
                    x = 2 * RandomProvider.GetThreadRandom().NextDouble() - 1;
                    y = 2 * RandomProvider.GetThreadRandom().NextDouble() - 1;
                    square = (x * x) + (y * y);
                } while (square >= 1);

                return x * Math.Sqrt(-2 * Math.Log(square) / square);
            }
        }
        internal static class RandomProvider
        {
            private static ThreadLocal<Random> randomWrapper = new ThreadLocal<Random>(() =>
                new Random(Interlocked.Increment(ref seed))
            );

            private static int seed = Environment.TickCount * 10;

            public static Random GetThreadRandom()
            {
                return randomWrapper.Value;
            }
        }
        public double calldelta(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S + 0.0001, r, vol, t);
            double cp2 = CallPrice(S - 0.0001, r, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double callgamma(double S, double r, double vol, double t)
        {
            return (calldelta(S + 0.01, r, vol, t) - calldelta(S, r, vol, t)) / 0.01;
        }
        public double callvega(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r, vol + 0.0001, t);
            double cp2 = CallPrice(S, r, vol - 0.0001, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double calltheta(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r, vol, t + 0.0001);
            double cp2 = CallPrice(S, r, vol, t - 0.0001);
            return (cp1 - cp2) / 0.0002;
        }
        public double callrho(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r + 0.0001, vol, t);
            double cp2 = CallPrice(S, r - 0.0001, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double putdelta(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S + 0.0001, r, vol, t);
            double cp2 = PutPrice(S - 0.0001, r, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double putgamma(double S, double r, double vol, double t)
        {
            return (putdelta(S + 0.01, r, vol, t) - putdelta(S, r, vol, t)) / 0.01;
        }
        public double putvega(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r, vol + 0.0001, t);
            double cp2 = PutPrice(S, r, vol - 0.0001, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double puttheta(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r, vol, t + 0.0001);
            double cp2 = PutPrice(S, r, vol, t - 0.0001);
            return (cp1 - cp2) / 0.0002;
        }
        public double putrho(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r + 0.0001, vol, t);
            double cp2 = PutPrice(S, r - 0.0001, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
    }

    public class DigitalAntiMT : PreparationAntiMT, CallPrice, StandardError, PutPrice, CallGreek, PutGreek
    {
        public static double[,] RandomList1;
        public static double[,] RandomList2;
        public void Initial()
        {
            optionprice = new double[Value.trials, Value.step + 1];
            optioninverse = new double[Value.trials, Value.step + 1];
            Option1 = new List<double>();
            Option2 = new List<double>();
        }
        public void RandomGenerated()
        {
            RandomList1 = new double[Value.trials, Value.step];
            Parallel.ForEach(Ienum.Step(0, Value.trials, 1), new ParallelOptions { MaxDegreeOfParallelism = Value.Thread }, i =>
            {
                for (int k = 0; k < Value.step; k++)
                {
                    RandomList1[i, k] = GaussianBoxMuller.NextDouble();
                }
            });
        }
        public void RandomAntithetic()
        {
            RandomList2 = new double[Value.trials, Value.step];
            Parallel.ForEach(Ienum.Step(0, Value.trials, 1), new ParallelOptions { MaxDegreeOfParallelism = Value.Thread }, i =>
            {
                for (int k = 0; k < Value.step; k++)
                {
                    RandomList2[i, k] = -RandomList1[i, k];
                }
            });
        }
        public double CallPrice(double S, double r, double vol, double t)
        {
            Initial();
            Parallel.ForEach(Ienum.Step(0, Value.trials, 1), new ParallelOptions { MaxDegreeOfParallelism = Value.Thread }, i =>
            {
                optionprice[i, 0] = S;
                for (int j = 1; j < Value.step + 1; j++)
                {
                    optionprice[i, j] = Price(optionprice[i, j - 1], r, vol, t, RandomList1[i, j - 1]);
                }
            });
            RandomAntithetic();
            Parallel.ForEach(Ienum.Step(0, Value.trials, 1), new ParallelOptions { MaxDegreeOfParallelism = Value.Thread }, i =>
            {
                optioninverse[i, 0] = S;
                for (int j = 1; j < Value.step + 1; j++)
                {
                    optioninverse[i, j] = Price(optioninverse[i, j - 1], r, vol, t, RandomList2[i, j - 1]);
                }
            });
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                if (optionprice[i, Value.step] - Value.K >= 0)
                { Option1.Add(Value.rebate); }
                else { Option1.Add(0); }
            }
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                if (optioninverse[i, Value.step] - Value.K >= 0)
                { Option2.Add(Value.rebate); }
                else { Option2.Add(0); }
            }
            double price1 = 0;
            price1 = Math.Exp(-r * t) * Option1.Average();
            double price2 = 0;
            price2 = Math.Exp(-r * t) * Option2.Average();
            Program.increase(1);
            return (price1 + price2) / 2;
        }
        public double PutPrice(double S, double r, double vol, double t)
        {
            Initial();
            Parallel.ForEach(Ienum.Step(0, Value.trials, 1), new ParallelOptions { MaxDegreeOfParallelism = Value.Thread }, i =>
            {
                optionprice[i, 0] = S;
                for (uint j = 1; j < Value.step + 1; j++)
                {
                    optionprice[i, j] = Price(optionprice[i, j - 1], r, vol, t, RandomList1[i, j - 1]);
                }
            });
            RandomAntithetic();
            Parallel.ForEach(Ienum.Step(0, Value.trials, 1), new ParallelOptions { MaxDegreeOfParallelism = Value.Thread }, i =>
            {
                optioninverse[i, 0] = S;
                for (int j = 1; j < Value.step + 1; j++)
                {
                    optioninverse[i, j] = Price(optioninverse[i, j - 1], r, vol, t, RandomList2[i, j - 1]);
                }
            });
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                if (Value.K - optionprice[i, Value.step] >= 0)
                { Option1.Add(Value.rebate); }
                else { Option1.Add(0); }
            }
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                if (Value.K - optioninverse[i, Value.step] >= 0)
                { Option2.Add(Value.rebate); }
                else { Option2.Add(0); }
            }
            double price1 = 0;
            price1 = Math.Exp(-r * t) * Option1.Average();
            double price2 = 0;
            price2 = Math.Exp(-r * t) * Option2.Average();
            Program.increase(1);
            return (price1 + price2) / 2;
        }
        public double SE()
        {
            List<double> Option = new List<double>();
            for (UInt32 i = 0; i < Option1.Count; i++)
            {
                Option.Add((Option1[(int)i] + Option2[(int)i]) / 2);
            }
            double avg = Option.Average();
            double sum = Option.Sum(v => Math.Pow(v - avg, 2));
            return (Math.Sqrt(sum / (Option.Count - 1))) / (Math.Sqrt(Option.Count));
        }
        internal static class GaussianBoxMuller
        {
            public static double NextDouble()
            {
                double x, y, square;

                do
                {
                    x = 2 * RandomProvider.GetThreadRandom().NextDouble() - 1;
                    y = 2 * RandomProvider.GetThreadRandom().NextDouble() - 1;
                    square = (x * x) + (y * y);
                } while (square >= 1);

                return x * Math.Sqrt(-2 * Math.Log(square) / square);
            }
        }
        internal static class RandomProvider
        {
            private static ThreadLocal<Random> randomWrapper = new ThreadLocal<Random>(() =>
                new Random(Interlocked.Increment(ref seed))
            );

            private static int seed = Environment.TickCount * 10;

            public static Random GetThreadRandom()
            {
                return randomWrapper.Value;
            }
        }
        public double calldelta(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S + 0.0001, r, vol, t);
            double cp2 = CallPrice(S - 0.0001, r, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double callgamma(double S, double r, double vol, double t)
        {
            return (calldelta(S + 0.01, r, vol, t) - calldelta(S, r, vol, t)) / 0.01;
        }
        public double callvega(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r, vol + 0.0001, t);
            double cp2 = CallPrice(S, r, vol - 0.0001, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double calltheta(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r, vol, t + 0.0001);
            double cp2 = CallPrice(S, r, vol, t - 0.0001);
            return (cp1 - cp2) / 0.0002;
        }
        public double callrho(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r + 0.0001, vol, t);
            double cp2 = CallPrice(S, r - 0.0001, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double putdelta(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S + 0.0001, r, vol, t);
            double cp2 = PutPrice(S - 0.0001, r, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double putgamma(double S, double r, double vol, double t)
        {
            return (putdelta(S + 0.01, r, vol, t) - putdelta(S, r, vol, t)) / 0.01;
        }
        public double putvega(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r, vol + 0.0001, t);
            double cp2 = PutPrice(S, r, vol - 0.0001, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double puttheta(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r, vol, t + 0.0001);
            double cp2 = PutPrice(S, r, vol, t - 0.0001);
            return (cp1 - cp2) / 0.0002;
        }
        public double putrho(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r + 0.0001, vol, t);
            double cp2 = PutPrice(S, r - 0.0001, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
    }

    public class BarrierDAOAntiMT : PreparationAntiMT, CallPrice, StandardError, PutPrice, CallGreek, PutGreek
    {
        public static double[,] RandomList1;
        public static double[,] RandomList2;
        public void Initial()
        {
            optionprice = new double[Value.trials, Value.step + 1];
            optioninverse = new double[Value.trials, Value.step + 1];
            Option1 = new List<double>();
            Option2 = new List<double>();
        }
        public void RandomGenerated()
        {
            RandomList1 = new double[Value.trials, Value.step];
            Parallel.ForEach(Ienum.Step(0, Value.trials, 1), new ParallelOptions { MaxDegreeOfParallelism = Value.Thread }, i =>
            {
                for (int k = 0; k < Value.step; k++)
                {
                    RandomList1[i, k] = GaussianBoxMuller.NextDouble();
                }
            });
        }
        public void RandomAntithetic()
        {
            RandomList2 = new double[Value.trials, Value.step];
            Parallel.ForEach(Ienum.Step(0, Value.trials, 1), new ParallelOptions { MaxDegreeOfParallelism = Value.Thread }, i =>
            {
                for (int k = 0; k < Value.step; k++)
                {
                    RandomList2[i, k] = -RandomList1[i, k];
                }
            });
        }
        public double CallPrice(double S, double r, double vol, double t)
        {
            Initial();
            Parallel.ForEach(Ienum.Step(0, Value.trials, 1), new ParallelOptions { MaxDegreeOfParallelism = Value.Thread }, i =>
            {
                optionprice[i, 0] = S;
                for (int j = 1; j < Value.step + 1; j++)
                {
                    optionprice[i, j] = Price(optionprice[i, j - 1], r, vol, t, RandomList1[i, j - 1]);
                }
            });
            RandomAntithetic();
            Parallel.ForEach(Ienum.Step(0, Value.trials, 1), new ParallelOptions { MaxDegreeOfParallelism = Value.Thread }, i =>
            {
                optioninverse[i, 0] = S;
                for (int j = 1; j < Value.step + 1; j++)
                {
                    optioninverse[i, j] = Price(optioninverse[i, j - 1], r, vol, t, RandomList2[i, j - 1]);
                }
            });
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                double[] m = new double[Value.step + 1];
                for (uint j = 0; j < Value.step + 1; j++)
                {
                    m[j] = optionprice[i, j];
                }
                if (m.Min() >= Value.barrier)
                { Option1.Add(Math.Max(optionprice[i, Value.step] - Value.K, 0)); }
                else { Option1.Add(0); }
            }
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                double[] m = new double[Value.step + 1];
                for (uint j = 0; j < Value.step + 1; j++)
                {
                    m[j] = optioninverse[i, j];
                }
                if (m.Min() >= Value.barrier)
                { Option2.Add(Math.Max(optioninverse[i, Value.step] - Value.K, 0)); }
                else { Option2.Add(0); }
            }
            double price1 = 0;
            price1 = Math.Exp(-r * t) * Option1.Average();
            double price2 = 0;
            price2 = Math.Exp(-r * t) * Option2.Average();
            Program.increase(1);
            return (price1 + price2) / 2;
        }
        public double PutPrice(double S, double r, double vol, double t)
        {
            Initial();
            Parallel.ForEach(Ienum.Step(0, Value.trials, 1), new ParallelOptions { MaxDegreeOfParallelism = Value.Thread }, i =>
            {
                optionprice[i, 0] = S;
                for (uint j = 1; j < Value.step + 1; j++)
                {
                    optionprice[i, j] = Price(optionprice[i, j - 1], r, vol, t, RandomList1[i, j - 1]);
                }
            });
            RandomAntithetic();
            Parallel.ForEach(Ienum.Step(0, Value.trials, 1), new ParallelOptions { MaxDegreeOfParallelism = Value.Thread }, i =>
            {
                optioninverse[i, 0] = S;
                for (int j = 1; j < Value.step + 1; j++)
                {
                    optioninverse[i, j] = Price(optioninverse[i, j - 1], r, vol, t, RandomList2[i, j - 1]);
                }
            });
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                double[] m = new double[Value.step + 1];
                for (uint j = 0; j < Value.step + 1; j++)
                {
                    m[j] = optionprice[i, j];
                }
                if (m.Min() >= Value.barrier)
                { Option1.Add(Math.Max(Value.K - optionprice[i, Value.step], 0)); }
                else { Option1.Add(0); }
            }
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                double[] m = new double[Value.step + 1];
                for (uint j = 0; j < Value.step + 1; j++)
                {
                    m[j] = optioninverse[i, j];
                }
                if (m.Min() >= Value.barrier)
                { Option2.Add(Math.Max(Value.K - optioninverse[i, Value.step], 0)); }
                else { Option2.Add(0); }
            }
            double price1 = 0;
            price1 = Math.Exp(-r * t) * Option1.Average();
            double price2 = 0;
            price2 = Math.Exp(-r * t) * Option2.Average();
            Program.increase(1);
            return (price1 + price2) / 2;
        }
        public double SE()
        {
            List<double> Option = new List<double>();
            for (UInt32 i = 0; i < Option1.Count; i++)
            {
                Option.Add((Option1[(int)i] + Option2[(int)i]) / 2);
            }
            double avg = Option.Average();
            double sum = Option.Sum(v => Math.Pow(v - avg, 2));
            return (Math.Sqrt(sum / (Option.Count - 1))) / (Math.Sqrt(Option.Count));
        }
        internal static class GaussianBoxMuller
        {
            public static double NextDouble()
            {
                double x, y, square;

                do
                {
                    x = 2 * RandomProvider.GetThreadRandom().NextDouble() - 1;
                    y = 2 * RandomProvider.GetThreadRandom().NextDouble() - 1;
                    square = (x * x) + (y * y);
                } while (square >= 1);

                return x * Math.Sqrt(-2 * Math.Log(square) / square);
            }
        }
        internal static class RandomProvider
        {
            private static ThreadLocal<Random> randomWrapper = new ThreadLocal<Random>(() =>
                new Random(Interlocked.Increment(ref seed))
            );

            private static int seed = Environment.TickCount * 10;

            public static Random GetThreadRandom()
            {
                return randomWrapper.Value;
            }
        }
        public double calldelta(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S + 0.0001, r, vol, t);
            double cp2 = CallPrice(S - 0.0001, r, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double callgamma(double S, double r, double vol, double t)
        {
            return (calldelta(S + 0.01, r, vol, t) - calldelta(S, r, vol, t)) / 0.01;
        }
        public double callvega(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r, vol + 0.0001, t);
            double cp2 = CallPrice(S, r, vol - 0.0001, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double calltheta(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r, vol, t + 0.0001);
            double cp2 = CallPrice(S, r, vol, t - 0.0001);
            return (cp1 - cp2) / 0.0002;
        }
        public double callrho(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r + 0.0001, vol, t);
            double cp2 = CallPrice(S, r - 0.0001, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double putdelta(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S + 0.0001, r, vol, t);
            double cp2 = PutPrice(S - 0.0001, r, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double putgamma(double S, double r, double vol, double t)
        {
            return (putdelta(S + 0.01, r, vol, t) - putdelta(S, r, vol, t)) / 0.01;
        }
        public double putvega(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r, vol + 0.0001, t);
            double cp2 = PutPrice(S, r, vol - 0.0001, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double puttheta(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r, vol, t + 0.0001);
            double cp2 = PutPrice(S, r, vol, t - 0.0001);
            return (cp1 - cp2) / 0.0002;
        }
        public double putrho(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r + 0.0001, vol, t);
            double cp2 = PutPrice(S, r - 0.0001, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
    }

    public class BarrierDAIAntiMT : PreparationAntiMT, CallPrice, StandardError, PutPrice, CallGreek, PutGreek
    {
        public static double[,] RandomList1;
        public static double[,] RandomList2;
        public void Initial()
        {
            optionprice = new double[Value.trials, Value.step + 1];
            optioninverse = new double[Value.trials, Value.step + 1];
            Option1 = new List<double>();
            Option2 = new List<double>();
        }
        public void RandomGenerated()
        {
            RandomList1 = new double[Value.trials, Value.step];
            Parallel.ForEach(Ienum.Step(0, Value.trials, 1), new ParallelOptions { MaxDegreeOfParallelism = Value.Thread }, i =>
            {
                for (int k = 0; k < Value.step; k++)
                {
                    RandomList1[i, k] = GaussianBoxMuller.NextDouble();
                }
            });
        }
        public void RandomAntithetic()
        {
            RandomList2 = new double[Value.trials, Value.step];
            Parallel.ForEach(Ienum.Step(0, Value.trials, 1), new ParallelOptions { MaxDegreeOfParallelism = Value.Thread }, i =>
            {
                for (int k = 0; k < Value.step; k++)
                {
                    RandomList2[i, k] = -RandomList1[i, k];
                }
            });
        }
        public double CallPrice(double S, double r, double vol, double t)
        {
            Initial();
            Parallel.ForEach(Ienum.Step(0, Value.trials, 1), new ParallelOptions { MaxDegreeOfParallelism = Value.Thread }, i =>
            {
                optionprice[i, 0] = S;
                for (int j = 1; j < Value.step + 1; j++)
                {
                    optionprice[i, j] = Price(optionprice[i, j - 1], r, vol, t, RandomList1[i, j - 1]);
                }
            });
            RandomAntithetic();
            Parallel.ForEach(Ienum.Step(0, Value.trials, 1), new ParallelOptions { MaxDegreeOfParallelism = Value.Thread }, i =>
            {
                optioninverse[i, 0] = S;
                for (int j = 1; j < Value.step + 1; j++)
                {
                    optioninverse[i, j] = Price(optioninverse[i, j - 1], r, vol, t, RandomList2[i, j - 1]);
                }
            });
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                double[] m = new double[Value.step + 1];
                for (uint j = 0; j < Value.step + 1; j++)
                {
                    m[j] = optionprice[i, j];
                }
                if (m.Min() < Value.barrier)
                { Option1.Add(Math.Max(optionprice[i, Value.step] - Value.K, 0)); }
                else { Option1.Add(0); }
            }
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                double[] m = new double[Value.step + 1];
                for (uint j = 0; j < Value.step + 1; j++)
                {
                    m[j] = optioninverse[i, j];
                }
                if (m.Min() < Value.barrier)
                { Option2.Add(Math.Max(optioninverse[i, Value.step] - Value.K, 0)); }
                else { Option2.Add(0); }
            }
            double price1 = 0;
            price1 = Math.Exp(-r * t) * Option1.Average();
            double price2 = 0;
            price2 = Math.Exp(-r * t) * Option2.Average();
            Program.increase(1);
            return (price1 + price2) / 2;
        }
        public double PutPrice(double S, double r, double vol, double t)
        {
            Initial();
            Parallel.ForEach(Ienum.Step(0, Value.trials, 1), new ParallelOptions { MaxDegreeOfParallelism = Value.Thread }, i =>
            {
                optionprice[i, 0] = S;
                for (uint j = 1; j < Value.step + 1; j++)
                {
                    optionprice[i, j] = Price(optionprice[i, j - 1], r, vol, t, RandomList1[i, j - 1]);
                }
            });
            RandomAntithetic();
            Parallel.ForEach(Ienum.Step(0, Value.trials, 1), new ParallelOptions { MaxDegreeOfParallelism = Value.Thread }, i =>
            {
                optioninverse[i, 0] = S;
                for (int j = 1; j < Value.step + 1; j++)
                {
                    optioninverse[i, j] = Price(optioninverse[i, j - 1], r, vol, t, RandomList2[i, j - 1]);
                }
            });
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                double[] m = new double[Value.step + 1];
                for (uint j = 0; j < Value.step + 1; j++)
                {
                    m[j] = optionprice[i, j];
                }
                if (m.Min() < Value.barrier)
                { Option1.Add(Math.Max(Value.K - optionprice[i, Value.step], 0)); }
                else { Option1.Add(0); }
            }
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                double[] m = new double[Value.step + 1];
                for (uint j = 0; j < Value.step + 1; j++)
                {
                    m[j] = optioninverse[i, j];
                }
                if (m.Min() < Value.barrier)
                { Option2.Add(Math.Max(Value.K - optioninverse[i, Value.step], 0)); }
                else { Option2.Add(0); }
            }
            double price1 = 0;
            price1 = Math.Exp(-r * t) * Option1.Average();
            double price2 = 0;
            price2 = Math.Exp(-r * t) * Option2.Average();
            Program.increase(1);
            return (price1 + price2) / 2;
        }
        public double SE()
        {
            List<double> Option = new List<double>();
            for (UInt32 i = 0; i < Option1.Count; i++)
            {
                Option.Add((Option1[(int)i] + Option2[(int)i]) / 2);
            }
            double avg = Option.Average();
            double sum = Option.Sum(v => Math.Pow(v - avg, 2));
            return (Math.Sqrt(sum / (Option.Count - 1))) / (Math.Sqrt(Option.Count));
        }
        internal static class GaussianBoxMuller
        {
            public static double NextDouble()
            {
                double x, y, square;

                do
                {
                    x = 2 * RandomProvider.GetThreadRandom().NextDouble() - 1;
                    y = 2 * RandomProvider.GetThreadRandom().NextDouble() - 1;
                    square = (x * x) + (y * y);
                } while (square >= 1);

                return x * Math.Sqrt(-2 * Math.Log(square) / square);
            }
        }
        internal static class RandomProvider
        {
            private static ThreadLocal<Random> randomWrapper = new ThreadLocal<Random>(() =>
                new Random(Interlocked.Increment(ref seed))
            );

            private static int seed = Environment.TickCount * 10;

            public static Random GetThreadRandom()
            {
                return randomWrapper.Value;
            }
        }
        public double calldelta(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S + 0.0001, r, vol, t);
            double cp2 = CallPrice(S - 0.0001, r, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double callgamma(double S, double r, double vol, double t)
        {
            return (calldelta(S + 0.01, r, vol, t) - calldelta(S, r, vol, t)) / 0.01;
        }
        public double callvega(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r, vol + 0.0001, t);
            double cp2 = CallPrice(S, r, vol - 0.0001, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double calltheta(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r, vol, t + 0.0001);
            double cp2 = CallPrice(S, r, vol, t - 0.0001);
            return (cp1 - cp2) / 0.0002;
        }
        public double callrho(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r + 0.0001, vol, t);
            double cp2 = CallPrice(S, r - 0.0001, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double putdelta(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S + 0.0001, r, vol, t);
            double cp2 = PutPrice(S - 0.0001, r, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double putgamma(double S, double r, double vol, double t)
        {
            return (putdelta(S + 0.01, r, vol, t) - putdelta(S, r, vol, t)) / 0.01;
        }
        public double putvega(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r, vol + 0.0001, t);
            double cp2 = PutPrice(S, r, vol - 0.0001, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double puttheta(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r, vol, t + 0.0001);
            double cp2 = PutPrice(S, r, vol, t - 0.0001);
            return (cp1 - cp2) / 0.0002;
        }
        public double putrho(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r + 0.0001, vol, t);
            double cp2 = PutPrice(S, r - 0.0001, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
    }

    public class BarrierUAOAntiMT : PreparationAntiMT, CallPrice, StandardError, PutPrice, CallGreek, PutGreek
    {
        public static double[,] RandomList1;
        public static double[,] RandomList2;
        public void Initial()
        {
            optionprice = new double[Value.trials, Value.step + 1];
            optioninverse = new double[Value.trials, Value.step + 1];
            Option1 = new List<double>();
            Option2 = new List<double>();
        }
        public void RandomGenerated()
        {
            RandomList1 = new double[Value.trials, Value.step];
            Parallel.ForEach(Ienum.Step(0, Value.trials, 1), new ParallelOptions { MaxDegreeOfParallelism = Value.Thread }, i =>
            {
                for (int k = 0; k < Value.step; k++)
                {
                    RandomList1[i, k] = GaussianBoxMuller.NextDouble();
                }
            });
        }
        public void RandomAntithetic()
        {
            RandomList2 = new double[Value.trials, Value.step];
            Parallel.ForEach(Ienum.Step(0, Value.trials, 1), new ParallelOptions { MaxDegreeOfParallelism = Value.Thread }, i =>
            {
                for (int k = 0; k < Value.step; k++)
                {
                    RandomList2[i, k] = -RandomList1[i, k];
                }
            });
        }
        public double CallPrice(double S, double r, double vol, double t)
        {
            Initial();
            Parallel.ForEach(Ienum.Step(0, Value.trials, 1), new ParallelOptions { MaxDegreeOfParallelism = Value.Thread }, i =>
            {
                optionprice[i, 0] = S;
                for (int j = 1; j < Value.step + 1; j++)
                {
                    optionprice[i, j] = Price(optionprice[i, j - 1], r, vol, t, RandomList1[i, j - 1]);
                }
            });
            RandomAntithetic();
            Parallel.ForEach(Ienum.Step(0, Value.trials, 1), new ParallelOptions { MaxDegreeOfParallelism = Value.Thread }, i =>
            {
                optioninverse[i, 0] = S;
                for (int j = 1; j < Value.step + 1; j++)
                {
                    optioninverse[i, j] = Price(optioninverse[i, j - 1], r, vol, t, RandomList2[i, j - 1]);
                }
            });
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                double[] m = new double[Value.step + 1];
                for (uint j = 0; j < Value.step + 1; j++)
                {
                    m[j] = optionprice[i, j];
                }
                if (m.Max() <= Value.barrier)
                { Option1.Add(Math.Max(optionprice[i, Value.step] - Value.K, 0)); }
                else { Option1.Add(0); }
            }
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                double[] m = new double[Value.step + 1];
                for (uint j = 0; j < Value.step + 1; j++)
                {
                    m[j] = optioninverse[i, j];
                }
                if (m.Max() <= Value.barrier)
                { Option2.Add(Math.Max(optioninverse[i, Value.step] - Value.K, 0)); }
                else { Option2.Add(0); }
            }
            double price1 = 0;
            price1 = Math.Exp(-r * t) * Option1.Average();
            double price2 = 0;
            price2 = Math.Exp(-r * t) * Option2.Average();
            Program.increase(1);
            return (price1 + price2) / 2;
        }
        public double PutPrice(double S, double r, double vol, double t)
        {
            Initial();
            Parallel.ForEach(Ienum.Step(0, Value.trials, 1), new ParallelOptions { MaxDegreeOfParallelism = Value.Thread }, i =>
            {
                optionprice[i, 0] = S;
                for (uint j = 1; j < Value.step + 1; j++)
                {
                    optionprice[i, j] = Price(optionprice[i, j - 1], r, vol, t, RandomList1[i, j - 1]);
                }
            });
            RandomAntithetic();
            Parallel.ForEach(Ienum.Step(0, Value.trials, 1), new ParallelOptions { MaxDegreeOfParallelism = Value.Thread }, i =>
            {
                optioninverse[i, 0] = S;
                for (int j = 1; j < Value.step + 1; j++)
                {
                    optioninverse[i, j] = Price(optioninverse[i, j - 1], r, vol, t, RandomList2[i, j - 1]);
                }
            });
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                double[] m = new double[Value.step + 1];
                for (uint j = 0; j < Value.step + 1; j++)
                {
                    m[j] = optionprice[i, j];
                }
                if (m.Max() <= Value.barrier)
                { Option1.Add(Math.Max(Value.K - optionprice[i, Value.step], 0)); }
                else { Option1.Add(0); }
            }
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                double[] m = new double[Value.step + 1];
                for (uint j = 0; j < Value.step + 1; j++)
                {
                    m[j] = optioninverse[i, j];
                }
                if (m.Max() <= Value.barrier)
                { Option2.Add(Math.Max(Value.K - optioninverse[i, Value.step], 0)); }
                else { Option2.Add(0); }
            }
            double price1 = 0;
            price1 = Math.Exp(-r * t) * Option1.Average();
            double price2 = 0;
            price2 = Math.Exp(-r * t) * Option2.Average();
            Program.increase(1);
            return (price1 + price2) / 2;
        }
        public double SE()
        {
            List<double> Option = new List<double>();
            for (UInt32 i = 0; i < Option1.Count; i++)
            {
                Option.Add((Option1[(int)i] + Option2[(int)i]) / 2);
            }
            double avg = Option.Average();
            double sum = Option.Sum(v => Math.Pow(v - avg, 2));
            return (Math.Sqrt(sum / (Option.Count - 1))) / (Math.Sqrt(Option.Count));
        }
        internal static class GaussianBoxMuller
        {
            public static double NextDouble()
            {
                double x, y, square;

                do
                {
                    x = 2 * RandomProvider.GetThreadRandom().NextDouble() - 1;
                    y = 2 * RandomProvider.GetThreadRandom().NextDouble() - 1;
                    square = (x * x) + (y * y);
                } while (square >= 1);

                return x * Math.Sqrt(-2 * Math.Log(square) / square);
            }
        }
        internal static class RandomProvider
        {
            private static ThreadLocal<Random> randomWrapper = new ThreadLocal<Random>(() =>
                new Random(Interlocked.Increment(ref seed))
            );

            private static int seed = Environment.TickCount * 10;

            public static Random GetThreadRandom()
            {
                return randomWrapper.Value;
            }
        }
        public double calldelta(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S + 0.0001, r, vol, t);
            double cp2 = CallPrice(S - 0.0001, r, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double callgamma(double S, double r, double vol, double t)
        {
            return (calldelta(S + 0.01, r, vol, t) - calldelta(S, r, vol, t)) / 0.01;
        }
        public double callvega(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r, vol + 0.0001, t);
            double cp2 = CallPrice(S, r, vol - 0.0001, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double calltheta(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r, vol, t + 0.0001);
            double cp2 = CallPrice(S, r, vol, t - 0.0001);
            return (cp1 - cp2) / 0.0002;
        }
        public double callrho(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r + 0.0001, vol, t);
            double cp2 = CallPrice(S, r - 0.0001, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double putdelta(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S + 0.0001, r, vol, t);
            double cp2 = PutPrice(S - 0.0001, r, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double putgamma(double S, double r, double vol, double t)
        {
            return (putdelta(S + 0.01, r, vol, t) - putdelta(S, r, vol, t)) / 0.01;
        }
        public double putvega(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r, vol + 0.0001, t);
            double cp2 = PutPrice(S, r, vol - 0.0001, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double puttheta(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r, vol, t + 0.0001);
            double cp2 = PutPrice(S, r, vol, t - 0.0001);
            return (cp1 - cp2) / 0.0002;
        }
        public double putrho(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r + 0.0001, vol, t);
            double cp2 = PutPrice(S, r - 0.0001, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
    }

    public class BarrierUAIAntiMT : PreparationAntiMT, CallPrice, StandardError, PutPrice, CallGreek, PutGreek
    {
        public static double[,] RandomList1;
        public static double[,] RandomList2;
        public void Initial()
        {
            optionprice = new double[Value.trials, Value.step + 1];
            optioninverse = new double[Value.trials, Value.step + 1];
            Option1 = new List<double>();
            Option2 = new List<double>();
        }
        public void RandomGenerated()
        {
            RandomList1 = new double[Value.trials, Value.step];
            Parallel.ForEach(Ienum.Step(0, Value.trials, 1), new ParallelOptions { MaxDegreeOfParallelism = Value.Thread }, i =>
            {
                for (int k = 0; k < Value.step; k++)
                {
                    RandomList1[i, k] = GaussianBoxMuller.NextDouble();
                }
            });
        }
        public void RandomAntithetic()
        {
            RandomList2 = new double[Value.trials, Value.step];
            Parallel.ForEach(Ienum.Step(0, Value.trials, 1), new ParallelOptions { MaxDegreeOfParallelism = Value.Thread }, i =>
            {
                for (int k = 0; k < Value.step; k++)
                {
                    RandomList2[i, k] = -RandomList1[i, k];
                }
            });
        }
        public double CallPrice(double S, double r, double vol, double t)
        {
            Initial();
            Parallel.ForEach(Ienum.Step(0, Value.trials, 1), new ParallelOptions { MaxDegreeOfParallelism = Value.Thread }, i =>
            {
                optionprice[i, 0] = S;
                for (int j = 1; j < Value.step + 1; j++)
                {
                    optionprice[i, j] = Price(optionprice[i, j - 1], r, vol, t, RandomList1[i, j - 1]);
                }
            });
            RandomAntithetic();
            Parallel.ForEach(Ienum.Step(0, Value.trials, 1), new ParallelOptions { MaxDegreeOfParallelism = Value.Thread }, i =>
            {
                optioninverse[i, 0] = S;
                for (int j = 1; j < Value.step + 1; j++)
                {
                    optioninverse[i, j] = Price(optioninverse[i, j - 1], r, vol, t, RandomList2[i, j - 1]);
                }
            });
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                double[] m = new double[Value.step + 1];
                for (uint j = 0; j < Value.step + 1; j++)
                {
                    m[j] = optionprice[i, j];
                }
                if (m.Max() > Value.barrier)
                { Option1.Add(Math.Max(optionprice[i, Value.step] - Value.K, 0)); }
                else { Option1.Add(0); }
            }
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                double[] m = new double[Value.step + 1];
                for (uint j = 0; j < Value.step + 1; j++)
                {
                    m[j] = optioninverse[i, j];
                }
                if (m.Max() > Value.barrier)
                { Option2.Add(Math.Max(optioninverse[i, Value.step] - Value.K, 0)); }
                else { Option2.Add(0); }
            }
            double price1 = 0;
            price1 = Math.Exp(-r * t) * Option1.Average();
            double price2 = 0;
            price2 = Math.Exp(-r * t) * Option2.Average();
            Program.increase(1);
            return (price1 + price2) / 2;
        }
        public double PutPrice(double S, double r, double vol, double t)
        {
            Initial();
            Parallel.ForEach(Ienum.Step(0, Value.trials, 1), new ParallelOptions { MaxDegreeOfParallelism = Value.Thread }, i =>
            {
                optionprice[i, 0] = S;
                for (uint j = 1; j < Value.step + 1; j++)
                {
                    optionprice[i, j] = Price(optionprice[i, j - 1], r, vol, t, RandomList1[i, j - 1]);
                }
            });
            RandomAntithetic();
            Parallel.ForEach(Ienum.Step(0, Value.trials, 1), new ParallelOptions { MaxDegreeOfParallelism = Value.Thread }, i =>
            {
                optioninverse[i, 0] = S;
                for (int j = 1; j < Value.step + 1; j++)
                {
                    optioninverse[i, j] = Price(optioninverse[i, j - 1], r, vol, t, RandomList2[i, j - 1]);
                }
            });
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                double[] m = new double[Value.step + 1];
                for (uint j = 0; j < Value.step + 1; j++)
                {
                    m[j] = optionprice[i, j];
                }
                if (m.Max() > Value.barrier)
                { Option1.Add(Math.Max(Value.K - optionprice[i, Value.step], 0)); }
                else { Option1.Add(0); }
            }
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                double[] m = new double[Value.step + 1];
                for (uint j = 0; j < Value.step + 1; j++)
                {
                    m[j] = optioninverse[i, j];
                }
                if (m.Max() > Value.barrier)
                { Option2.Add(Math.Max(Value.K - optioninverse[i, Value.step], 0)); }
                else { Option2.Add(0); }
            }
            double price1 = 0;
            price1 = Math.Exp(-r * t) * Option1.Average();
            double price2 = 0;
            price2 = Math.Exp(-r * t) * Option2.Average();
            Program.increase(1);
            return (price1 + price2) / 2;
        }
        public double SE()
        {
            List<double> Option = new List<double>();
            for (UInt32 i = 0; i < Option1.Count; i++)
            {
                Option.Add((Option1[(int)i] + Option2[(int)i]) / 2);
            }
            double avg = Option.Average();
            double sum = Option.Sum(v => Math.Pow(v - avg, 2));
            return (Math.Sqrt(sum / (Option.Count - 1))) / (Math.Sqrt(Option.Count));
        }
        internal static class GaussianBoxMuller
        {
            public static double NextDouble()
            {
                double x, y, square;

                do
                {
                    x = 2 * RandomProvider.GetThreadRandom().NextDouble() - 1;
                    y = 2 * RandomProvider.GetThreadRandom().NextDouble() - 1;
                    square = (x * x) + (y * y);
                } while (square >= 1);

                return x * Math.Sqrt(-2 * Math.Log(square) / square);
            }
        }
        internal static class RandomProvider
        {
            private static ThreadLocal<Random> randomWrapper = new ThreadLocal<Random>(() =>
                new Random(Interlocked.Increment(ref seed))
            );

            private static int seed = Environment.TickCount * 10;

            public static Random GetThreadRandom()
            {
                return randomWrapper.Value;
            }
        }
        public double calldelta(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S + 0.0001, r, vol, t);
            double cp2 = CallPrice(S - 0.0001, r, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double callgamma(double S, double r, double vol, double t)
        {
            return (calldelta(S + 0.01, r, vol, t) - calldelta(S, r, vol, t)) / 0.01;
        }
        public double callvega(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r, vol + 0.0001, t);
            double cp2 = CallPrice(S, r, vol - 0.0001, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double calltheta(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r, vol, t + 0.0001);
            double cp2 = CallPrice(S, r, vol, t - 0.0001);
            return (cp1 - cp2) / 0.0002;
        }
        public double callrho(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r + 0.0001, vol, t);
            double cp2 = CallPrice(S, r - 0.0001, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double putdelta(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S + 0.0001, r, vol, t);
            double cp2 = PutPrice(S - 0.0001, r, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double putgamma(double S, double r, double vol, double t)
        {
            return (putdelta(S + 0.01, r, vol, t) - putdelta(S, r, vol, t)) / 0.01;
        }
        public double putvega(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r, vol + 0.0001, t);
            double cp2 = PutPrice(S, r, vol - 0.0001, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double puttheta(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r, vol, t + 0.0001);
            double cp2 = PutPrice(S, r, vol, t - 0.0001);
            return (cp1 - cp2) / 0.0002;
        }
        public double putrho(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r + 0.0001, vol, t);
            double cp2 = PutPrice(S, r - 0.0001, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
    }

    public class LookbackAntiMT : PreparationAntiMT, CallPrice, StandardError, PutPrice, CallGreek, PutGreek
    {
        public static double[,] RandomList1;
        public static double[,] RandomList2;
        public void Initial()
        {
            optionprice = new double[Value.trials, Value.step + 1];
            optioninverse = new double[Value.trials, Value.step + 1];
            Option1 = new List<double>();
            Option2 = new List<double>();
        }
        public void RandomGenerated()
        {
            RandomList1 = new double[Value.trials, Value.step];
            Parallel.ForEach(Ienum.Step(0, Value.trials, 1), new ParallelOptions { MaxDegreeOfParallelism = Value.Thread }, i =>
            {
                for (int k = 0; k < Value.step; k++)
                {
                    RandomList1[i, k] = GaussianBoxMuller.NextDouble();
                }
            });
        }
        public void RandomAntithetic()
        {
            RandomList2 = new double[Value.trials, Value.step];
            Parallel.ForEach(Ienum.Step(0, Value.trials, 1), new ParallelOptions { MaxDegreeOfParallelism = Value.Thread }, i =>
            {
                for (int k = 0; k < Value.step; k++)
                {
                    RandomList2[i, k] = -RandomList1[i, k];
                }
            });
        }
        public double CallPrice(double S, double r, double vol, double t)
        {
            Initial();
            Parallel.ForEach(Ienum.Step(0, Value.trials, 1), new ParallelOptions { MaxDegreeOfParallelism = Value.Thread }, i =>
            {
                optionprice[i, 0] = S;
                for (int j = 1; j < Value.step + 1; j++)
                {
                    optionprice[i, j] = Price(optionprice[i, j - 1], r, vol, t, RandomList1[i, j - 1]);
                }
            });
            RandomAntithetic();
            Parallel.ForEach(Ienum.Step(0, Value.trials, 1), new ParallelOptions { MaxDegreeOfParallelism = Value.Thread }, i =>
            {
                optioninverse[i, 0] = S;
                for (int j = 1; j < Value.step + 1; j++)
                {
                    optioninverse[i, j] = Price(optioninverse[i, j - 1], r, vol, t, RandomList2[i, j - 1]);
                }
            });
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                double[] m = new double[Value.step + 1];
                for (uint j = 0; j < Value.step + 1; j++)
                {
                    m[j] = optionprice[i, j];
                }
                Option1.Add(Math.Max(m.Max() - Value.K, 0));
            }
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                double[] m = new double[Value.step + 1];
                for (uint j = 0; j < Value.step + 1; j++)
                {
                    m[j] = optioninverse[i, j];
                }
                Option2.Add(Math.Max(m.Max() - Value.K, 0));
            }
            double price1 = 0;
            price1 = Math.Exp(-r * t) * Option1.Average();
            double price2 = 0;
            price2 = Math.Exp(-r * t) * Option2.Average();
            Program.increase(1);
            return (price1 + price2) / 2;
        }
        public double PutPrice(double S, double r, double vol, double t)
        {
            Initial();
            Parallel.ForEach(Ienum.Step(0, Value.trials, 1), new ParallelOptions { MaxDegreeOfParallelism = Value.Thread }, i =>
            {
                optionprice[i, 0] = S;
                for (uint j = 1; j < Value.step + 1; j++)
                {
                    optionprice[i, j] = Price(optionprice[i, j - 1], r, vol, t, RandomList1[i, j - 1]);
                }
            });
            RandomAntithetic();
            Parallel.ForEach(Ienum.Step(0, Value.trials, 1), new ParallelOptions { MaxDegreeOfParallelism = Value.Thread }, i =>
            {
                optioninverse[i, 0] = S;
                for (int j = 1; j < Value.step + 1; j++)
                {
                    optioninverse[i, j] = Price(optioninverse[i, j - 1], r, vol, t, RandomList2[i, j - 1]);
                }
            });
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                double[] m = new double[Value.step + 1];
                for (uint j = 0; j < Value.step + 1; j++)
                {
                    m[j] = optionprice[i, j];
                }
                Option1.Add(Math.Max(Value.K - m.Min(), 0));
            }
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                double[] m = new double[Value.step + 1];
                for (uint j = 0; j < Value.step + 1; j++)
                {
                    m[j] = optioninverse[i, j];
                }
                Option2.Add(Math.Max(Value.K - m.Min(), 0));
            }
            double price1 = 0;
            price1 = Math.Exp(-r * t) * Option1.Average();
            double price2 = 0;
            price2 = Math.Exp(-r * t) * Option2.Average();
            Program.increase(1);
            return (price1 + price2) / 2;
        }
        public double SE()
        {
            List<double> Option = new List<double>();
            for (UInt32 i = 0; i < Option1.Count; i++)
            {
                Option.Add((Option1[(int)i] + Option2[(int)i]) / 2);
            }
            double avg = Option.Average();
            double sum = Option.Sum(v => Math.Pow(v - avg, 2));
            return (Math.Sqrt(sum / (Option.Count - 1))) / (Math.Sqrt(Option.Count));
        }
        internal static class GaussianBoxMuller
        {
            public static double NextDouble()
            {
                double x, y, square;

                do
                {
                    x = 2 * RandomProvider.GetThreadRandom().NextDouble() - 1;
                    y = 2 * RandomProvider.GetThreadRandom().NextDouble() - 1;
                    square = (x * x) + (y * y);
                } while (square >= 1);

                return x * Math.Sqrt(-2 * Math.Log(square) / square);
            }
        }
        internal static class RandomProvider
        {
            private static ThreadLocal<Random> randomWrapper = new ThreadLocal<Random>(() =>
                new Random(Interlocked.Increment(ref seed))
            );

            private static int seed = Environment.TickCount * 10;

            public static Random GetThreadRandom()
            {
                return randomWrapper.Value;
            }
        }
        public double calldelta(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S + 0.0001, r, vol, t);
            double cp2 = CallPrice(S - 0.0001, r, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double callgamma(double S, double r, double vol, double t)
        {
            return (calldelta(S + 0.01, r, vol, t) - calldelta(S, r, vol, t)) / 0.01;
        }
        public double callvega(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r, vol + 0.0001, t);
            double cp2 = CallPrice(S, r, vol - 0.0001, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double calltheta(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r, vol, t + 0.0001);
            double cp2 = CallPrice(S, r, vol, t - 0.0001);
            return (cp1 - cp2) / 0.0002;
        }
        public double callrho(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r + 0.0001, vol, t);
            double cp2 = CallPrice(S, r - 0.0001, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double putdelta(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S + 0.0001, r, vol, t);
            double cp2 = PutPrice(S - 0.0001, r, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double putgamma(double S, double r, double vol, double t)
        {
            return (putdelta(S + 0.01, r, vol, t) - putdelta(S, r, vol, t)) / 0.01;
        }
        public double putvega(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r, vol + 0.0001, t);
            double cp2 = PutPrice(S, r, vol - 0.0001, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double puttheta(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r, vol, t + 0.0001);
            double cp2 = PutPrice(S, r, vol, t - 0.0001);
            return (cp1 - cp2) / 0.0002;
        }
        public double putrho(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r + 0.0001, vol, t);
            double cp2 = PutPrice(S, r - 0.0001, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
    }

    public class RangeAntiMT: PreparationAntiMT, CallPrice, StandardError, PutPrice, CallGreek, PutGreek
    {
        public static double[,] RandomList1;
        public static double[,] RandomList2;
        public void Initial()
        {
            optionprice = new double[Value.trials, Value.step + 1];
            optioninverse = new double[Value.trials, Value.step + 1];
            Option1 = new List<double>();
            Option2 = new List<double>();
        }
        public void RandomGenerated()
        {
            RandomList1 = new double[Value.trials, Value.step];
            Parallel.ForEach(Ienum.Step(0, Value.trials, 1), new ParallelOptions { MaxDegreeOfParallelism = Value.Thread }, i =>
            {
                for (int k = 0; k < Value.step; k++)
                {
                    RandomList1[i, k] = GaussianBoxMuller.NextDouble();
                }
            });
        }
        public void RandomAntithetic()
        {
            RandomList2 = new double[Value.trials, Value.step];
            Parallel.ForEach(Ienum.Step(0, Value.trials, 1), new ParallelOptions { MaxDegreeOfParallelism = Value.Thread }, i =>
            {
                for (int k = 0; k < Value.step; k++)
                {
                    RandomList2[i, k] = -RandomList1[i, k];
                }
            });
        }
        public double CallPrice(double S, double r, double vol, double t)
        {
            Initial();
            Parallel.ForEach(Ienum.Step(0, Value.trials, 1), new ParallelOptions { MaxDegreeOfParallelism = Value.Thread }, i =>
            {
                optionprice[i, 0] = S;
                for (int j = 1; j < Value.step + 1; j++)
                {
                    optionprice[i, j] = Price(optionprice[i, j - 1], r, vol, t, RandomList1[i, j - 1]);
                }
            });
            RandomAntithetic();
            Parallel.ForEach(Ienum.Step(0, Value.trials, 1), new ParallelOptions { MaxDegreeOfParallelism = Value.Thread }, i =>
            {
                optioninverse[i, 0] = S;
                for (int j = 1; j < Value.step + 1; j++)
                {
                    optioninverse[i, j] = Price(optioninverse[i, j - 1], r, vol, t, RandomList2[i, j - 1]);
                }
            });
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                double[] m = new double[Value.step + 1];
                for (uint j = 0; j < Value.step + 1; j++)
                {
                    m[j] = optionprice[i, j];
                }
                Option1.Add(m.Max() - m.Min());
            }
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                double[] m = new double[Value.step + 1];
                for (uint j = 0; j < Value.step + 1; j++)
                {
                    m[j] = optioninverse[i, j];
                }
                Option2.Add(m.Max() - m.Min());
            }
            double price1 = 0;
            price1 = Math.Exp(-r * t) * Option1.Average();
            double price2 = 0;
            price2 = Math.Exp(-r * t) * Option2.Average();
            Program.increase(1);
            return (price1 + price2) / 2;
        }
        public double PutPrice(double S, double r, double vol, double t)
        {
            Initial();
            Parallel.ForEach(Ienum.Step(0, Value.trials, 1), new ParallelOptions { MaxDegreeOfParallelism = Value.Thread }, i =>
            {
                optionprice[i, 0] = S;
                for (uint j = 1; j < Value.step + 1; j++)
                {
                    optionprice[i, j] = Price(optionprice[i, j - 1], r, vol, t, RandomList1[i, j - 1]);
                }
            });
            RandomAntithetic();
            Parallel.ForEach(Ienum.Step(0, Value.trials, 1), new ParallelOptions { MaxDegreeOfParallelism = Value.Thread }, i =>
            {
                optioninverse[i, 0] = S;
                for (int j = 1; j < Value.step + 1; j++)
                {
                    optioninverse[i, j] = Price(optioninverse[i, j - 1], r, vol, t, RandomList2[i, j - 1]);
                }
            });
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                double[] m = new double[Value.step + 1];
                for (uint j = 0; j < Value.step + 1; j++)
                {
                    m[j] = optionprice[i, j];
                }
                Option1.Add(m.Max() - m.Min());
            }
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                double[] m = new double[Value.step + 1];
                for (uint j = 0; j < Value.step + 1; j++)
                {
                    m[j] = optioninverse[i, j];
                }
                Option2.Add(m.Max() - m.Min());
            }
            double price1 = 0;
            price1 = Math.Exp(-r * t) * Option1.Average();
            double price2 = 0;
            price2 = Math.Exp(-r * t) * Option2.Average();
            Program.increase(1);
            return (price1 + price2) / 2;
        }
        public double SE()
        {
            List<double> Option = new List<double>();
            for (UInt32 i = 0; i < Option1.Count; i++)
            {
                Option.Add((Option1[(int)i] + Option2[(int)i]) / 2);
            }
            double avg = Option.Average();
            double sum = Option.Sum(v => Math.Pow(v - avg, 2));
            return (Math.Sqrt(sum / (Option.Count - 1))) / (Math.Sqrt(Option.Count));
        }
        internal static class GaussianBoxMuller
        {
            public static double NextDouble()
            {
                double x, y, square;
                do
                {
                    x = 2 * RandomProvider.GetThreadRandom().NextDouble() - 1;
                    y = 2 * RandomProvider.GetThreadRandom().NextDouble() - 1;
                    square = (x * x) + (y * y);
                } while (square >= 1);

                return x * Math.Sqrt(-2 * Math.Log(square) / square);
            }
        }
        internal static class RandomProvider
        {
            private static ThreadLocal<Random> randomWrapper = new ThreadLocal<Random>(() =>
                new Random(Interlocked.Increment(ref seed))
            );

            private static int seed = Environment.TickCount * 10;

            public static Random GetThreadRandom()
            {
                return randomWrapper.Value;
            }
        }
        public double calldelta(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S + 0.0001, r, vol, t);
            double cp2 = CallPrice(S - 0.0001, r, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double callgamma(double S, double r, double vol, double t)
        {
            return (calldelta(S + 0.01, r, vol, t) - calldelta(S, r, vol, t)) / 0.01;
        }
        public double callvega(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r, vol + 0.0001, t);
            double cp2 = CallPrice(S, r, vol - 0.0001, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double calltheta(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r, vol, t + 0.0001);
            double cp2 = CallPrice(S, r, vol, t - 0.0001);
            return (cp1 - cp2) / 0.0002;
        }
        public double callrho(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r + 0.0001, vol, t);
            double cp2 = CallPrice(S, r - 0.0001, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double putdelta(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S + 0.0001, r, vol, t);
            double cp2 = PutPrice(S - 0.0001, r, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double putgamma(double S, double r, double vol, double t)
        {
            return (putdelta(S + 0.01, r, vol, t) - putdelta(S, r, vol, t)) / 0.01;
        }
        public double putvega(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r, vol + 0.0001, t);
            double cp2 = PutPrice(S, r, vol - 0.0001, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double puttheta(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r, vol, t + 0.0001);
            double cp2 = PutPrice(S, r, vol, t - 0.0001);
            return (cp1 - cp2) / 0.0002;
        }
        public double putrho(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r + 0.0001, vol, t);
            double cp2 = PutPrice(S, r - 0.0001, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
    }
}
