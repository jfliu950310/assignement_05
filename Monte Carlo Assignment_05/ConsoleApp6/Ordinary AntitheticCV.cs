﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ConsoleApp6
{
    public class PreparationAntiCV
    {
        public double[] optionprice { get; set; }
        public double[] optioninverse { get; set; }
        public double Price(double S, double r, double vol, double t, double ep)
        {
            double price;
            double deltaT = t / Convert.ToDouble(Value.step);
            price = S * Math.Exp((r - Math.Pow(vol, 2) / 2) * deltaT + vol * Math.Sqrt(deltaT) * ep);
            return price;
        }
    }

    public class EuropeanAntiCV: PreparationAntiCV, CallPrice,StandardError,PutPrice,CallGreek,PutGreek
    {
        public List<double> PriceList;
        public List<double> PriceInverse;
        public static double[,] RandomList1;
        public static double[,] RandomList2;
        public void Initial()
        {
            optionprice = new double[Value.step + 1];
            optioninverse = new double[Value.step + 1];
            PriceList = new List<double>();
            PriceInverse = new List<double>();
        }
        public void RandomGenerated()
        {
            RandomList1 = new double[Value.trials, Value.step];
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                for (int k = 0; k < Value.step; k++)
                {
                    RandomList1[i, k] = GaussianBoxMuller.NextDouble();
                }
            }
        }
        public void RandomAntithetic()
        {
            RandomList2 = new double[Value.trials, Value.step];
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                for (int k = 0; k < Value.step; k++)
                {
                    RandomList2[i, k] = -RandomList1[i, k];
                }
            }
        }
        public double CallPrice(double S, double r, double vol, double t)
        {
            Initial();
            double tl1;
            double tl2;
            double d1;
            double d2;
            double delta1;
            double delta2;
            double deltaT = t / Convert.ToDouble(Value.step);
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                optionprice[0] = S;
                double cv = 0;
                for (int j = 1; j < Value.step + 1; j++)
                {
                    optionprice[j] = Price(optionprice[j - 1], r, vol, t, RandomList1[i, j - 1]);
                    tl1 = t - (j - 1) * deltaT;
                    d1 = (Math.Log(optionprice[j - 1] / Value.K) + (r + Math.Pow(vol, 2) / 2) * tl1) / (vol * Math.Sqrt(tl1));
                    delta1 = normalCDF(d1);
                    cv = cv + delta1 * (optionprice[j] - optionprice[j - 1] * Math.Exp(r * deltaT));
                }
                PriceList.Add(Math.Max(optionprice.Last()-Value.K,0)-cv);
            }
            RandomAntithetic();
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                optioninverse[0] = S;
                double cv = 0;
                for (int j = 1; j < Value.step + 1; j++)
                {
                    optioninverse[j] = Price(optioninverse[j - 1], r, vol, t, RandomList2[i, j - 1]);
                    tl2 = t - (j - 1) * deltaT;
                    d2 = (Math.Log(optioninverse[j - 1] / Value.K) + (r + Math.Pow(vol, 2) / 2) * tl2) / (vol * Math.Sqrt(tl2));
                    delta2 = normalCDF(d2);
                    cv = cv + delta2 * (optioninverse[j] - optioninverse[j - 1] * Math.Exp(r * deltaT));
                }
                PriceInverse.Add(Math.Max(optioninverse.Last()-Value.K , 0) -cv);
            }
            double price1 = 0;
            price1 = Math.Exp(-r * t) * PriceList.Average();
            double price2 = 0;
            price2 = Math.Exp(-r * t) * PriceInverse.Average();
            Program.increase(1);
            return (price1 + price2) / 2;
        }
        public double PutPrice(double S, double r, double vol, double t)
        {
            Initial();
            double tl1;
            double tl2;
            double d1;
            double d2;
            double delta1;
            double delta2;
            double deltaT = t / Convert.ToDouble(Value.step);
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                optionprice[0] = S;
                double cv = 0;
                for (int j = 1; j < Value.step + 1; j++)
                {
                    optionprice[j] = Price(optionprice[j - 1], r, vol, t, RandomList1[i, j - 1]);
                    tl1 = t - (j - 1) * deltaT;
                    d1 = (Math.Log(optionprice[j - 1] / Value.K) + (r + Math.Pow(vol, 2) / 2) * tl1) / (vol * Math.Sqrt(tl1));
                    delta1 = normalCDF(d1)-1;
                    cv = cv + delta1 * (optionprice[j] - optionprice[j - 1] * Math.Exp(r * deltaT));
                }
                PriceList.Add(Math.Max(Value.K - optionprice.Last(), 0) - cv);
            }
            RandomAntithetic();
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                optioninverse[0] = S;
                double cv = 0;
                for (int j = 1; j < Value.step + 1; j++)
                {
                    optioninverse[j] = Price(optioninverse[j - 1], r, vol, t, RandomList2[i, j - 1]);
                    tl2 = t - (j - 1) * deltaT;
                    d2 = (Math.Log(optioninverse[j - 1] / Value.K) + (r + Math.Pow(vol, 2) / 2) * tl2) / (vol * Math.Sqrt(tl2));
                    delta2 = normalCDF(d2)-1;
                    cv = cv + delta2 * (optioninverse[j] - optioninverse[j - 1] * Math.Exp(r * deltaT));
                }
                PriceInverse.Add(Math.Max(Value.K - optioninverse.Last(), 0) - cv);
            }
            double price1 = 0;
            price1 = Math.Exp(-r * t) * PriceList.Average();
            double price2 = 0;
            price2 = Math.Exp(-r * t) * PriceInverse.Average();
            Program.increase(1);
            return (price1 + price2) / 2;
        }
        public double SE()
        {
            List<double> Option = new List<double>();
            for (UInt32 i = 0; i < PriceList.Count; i++)
            {
                Option.Add((PriceList[(int)i] + PriceInverse[(int)i]) / 2);
            }
            double avg = Option.Average();
            double sum = Option.Sum(v => Math.Pow(v - avg, 2));
            return (Math.Sqrt(sum / (Option.Count - 1))) / (Math.Sqrt(Option.Count));
        }
        public double normalCDF(double value)
        {
            const double A1 = 0.254829592;
            const double A2 = -0.284496736;
            const double A3 = 1.421413741;
            const double A4 = -1.453152027;
            const double A5 = 1.061405429;
            const double P = 0.3275911;

            int sign;
            if (value < 0)
                sign = -1;
            else
                sign = 1;

            double x = Math.Abs(value) / Math.Sqrt(2);
            double t = 1 / (1 + P * x);
            double erf = 1 - (((((A5 * t + A4) * t + A3) * t + A2) * t + A1) * t * Math.Exp(-x * x));


            return 0.5 * (1 + sign * erf);
        }
        internal static class GaussianBoxMuller
        {
            public static double NextDouble()
            {
                double x, y, square;

                do
                {
                    x = 2 * RandomProvider.GetThreadRandom().NextDouble() - 1;
                    y = 2 * RandomProvider.GetThreadRandom().NextDouble() - 1;
                    square = (x * x) + (y * y);
                } while (square >= 1);

                return x * Math.Sqrt(-2 * Math.Log(square) / square);
            }
        }
        internal static class RandomProvider
        {
            private static ThreadLocal<Random> randomWrapper = new ThreadLocal<Random>(() =>
                new Random(Interlocked.Increment(ref seed))
            );

            private static int seed = Environment.TickCount;

            public static Random GetThreadRandom()
            {
                return randomWrapper.Value;
            }
        }
        public double calldelta(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S + 0.0001, r, vol, t);
            double cp2 = CallPrice(S - 0.0001, r, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double callgamma(double S, double r, double vol, double t)
        {
            return (calldelta(S + 0.01, r, vol, t) - calldelta(S, r, vol, t)) / 0.01;
        }
        public double callvega(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r, vol + 0.0001, t);
            double cp2 = CallPrice(S, r, vol - 0.0001, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double calltheta(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r, vol, t + 0.0001);
            double cp2 = CallPrice(S, r, vol, t - 0.0001);
            return (cp1 - cp2) / 0.0002;
        }
        public double callrho(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r + 0.0001, vol, t);
            double cp2 = CallPrice(S, r - 0.0001, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double putdelta(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S + 0.0001, r, vol, t);
            double cp2 = PutPrice(S - 0.0001, r, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double putgamma(double S, double r, double vol, double t)
        {
            return (putdelta(S + 0.01, r, vol, t) - putdelta(S, r, vol, t)) / 0.01;
        }
        public double putvega(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r, vol + 0.0001, t);
            double cp2 = PutPrice(S, r, vol - 0.0001, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double puttheta(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r, vol, t + 0.0001);
            double cp2 = PutPrice(S, r, vol, t - 0.0001);
            return (cp1 - cp2) / 0.0002;
        }
        public double putrho(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r + 0.0001, vol, t);
            double cp2 = PutPrice(S, r - 0.0001, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
    }

    public class AsianAntiCV : PreparationAntiCV, CallPrice, StandardError, PutPrice, CallGreek, PutGreek
    {
        public List<double> PriceList;
        public List<double> PriceInverse;
        public static double[,] RandomList1;
        public static double[,] RandomList2;
        public void Initial()
        {
            optionprice = new double[Value.step + 1];
            optioninverse = new double[Value.step + 1];
            PriceList = new List<double>();
            PriceInverse = new List<double>();
        }
        public void RandomGenerated()
        {
            RandomList1 = new double[Value.trials, Value.step];
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                for (int k = 0; k < Value.step; k++)
                {
                    RandomList1[i, k] = GaussianBoxMuller.NextDouble();
                }
            }
        }
        public void RandomAntithetic()
        {
            RandomList2 = new double[Value.trials, Value.step];
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                for (int k = 0; k < Value.step; k++)
                {
                    RandomList2[i, k] = -RandomList1[i, k];
                }
            }
        }
        public double CallPrice(double S, double r, double vol, double t)
        {
            Initial();
            double tl1;
            double tl2;
            double d1;
            double d2;
            double delta1;
            double delta2;
            double deltaT = t / Convert.ToDouble(Value.step);
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                optionprice[0] = S;
                double cv = 0;
                for (int j = 1; j < Value.step + 1; j++)
                {
                    optionprice[j] = Price(optionprice[j - 1], r, vol, t, RandomList1[i, j - 1]);
                    tl1 = t - (j - 1) * deltaT;
                    d1 = (Math.Log(optionprice[j - 1] / Value.K) + (r + Math.Pow(vol, 2) / 2) * tl1) / (vol * Math.Sqrt(tl1));
                    delta1 = normalCDF(d1);
                    cv = cv + delta1 * (optionprice[j] - optionprice[j - 1] * Math.Exp(r * deltaT));
                }
                PriceList.Add(Math.Max((optionprice.Average() * (Value.step + 1) - optionprice[0]) / (Value.step) - Value.K, 0) - cv);
            }
            RandomAntithetic();
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                optioninverse[0] = S;
                double cv = 0;
                for (int j = 1; j < Value.step + 1; j++)
                {
                    optioninverse[j] = Price(optioninverse[j - 1], r, vol, t, RandomList2[i, j - 1]);
                    tl2 = t - (j - 1) * deltaT;
                    d2 = (Math.Log(optioninverse[j - 1] / Value.K) + (r + Math.Pow(vol, 2) / 2) * tl2) / (vol * Math.Sqrt(tl2));
                    delta2 = normalCDF(d2);
                    cv = cv + delta2 * (optioninverse[j] - optioninverse[j - 1] * Math.Exp(r * deltaT));
                }
                PriceInverse.Add(Math.Max((optioninverse.Average() * (Value.step + 1) - optioninverse[0]) / (Value.step) - Value.K, 0) - cv);
            }
            double price1 = 0;
            price1 = Math.Exp(-r * t) * PriceList.Average();
            double price2 = 0;
            price2 = Math.Exp(-r * t) * PriceInverse.Average();
            Program.increase(1);
            return (price1 + price2) / 2;
        }
        public double PutPrice(double S, double r, double vol, double t)
        {
            Initial();
            double tl1;
            double tl2;
            double d1;
            double d2;
            double delta1;
            double delta2;
            double deltaT = t / Convert.ToDouble(Value.step);
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                optionprice[0] = S;
                double cv = 0;
                for (int j = 1; j < Value.step + 1; j++)
                {
                    optionprice[j] = Price(optionprice[j - 1], r, vol, t, RandomList1[i, j - 1]);
                    tl1 = t - (j - 1) * deltaT;
                    d1 = (Math.Log(optionprice[j - 1] / Value.K) + (r + Math.Pow(vol, 2) / 2) * tl1) / (vol * Math.Sqrt(tl1));
                    delta1 = normalCDF(d1)-1;
                    cv = cv + delta1 * (optionprice[j] - optionprice[j - 1] * Math.Exp(r * deltaT));
                }
                PriceList.Add(Math.Max(Value.K - (optionprice.Average() * (Value.step + 1) - optionprice[0]) / (Value.step), 0) - cv);
            }
            RandomAntithetic();
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                optioninverse[0] = S;
                double cv = 0;
                for (int j = 1; j < Value.step + 1; j++)
                {
                    optioninverse[j] = Price(optioninverse[j - 1], r, vol, t, RandomList2[i, j - 1]);
                    tl2 = t - (j - 1) * deltaT;
                    d2 = (Math.Log(optioninverse[j - 1] / Value.K) + (r + Math.Pow(vol, 2) / 2) * tl2) / (vol * Math.Sqrt(tl2));
                    delta2 = normalCDF(d2)-1;
                    cv = cv + delta2 * (optioninverse[j] - optioninverse[j - 1] * Math.Exp(r * deltaT));
                }
                PriceInverse.Add(Math.Max(Value.K - (optioninverse.Average() * (Value.step + 1) - optioninverse[0]) / (Value.step), 0) - cv);
            }
            double price1 = 0;
            price1 = Math.Exp(-r * t) * PriceList.Average();
            double price2 = 0;
            price2 = Math.Exp(-r * t) * PriceInverse.Average();
            Program.increase(1);
            return (price1 + price2) / 2;
        }
        public double SE()
        {
            List<double> Option = new List<double>();
            for (UInt32 i = 0; i < PriceList.Count; i++)
            {
                Option.Add((PriceList[(int)i] + PriceInverse[(int)i]) / 2);
            }
            double avg = Option.Average();
            double sum = Option.Sum(v => Math.Pow(v - avg, 2));
            return (Math.Sqrt(sum / (Option.Count - 1))) / (Math.Sqrt(Option.Count));
        }
        public double normalCDF(double value)
        {
            const double A1 = 0.254829592;
            const double A2 = -0.284496736;
            const double A3 = 1.421413741;
            const double A4 = -1.453152027;
            const double A5 = 1.061405429;
            const double P = 0.3275911;

            int sign;
            if (value < 0)
                sign = -1;
            else
                sign = 1;

            double x = Math.Abs(value) / Math.Sqrt(2);
            double t = 1 / (1 + P * x);
            double erf = 1 - (((((A5 * t + A4) * t + A3) * t + A2) * t + A1) * t * Math.Exp(-x * x));


            return 0.5 * (1 + sign * erf);
        }
        internal static class GaussianBoxMuller
        {
            public static double NextDouble()
            {
                double x, y, square;

                do
                {
                    x = 2 * RandomProvider.GetThreadRandom().NextDouble() - 1;
                    y = 2 * RandomProvider.GetThreadRandom().NextDouble() - 1;
                    square = (x * x) + (y * y);
                } while (square >= 1);

                return x * Math.Sqrt(-2 * Math.Log(square) / square);
            }
        }
        internal static class RandomProvider
        {
            private static ThreadLocal<Random> randomWrapper = new ThreadLocal<Random>(() =>
                new Random(Interlocked.Increment(ref seed))
            );

            private static int seed = Environment.TickCount;

            public static Random GetThreadRandom()
            {
                return randomWrapper.Value;
            }
        }
        public double calldelta(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S + 0.0001, r, vol, t);
            double cp2 = CallPrice(S - 0.0001, r, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double callgamma(double S, double r, double vol, double t)
        {
            return (calldelta(S + 0.01, r, vol, t) - calldelta(S, r, vol, t)) / 0.01;
        }
        public double callvega(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r, vol + 0.0001, t);
            double cp2 = CallPrice(S, r, vol - 0.0001, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double calltheta(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r, vol, t + 0.0001);
            double cp2 = CallPrice(S, r, vol, t - 0.0001);
            return (cp1 - cp2) / 0.0002;
        }
        public double callrho(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r + 0.0001, vol, t);
            double cp2 = CallPrice(S, r - 0.0001, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double putdelta(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S + 0.0001, r, vol, t);
            double cp2 = PutPrice(S - 0.0001, r, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double putgamma(double S, double r, double vol, double t)
        {
            return (putdelta(S + 0.01, r, vol, t) - putdelta(S, r, vol, t)) / 0.01;
        }
        public double putvega(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r, vol + 0.0001, t);
            double cp2 = PutPrice(S, r, vol - 0.0001, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double puttheta(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r, vol, t + 0.0001);
            double cp2 = PutPrice(S, r, vol, t - 0.0001);
            return (cp1 - cp2) / 0.0002;
        }
        public double putrho(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r + 0.0001, vol, t);
            double cp2 = PutPrice(S, r - 0.0001, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
    }

    public class DigitalAntiCV: PreparationAntiCV, CallPrice, StandardError, PutPrice, CallGreek, PutGreek
    {
        public List<double> PriceList;
        public List<double> PriceInverse;
        public static double[,] RandomList1;
        public static double[,] RandomList2;
        public void Initial()
        {
            optionprice = new double[Value.step + 1];
            optioninverse = new double[Value.step + 1];
            PriceList = new List<double>();
            PriceInverse = new List<double>();
        }
        public void RandomGenerated()
        {
            RandomList1 = new double[Value.trials, Value.step];
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                for (int k = 0; k < Value.step; k++)
                {
                    RandomList1[i, k] = GaussianBoxMuller.NextDouble();
                }
            }
        }
        public void RandomAntithetic()
        {
            RandomList2 = new double[Value.trials, Value.step];
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                for (int k = 0; k < Value.step; k++)
                {
                    RandomList2[i, k] = -RandomList1[i, k];
                }
            }
        }
        public double CallPrice(double S, double r, double vol, double t)
        {
            Initial();
            double tl1;
            double tl2;
            double d1;
            double d2;
            double delta1;
            double delta2;
            double deltaT = t / Convert.ToDouble(Value.step);
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                optionprice[0] = S;
                double cv = 0;
                for (int j = 1; j < Value.step + 1; j++)
                {
                    optionprice[j] = Price(optionprice[j - 1], r, vol, t, RandomList1[i, j - 1]);
                    tl1 = t - (j - 1) * deltaT;
                    d1 = (Math.Log(optionprice[j - 1] / Value.K) + (r + Math.Pow(vol, 2) / 2) * tl1) / (vol * Math.Sqrt(tl1));
                    delta1 = normalCDF(d1);
                    cv = cv + delta1 * (optionprice[j] - optionprice[j - 1] * Math.Exp(r * deltaT));
                }
                if (optionprice.Last() - Value.K >= 0)
                { PriceList.Add(Value.rebate - cv); }
                else
                { PriceList.Add(0 - cv); }
            }
            RandomAntithetic();
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                optioninverse[0] = S;
                double cv = 0;
                for (int j = 1; j < Value.step + 1; j++)
                {
                    optioninverse[j] = Price(optioninverse[j - 1], r, vol, t, RandomList2[i, j - 1]);
                    tl2 = t - (j - 1) * deltaT;
                    d2 = (Math.Log(optioninverse[j - 1] / Value.K) + (r + Math.Pow(vol, 2) / 2) * tl2) / (vol * Math.Sqrt(tl2));
                    delta2 = normalCDF(d2);
                    cv = cv + delta2 * (optioninverse[j] - optioninverse[j - 1] * Math.Exp(r * deltaT));
                }
                if (optioninverse.Last() - Value.K >= 0)
                { PriceInverse.Add(Value.rebate - cv); }
                else
                { PriceInverse.Add(0 - cv); }
            }
            double price1 = 0;
            price1 = Math.Exp(-r * t) * PriceList.Average();
            double price2 = 0;
            price2 = Math.Exp(-r * t) * PriceInverse.Average();
            Program.increase(1);
            return (price1 + price2) / 2;
        }
        public double PutPrice(double S, double r, double vol, double t)
        {
            Initial();
            double tl1;
            double tl2;
            double d1;
            double d2;
            double delta1;
            double delta2;
            double deltaT = t / Convert.ToDouble(Value.step);
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                optionprice[0] = S;
                double cv = 0;
                for (int j = 1; j < Value.step + 1; j++)
                {
                    optionprice[j] = Price(optionprice[j - 1], r, vol, t, RandomList1[i, j - 1]);
                    tl1 = t - (j - 1) * deltaT;
                    d1 = (Math.Log(optionprice[j - 1] / Value.K) + (r + Math.Pow(vol, 2) / 2) * tl1) / (vol * Math.Sqrt(tl1));
                    delta1 = normalCDF(d1)-1;
                    cv = cv + delta1 * (optionprice[j] - optionprice[j - 1] * Math.Exp(r * deltaT));
                }
                if (Value.K - optionprice.Last() >= 0)
                { PriceList.Add(Value.rebate - cv); }
                else
                { PriceList.Add(0 - cv); }
            }
            RandomAntithetic();
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                optioninverse[0] = S;
                double cv = 0;
                for (int j = 1; j < Value.step + 1; j++)
                {
                    optioninverse[j] = Price(optioninverse[j - 1], r, vol, t, RandomList2[i, j - 1]);
                    tl2 = t - (j - 1) * deltaT;
                    d2 = (Math.Log(optioninverse[j - 1] / Value.K) + (r + Math.Pow(vol, 2) / 2) * tl2) / (vol * Math.Sqrt(tl2));
                    delta2 = normalCDF(d2)-1;
                    cv = cv + delta2 * (optioninverse[j] - optioninverse[j - 1] * Math.Exp(r * deltaT));
                }
                if (Value.K - optioninverse.Last() >= 0)
                { PriceInverse.Add(Value.rebate - cv); }
                else
                { PriceInverse.Add(0 - cv); }
            }
            double price1 = 0;
            price1 = Math.Exp(-r * t) * PriceList.Average();
            double price2 = 0;
            price2 = Math.Exp(-r * t) * PriceInverse.Average();
            Program.increase(1);
            return (price1 + price2) / 2;
        }
        public double SE()
        {
            List<double> Option = new List<double>();
            for (UInt32 i = 0; i < PriceList.Count; i++)
            {
                Option.Add((PriceList[(int)i] + PriceInverse[(int)i]) / 2);
            }
            double avg = Option.Average();
            double sum = Option.Sum(v => Math.Pow(v - avg, 2));
            return (Math.Sqrt(sum / (Option.Count - 1))) / (Math.Sqrt(Option.Count));
        }
        public double normalCDF(double value)
        {
            const double A1 = 0.254829592;
            const double A2 = -0.284496736;
            const double A3 = 1.421413741;
            const double A4 = -1.453152027;
            const double A5 = 1.061405429;
            const double P = 0.3275911;

            int sign;
            if (value < 0)
                sign = -1;
            else
                sign = 1;

            double x = Math.Abs(value) / Math.Sqrt(2);
            double t = 1 / (1 + P * x);
            double erf = 1 - (((((A5 * t + A4) * t + A3) * t + A2) * t + A1) * t * Math.Exp(-x * x));


            return 0.5 * (1 + sign * erf);
        }
        internal static class GaussianBoxMuller
        {
            public static double NextDouble()
            {
                double x, y, square;

                do
                {
                    x = 2 * RandomProvider.GetThreadRandom().NextDouble() - 1;
                    y = 2 * RandomProvider.GetThreadRandom().NextDouble() - 1;
                    square = (x * x) + (y * y);
                } while (square >= 1);

                return x * Math.Sqrt(-2 * Math.Log(square) / square);
            }
        }
        internal static class RandomProvider
        {
            private static ThreadLocal<Random> randomWrapper = new ThreadLocal<Random>(() =>
                new Random(Interlocked.Increment(ref seed))
            );

            private static int seed = Environment.TickCount;

            public static Random GetThreadRandom()
            {
                return randomWrapper.Value;
            }
        }
        public double calldelta(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S + 0.0001, r, vol, t);
            double cp2 = CallPrice(S - 0.0001, r, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double callgamma(double S, double r, double vol, double t)
        {
            return (calldelta(S + 0.01, r, vol, t) - calldelta(S, r, vol, t)) / 0.01;
        }
        public double callvega(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r, vol + 0.0001, t);
            double cp2 = CallPrice(S, r, vol - 0.0001, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double calltheta(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r, vol, t + 0.0001);
            double cp2 = CallPrice(S, r, vol, t - 0.0001);
            return (cp1 - cp2) / 0.0002;
        }
        public double callrho(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r + 0.0001, vol, t);
            double cp2 = CallPrice(S, r - 0.0001, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double putdelta(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S + 0.0001, r, vol, t);
            double cp2 = PutPrice(S - 0.0001, r, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double putgamma(double S, double r, double vol, double t)
        {
            return (putdelta(S + 0.01, r, vol, t) - putdelta(S, r, vol, t)) / 0.01;
        }
        public double putvega(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r, vol + 0.0001, t);
            double cp2 = PutPrice(S, r, vol - 0.0001, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double puttheta(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r, vol, t + 0.0001);
            double cp2 = PutPrice(S, r, vol, t - 0.0001);
            return (cp1 - cp2) / 0.0002;
        }
        public double putrho(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r + 0.0001, vol, t);
            double cp2 = PutPrice(S, r - 0.0001, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
    }

    public class BarrierDAOAntiCV: PreparationAntiCV, CallPrice, StandardError, PutPrice, CallGreek, PutGreek
    {
        public List<double> PriceList;
        public List<double> PriceInverse;
        public static double[,] RandomList1;
        public static double[,] RandomList2;
        public void Initial()
        {
            optionprice = new double[Value.step + 1];
            optioninverse = new double[Value.step + 1];
            PriceList = new List<double>();
            PriceInverse = new List<double>();
        }
        public void RandomGenerated()
        {
            RandomList1 = new double[Value.trials, Value.step];
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                for (int k = 0; k < Value.step; k++)
                {
                    RandomList1[i, k] = GaussianBoxMuller.NextDouble();
                }
            }
        }
        public void RandomAntithetic()
        {
            RandomList2 = new double[Value.trials, Value.step];
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                for (int k = 0; k < Value.step; k++)
                {
                    RandomList2[i, k] = -RandomList1[i, k];
                }
            }
        }
        public double CallPrice(double S, double r, double vol, double t)
        {
            Initial();
            double tl1;
            double tl2;
            double d1;
            double d2;
            double delta1;
            double delta2;
            double deltaT = t / Convert.ToDouble(Value.step);
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                optionprice[0] = S;
                double cv = 0;
                for (int j = 1; j < Value.step + 1; j++)
                {
                    optionprice[j] = Price(optionprice[j - 1], r, vol, t, RandomList1[i, j - 1]);
                    tl1 = t - (j - 1) * deltaT;
                    d1 = (Math.Log(optionprice[j - 1] / Value.K) + (r + Math.Pow(vol, 2) / 2) * tl1) / (vol * Math.Sqrt(tl1));
                    delta1 = normalCDF(d1);
                    cv = cv + delta1 * (optionprice[j] - optionprice[j - 1] * Math.Exp(r * deltaT));
                }
                if (optionprice.Min() >= Value.barrier)
                { PriceList.Add(Math.Max(optionprice.Last() - Value.K, 0) - cv); }
                else
                {
                    PriceList.Add(0);
                }
            }
            RandomAntithetic();
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                optioninverse[0] = S;
                double cv = 0;
                for (int j = 1; j < Value.step + 1; j++)
                {
                    optioninverse[j] = Price(optioninverse[j - 1], r, vol, t, RandomList2[i, j - 1]);
                    tl2 = t - (j - 1) * deltaT;
                    d2 = (Math.Log(optioninverse[j - 1] / Value.K) + (r + Math.Pow(vol, 2) / 2) * tl2) / (vol * Math.Sqrt(tl2));
                    delta2 = normalCDF(d2);
                    cv = cv + delta2 * (optioninverse[j] - optioninverse[j - 1] * Math.Exp(r * deltaT));
                }
                if (optioninverse.Min() >= Value.barrier)
                { PriceInverse.Add(Math.Max(optioninverse.Last() - Value.K, 0) - cv); }
                else
                {
                    PriceInverse.Add(0);
                }
            }
            double price1 = 0;
            price1 = Math.Exp(-r * t) * PriceList.Average();
            double price2 = 0;
            price2 = Math.Exp(-r * t) * PriceInverse.Average();
            Program.increase(1);
            return (price1 + price2) / 2;
        }
        public double PutPrice(double S, double r, double vol, double t)
        {
            Initial();
            double tl1;
            double tl2;
            double d1;
            double d2;
            double delta1;
            double delta2;
            double deltaT = t / Convert.ToDouble(Value.step);
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                optionprice[0] = S;
                double cv = 0;
                for (int j = 1; j < Value.step + 1; j++)
                {
                    optionprice[j] = Price(optionprice[j - 1], r, vol, t, RandomList1[i, j - 1]);
                    tl1 = t - (j - 1) * deltaT;
                    d1 = (Math.Log(optionprice[j - 1] / Value.K) + (r + Math.Pow(vol, 2) / 2) * tl1) / (vol * Math.Sqrt(tl1));
                    delta1 = normalCDF(d1)-1;
                    cv = cv + delta1 * (optionprice[j] - optionprice[j - 1] * Math.Exp(r * deltaT));
                }
                if (optionprice.Min() >= Value.barrier)
                { PriceList.Add(Math.Max(Value.K - optionprice.Last(), 0) - cv); }
                else
                {
                    PriceList.Add(0);
                }
            }
            RandomAntithetic();
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                optioninverse[0] = S;
                double cv = 0;
                for (int j = 1; j < Value.step + 1; j++)
                {
                    optioninverse[j] = Price(optioninverse[j - 1], r, vol, t, RandomList2[i, j - 1]);
                    tl2 = t - (j - 1) * deltaT;
                    d2 = (Math.Log(optioninverse[j - 1] / Value.K) + (r + Math.Pow(vol, 2) / 2) * tl2) / (vol * Math.Sqrt(tl2));
                    delta2 = normalCDF(d2)-1;
                    cv = cv + delta2 * (optioninverse[j] - optioninverse[j - 1] * Math.Exp(r * deltaT));
                }
                if (optioninverse.Min() >= Value.barrier)
                { PriceInverse.Add(Math.Max(Value.K - optioninverse.Last(), 0) - cv); }
                else
                {
                    PriceInverse.Add(0);
                }
            }
            double price1 = 0;
            price1 = Math.Exp(-r * t) * PriceList.Average();
            double price2 = 0;
            price2 = Math.Exp(-r * t) * PriceInverse.Average();
            Program.increase(1);
            return (price1 + price2) / 2;
        }
        public double SE()
        {
            List<double> Option = new List<double>();
            for (UInt32 i = 0; i < PriceList.Count; i++)
            {
                Option.Add((PriceList[(int)i] + PriceInverse[(int)i]) / 2);
            }
            double avg = Option.Average();
            double sum = Option.Sum(v => Math.Pow(v - avg, 2));
            return (Math.Sqrt(sum / (Option.Count - 1))) / (Math.Sqrt(Option.Count));
        }
        public double normalCDF(double value)
        {
            const double A1 = 0.254829592;
            const double A2 = -0.284496736;
            const double A3 = 1.421413741;
            const double A4 = -1.453152027;
            const double A5 = 1.061405429;
            const double P = 0.3275911;

            int sign;
            if (value < 0)
                sign = -1;
            else
                sign = 1;

            double x = Math.Abs(value) / Math.Sqrt(2);
            double t = 1 / (1 + P * x);
            double erf = 1 - (((((A5 * t + A4) * t + A3) * t + A2) * t + A1) * t * Math.Exp(-x * x));


            return 0.5 * (1 + sign * erf);
        }
        internal static class GaussianBoxMuller
        {
            public static double NextDouble()
            {
                double x, y, square;

                do
                {
                    x = 2 * RandomProvider.GetThreadRandom().NextDouble() - 1;
                    y = 2 * RandomProvider.GetThreadRandom().NextDouble() - 1;
                    square = (x * x) + (y * y);
                } while (square >= 1);

                return x * Math.Sqrt(-2 * Math.Log(square) / square);
            }
        }
        internal static class RandomProvider
        {
            private static ThreadLocal<Random> randomWrapper = new ThreadLocal<Random>(() =>
                new Random(Interlocked.Increment(ref seed))
            );

            private static int seed = Environment.TickCount;

            public static Random GetThreadRandom()
            {
                return randomWrapper.Value;
            }
        }
        public double calldelta(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S + 0.0001, r, vol, t);
            double cp2 = CallPrice(S - 0.0001, r, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double callgamma(double S, double r, double vol, double t)
        {
            return (calldelta(S + 0.01, r, vol, t) - calldelta(S, r, vol, t)) / 0.01;
        }
        public double callvega(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r, vol + 0.0001, t);
            double cp2 = CallPrice(S, r, vol - 0.0001, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double calltheta(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r, vol, t + 0.0001);
            double cp2 = CallPrice(S, r, vol, t - 0.0001);
            return (cp1 - cp2) / 0.0002;
        }
        public double callrho(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r + 0.0001, vol, t);
            double cp2 = CallPrice(S, r - 0.0001, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double putdelta(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S + 0.0001, r, vol, t);
            double cp2 = PutPrice(S - 0.0001, r, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double putgamma(double S, double r, double vol, double t)
        {
            return (putdelta(S + 0.01, r, vol, t) - putdelta(S, r, vol, t)) / 0.01;
        }
        public double putvega(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r, vol + 0.0001, t);
            double cp2 = PutPrice(S, r, vol - 0.0001, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double puttheta(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r, vol, t + 0.0001);
            double cp2 = PutPrice(S, r, vol, t - 0.0001);
            return (cp1 - cp2) / 0.0002;
        }
        public double putrho(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r + 0.0001, vol, t);
            double cp2 = PutPrice(S, r - 0.0001, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
    }

    public class BarrierDAIAntiCV : PreparationAntiCV, CallPrice, StandardError, PutPrice, CallGreek, PutGreek
    {
        public List<double> PriceList;
        public List<double> PriceInverse;
        public static double[,] RandomList1;
        public static double[,] RandomList2;
        public void Initial()
        {
            optionprice = new double[Value.step + 1];
            optioninverse = new double[Value.step + 1];
            PriceList = new List<double>();
            PriceInverse = new List<double>();
        }
        public void RandomGenerated()
        {
            RandomList1 = new double[Value.trials, Value.step];
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                for (int k = 0; k < Value.step; k++)
                {
                    RandomList1[i, k] = GaussianBoxMuller.NextDouble();
                }
            }
        }
        public void RandomAntithetic()
        {
            RandomList2 = new double[Value.trials, Value.step];
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                for (int k = 0; k < Value.step; k++)
                {
                    RandomList2[i, k] = -RandomList1[i, k];
                }
            }
        }
        public double CallPrice(double S, double r, double vol, double t)
        {
            Initial();
            double tl1;
            double tl2;
            double d1;
            double d2;
            double delta1;
            double delta2;
            double deltaT = t / Convert.ToDouble(Value.step);
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                optionprice[0] = S;
                double cv = 0;
                for (int j = 1; j < Value.step + 1; j++)
                {
                    optionprice[j] = Price(optionprice[j - 1], r, vol, t, RandomList1[i, j - 1]);
                    tl1 = t - (j - 1) * deltaT;
                    d1 = (Math.Log(optionprice[j - 1] / Value.K) + (r + Math.Pow(vol, 2) / 2) * tl1) / (vol * Math.Sqrt(tl1));
                    delta1 = normalCDF(d1);
                    cv = cv + delta1 * (optionprice[j] - optionprice[j - 1] * Math.Exp(r * deltaT));
                }
                if (optionprice.Min() < Value.barrier)
                { PriceList.Add(Math.Max(optionprice.Last() - Value.K, 0) - cv); }
                else
                {
                    PriceList.Add(0);
                }
            }
            RandomAntithetic();
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                optioninverse[0] = S;
                double cv = 0;
                for (int j = 1; j < Value.step + 1; j++)
                {
                    optioninverse[j] = Price(optioninverse[j - 1], r, vol, t, RandomList2[i, j - 1]);
                    tl2 = t - (j - 1) * deltaT;
                    d2 = (Math.Log(optioninverse[j - 1] / Value.K) + (r + Math.Pow(vol, 2) / 2) * tl2) / (vol * Math.Sqrt(tl2));
                    delta2 = normalCDF(d2);
                    cv = cv + delta2 * (optioninverse[j] - optioninverse[j - 1] * Math.Exp(r * deltaT));
                }
                if (optioninverse.Min() < Value.barrier)
                { PriceInverse.Add(Math.Max(optioninverse.Last() - Value.K, 0) - cv); }
                else
                {
                    PriceInverse.Add(0);
                }
            }
            double price1 = 0;
            price1 = Math.Exp(-r * t) * PriceList.Average();
            double price2 = 0;
            price2 = Math.Exp(-r * t) * PriceInverse.Average();
            Program.increase(1);
            return (price1 + price2) / 2;
        }
        public double PutPrice(double S, double r, double vol, double t)
        {
            Initial();
            double tl1;
            double tl2;
            double d1;
            double d2;
            double delta1;
            double delta2;
            double deltaT = t / Convert.ToDouble(Value.step);
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                optionprice[0] = S;
                double cv = 0;
                for (int j = 1; j < Value.step + 1; j++)
                {
                    optionprice[j] = Price(optionprice[j - 1], r, vol, t, RandomList1[i, j - 1]);
                    tl1 = t - (j - 1) * deltaT;
                    d1 = (Math.Log(optionprice[j - 1] / Value.K) + (r + Math.Pow(vol, 2) / 2) * tl1) / (vol * Math.Sqrt(tl1));
                    delta1 = normalCDF(d1)-1;
                    cv = cv + delta1 * (optionprice[j] - optionprice[j - 1] * Math.Exp(r * deltaT));
                }
                if (optionprice.Min() >= Value.barrier)
                { PriceList.Add(Math.Max(Value.K - optionprice.Last(), 0) - cv); }
                else
                {
                    PriceList.Add(0);
                }
            }
            RandomAntithetic();
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                optioninverse[0] = S;
                double cv = 0;
                for (int j = 1; j < Value.step + 1; j++)
                {
                    optioninverse[j] = Price(optioninverse[j - 1], r, vol, t, RandomList2[i, j - 1]);
                    tl2 = t - (j - 1) * deltaT;
                    d2 = (Math.Log(optioninverse[j - 1] / Value.K) + (r + Math.Pow(vol, 2) / 2) * tl2) / (vol * Math.Sqrt(tl2));
                    delta2 = normalCDF(d2)-1;
                    cv = cv + delta2 * (optioninverse[j] - optioninverse[j - 1] * Math.Exp(r * deltaT));
                }
                if (optioninverse.Min() >= Value.barrier)
                { PriceInverse.Add(Math.Max(Value.K - optioninverse.Last(), 0) - cv); }
                else
                {
                    PriceInverse.Add(0);
                }
            }
            double price1 = 0;
            price1 = Math.Exp(-r * t) * PriceList.Average();
            double price2 = 0;
            price2 = Math.Exp(-r * t) * PriceInverse.Average();
            Program.increase(1);
            return (price1 + price2) / 2;
        }
        public double SE()
        {
            List<double> Option = new List<double>();
            for (UInt32 i = 0; i < PriceList.Count; i++)
            {
                Option.Add((PriceList[(int)i] + PriceInverse[(int)i]) / 2);
            }
            double avg = Option.Average();
            double sum = Option.Sum(v => Math.Pow(v - avg, 2));
            return (Math.Sqrt(sum / (Option.Count - 1))) / (Math.Sqrt(Option.Count));
        }
        public double normalCDF(double value)
        {
            const double A1 = 0.254829592;
            const double A2 = -0.284496736;
            const double A3 = 1.421413741;
            const double A4 = -1.453152027;
            const double A5 = 1.061405429;
            const double P = 0.3275911;

            int sign;
            if (value < 0)
                sign = -1;
            else
                sign = 1;

            double x = Math.Abs(value) / Math.Sqrt(2);
            double t = 1 / (1 + P * x);
            double erf = 1 - (((((A5 * t + A4) * t + A3) * t + A2) * t + A1) * t * Math.Exp(-x * x));


            return 0.5 * (1 + sign * erf);
        }
        internal static class GaussianBoxMuller
        {
            public static double NextDouble()
            {
                double x, y, square;

                do
                {
                    x = 2 * RandomProvider.GetThreadRandom().NextDouble() - 1;
                    y = 2 * RandomProvider.GetThreadRandom().NextDouble() - 1;
                    square = (x * x) + (y * y);
                } while (square >= 1);

                return x * Math.Sqrt(-2 * Math.Log(square) / square);
            }
        }
        internal static class RandomProvider
        {
            private static ThreadLocal<Random> randomWrapper = new ThreadLocal<Random>(() =>
                new Random(Interlocked.Increment(ref seed))
            );

            private static int seed = Environment.TickCount;

            public static Random GetThreadRandom()
            {
                return randomWrapper.Value;
            }
        }
        public double calldelta(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S + 0.0001, r, vol, t);
            double cp2 = CallPrice(S - 0.0001, r, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double callgamma(double S, double r, double vol, double t)
        {
            return (calldelta(S + 0.01, r, vol, t) - calldelta(S, r, vol, t)) / 0.01;
        }
        public double callvega(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r, vol + 0.0001, t);
            double cp2 = CallPrice(S, r, vol - 0.0001, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double calltheta(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r, vol, t + 0.0001);
            double cp2 = CallPrice(S, r, vol, t - 0.0001);
            return (cp1 - cp2) / 0.0002;
        }
        public double callrho(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r + 0.0001, vol, t);
            double cp2 = CallPrice(S, r - 0.0001, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double putdelta(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S + 0.0001, r, vol, t);
            double cp2 = PutPrice(S - 0.0001, r, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double putgamma(double S, double r, double vol, double t)
        {
            return (putdelta(S + 0.01, r, vol, t) - putdelta(S, r, vol, t)) / 0.01;
        }
        public double putvega(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r, vol + 0.0001, t);
            double cp2 = PutPrice(S, r, vol - 0.0001, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double puttheta(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r, vol, t + 0.0001);
            double cp2 = PutPrice(S, r, vol, t - 0.0001);
            return (cp1 - cp2) / 0.0002;
        }
        public double putrho(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r + 0.0001, vol, t);
            double cp2 = PutPrice(S, r - 0.0001, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
    }

    public class BarrierUAOAntiCV : PreparationAntiCV, CallPrice, StandardError, PutPrice, CallGreek, PutGreek
    {
        public List<double> PriceList;
        public List<double> PriceInverse;
        public static double[,] RandomList1;
        public static double[,] RandomList2;
        public void Initial()
        {
            optionprice = new double[Value.step + 1];
            optioninverse = new double[Value.step + 1];
            PriceList = new List<double>();
            PriceInverse = new List<double>();
        }
        public void RandomGenerated()
        {
            RandomList1 = new double[Value.trials, Value.step];
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                for (int k = 0; k < Value.step; k++)
                {
                    RandomList1[i, k] = GaussianBoxMuller.NextDouble();
                }
            }
        }
        public void RandomAntithetic()
        {
            RandomList2 = new double[Value.trials, Value.step];
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                for (int k = 0; k < Value.step; k++)
                {
                    RandomList2[i, k] = -RandomList1[i, k];
                }
            }
        }
        public double CallPrice(double S, double r, double vol, double t)
        {
            Initial();
            double tl1;
            double tl2;
            double d1;
            double d2;
            double delta1;
            double delta2;
            double deltaT = t / Convert.ToDouble(Value.step);
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                optionprice[0] = S;
                double cv = 0;
                for (int j = 1; j < Value.step + 1; j++)
                {
                    optionprice[j] = Price(optionprice[j - 1], r, vol, t, RandomList1[i, j - 1]);
                    tl1 = t - (j - 1) * deltaT;
                    d1 = (Math.Log(optionprice[j - 1] / Value.K) + (r + Math.Pow(vol, 2) / 2) * tl1) / (vol * Math.Sqrt(tl1));
                    delta1 = normalCDF(d1);
                    cv = cv + delta1 * (optionprice[j] - optionprice[j - 1] * Math.Exp(r * deltaT));
                }
                if (optionprice.Max() <= Value.barrier)
                { PriceList.Add(Math.Max(optionprice.Last() - Value.K, 0) - cv); }
                else
                {
                    PriceList.Add(0);
                }
            }
            RandomAntithetic();
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                optioninverse[0] = S;
                double cv = 0;
                for (int j = 1; j < Value.step + 1; j++)
                {
                    optioninverse[j] = Price(optioninverse[j - 1], r, vol, t, RandomList2[i, j - 1]);
                    tl2 = t - (j - 1) * deltaT;
                    d2 = (Math.Log(optioninverse[j - 1] / Value.K) + (r + Math.Pow(vol, 2) / 2) * tl2) / (vol * Math.Sqrt(tl2));
                    delta2 = normalCDF(d2);
                    cv = cv + delta2 * (optioninverse[j] - optioninverse[j - 1] * Math.Exp(r * deltaT));
                }
                if (optioninverse.Max() <= Value.barrier)
                { PriceInverse.Add(Math.Max(optioninverse.Last() - Value.K, 0) - cv); }
                else
                {
                    PriceInverse.Add(0);
                }
            }
            double price1 = 0;
            price1 = Math.Exp(-r * t) * PriceList.Average();
            double price2 = 0;
            price2 = Math.Exp(-r * t) * PriceInverse.Average();
            Program.increase(1);
            return (price1 + price2) / 2;
        }
        public double PutPrice(double S, double r, double vol, double t)
        {
            Initial();
            double tl1;
            double tl2;
            double d1;
            double d2;
            double delta1;
            double delta2;
            double deltaT = t / Convert.ToDouble(Value.step);
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                optionprice[0] = S;
                double cv = 0;
                for (int j = 1; j < Value.step + 1; j++)
                {
                    optionprice[j] = Price(optionprice[j - 1], r, vol, t, RandomList1[i, j - 1]);
                    tl1 = t - (j - 1) * deltaT;
                    d1 = (Math.Log(optionprice[j - 1] / Value.K) + (r + Math.Pow(vol, 2) / 2) * tl1) / (vol * Math.Sqrt(tl1));
                    delta1 = normalCDF(d1)-1;
                    cv = cv + delta1 * (optionprice[j] - optionprice[j - 1] * Math.Exp(r * deltaT));
                }
                if (optionprice.Max() <= Value.barrier)
                { PriceList.Add(Math.Max(Value.K - optionprice.Last(), 0) - cv); }
                else
                {
                    PriceList.Add(0);
                }
            }
            RandomAntithetic();
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                optioninverse[0] = S;
                double cv = 0;
                for (int j = 1; j < Value.step + 1; j++)
                {
                    optioninverse[j] = Price(optioninverse[j - 1], r, vol, t, RandomList2[i, j - 1]);
                    tl2 = t - (j - 1) * deltaT;
                    d2 = (Math.Log(optioninverse[j - 1] / Value.K) + (r + Math.Pow(vol, 2) / 2) * tl2) / (vol * Math.Sqrt(tl2));
                    delta2 = normalCDF(d2)-1;
                    cv = cv + delta2 * (optioninverse[j] - optioninverse[j - 1] * Math.Exp(r * deltaT));
                }
                if (optioninverse.Max() <= Value.barrier)
                { PriceInverse.Add(Math.Max(Value.K - optioninverse.Last(), 0) - cv); }
                else
                {
                    PriceInverse.Add(0);
                }
            }
            double price1 = 0;
            price1 = Math.Exp(-r * t) * PriceList.Average();
            double price2 = 0;
            price2 = Math.Exp(-r * t) * PriceInverse.Average();
            Program.increase(1);
            return (price1 + price2) / 2;
        }
        public double SE()
        {
            List<double> Option = new List<double>();
            for (UInt32 i = 0; i < PriceList.Count; i++)
            {
                Option.Add((PriceList[(int)i] + PriceInverse[(int)i]) / 2);
            }
            double avg = Option.Average();
            double sum = Option.Sum(v => Math.Pow(v - avg, 2));
            return (Math.Sqrt(sum / (Option.Count - 1))) / (Math.Sqrt(Option.Count));
        }
        public double normalCDF(double value)
        {
            const double A1 = 0.254829592;
            const double A2 = -0.284496736;
            const double A3 = 1.421413741;
            const double A4 = -1.453152027;
            const double A5 = 1.061405429;
            const double P = 0.3275911;

            int sign;
            if (value < 0)
                sign = -1;
            else
                sign = 1;

            double x = Math.Abs(value) / Math.Sqrt(2);
            double t = 1 / (1 + P * x);
            double erf = 1 - (((((A5 * t + A4) * t + A3) * t + A2) * t + A1) * t * Math.Exp(-x * x));


            return 0.5 * (1 + sign * erf);
        }
        internal static class GaussianBoxMuller
        {
            public static double NextDouble()
            {
                double x, y, square;

                do
                {
                    x = 2 * RandomProvider.GetThreadRandom().NextDouble() - 1;
                    y = 2 * RandomProvider.GetThreadRandom().NextDouble() - 1;
                    square = (x * x) + (y * y);
                } while (square >= 1);

                return x * Math.Sqrt(-2 * Math.Log(square) / square);
            }
        }
        internal static class RandomProvider
        {
            private static ThreadLocal<Random> randomWrapper = new ThreadLocal<Random>(() =>
                new Random(Interlocked.Increment(ref seed))
            );

            private static int seed = Environment.TickCount;

            public static Random GetThreadRandom()
            {
                return randomWrapper.Value;
            }
        }
        public double calldelta(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S + 0.0001, r, vol, t);
            double cp2 = CallPrice(S - 0.0001, r, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double callgamma(double S, double r, double vol, double t)
        {
            return (calldelta(S + 0.01, r, vol, t) - calldelta(S, r, vol, t)) / 0.01;
        }
        public double callvega(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r, vol + 0.0001, t);
            double cp2 = CallPrice(S, r, vol - 0.0001, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double calltheta(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r, vol, t + 0.0001);
            double cp2 = CallPrice(S, r, vol, t - 0.0001);
            return (cp1 - cp2) / 0.0002;
        }
        public double callrho(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r + 0.0001, vol, t);
            double cp2 = CallPrice(S, r - 0.0001, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double putdelta(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S + 0.0001, r, vol, t);
            double cp2 = PutPrice(S - 0.0001, r, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double putgamma(double S, double r, double vol, double t)
        {
            return (putdelta(S + 0.01, r, vol, t) - putdelta(S, r, vol, t)) / 0.01;
        }
        public double putvega(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r, vol + 0.0001, t);
            double cp2 = PutPrice(S, r, vol - 0.0001, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double puttheta(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r, vol, t + 0.0001);
            double cp2 = PutPrice(S, r, vol, t - 0.0001);
            return (cp1 - cp2) / 0.0002;
        }
        public double putrho(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r + 0.0001, vol, t);
            double cp2 = PutPrice(S, r - 0.0001, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
    }

    public class BarrierUAIAntiCV : PreparationAntiCV, CallPrice, StandardError, PutPrice, CallGreek, PutGreek
    {
        public List<double> PriceList;
        public List<double> PriceInverse;
        public static double[,] RandomList1;
        public static double[,] RandomList2;
        public void Initial()
        {
            optionprice = new double[Value.step + 1];
            optioninverse = new double[Value.step + 1];
            PriceList = new List<double>();
            PriceInverse = new List<double>();
        }
        public void RandomGenerated()
        {
            RandomList1 = new double[Value.trials, Value.step];
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                for (int k = 0; k < Value.step; k++)
                {
                    RandomList1[i, k] = GaussianBoxMuller.NextDouble();
                }
            }
        }
        public void RandomAntithetic()
        {
            RandomList2 = new double[Value.trials, Value.step];
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                for (int k = 0; k < Value.step; k++)
                {
                    RandomList2[i, k] = -RandomList1[i, k];
                }
            }
        }
        public double CallPrice(double S, double r, double vol, double t)
        {
            Initial();
            double tl1;
            double tl2;
            double d1;
            double d2;
            double delta1;
            double delta2;
            double deltaT = t / Convert.ToDouble(Value.step);
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                optionprice[0] = S;
                double cv = 0;
                for (int j = 1; j < Value.step + 1; j++)
                {
                    optionprice[j] = Price(optionprice[j - 1], r, vol, t, RandomList1[i, j - 1]);
                    tl1 = t - (j - 1) * deltaT;
                    d1 = (Math.Log(optionprice[j - 1] / Value.K) + (r + Math.Pow(vol, 2) / 2) * tl1) / (vol * Math.Sqrt(tl1));
                    delta1 = normalCDF(d1);
                    cv = cv + delta1 * (optionprice[j] - optionprice[j - 1] * Math.Exp(r * deltaT));
                }
                if (optionprice.Max() > Value.barrier)
                { PriceList.Add(Math.Max(optionprice.Last() - Value.K, 0) - cv); }
                else
                {
                    PriceList.Add(0);
                }
            }
            RandomAntithetic();
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                optioninverse[0] = S;
                double cv = 0;
                for (int j = 1; j < Value.step + 1; j++)
                {
                    optioninverse[j] = Price(optioninverse[j - 1], r, vol, t, RandomList2[i, j - 1]);
                    tl2 = t - (j - 1) * deltaT;
                    d2 = (Math.Log(optioninverse[j - 1] / Value.K) + (r + Math.Pow(vol, 2) / 2) * tl2) / (vol * Math.Sqrt(tl2));
                    delta2 = normalCDF(d2);
                    cv = cv + delta2 * (optioninverse[j] - optioninverse[j - 1] * Math.Exp(r * deltaT));
                }
                if (optioninverse.Max() > Value.barrier)
                { PriceInverse.Add(Math.Max(optioninverse.Last() - Value.K, 0) - cv); }
                else
                {
                    PriceInverse.Add(0);
                }
            }
            double price1 = 0;
            price1 = Math.Exp(-r * t) * PriceList.Average();
            double price2 = 0;
            price2 = Math.Exp(-r * t) * PriceInverse.Average();
            Program.increase(1);
            return (price1 + price2) / 2;
        }
        public double PutPrice(double S, double r, double vol, double t)
        {
            Initial();
            double tl1;
            double tl2;
            double d1;
            double d2;
            double delta1;
            double delta2;
            double deltaT = t / Convert.ToDouble(Value.step);
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                optionprice[0] = S;
                double cv = 0;
                for (int j = 1; j < Value.step + 1; j++)
                {
                    optionprice[j] = Price(optionprice[j - 1], r, vol, t, RandomList1[i, j - 1]);
                    tl1 = t - (j - 1) * deltaT;
                    d1 = (Math.Log(optionprice[j - 1] / Value.K) + (r + Math.Pow(vol, 2) / 2) * tl1) / (vol * Math.Sqrt(tl1));
                    delta1 = normalCDF(d1)-1;
                    cv = cv + delta1 * (optionprice[j] - optionprice[j - 1] * Math.Exp(r * deltaT));
                }
                if (optionprice.Max() <= Value.barrier)
                { PriceList.Add(Math.Max(Value.K - optionprice.Last(), 0) - cv); }
                else
                {
                    PriceList.Add(0);
                }
            }
            RandomAntithetic();
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                optioninverse[0] = S;
                double cv = 0;
                for (int j = 1; j < Value.step + 1; j++)
                {
                    optioninverse[j] = Price(optioninverse[j - 1], r, vol, t, RandomList2[i, j - 1]);
                    tl2 = t - (j - 1) * deltaT;
                    d2 = (Math.Log(optioninverse[j - 1] / Value.K) + (r + Math.Pow(vol, 2) / 2) * tl2) / (vol * Math.Sqrt(tl2));
                    delta2 = normalCDF(d2)-1;
                    cv = cv + delta2 * (optioninverse[j] - optioninverse[j - 1] * Math.Exp(r * deltaT));
                }
                if (optioninverse.Max() <= Value.barrier)
                { PriceInverse.Add(Math.Max(Value.K - optioninverse.Last(), 0) - cv); }
                else
                {
                    PriceInverse.Add(0);
                }
            }
            double price1 = 0;
            price1 = Math.Exp(-r * t) * PriceList.Average();
            double price2 = 0;
            price2 = Math.Exp(-r * t) * PriceInverse.Average();
            Program.increase(1);
            return (price1 + price2) / 2;
        }
        public double SE()
        {
            List<double> Option = new List<double>();
            for (UInt32 i = 0; i < PriceList.Count; i++)
            {
                Option.Add((PriceList[(int)i] + PriceInverse[(int)i]) / 2);
            }
            double avg = Option.Average();
            double sum = Option.Sum(v => Math.Pow(v - avg, 2));
            return (Math.Sqrt(sum / (Option.Count - 1))) / (Math.Sqrt(Option.Count));
        }
        public double normalCDF(double value)
        {
            const double A1 = 0.254829592;
            const double A2 = -0.284496736;
            const double A3 = 1.421413741;
            const double A4 = -1.453152027;
            const double A5 = 1.061405429;
            const double P = 0.3275911;

            int sign;
            if (value < 0)
                sign = -1;
            else
                sign = 1;

            double x = Math.Abs(value) / Math.Sqrt(2);
            double t = 1 / (1 + P * x);
            double erf = 1 - (((((A5 * t + A4) * t + A3) * t + A2) * t + A1) * t * Math.Exp(-x * x));


            return 0.5 * (1 + sign * erf);
        }
        internal static class GaussianBoxMuller
        {
            public static double NextDouble()
            {
                double x, y, square;

                do
                {
                    x = 2 * RandomProvider.GetThreadRandom().NextDouble() - 1;
                    y = 2 * RandomProvider.GetThreadRandom().NextDouble() - 1;
                    square = (x * x) + (y * y);
                } while (square >= 1);

                return x * Math.Sqrt(-2 * Math.Log(square) / square);
            }
        }
        internal static class RandomProvider
        {
            private static ThreadLocal<Random> randomWrapper = new ThreadLocal<Random>(() =>
                new Random(Interlocked.Increment(ref seed))
            );

            private static int seed = Environment.TickCount;

            public static Random GetThreadRandom()
            {
                return randomWrapper.Value;
            }
        }
        public double calldelta(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S + 0.0001, r, vol, t);
            double cp2 = CallPrice(S - 0.0001, r, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double callgamma(double S, double r, double vol, double t)
        {
            return (calldelta(S + 0.01, r, vol, t) - calldelta(S, r, vol, t)) / 0.01;
        }
        public double callvega(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r, vol + 0.0001, t);
            double cp2 = CallPrice(S, r, vol - 0.0001, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double calltheta(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r, vol, t + 0.0001);
            double cp2 = CallPrice(S, r, vol, t - 0.0001);
            return (cp1 - cp2) / 0.0002;
        }
        public double callrho(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r + 0.0001, vol, t);
            double cp2 = CallPrice(S, r - 0.0001, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double putdelta(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S + 0.0001, r, vol, t);
            double cp2 = PutPrice(S - 0.0001, r, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double putgamma(double S, double r, double vol, double t)
        {
            return (putdelta(S + 0.01, r, vol, t) - putdelta(S, r, vol, t)) / 0.01;
        }
        public double putvega(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r, vol + 0.0001, t);
            double cp2 = PutPrice(S, r, vol - 0.0001, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double puttheta(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r, vol, t + 0.0001);
            double cp2 = PutPrice(S, r, vol, t - 0.0001);
            return (cp1 - cp2) / 0.0002;
        }
        public double putrho(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r + 0.0001, vol, t);
            double cp2 = PutPrice(S, r - 0.0001, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
    }

    public class LookbackAntiCV : PreparationAntiCV, CallPrice, StandardError, PutPrice, CallGreek, PutGreek
    {
        public List<double> PriceList;
        public List<double> PriceInverse;
        public static double[,] RandomList1;
        public static double[,] RandomList2;
        public void Initial()
        {
            optionprice = new double[Value.step + 1];
            optioninverse = new double[Value.step + 1];
            PriceList = new List<double>();
            PriceInverse = new List<double>();
        }
        public void RandomGenerated()
        {
            RandomList1 = new double[Value.trials, Value.step];
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                for (int k = 0; k < Value.step; k++)
                {
                    RandomList1[i, k] = GaussianBoxMuller.NextDouble();
                }
            }
        }
        public void RandomAntithetic()
        {
            RandomList2 = new double[Value.trials, Value.step];
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                for (int k = 0; k < Value.step; k++)
                {
                    RandomList2[i, k] = -RandomList1[i, k];
                }
            }
        }
        public double CallPrice(double S, double r, double vol, double t)
        {
            Initial();
            double tl1;
            double tl2;
            double d1;
            double d2;
            double delta1;
            double delta2;
            double deltaT = t / Convert.ToDouble(Value.step);
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                optionprice[0] = S;
                double cv = 0;
                for (int j = 1; j < Value.step + 1; j++)
                {
                    optionprice[j] = Price(optionprice[j - 1], r, vol, t, RandomList1[i, j - 1]);
                    tl1 = t - (j - 1) * deltaT;
                    d1 = (Math.Log(optionprice[j - 1] / Value.K) + (r + Math.Pow(vol, 2) / 2) * tl1) / (vol * Math.Sqrt(tl1));
                    delta1 = normalCDF(d1);
                    cv = cv + delta1 * (optionprice[j] - optionprice[j - 1] * Math.Exp(r * deltaT));
                }
                PriceList.Add(Math.Max(optionprice.Max() - Value.K, 0) - cv);
            }
            RandomAntithetic();
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                optioninverse[0] = S;
                double cv = 0;
                for (int j = 1; j < Value.step + 1; j++)
                {
                    optioninverse[j] = Price(optioninverse[j - 1], r, vol, t, RandomList2[i, j - 1]);
                    tl2 = t - (j - 1) * deltaT;
                    d2 = (Math.Log(optioninverse[j - 1] / Value.K) + (r + Math.Pow(vol, 2) / 2) * tl2) / (vol * Math.Sqrt(tl2));
                    delta2 = normalCDF(d2);
                    cv = cv + delta2 * (optioninverse[j] - optioninverse[j - 1] * Math.Exp(r * deltaT));
                }
                PriceInverse.Add(Math.Max(optioninverse.Max() - Value.K, 0) - cv);
            }
            double price1 = 0;
            price1 = Math.Exp(-r * t) * PriceList.Average();
            double price2 = 0;
            price2 = Math.Exp(-r * t) * PriceInverse.Average();
            Program.increase(1);
            return (price1 + price2) / 2;
        }
        public double PutPrice(double S, double r, double vol, double t)
        {
            Initial();
            double tl1;
            double tl2;
            double d1;
            double d2;
            double delta1;
            double delta2;
            double deltaT = t / Convert.ToDouble(Value.step);
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                optionprice[0] = S;
                double cv = 0;
                for (int j = 1; j < Value.step + 1; j++)
                {
                    optionprice[j] = Price(optionprice[j - 1], r, vol, t, RandomList1[i, j - 1]);
                    tl1 = t - (j - 1) * deltaT;
                    d1 = (Math.Log(optionprice[j - 1] / Value.K) + (r + Math.Pow(vol, 2) / 2) * tl1) / (vol * Math.Sqrt(tl1));
                    delta1 = normalCDF(d1)-1;
                    cv = cv + delta1 * (optionprice[j] - optionprice[j - 1] * Math.Exp(r * deltaT));
                }
                PriceList.Add(Math.Max(Value.K - optionprice.Min(), 0) - cv);
            }
            RandomAntithetic();
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                optioninverse[0] = S;
                double cv = 0;
                for (int j = 1; j < Value.step + 1; j++)
                {
                    optioninverse[j] = Price(optioninverse[j - 1], r, vol, t, RandomList2[i, j - 1]);
                    tl2 = t - (j - 1) * deltaT;
                    d2 = (Math.Log(optioninverse[j - 1] / Value.K) + (r + Math.Pow(vol, 2) / 2) * tl2) / (vol * Math.Sqrt(tl2));
                    delta2 = normalCDF(d2)-1;
                    cv = cv + delta2 * (optioninverse[j] - optioninverse[j - 1] * Math.Exp(r * deltaT));
                }
                PriceInverse.Add(Math.Max(Value.K - optioninverse.Min(), 0) - cv);
            }
            double price1 = 0;
            price1 = Math.Exp(-r * t) * PriceList.Average();
            double price2 = 0;
            price2 = Math.Exp(-r * t) * PriceInverse.Average();
            Program.increase(1);
            return (price1 + price2) / 2;
        }
        public double SE()
        {
            List<double> Option = new List<double>();
            for (UInt32 i = 0; i < PriceList.Count; i++)
            {
                Option.Add((PriceList[(int)i] + PriceInverse[(int)i]) / 2);
            }
            double avg = Option.Average();
            double sum = Option.Sum(v => Math.Pow(v - avg, 2));
            return (Math.Sqrt(sum / (Option.Count - 1))) / (Math.Sqrt(Option.Count));
        }
        public double normalCDF(double value)
        {
            const double A1 = 0.254829592;
            const double A2 = -0.284496736;
            const double A3 = 1.421413741;
            const double A4 = -1.453152027;
            const double A5 = 1.061405429;
            const double P = 0.3275911;

            int sign;
            if (value < 0)
                sign = -1;
            else
                sign = 1;

            double x = Math.Abs(value) / Math.Sqrt(2);
            double t = 1 / (1 + P * x);
            double erf = 1 - (((((A5 * t + A4) * t + A3) * t + A2) * t + A1) * t * Math.Exp(-x * x));


            return 0.5 * (1 + sign * erf);
        }
        internal static class GaussianBoxMuller
        {
            public static double NextDouble()
            {
                double x, y, square;

                do
                {
                    x = 2 * RandomProvider.GetThreadRandom().NextDouble() - 1;
                    y = 2 * RandomProvider.GetThreadRandom().NextDouble() - 1;
                    square = (x * x) + (y * y);
                } while (square >= 1);

                return x * Math.Sqrt(-2 * Math.Log(square) / square);
            }
        }
        internal static class RandomProvider
        {
            private static ThreadLocal<Random> randomWrapper = new ThreadLocal<Random>(() =>
                new Random(Interlocked.Increment(ref seed))
            );

            private static int seed = Environment.TickCount;

            public static Random GetThreadRandom()
            {
                return randomWrapper.Value;
            }
        }
        public double calldelta(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S + 0.0001, r, vol, t);
            double cp2 = CallPrice(S - 0.0001, r, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double callgamma(double S, double r, double vol, double t)
        {
            return (calldelta(S + 0.01, r, vol, t) - calldelta(S, r, vol, t)) / 0.01;
        }
        public double callvega(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r, vol + 0.0001, t);
            double cp2 = CallPrice(S, r, vol - 0.0001, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double calltheta(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r, vol, t + 0.0001);
            double cp2 = CallPrice(S, r, vol, t - 0.0001);
            return (cp1 - cp2) / 0.0002;
        }
        public double callrho(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r + 0.0001, vol, t);
            double cp2 = CallPrice(S, r - 0.0001, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double putdelta(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S + 0.0001, r, vol, t);
            double cp2 = PutPrice(S - 0.0001, r, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double putgamma(double S, double r, double vol, double t)
        {
            return (putdelta(S + 0.01, r, vol, t) - putdelta(S, r, vol, t)) / 0.01;
        }
        public double putvega(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r, vol + 0.0001, t);
            double cp2 = PutPrice(S, r, vol - 0.0001, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double puttheta(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r, vol, t + 0.0001);
            double cp2 = PutPrice(S, r, vol, t - 0.0001);
            return (cp1 - cp2) / 0.0002;
        }
        public double putrho(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r + 0.0001, vol, t);
            double cp2 = PutPrice(S, r - 0.0001, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
    }

    public class RangeAntiCV: PreparationAntiCV, CallPrice, StandardError, PutPrice, CallGreek, PutGreek
    {
        public List<double> PriceList;
        public List<double> PriceInverse;
        public static double[,] RandomList1;
        public static double[,] RandomList2;
        public void Initial()
        {
            optionprice = new double[Value.step + 1];
            optioninverse = new double[Value.step + 1];
            PriceList = new List<double>();
            PriceInverse = new List<double>();
        }
        public void RandomGenerated()
        {
            RandomList1 = new double[Value.trials, Value.step];
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                for (int k = 0; k < Value.step; k++)
                {
                    RandomList1[i, k] = GaussianBoxMuller.NextDouble();
                }
            }
        }
        public void RandomAntithetic()
        {
            RandomList2 = new double[Value.trials, Value.step];
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                for (int k = 0; k < Value.step; k++)
                {
                    RandomList2[i, k] = -RandomList1[i, k];
                }
            }
        }
        public double CallPrice(double S, double r, double vol, double t)
        {
            Initial();
            double tl1;
            double tl2;
            double d1;
            double d2;
            double delta1;
            double delta2;
            double deltaT = t / Convert.ToDouble(Value.step);
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                optionprice[0] = S;
                double cv = 0;
                for (int j = 1; j < Value.step + 1; j++)
                {
                    optionprice[j] = Price(optionprice[j - 1], r, vol, t, RandomList1[i, j - 1]);
                    tl1 = t - (j - 1) * deltaT;
                    d1 = (Math.Log(optionprice[j - 1] / Value.K) + (r + Math.Pow(vol, 2) / 2) * tl1) / (vol * Math.Sqrt(tl1));
                    delta1 = normalCDF(d1);
                    cv = cv + delta1 * (optionprice[j] - optionprice[j - 1] * Math.Exp(r * deltaT));
                }
                PriceList.Add(optionprice.Max() - optionprice.Min() - cv);
            }
            RandomAntithetic();
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                optioninverse[0] = S;
                double cv = 0;
                for (int j = 1; j < Value.step + 1; j++)
                {
                    optioninverse[j] = Price(optioninverse[j - 1], r, vol, t, RandomList2[i, j - 1]);
                    tl2 = t - (j - 1) * deltaT;
                    d2 = (Math.Log(optioninverse[j - 1] / Value.K) + (r + Math.Pow(vol, 2) / 2) * tl2) / (vol * Math.Sqrt(tl2));
                    delta2 = normalCDF(d2);
                    cv = cv + delta2 * (optioninverse[j] - optioninverse[j - 1] * Math.Exp(r * deltaT));
                }
                PriceInverse.Add(optioninverse.Max() - optioninverse.Min() - cv);
            }
            double price1 = 0;
            price1 = Math.Exp(-r * t) * PriceList.Average();
            double price2 = 0;
            price2 = Math.Exp(-r * t) * PriceInverse.Average();
            Program.increase(1);
            return (price1 + price2) / 2;
        }
        public double PutPrice(double S, double r, double vol, double t)
        {
            Initial();
            double tl1;
            double tl2;
            double d1;
            double d2;
            double delta1;
            double delta2;
            double deltaT = t / Convert.ToDouble(Value.step);
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                optionprice[0] = S;
                double cv = 0;
                for (int j = 1; j < Value.step + 1; j++)
                {
                    optionprice[j] = Price(optionprice[j - 1], r, vol, t, RandomList1[i, j - 1]);
                    tl1 = t - (j - 1) * deltaT;
                    d1 = (Math.Log(optionprice[j - 1] / Value.K) + (r + Math.Pow(vol, 2) / 2) * tl1) / (vol * Math.Sqrt(tl1));
                    delta1 = normalCDF(d1)-1;
                    cv = cv + delta1 * (optionprice[j] - optionprice[j - 1] * Math.Exp(r * deltaT));
                }
                PriceList.Add(optionprice.Max() - optionprice.Min() - cv);
            }
            RandomAntithetic();
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                optioninverse[0] = S;
                double cv = 0;
                for (int j = 1; j < Value.step + 1; j++)
                {
                    optioninverse[j] = Price(optioninverse[j - 1], r, vol, t, RandomList2[i, j - 1]);
                    tl2 = t - (j - 1) * deltaT;
                    d2 = (Math.Log(optioninverse[j - 1] / Value.K) + (r + Math.Pow(vol, 2) / 2) * tl2) / (vol * Math.Sqrt(tl2));
                    delta2 = normalCDF(d2)-1;
                    cv = cv + delta2 * (optioninverse[j] - optioninverse[j - 1] * Math.Exp(r * deltaT));
                }
                PriceInverse.Add(optioninverse.Max() - optioninverse.Min() - cv);
            }
            double price1 = 0;
            price1 = Math.Exp(-r * t) * PriceList.Average();
            double price2 = 0;
            price2 = Math.Exp(-r * t) * PriceInverse.Average();
            Program.increase(1);
            return (price1 + price2) / 2;
        }
        public double SE()
        {
            List<double> Option = new List<double>();
            for (UInt32 i = 0; i < PriceList.Count; i++)
            {
                Option.Add((PriceList[(int)i] + PriceInverse[(int)i]) / 2);
            }
            double avg = Option.Average();
            double sum = Option.Sum(v => Math.Pow(v - avg, 2));
            return (Math.Sqrt(sum / (Option.Count - 1))) / (Math.Sqrt(Option.Count));
        }
        public double normalCDF(double value)
        {
            const double A1 = 0.254829592;
            const double A2 = -0.284496736;
            const double A3 = 1.421413741;
            const double A4 = -1.453152027;
            const double A5 = 1.061405429;
            const double P = 0.3275911;

            int sign;
            if (value < 0)
                sign = -1;
            else
                sign = 1;

            double x = Math.Abs(value) / Math.Sqrt(2);
            double t = 1 / (1 + P * x);
            double erf = 1 - (((((A5 * t + A4) * t + A3) * t + A2) * t + A1) * t * Math.Exp(-x * x));


            return 0.5 * (1 + sign * erf);
        }
        internal static class GaussianBoxMuller
        {
            public static double NextDouble()
            {
                double x, y, square;

                do
                {
                    x = 2 * RandomProvider.GetThreadRandom().NextDouble() - 1;
                    y = 2 * RandomProvider.GetThreadRandom().NextDouble() - 1;
                    square = (x * x) + (y * y);
                } while (square >= 1);

                return x * Math.Sqrt(-2 * Math.Log(square) / square);
            }
        }
        internal static class RandomProvider
        {
            private static ThreadLocal<Random> randomWrapper = new ThreadLocal<Random>(() =>
                new Random(Interlocked.Increment(ref seed))
            );

            private static int seed = Environment.TickCount;

            public static Random GetThreadRandom()
            {
                return randomWrapper.Value;
            }
        }
        public double calldelta(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S + 0.0001, r, vol, t);
            double cp2 = CallPrice(S - 0.0001, r, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double callgamma(double S, double r, double vol, double t)
        {
            return (calldelta(S + 0.01, r, vol, t) - calldelta(S, r, vol, t)) / 0.01;
        }
        public double callvega(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r, vol + 0.0001, t);
            double cp2 = CallPrice(S, r, vol - 0.0001, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double calltheta(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r, vol, t + 0.0001);
            double cp2 = CallPrice(S, r, vol, t - 0.0001);
            return (cp1 - cp2) / 0.0002;
        }
        public double callrho(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r + 0.0001, vol, t);
            double cp2 = CallPrice(S, r - 0.0001, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double putdelta(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S + 0.0001, r, vol, t);
            double cp2 = PutPrice(S - 0.0001, r, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double putgamma(double S, double r, double vol, double t)
        {
            return (putdelta(S + 0.01, r, vol, t) - putdelta(S, r, vol, t)) / 0.01;
        }
        public double putvega(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r, vol + 0.0001, t);
            double cp2 = PutPrice(S, r, vol - 0.0001, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double puttheta(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r, vol, t + 0.0001);
            double cp2 = PutPrice(S, r, vol, t - 0.0001);
            return (cp1 - cp2) / 0.0002;
        }
        public double putrho(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r + 0.0001, vol, t);
            double cp2 = PutPrice(S, r - 0.0001, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
    }
}
