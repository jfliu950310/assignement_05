﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Diagnostics;

namespace ConsoleApp6
{
    public class AsianPresent
    {
        Stopwatch watch = new Stopwatch();

        public void AsianCall()
        {
            Asian euro = new Asian();
            watch.Start();
            euro.RandomGenerated();
            Value.Price = euro.CallPrice(Value.S, Value.r, Value.vol, Value.t);
            Value.SE = euro.SE();
            Value.delta = euro.calldelta(Value.S, Value.r, Value.vol, Value.t);
            Value.gamma = euro.callgamma(Value.S, Value.r, Value.vol, Value.t);
            Value.theta = euro.calltheta(Value.S, Value.r, Value.vol, Value.t);
            Value.vega = euro.callvega(Value.S, Value.r, Value.vol, Value.t);
            Value.rho = euro.callrho(Value.S, Value.r, Value.vol, Value.t);
            Value.Thread = 1;
            watch.Stop();
            Value.timer = watch.Elapsed.Seconds.ToString() + " : " + watch.Elapsed.Milliseconds.ToString();
            watch.Reset();
            Program.finished();
            GC.Collect();
        }

        public void AsianPut()
        {
            Asian euro = new Asian();
            watch.Start();
            euro.RandomGenerated();
            Value.Price = euro.PutPrice(Value.S, Value.r, Value.vol, Value.t);
            Value.SE = euro.SE();
            Value.delta = euro.putdelta(Value.S, Value.r, Value.vol, Value.t);
            Value.gamma = euro.putgamma(Value.S, Value.r, Value.vol, Value.t);
            Value.theta = euro.puttheta(Value.S, Value.r, Value.vol, Value.t);
            Value.vega = euro.putvega(Value.S, Value.r, Value.vol, Value.t);
            Value.rho = euro.putrho(Value.S, Value.r, Value.vol, Value.t);
            Value.Thread = 1;
            watch.Stop();
            Value.timer = watch.Elapsed.Seconds.ToString() + " : " + watch.Elapsed.Milliseconds.ToString();
            watch.Reset();
            Program.finished();
            GC.Collect();
        }

        public void AsianCallAnti()
        {
            AsianAntithetic euro = new AsianAntithetic();
            watch.Start();
            euro.RandomGenerated();
            Value.Price = euro.CallPrice(Value.S, Value.r, Value.vol, Value.t);
            Value.SE = euro.SE();
            Value.delta = euro.calldelta(Value.S, Value.r, Value.vol, Value.t);
            Value.gamma = euro.callgamma(Value.S, Value.r, Value.vol, Value.t);
            Value.theta = euro.calltheta(Value.S, Value.r, Value.vol, Value.t);
            Value.vega = euro.callvega(Value.S, Value.r, Value.vol, Value.t);
            Value.rho = euro.callrho(Value.S, Value.r, Value.vol, Value.t);
            Value.Thread = 1;
            watch.Stop();
            Value.timer = watch.Elapsed.Seconds.ToString() + " : " + watch.Elapsed.Milliseconds.ToString();
            watch.Reset();
            Program.finished();
            GC.Collect();
        }

        public void AsianPutAnti()
        {
            AsianAntithetic euro = new AsianAntithetic();
            watch.Start();
            euro.RandomGenerated();
            Value.Price = euro.PutPrice(Value.S, Value.r, Value.vol, Value.t);
            Value.SE = euro.SE();
            Value.delta = euro.putdelta(Value.S, Value.r, Value.vol, Value.t);
            Value.gamma = euro.putgamma(Value.S, Value.r, Value.vol, Value.t);
            Value.theta = euro.puttheta(Value.S, Value.r, Value.vol, Value.t);
            Value.vega = euro.putvega(Value.S, Value.r, Value.vol, Value.t);
            Value.rho = euro.putrho(Value.S, Value.r, Value.vol, Value.t);
            Value.Thread = 1;
            watch.Stop();
            Value.timer = watch.Elapsed.Seconds.ToString() + " : " + watch.Elapsed.Milliseconds.ToString();
            watch.Reset();
            Program.finished();
            GC.Collect();
        }

        public void AsianCallCV()
        {
            AsianCV euro = new AsianCV();
            watch.Start();
            euro.RandomGenerated();
            Value.Price = euro.CallPrice(Value.S, Value.r, Value.vol, Value.t);
            Value.SE = euro.SE();
            Value.delta = euro.calldelta(Value.S, Value.r, Value.vol, Value.t);
            Value.gamma = euro.callgamma(Value.S, Value.r, Value.vol, Value.t);
            Value.theta = euro.calltheta(Value.S, Value.r, Value.vol, Value.t);
            Value.vega = euro.callvega(Value.S, Value.r, Value.vol, Value.t);
            Value.rho = euro.callrho(Value.S, Value.r, Value.vol, Value.t);
            Value.Thread = 1;
            watch.Stop();
            Value.timer = watch.Elapsed.Seconds.ToString() + " : " + watch.Elapsed.Milliseconds.ToString();
            watch.Reset();
            Program.finished();
            GC.Collect();
        }

        public void AsianPutCV()
        {
            AsianCV euro = new AsianCV();
            watch.Start();
            euro.RandomGenerated();
            Value.Price = euro.PutPrice(Value.S, Value.r, Value.vol, Value.t);
            Value.SE = euro.SE();
            Value.delta = euro.putdelta(Value.S, Value.r, Value.vol, Value.t);
            Value.gamma = euro.putgamma(Value.S, Value.r, Value.vol, Value.t);
            Value.theta = euro.puttheta(Value.S, Value.r, Value.vol, Value.t);
            Value.vega = euro.putvega(Value.S, Value.r, Value.vol, Value.t);
            Value.rho = euro.putrho(Value.S, Value.r, Value.vol, Value.t);
            Value.Thread = 1;
            watch.Stop();
            Value.timer = watch.Elapsed.Seconds.ToString() + " : " + watch.Elapsed.Milliseconds.ToString();
            watch.Reset();
            Program.finished();
            GC.Collect();
        }

        public void AsianCallAntiCV()
        {
            AsianAntiCV euro = new AsianAntiCV();
            watch.Start();
            euro.RandomGenerated();
            Value.Price = euro.CallPrice(Value.S, Value.r, Value.vol, Value.t);
            Value.SE = euro.SE();
            Value.delta = euro.calldelta(Value.S, Value.r, Value.vol, Value.t);
            Value.gamma = euro.callgamma(Value.S, Value.r, Value.vol, Value.t);
            Value.theta = euro.calltheta(Value.S, Value.r, Value.vol, Value.t);
            Value.vega = euro.callvega(Value.S, Value.r, Value.vol, Value.t);
            Value.rho = euro.callrho(Value.S, Value.r, Value.vol, Value.t);
            Value.Thread = 1;
            watch.Stop();
            Value.timer = watch.Elapsed.Seconds.ToString() + " : " + watch.Elapsed.Milliseconds.ToString();
            watch.Reset();
            Program.finished();
            GC.Collect();
        }

        public void AsianPutAntiCV()
        {
            AsianAntiCV euro = new AsianAntiCV();
            watch.Start();
            euro.RandomGenerated();
            Value.Price = euro.PutPrice(Value.S, Value.r, Value.vol, Value.t);
            Value.SE = euro.SE();
            Value.delta = euro.putdelta(Value.S, Value.r, Value.vol, Value.t);
            Value.gamma = euro.putgamma(Value.S, Value.r, Value.vol, Value.t);
            Value.theta = euro.puttheta(Value.S, Value.r, Value.vol, Value.t);
            Value.vega = euro.putvega(Value.S, Value.r, Value.vol, Value.t);
            Value.rho = euro.putrho(Value.S, Value.r, Value.vol, Value.t);
            Value.Thread = 1;
            watch.Stop();
            Value.timer = watch.Elapsed.Seconds.ToString() + " : " + watch.Elapsed.Milliseconds.ToString();
            watch.Reset();
            Program.finished();
            GC.Collect();
        }

        public void AsianCallMT()
        {
            AsianMT euro = new AsianMT();
            watch.Start();
            euro.RandomGenerated();
            Value.Price = euro.CallPrice(Value.S, Value.r, Value.vol, Value.t);
            Value.SE = euro.SE();
            Value.delta = euro.calldelta(Value.S, Value.r, Value.vol, Value.t);
            Value.gamma = euro.callgamma(Value.S, Value.r, Value.vol, Value.t);
            Value.theta = euro.calltheta(Value.S, Value.r, Value.vol, Value.t);
            Value.vega = euro.callvega(Value.S, Value.r, Value.vol, Value.t);
            Value.rho = euro.callrho(Value.S, Value.r, Value.vol, Value.t);
            Value.Thread = 4;
            watch.Stop();
            Value.timer = watch.Elapsed.Seconds.ToString() + " : " + watch.Elapsed.Milliseconds.ToString();
            watch.Reset();
            Program.finished();
            GC.Collect();
        }

        public void AsianPutMT()
        {
            AsianMT euro = new AsianMT();
            watch.Start();
            euro.RandomGenerated();
            Value.Price = euro.PutPrice(Value.S, Value.r, Value.vol, Value.t);
            Value.SE = euro.SE();
            Value.delta = euro.putdelta(Value.S, Value.r, Value.vol, Value.t);
            Value.gamma = euro.putgamma(Value.S, Value.r, Value.vol, Value.t);
            Value.theta = euro.puttheta(Value.S, Value.r, Value.vol, Value.t);
            Value.vega = euro.putvega(Value.S, Value.r, Value.vol, Value.t);
            Value.rho = euro.putrho(Value.S, Value.r, Value.vol, Value.t);
            Value.Thread = 4;
            watch.Stop();
            Value.timer = watch.Elapsed.Seconds.ToString() + " : " + watch.Elapsed.Milliseconds.ToString();
            watch.Reset();
            Program.finished();
            GC.Collect();
        }

        public void AsianCallAntiMT()
        {
            AsianAntiMT euro = new AsianAntiMT();
            watch.Start();
            euro.RandomGenerated();
            Value.Price = euro.CallPrice(Value.S, Value.r, Value.vol, Value.t);
            Value.SE = euro.SE();
            Value.delta = euro.calldelta(Value.S, Value.r, Value.vol, Value.t);
            Value.gamma = euro.callgamma(Value.S, Value.r, Value.vol, Value.t);
            Value.theta = euro.calltheta(Value.S, Value.r, Value.vol, Value.t);
            Value.vega = euro.callvega(Value.S, Value.r, Value.vol, Value.t);
            Value.rho = euro.callrho(Value.S, Value.r, Value.vol, Value.t);
            Value.Thread = 4;
            watch.Stop();
            Value.timer = watch.Elapsed.Seconds.ToString() + " : " + watch.Elapsed.Milliseconds.ToString();
            watch.Reset();
            Program.finished();
            GC.Collect();
        }

        public void AsianPutAntiMT()
        {
            AsianAntiMT euro = new AsianAntiMT();
            watch.Start();
            euro.RandomGenerated();
            Value.Price = euro.PutPrice(Value.S, Value.r, Value.vol, Value.t);
            Value.SE = euro.SE();
            Value.delta = euro.putdelta(Value.S, Value.r, Value.vol, Value.t);
            Value.gamma = euro.putgamma(Value.S, Value.r, Value.vol, Value.t);
            Value.theta = euro.puttheta(Value.S, Value.r, Value.vol, Value.t);
            Value.vega = euro.putvega(Value.S, Value.r, Value.vol, Value.t);
            Value.rho = euro.putrho(Value.S, Value.r, Value.vol, Value.t);
            Value.Thread = 4;
            watch.Stop();
            Value.timer = watch.Elapsed.Seconds.ToString() + " : " + watch.Elapsed.Milliseconds.ToString();
            watch.Reset();
            Program.finished();
            GC.Collect();
        }

        public void AsianCallCVMT()
        {
            AsianCVMT euro = new AsianCVMT();
            watch.Start();
            euro.RandomGenerated();
            Value.Price = euro.CallPrice(Value.S, Value.r, Value.vol, Value.t);
            Value.SE = euro.SE();
            Value.delta = euro.calldelta(Value.S, Value.r, Value.vol, Value.t);
            Value.gamma = euro.callgamma(Value.S, Value.r, Value.vol, Value.t);
            Value.theta = euro.calltheta(Value.S, Value.r, Value.vol, Value.t);
            Value.vega = euro.callvega(Value.S, Value.r, Value.vol, Value.t);
            Value.rho = euro.callrho(Value.S, Value.r, Value.vol, Value.t);
            Value.Thread = 4;
            watch.Stop();
            Value.timer = watch.Elapsed.Seconds.ToString() + " : " + watch.Elapsed.Milliseconds.ToString();
            watch.Reset();
            Program.finished();
            GC.Collect();
        }

        public void AsianPutCVMT()
        {
            AsianCVMT euro = new AsianCVMT();
            watch.Start();
            euro.RandomGenerated();
            Value.Price = euro.PutPrice(Value.S, Value.r, Value.vol, Value.t);
            Value.SE = euro.SE();
            Value.delta = euro.putdelta(Value.S, Value.r, Value.vol, Value.t);
            Value.gamma = euro.putgamma(Value.S, Value.r, Value.vol, Value.t);
            Value.theta = euro.puttheta(Value.S, Value.r, Value.vol, Value.t);
            Value.vega = euro.putvega(Value.S, Value.r, Value.vol, Value.t);
            Value.rho = euro.putrho(Value.S, Value.r, Value.vol, Value.t);
            Value.Thread = 4;
            watch.Stop();
            Value.timer = watch.Elapsed.Seconds.ToString() + " : " + watch.Elapsed.Milliseconds.ToString();
            watch.Reset();
            Program.finished();
            GC.Collect();
        }

        public void AsianCallAntiCVMT()
        {
            AsianAntiCVMT euro = new AsianAntiCVMT();
            watch.Start();
            euro.RandomGenerated();
            Value.Price = euro.CallPrice(Value.S, Value.r, Value.vol, Value.t);
            Value.SE = euro.SE();
            Value.delta = euro.calldelta(Value.S, Value.r, Value.vol, Value.t);
            Value.gamma = euro.callgamma(Value.S, Value.r, Value.vol, Value.t);
            Value.theta = euro.calltheta(Value.S, Value.r, Value.vol, Value.t);
            Value.vega = euro.callvega(Value.S, Value.r, Value.vol, Value.t);
            Value.rho = euro.callrho(Value.S, Value.r, Value.vol, Value.t);
            Value.Thread = 4;
            watch.Stop();
            Value.timer = watch.Elapsed.Seconds.ToString() + " : " + watch.Elapsed.Milliseconds.ToString();
            watch.Reset();
            Program.finished();
            GC.Collect();
        }

        public void AsianPutAntiCVMT()
        {
            AsianAntiCVMT euro = new AsianAntiCVMT();
            watch.Start();
            euro.RandomGenerated();
            Value.Price = euro.PutPrice(Value.S, Value.r, Value.vol, Value.t);
            Value.SE = euro.SE();
            Value.delta = euro.putdelta(Value.S, Value.r, Value.vol, Value.t);
            Value.gamma = euro.putgamma(Value.S, Value.r, Value.vol, Value.t);
            Value.theta = euro.puttheta(Value.S, Value.r, Value.vol, Value.t);
            Value.vega = euro.putvega(Value.S, Value.r, Value.vol, Value.t);
            Value.rho = euro.putrho(Value.S, Value.r, Value.vol, Value.t);
            Value.Thread = 4;
            watch.Stop();
            Value.timer = watch.Elapsed.Seconds.ToString() + " : " + watch.Elapsed.Milliseconds.ToString();
            watch.Reset();
            Program.finished();
            GC.Collect();
        }

    }
}
