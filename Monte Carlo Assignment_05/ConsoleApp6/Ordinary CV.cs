﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ConsoleApp6
{
    public class PreparationCV
    {
        public double[] optionprice { get; set; }
        public double Price(double S, double r, double vol, double t, double ep)
        {
            double price;
            double deltaT = t / Convert.ToDouble(Value.step);
            price = S * Math.Exp((r - Math.Pow(vol, 2) / 2) * deltaT + vol * Math.Sqrt(deltaT) * ep);
            return price;
        }
    }

    public class EuropeanCV : PreparationCV, CallPrice, StandardError, PutPrice, CallGreek, PutGreek
    {
        public List<double> PriceList;
        public static double[,] RandomList;
        public void Initial()
        {
            optionprice = new double[Value.step + 1];
            PriceList = new List<double>();
        }
        public void RandomGenerated()
        {
            RandomList = new double[Value.trials, Value.step];
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                for (int k = 0; k < Value.step; k++)
                {
                    RandomList[i, k] = GaussianBoxMuller.NextDouble();
                }
            }
        }
        public double CallPrice(double S, double r, double vol, double t)
        {
            Initial();
            double tl;
            double d1;
            double delta;
            double deltaT = t / Convert.ToDouble(Value.step);
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                optionprice[0] = S;
                double cv = 0;
                for (int j = 1; j < Value.step + 1; j++)
                {
                    optionprice[j] = Price(optionprice[j - 1], r, vol, t, RandomList[i, j - 1]);
                    tl = t - (j - 1) * deltaT;
                    d1 = (Math.Log(optionprice[j - 1] / Value.K) + (r + Math.Pow(vol, 2) / 2) * tl) / (vol * Math.Sqrt(tl));
                    delta = normalCDF(d1);
                    cv = cv + delta * (optionprice[j] - optionprice[j - 1] * Math.Exp(r * deltaT));
                }
                PriceList.Add(Math.Max(optionprice.Last() - Value.K , 0) - cv);
            }
            double price = 0;
            price = Math.Exp(-r * t) * PriceList.Average();
            Program.increase(1);
            return price;
        }
        public double PutPrice(double S, double r, double vol, double t)
        {
            Initial();
            double tl;
            double d1;
            double delta;
            double deltaT = t / Convert.ToDouble(Value.step);
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                optionprice[0] = S;
                double cv = 0;
                for (int j = 1; j < Value.step + 1; j++)
                {
                    optionprice[j] = Price(optionprice[j - 1], r, vol, t, RandomList[i, j - 1]);
                    tl = t - (j - 1) * deltaT;
                    d1 = (Math.Log(optionprice[j - 1] / Value.K) + (r + Math.Pow(vol, 2) / 2) * tl) / (vol * Math.Sqrt(tl));
                    delta = normalCDF(d1)-1;
                    cv = cv + delta * (optionprice[j] - optionprice[j - 1] * Math.Exp(r * deltaT));
                }
                PriceList.Add(Math.Max(Value.K - optionprice.Last(), 0) - cv);
            }
            double price = 0;
            price = Math.Exp(-r * t) * PriceList.Average();
            Program.increase(1);
            return price;
        }
        public double SE()
        {
            double avg = PriceList.Average();
            double sum = PriceList.Sum(v => Math.Pow(v - avg, 2));
            return (Math.Sqrt(sum / (PriceList.Count - 1))) / (Math.Sqrt(PriceList.Count));
        }
        public double normalCDF(double value)
        {
            const double A1 = 0.254829592;
            const double A2 = -0.284496736;
            const double A3 = 1.421413741;
            const double A4 = -1.453152027;
            const double A5 = 1.061405429;
            const double P = 0.3275911;

            int sign;
            if (value < 0)
                sign = -1;
            else
                sign = 1;

            double x = Math.Abs(value) / Math.Sqrt(2);
            double t = 1 / (1 + P * x);
            double erf = 1 - (((((A5 * t + A4) * t + A3) * t + A2) * t + A1) * t * Math.Exp(-x * x));


            return 0.5 * (1 + sign * erf);
        }
        internal static class GaussianBoxMuller
        {
            public static double NextDouble()
            {
                double x, y, square;

                do
                {
                    x = 2 * RandomProvider.GetThreadRandom().NextDouble() - 1;
                    y = 2 * RandomProvider.GetThreadRandom().NextDouble() - 1;
                    square = (x * x) + (y * y);
                } while (square >= 1);

                return x * Math.Sqrt(-2 * Math.Log(square) / square);
            }
        }
        internal static class RandomProvider
        {
            private static ThreadLocal<Random> randomWrapper = new ThreadLocal<Random>(() =>
                new Random(Interlocked.Increment(ref seed))
            );

            private static int seed = Environment.TickCount * 10;

            public static Random GetThreadRandom()
            {
                return randomWrapper.Value;
            }
        }
        public double calldelta(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S + 0.0001, r, vol, t);
            double cp2 = CallPrice(S - 0.0001, r, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double callgamma(double S, double r, double vol, double t)
        {
            return (calldelta(S + 0.01, r, vol, t) - calldelta(S, r, vol, t)) / 0.01;
        }
        public double callvega(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r, vol + 0.0001, t);
            double cp2 = CallPrice(S, r, vol - 0.0001, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double calltheta(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r, vol, t + 0.0001);
            double cp2 = CallPrice(S, r, vol, t - 0.0001);
            return (cp1 - cp2) / 0.0002;
        }
        public double callrho(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r + 0.0001, vol, t);
            double cp2 = CallPrice(S, r - 0.0001, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double putdelta(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S + 0.0001, r, vol, t);
            double cp2 = PutPrice(S - 0.0001, r, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double putgamma(double S, double r, double vol, double t)
        {
            return (putdelta(S + 0.01, r, vol, t) - putdelta(S, r, vol, t)) / 0.01;
        }
        public double putvega(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r, vol + 0.0001, t);
            double cp2 = PutPrice(S, r, vol - 0.0001, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double puttheta(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r, vol, t + 0.0001);
            double cp2 = PutPrice(S, r, vol, t - 0.0001);
            return (cp1 - cp2) / 0.0002;
        }
        public double putrho(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r + 0.0001, vol, t);
            double cp2 = PutPrice(S, r - 0.0001, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
    }

    public class AsianCV : PreparationCV, CallPrice, StandardError, PutPrice, CallGreek, PutGreek
    {
        public List<double> PriceList;
        public static double[,] RandomList;
        public void Initial()
        {
            optionprice = new double[Value.step + 1];
            PriceList = new List<double>();
        }
        public void RandomGenerated()
        {
            RandomList = new double[Value.trials, Value.step];
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                for (int k = 0; k < Value.step; k++)
                {
                    RandomList[i, k] = GaussianBoxMuller.NextDouble();
                }
            }
        }
        public double CallPrice(double S, double r, double vol, double t)
        {
            Initial();
            double tl;
            double d1;
            double delta;
            double deltaT = t / Convert.ToDouble(Value.step);
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                optionprice[0] = S;
                double cv = 0;
                for (int j = 1; j < Value.step + 1; j++)
                {
                    optionprice[j] = Price(optionprice[j - 1], r, vol, t, RandomList[i, j - 1]);
                    tl = t - (j - 1) * deltaT;
                    d1 = (Math.Log(optionprice[j - 1] / Value.K) + (r + Math.Pow(vol, 2) / 2) * tl) / (vol * Math.Sqrt(tl));
                    delta = normalCDF(d1);
                    cv = cv + delta * (optionprice[j] - optionprice[j - 1] * Math.Exp(r * deltaT));
                }
                PriceList.Add(Math.Max((optionprice.Average() * (Value.step + 1) - optionprice[0]) / (Value.step) - Value.K, 0) - cv);
            }
            double price = 0;
            price = Math.Exp(-r * t) * PriceList.Average();
            Program.increase(1);
            return price;
        }
        public double PutPrice(double S, double r, double vol, double t)
        {
            Initial();
            double tl;
            double d1;
            double delta;
            double deltaT = t / Convert.ToDouble(Value.step);
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                optionprice[0] = S;
                double cv = 0;
                for (int j = 1; j < Value.step + 1; j++)
                {
                    optionprice[j] = Price(optionprice[j - 1], r, vol, t, RandomList[i, j - 1]);
                    tl = t - (j - 1) * deltaT;
                    d1 = (Math.Log(optionprice[j - 1] / Value.K) + (r + Math.Pow(vol, 2) / 2) * tl) / (vol * Math.Sqrt(tl));
                    delta = normalCDF(d1)-1;
                    cv = cv + delta * (optionprice[j] - optionprice[j - 1] * Math.Exp(r * deltaT));
                }
                PriceList.Add(Math.Max(Value.K - (optionprice.Average() * (Value.step + 1) - optionprice[0]) / (Value.step), 0) - cv);
            }
            double price = 0;
            price = Math.Exp(-r * t) * PriceList.Average();
            Program.increase(1);
            return price;
        }
        public double SE()
        {
            double avg = PriceList.Average();
            double sum = PriceList.Sum(v => Math.Pow(v - avg, 2));
            return (Math.Sqrt(sum / (PriceList.Count - 1))) / (Math.Sqrt(PriceList.Count));
        }
        public double normalCDF(double value)
        {
            const double A1 = 0.254829592;
            const double A2 = -0.284496736;
            const double A3 = 1.421413741;
            const double A4 = -1.453152027;
            const double A5 = 1.061405429;
            const double P = 0.3275911;

            int sign;
            if (value < 0)
                sign = -1;
            else
                sign = 1;

            double x = Math.Abs(value) / Math.Sqrt(2);
            double t = 1 / (1 + P * x);
            double erf = 1 - (((((A5 * t + A4) * t + A3) * t + A2) * t + A1) * t * Math.Exp(-x * x));


            return 0.5 * (1 + sign * erf);
        }
        internal static class GaussianBoxMuller
        {
            public static double NextDouble()
            {
                double x, y, square;

                do
                {
                    x = 2 * RandomProvider.GetThreadRandom().NextDouble() - 1;
                    y = 2 * RandomProvider.GetThreadRandom().NextDouble() - 1;
                    square = (x * x) + (y * y);
                } while (square >= 1);

                return x * Math.Sqrt(-2 * Math.Log(square) / square);
            }
        }
        internal static class RandomProvider
        {
            private static ThreadLocal<Random> randomWrapper = new ThreadLocal<Random>(() =>
                new Random(Interlocked.Increment(ref seed))
            );

            private static int seed = Environment.TickCount * 10;

            public static Random GetThreadRandom()
            {
                return randomWrapper.Value;
            }
        }
        public double calldelta(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S + 0.0001, r, vol, t);
            double cp2 = CallPrice(S - 0.0001, r, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double callgamma(double S, double r, double vol, double t)
        {
            return (calldelta(S + 0.01, r, vol, t) - calldelta(S, r, vol, t)) / 0.01;
        }
        public double callvega(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r, vol + 0.0001, t);
            double cp2 = CallPrice(S, r, vol - 0.0001, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double calltheta(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r, vol, t + 0.0001);
            double cp2 = CallPrice(S, r, vol, t - 0.0001);
            return (cp1 - cp2) / 0.0002;
        }
        public double callrho(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r + 0.0001, vol, t);
            double cp2 = CallPrice(S, r - 0.0001, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double putdelta(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S + 0.0001, r, vol, t);
            double cp2 = PutPrice(S - 0.0001, r, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double putgamma(double S, double r, double vol, double t)
        {
            return (putdelta(S + 0.01, r, vol, t) - putdelta(S, r, vol, t)) / 0.01;
        }
        public double putvega(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r, vol + 0.0001, t);
            double cp2 = PutPrice(S, r, vol - 0.0001, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double puttheta(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r, vol, t + 0.0001);
            double cp2 = PutPrice(S, r, vol, t - 0.0001);
            return (cp1 - cp2) / 0.0002;
        }
        public double putrho(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r + 0.0001, vol, t);
            double cp2 = PutPrice(S, r - 0.0001, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
    }

    public class DigitalCV: PreparationCV, CallPrice, StandardError, PutPrice, CallGreek, PutGreek
    {
        public List<double> PriceList;
        public static double[,] RandomList;
        public void Initial()
        {
            optionprice = new double[Value.step + 1];
            PriceList = new List<double>();
        }
        public void RandomGenerated()
        {
            RandomList = new double[Value.trials, Value.step];
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                for (int k = 0; k < Value.step; k++)
                {
                    RandomList[i, k] = GaussianBoxMuller.NextDouble();
                }
            }
        }
        public double CallPrice(double S, double r, double vol, double t)
        {
            Initial();
            double tl;
            double d1;
            double delta;
            double deltaT = t / Convert.ToDouble(Value.step);
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                optionprice[0] = S;
                double cv = 0;
                for (int j = 1; j < Value.step + 1; j++)
                {
                    optionprice[j] = Price(optionprice[j - 1], r, vol, t, RandomList[i, j - 1]);
                    tl = t - (j - 1) * deltaT;
                    d1 = (Math.Log(optionprice[j - 1] / Value.K) + (r + Math.Pow(vol, 2) / 2) * tl) / (vol * Math.Sqrt(tl));
                    delta = normalCDF(d1);
                    cv = cv + delta * (optionprice[j] - optionprice[j - 1] * Math.Exp(r * deltaT));
                }
                if (optionprice.Last() >= Value.K)
                { PriceList.Add(Value.rebate - cv); }
                else
                { PriceList.Add(0 -cv); }
            }
            double price = 0;
            price = Math.Exp(-r * t) * PriceList.Average();
            Program.increase(1);
            return price;
        }
        public double PutPrice(double S, double r, double vol, double t)
        {
            Initial();
            double tl;
            double d1;
            double delta;
            double deltaT = t / Convert.ToDouble(Value.step);
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                optionprice[0] = S;
                double cv = 0;
                for (int j = 1; j < Value.step + 1; j++)
                {
                    optionprice[j] = Price(optionprice[j - 1], r, vol, t, RandomList[i, j - 1]);
                    tl = t - (j - 1) * deltaT;
                    d1 = (Math.Log(optionprice[j - 1] / Value.K) + (r + Math.Pow(vol, 2) / 2) * tl) / (vol * Math.Sqrt(tl));
                    delta = normalCDF(d1)-1;
                    cv = cv + delta * (optionprice[j] - optionprice[j - 1] * Math.Exp(r * deltaT));
                }
                if (optionprice.Last() <= Value.K)
                { PriceList.Add(Value.rebate - cv); }
                else
                { PriceList.Add(0 - cv); }
            }
            double price = 0;
            price = Math.Exp(-r * t) * PriceList.Average();
            Program.increase(1);
            return price;
        }
        public double SE()
        {
            double avg = PriceList.Average();
            double sum = PriceList.Sum(v => Math.Pow(v - avg, 2));
            return (Math.Sqrt(sum / (PriceList.Count - 1))) / (Math.Sqrt(PriceList.Count));
        }
        public double normalCDF(double value)
        {
            const double A1 = 0.254829592;
            const double A2 = -0.284496736;
            const double A3 = 1.421413741;
            const double A4 = -1.453152027;
            const double A5 = 1.061405429;
            const double P = 0.3275911;

            int sign;
            if (value < 0)
                sign = -1;
            else
                sign = 1;

            double x = Math.Abs(value) / Math.Sqrt(2);
            double t = 1 / (1 + P * x);
            double erf = 1 - (((((A5 * t + A4) * t + A3) * t + A2) * t + A1) * t * Math.Exp(-x * x));


            return 0.5 * (1 + sign * erf);
        }
        internal static class GaussianBoxMuller
        {
            public static double NextDouble()
            {
                double x, y, square;

                do
                {
                    x = 2 * RandomProvider.GetThreadRandom().NextDouble() - 1;
                    y = 2 * RandomProvider.GetThreadRandom().NextDouble() - 1;
                    square = (x * x) + (y * y);
                } while (square >= 1);

                return x * Math.Sqrt(-2 * Math.Log(square) / square);
            }
        }
        internal static class RandomProvider
        {
            private static ThreadLocal<Random> randomWrapper = new ThreadLocal<Random>(() =>
                new Random(Interlocked.Increment(ref seed))
            );

            private static int seed = Environment.TickCount * 10;

            public static Random GetThreadRandom()
            {
                return randomWrapper.Value;
            }
        }
        public double calldelta(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S + 0.0001, r, vol, t);
            double cp2 = CallPrice(S - 0.0001, r, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double callgamma(double S, double r, double vol, double t)
        {
            return (calldelta(S + 0.01, r, vol, t) - calldelta(S, r, vol, t)) / 0.01;
        }
        public double callvega(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r, vol + 0.0001, t);
            double cp2 = CallPrice(S, r, vol - 0.0001, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double calltheta(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r, vol, t + 0.0001);
            double cp2 = CallPrice(S, r, vol, t - 0.0001);
            return (cp1 - cp2) / 0.0002;
        }
        public double callrho(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r + 0.0001, vol, t);
            double cp2 = CallPrice(S, r - 0.0001, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double putdelta(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S + 0.0001, r, vol, t);
            double cp2 = PutPrice(S - 0.0001, r, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double putgamma(double S, double r, double vol, double t)
        {
            return (putdelta(S + 0.01, r, vol, t) - putdelta(S, r, vol, t)) / 0.01;
        }
        public double putvega(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r, vol + 0.0001, t);
            double cp2 = PutPrice(S, r, vol - 0.0001, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double puttheta(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r, vol, t + 0.0001);
            double cp2 = PutPrice(S, r, vol, t - 0.0001);
            return (cp1 - cp2) / 0.0002;
        }
        public double putrho(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r + 0.0001, vol, t);
            double cp2 = PutPrice(S, r - 0.0001, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
    }

    public class BarrierDAOCV:PreparationCV, CallPrice, StandardError, PutPrice, CallGreek, PutGreek
    {
        public List<double> PriceList;
        public static double[,] RandomList;
        public void Initial()
        {
            optionprice = new double[Value.step + 1];
            PriceList = new List<double>();
        }
        public void RandomGenerated()
        {
            RandomList = new double[Value.trials, Value.step];
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                for (int k = 0; k < Value.step; k++)
                {
                    RandomList[i, k] = GaussianBoxMuller.NextDouble();
                }
            }
        }
        public double CallPrice(double S, double r, double vol, double t)
        {
            Initial();
            double tl;
            double d1;
            double delta;
            double deltaT = t / Convert.ToDouble(Value.step);
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                optionprice[0] = S;
                double cv = 0;
                for (int j = 1; j < Value.step + 1; j++)
                {
                    optionprice[j] = Price(optionprice[j - 1], r, vol, t, RandomList[i, j - 1]);
                    tl = t - (j - 1) * deltaT;
                    d1 = (Math.Log(optionprice[j - 1] / Value.K) + (r + Math.Pow(vol, 2) / 2) * tl) / (vol * Math.Sqrt(tl));
                    delta = normalCDF(d1);
                    cv = cv + delta * (optionprice[j] - optionprice[j - 1] * Math.Exp(r * deltaT));
                }
                if (optionprice.Min() >= Value.barrier)
                { PriceList.Add(Math.Max(optionprice.Last() - Value.K, 0) - cv); }
                else { PriceList.Add(0); }
            }
            double price = 0;
            price = Math.Exp(-r * t) * PriceList.Average();
            Program.increase(1);
            return price;
        }
        public double PutPrice(double S, double r, double vol, double t)
        {
            Initial();
            double tl;
            double d1;
            double delta;
            double deltaT = t / Convert.ToDouble(Value.step);
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                optionprice[0] = S;
                double cv = 0;
                for (int j = 1; j < Value.step + 1; j++)
                {
                    optionprice[j] = Price(optionprice[j - 1], r, vol, t, RandomList[i, j - 1]);
                    tl = t - (j - 1) * deltaT;
                    d1 = (Math.Log(optionprice[j - 1] / Value.K) + (r + Math.Pow(vol, 2) / 2) * tl) / (vol * Math.Sqrt(tl));
                    delta = normalCDF(d1)-1;
                    cv = cv + delta * (optionprice[j] - optionprice[j - 1] * Math.Exp(r * deltaT));
                }
                if (optionprice.Min() >= Value.barrier)
                { PriceList.Add(Math.Max(Value.K - optionprice.Last(), 0)-cv); }
                else { PriceList.Add(0); }
            }
            double price = 0;
            price = Math.Exp(-r * t) * PriceList.Average();
            Program.increase(1);
            return price;
        }
        public double SE()
        {
            double avg = PriceList.Average();
            double sum = PriceList.Sum(v => Math.Pow(v - avg, 2));
            return (Math.Sqrt(sum / (PriceList.Count - 1))) / (Math.Sqrt(PriceList.Count));
        }
        public double normalCDF(double value)
        {
            const double A1 = 0.254829592;
            const double A2 = -0.284496736;
            const double A3 = 1.421413741;
            const double A4 = -1.453152027;
            const double A5 = 1.061405429;
            const double P = 0.3275911;

            int sign;
            if (value < 0)
                sign = -1;
            else
                sign = 1;

            double x = Math.Abs(value) / Math.Sqrt(2);
            double t = 1 / (1 + P * x);
            double erf = 1 - (((((A5 * t + A4) * t + A3) * t + A2) * t + A1) * t * Math.Exp(-x * x));


            return 0.5 * (1 + sign * erf);
        }
        internal static class GaussianBoxMuller
        {
            public static double NextDouble()
            {
                double x, y, square;

                do
                {
                    x = 2 * RandomProvider.GetThreadRandom().NextDouble() - 1;
                    y = 2 * RandomProvider.GetThreadRandom().NextDouble() - 1;
                    square = (x * x) + (y * y);
                } while (square >= 1);

                return x * Math.Sqrt(-2 * Math.Log(square) / square);
            }
        }
        internal static class RandomProvider
        {
            private static ThreadLocal<Random> randomWrapper = new ThreadLocal<Random>(() =>
                new Random(Interlocked.Increment(ref seed))
            );

            private static int seed = Environment.TickCount * 10;

            public static Random GetThreadRandom()
            {
                return randomWrapper.Value;
            }
        }
        public double calldelta(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S + 0.0001, r, vol, t);
            double cp2 = CallPrice(S - 0.0001, r, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double callgamma(double S, double r, double vol, double t)
        {
            return (calldelta(S + 0.01, r, vol, t) - calldelta(S, r, vol, t)) / 0.01;
        }
        public double callvega(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r, vol + 0.0001, t);
            double cp2 = CallPrice(S, r, vol - 0.0001, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double calltheta(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r, vol, t + 0.0001);
            double cp2 = CallPrice(S, r, vol, t - 0.0001);
            return (cp1 - cp2) / 0.0002;
        }
        public double callrho(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r + 0.0001, vol, t);
            double cp2 = CallPrice(S, r - 0.0001, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double putdelta(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S + 0.0001, r, vol, t);
            double cp2 = PutPrice(S - 0.0001, r, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double putgamma(double S, double r, double vol, double t)
        {
            return (putdelta(S + 0.01, r, vol, t) - putdelta(S, r, vol, t)) / 0.01;
        }
        public double putvega(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r, vol + 0.0001, t);
            double cp2 = PutPrice(S, r, vol - 0.0001, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double puttheta(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r, vol, t + 0.0001);
            double cp2 = PutPrice(S, r, vol, t - 0.0001);
            return (cp1 - cp2) / 0.0002;
        }
        public double putrho(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r + 0.0001, vol, t);
            double cp2 = PutPrice(S, r - 0.0001, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
    }

    public class BarrierDAICV : PreparationCV, CallPrice, StandardError, PutPrice, CallGreek, PutGreek
    {
        public List<double> PriceList;
        public static double[,] RandomList;
        public void Initial()
        {
            optionprice = new double[Value.step + 1];
            PriceList = new List<double>();
        }
        public void RandomGenerated()
        {
            RandomList = new double[Value.trials, Value.step];
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                for (int k = 0; k < Value.step; k++)
                {
                    RandomList[i, k] = GaussianBoxMuller.NextDouble();
                }
            }
        }
        public double CallPrice(double S, double r, double vol, double t)
        {
            Initial();
            double tl;
            double d1;
            double delta;
            double deltaT = t / Convert.ToDouble(Value.step);
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                optionprice[0] = S;
                double cv = 0;
                for (int j = 1; j < Value.step + 1; j++)
                {
                    optionprice[j] = Price(optionprice[j - 1], r, vol, t, RandomList[i, j - 1]);
                    tl = t - (j - 1) * deltaT;
                    d1 = (Math.Log(optionprice[j - 1] / Value.K) + (r + Math.Pow(vol, 2) / 2) * tl) / (vol * Math.Sqrt(tl));
                    delta = normalCDF(d1);
                    cv = cv + delta * (optionprice[j] - optionprice[j - 1] * Math.Exp(r * deltaT));
                }
                if (optionprice.Min() < Value.barrier)
                { PriceList.Add(Math.Max(optionprice.Last() - Value.K, 0) - cv); }
                else { PriceList.Add(0); }
            }
            double price = 0;
            price = Math.Exp(-r * t) * PriceList.Average();
            Program.increase(1);
            return price;
        }
        public double PutPrice(double S, double r, double vol, double t)
        {
            Initial();
            double tl;
            double d1;
            double delta;
            double deltaT = t / Convert.ToDouble(Value.step);
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                optionprice[0] = S;
                double cv = 0;
                for (int j = 1; j < Value.step + 1; j++)
                {
                    optionprice[j] = Price(optionprice[j - 1], r, vol, t, RandomList[i, j - 1]);
                    tl = t - (j - 1) * deltaT;
                    d1 = (Math.Log(optionprice[j - 1] / Value.K) + (r + Math.Pow(vol, 2) / 2) * tl) / (vol * Math.Sqrt(tl));
                    delta = normalCDF(d1)-1;
                    cv = cv + delta * (optionprice[j] - optionprice[j - 1] * Math.Exp(r * deltaT));
                }
                if (optionprice.Min() < Value.barrier)
                { PriceList.Add(Math.Max(Value.K - optionprice.Last(), 0) - cv); }
                else { PriceList.Add(0); }
            }
            double price = 0;
            price = Math.Exp(-r * t) * PriceList.Average();
            Program.increase(1);
            return price;
        }
        public double SE()
        {
            double avg = PriceList.Average();
            double sum = PriceList.Sum(v => Math.Pow(v - avg, 2));
            return (Math.Sqrt(sum / (PriceList.Count - 1))) / (Math.Sqrt(PriceList.Count));
        }
        public double normalCDF(double value)
        {
            const double A1 = 0.254829592;
            const double A2 = -0.284496736;
            const double A3 = 1.421413741;
            const double A4 = -1.453152027;
            const double A5 = 1.061405429;
            const double P = 0.3275911;

            int sign;
            if (value < 0)
                sign = -1;
            else
                sign = 1;

            double x = Math.Abs(value) / Math.Sqrt(2);
            double t = 1 / (1 + P * x);
            double erf = 1 - (((((A5 * t + A4) * t + A3) * t + A2) * t + A1) * t * Math.Exp(-x * x));


            return 0.5 * (1 + sign * erf);
        }
        internal static class GaussianBoxMuller
        {
            public static double NextDouble()
            {
                double x, y, square;

                do
                {
                    x = 2 * RandomProvider.GetThreadRandom().NextDouble() - 1;
                    y = 2 * RandomProvider.GetThreadRandom().NextDouble() - 1;
                    square = (x * x) + (y * y);
                } while (square >= 1);

                return x * Math.Sqrt(-2 * Math.Log(square) / square);
            }
        }
        internal static class RandomProvider
        {
            private static ThreadLocal<Random> randomWrapper = new ThreadLocal<Random>(() =>
                new Random(Interlocked.Increment(ref seed))
            );

            private static int seed = Environment.TickCount * 10;

            public static Random GetThreadRandom()
            {
                return randomWrapper.Value;
            }
        }
        public double calldelta(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S + 0.0001, r, vol, t);
            double cp2 = CallPrice(S - 0.0001, r, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double callgamma(double S, double r, double vol, double t)
        {
            return (calldelta(S + 0.01, r, vol, t) - calldelta(S, r, vol, t)) / 0.01;
        }
        public double callvega(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r, vol + 0.0001, t);
            double cp2 = CallPrice(S, r, vol - 0.0001, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double calltheta(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r, vol, t + 0.0001);
            double cp2 = CallPrice(S, r, vol, t - 0.0001);
            return (cp1 - cp2) / 0.0002;
        }
        public double callrho(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r + 0.0001, vol, t);
            double cp2 = CallPrice(S, r - 0.0001, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double putdelta(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S + 0.0001, r, vol, t);
            double cp2 = PutPrice(S - 0.0001, r, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double putgamma(double S, double r, double vol, double t)
        {
            return (putdelta(S + 0.01, r, vol, t) - putdelta(S, r, vol, t)) / 0.01;
        }
        public double putvega(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r, vol + 0.0001, t);
            double cp2 = PutPrice(S, r, vol - 0.0001, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double puttheta(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r, vol, t + 0.0001);
            double cp2 = PutPrice(S, r, vol, t - 0.0001);
            return (cp1 - cp2) / 0.0002;
        }
        public double putrho(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r + 0.0001, vol, t);
            double cp2 = PutPrice(S, r - 0.0001, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
    }

    public class BarrierUAOCV : PreparationCV, CallPrice, StandardError, PutPrice, CallGreek, PutGreek
    {
        public List<double> PriceList;
        public static double[,] RandomList;
        public void Initial()
        {
            optionprice = new double[Value.step + 1];
            PriceList = new List<double>();
        }
        public void RandomGenerated()
        {
            RandomList = new double[Value.trials, Value.step];
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                for (int k = 0; k < Value.step; k++)
                {
                    RandomList[i, k] = GaussianBoxMuller.NextDouble();
                }
            }
        }
        public double CallPrice(double S, double r, double vol, double t)
        {
            Initial();
            double tl;
            double d1;
            double delta;
            double deltaT = t / Convert.ToDouble(Value.step);
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                optionprice[0] = S;
                double cv = 0;
                for (int j = 1; j < Value.step + 1; j++)
                {
                    optionprice[j] = Price(optionprice[j - 1], r, vol, t, RandomList[i, j - 1]);
                    tl = t - (j - 1) * deltaT;
                    d1 = (Math.Log(optionprice[j - 1] / Value.K) + (r + Math.Pow(vol, 2) / 2) * tl) / (vol * Math.Sqrt(tl));
                    delta = normalCDF(d1);
                    cv = cv + delta * (optionprice[j] - optionprice[j - 1] * Math.Exp(r * deltaT));
                }
                if (optionprice.Max() <= Value.barrier)
                { PriceList.Add(Math.Max(optionprice.Last() - Value.K, 0) - cv); }
                else { PriceList.Add(0); }
            }
            double price = 0;
            price = Math.Exp(-r * t) * PriceList.Average();
            Program.increase(1);
            return price;
        }
        public double PutPrice(double S, double r, double vol, double t)
        {
            Initial();
            double tl;
            double d1;
            double delta;
            double deltaT = t / Convert.ToDouble(Value.step);
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                optionprice[0] = S;
                double cv = 0;
                for (int j = 1; j < Value.step + 1; j++)
                {
                    optionprice[j] = Price(optionprice[j - 1], r, vol, t, RandomList[i, j - 1]);
                    tl = t - (j - 1) * deltaT;
                    d1 = (Math.Log(optionprice[j - 1] / Value.K) + (r + Math.Pow(vol, 2) / 2) * tl) / (vol * Math.Sqrt(tl));
                    delta = normalCDF(d1)-1;
                    cv = cv + delta * (optionprice[j] - optionprice[j - 1] * Math.Exp(r * deltaT));
                }
                if (optionprice.Max() <= Value.barrier)
                { PriceList.Add(Math.Max(Value.K - optionprice.Last(), 0) - cv); }
                else { PriceList.Add(0); }
            }
            double price = 0;
            price = Math.Exp(-r * t) * PriceList.Average();
            Program.increase(1);
            return price;
        }
        public double SE()
        {
            double avg = PriceList.Average();
            double sum = PriceList.Sum(v => Math.Pow(v - avg, 2));
            return (Math.Sqrt(sum / (PriceList.Count - 1))) / (Math.Sqrt(PriceList.Count));
        }
        public double normalCDF(double value)
        {
            const double A1 = 0.254829592;
            const double A2 = -0.284496736;
            const double A3 = 1.421413741;
            const double A4 = -1.453152027;
            const double A5 = 1.061405429;
            const double P = 0.3275911;

            int sign;
            if (value < 0)
                sign = -1;
            else
                sign = 1;

            double x = Math.Abs(value) / Math.Sqrt(2);
            double t = 1 / (1 + P * x);
            double erf = 1 - (((((A5 * t + A4) * t + A3) * t + A2) * t + A1) * t * Math.Exp(-x * x));


            return 0.5 * (1 + sign * erf);
        }
        internal static class GaussianBoxMuller
        {
            public static double NextDouble()
            {
                double x, y, square;

                do
                {
                    x = 2 * RandomProvider.GetThreadRandom().NextDouble() - 1;
                    y = 2 * RandomProvider.GetThreadRandom().NextDouble() - 1;
                    square = (x * x) + (y * y);
                } while (square >= 1);

                return x * Math.Sqrt(-2 * Math.Log(square) / square);
            }
        }
        internal static class RandomProvider
        {
            private static ThreadLocal<Random> randomWrapper = new ThreadLocal<Random>(() =>
                new Random(Interlocked.Increment(ref seed))
            );

            private static int seed = Environment.TickCount * 10;

            public static Random GetThreadRandom()
            {
                return randomWrapper.Value;
            }
        }
        public double calldelta(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S + 0.0001, r, vol, t);
            double cp2 = CallPrice(S - 0.0001, r, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double callgamma(double S, double r, double vol, double t)
        {
            return (calldelta(S + 0.01, r, vol, t) - calldelta(S, r, vol, t)) / 0.01;
        }
        public double callvega(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r, vol + 0.0001, t);
            double cp2 = CallPrice(S, r, vol - 0.0001, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double calltheta(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r, vol, t + 0.0001);
            double cp2 = CallPrice(S, r, vol, t - 0.0001);
            return (cp1 - cp2) / 0.0002;
        }
        public double callrho(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r + 0.0001, vol, t);
            double cp2 = CallPrice(S, r - 0.0001, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double putdelta(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S + 0.0001, r, vol, t);
            double cp2 = PutPrice(S - 0.0001, r, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double putgamma(double S, double r, double vol, double t)
        {
            return (putdelta(S + 0.01, r, vol, t) - putdelta(S, r, vol, t)) / 0.01;
        }
        public double putvega(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r, vol + 0.0001, t);
            double cp2 = PutPrice(S, r, vol - 0.0001, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double puttheta(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r, vol, t + 0.0001);
            double cp2 = PutPrice(S, r, vol, t - 0.0001);
            return (cp1 - cp2) / 0.0002;
        }
        public double putrho(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r + 0.0001, vol, t);
            double cp2 = PutPrice(S, r - 0.0001, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
    }

    public class BarrierUAICV : PreparationCV, CallPrice, StandardError, PutPrice, CallGreek, PutGreek
    {
        public List<double> PriceList;
        public static double[,] RandomList;
        public void Initial()
        {
            optionprice = new double[Value.step + 1];
            PriceList = new List<double>();
        }
        public void RandomGenerated()
        {
            RandomList = new double[Value.trials, Value.step];
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                for (int k = 0; k < Value.step; k++)
                {
                    RandomList[i, k] = GaussianBoxMuller.NextDouble();
                }
            }
        }
        public double CallPrice(double S, double r, double vol, double t)
        {
            Initial();
            double tl;
            double d1;
            double delta;
            double deltaT = t / Convert.ToDouble(Value.step);
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                optionprice[0] = S;
                double cv = 0;
                for (int j = 1; j < Value.step + 1; j++)
                {
                    optionprice[j] = Price(optionprice[j - 1], r, vol, t, RandomList[i, j - 1]);
                    tl = t - (j - 1) * deltaT;
                    d1 = (Math.Log(optionprice[j - 1] / Value.K) + (r + Math.Pow(vol, 2) / 2) * tl) / (vol * Math.Sqrt(tl));
                    delta = normalCDF(d1);
                    cv = cv + delta * (optionprice[j] - optionprice[j - 1] * Math.Exp(r * deltaT));
                }
                if (optionprice.Max() > Value.barrier)
                { PriceList.Add(Math.Max(optionprice.Last() - Value.K, 0) - cv); }
                else { PriceList.Add(0); }
            }
            double price = 0;
            price = Math.Exp(-r * t) * PriceList.Average();
            Program.increase(1);
            return price;
        }
        public double PutPrice(double S, double r, double vol, double t)
        {
            Initial();
            double tl;
            double d1;
            double delta;
            double deltaT = t / Convert.ToDouble(Value.step);
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                optionprice[0] = S;
                double cv = 0;
                for (int j = 1; j < Value.step + 1; j++)
                {
                    optionprice[j] = Price(optionprice[j - 1], r, vol, t, RandomList[i, j - 1]);
                    tl = t - (j - 1) * deltaT;
                    d1 = (Math.Log(optionprice[j - 1] / Value.K) + (r + Math.Pow(vol, 2) / 2) * tl) / (vol * Math.Sqrt(tl));
                    delta = normalCDF(d1)-1;
                    cv = cv + delta * (optionprice[j] - optionprice[j - 1] * Math.Exp(r * deltaT));
                }
                if (optionprice.Max() > Value.barrier)
                { PriceList.Add(Math.Max(Value.K - optionprice.Last(), 0) - cv); }
                else { PriceList.Add(0); }
            }
            double price = 0;
            price = Math.Exp(-r * t) * PriceList.Average();
            Program.increase(1);
            return price;
        }
        public double SE()
        {
            double avg = PriceList.Average();
            double sum = PriceList.Sum(v => Math.Pow(v - avg, 2));
            return (Math.Sqrt(sum / (PriceList.Count - 1))) / (Math.Sqrt(PriceList.Count));
        }
        public double normalCDF(double value)
        {
            const double A1 = 0.254829592;
            const double A2 = -0.284496736;
            const double A3 = 1.421413741;
            const double A4 = -1.453152027;
            const double A5 = 1.061405429;
            const double P = 0.3275911;

            int sign;
            if (value < 0)
                sign = -1;
            else
                sign = 1;

            double x = Math.Abs(value) / Math.Sqrt(2);
            double t = 1 / (1 + P * x);
            double erf = 1 - (((((A5 * t + A4) * t + A3) * t + A2) * t + A1) * t * Math.Exp(-x * x));


            return 0.5 * (1 + sign * erf);
        }
        internal static class GaussianBoxMuller
        {
            public static double NextDouble()
            {
                double x, y, square;

                do
                {
                    x = 2 * RandomProvider.GetThreadRandom().NextDouble() - 1;
                    y = 2 * RandomProvider.GetThreadRandom().NextDouble() - 1;
                    square = (x * x) + (y * y);
                } while (square >= 1);

                return x * Math.Sqrt(-2 * Math.Log(square) / square);
            }
        }
        internal static class RandomProvider
        {
            private static ThreadLocal<Random> randomWrapper = new ThreadLocal<Random>(() =>
                new Random(Interlocked.Increment(ref seed))
            );

            private static int seed = Environment.TickCount * 10;

            public static Random GetThreadRandom()
            {
                return randomWrapper.Value;
            }
        }
        public double calldelta(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S + 0.0001, r, vol, t);
            double cp2 = CallPrice(S - 0.0001, r, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double callgamma(double S, double r, double vol, double t)
        {
            return (calldelta(S + 0.01, r, vol, t) - calldelta(S, r, vol, t)) / 0.01;
        }
        public double callvega(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r, vol + 0.0001, t);
            double cp2 = CallPrice(S, r, vol - 0.0001, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double calltheta(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r, vol, t + 0.0001);
            double cp2 = CallPrice(S, r, vol, t - 0.0001);
            return (cp1 - cp2) / 0.0002;
        }
        public double callrho(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r + 0.0001, vol, t);
            double cp2 = CallPrice(S, r - 0.0001, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double putdelta(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S + 0.0001, r, vol, t);
            double cp2 = PutPrice(S - 0.0001, r, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double putgamma(double S, double r, double vol, double t)
        {
            return (putdelta(S + 0.01, r, vol, t) - putdelta(S, r, vol, t)) / 0.01;
        }
        public double putvega(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r, vol + 0.0001, t);
            double cp2 = PutPrice(S, r, vol - 0.0001, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double puttheta(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r, vol, t + 0.0001);
            double cp2 = PutPrice(S, r, vol, t - 0.0001);
            return (cp1 - cp2) / 0.0002;
        }
        public double putrho(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r + 0.0001, vol, t);
            double cp2 = PutPrice(S, r - 0.0001, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
    }

    public class LookbackCV: PreparationCV, CallPrice, StandardError, PutPrice, CallGreek, PutGreek
    {
        public List<double> PriceList;
        public static double[,] RandomList;
        public void Initial()
        {
            optionprice = new double[Value.step + 1];
            PriceList = new List<double>();
        }
        public void RandomGenerated()
        {
            RandomList = new double[Value.trials, Value.step];
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                for (int k = 0; k < Value.step; k++)
                {
                    RandomList[i, k] = GaussianBoxMuller.NextDouble();
                }
            }
        }
        public double CallPrice(double S, double r, double vol, double t)
        {
            Initial();
            double tl;
            double d1;
            double delta;
            double deltaT = t / Convert.ToDouble(Value.step);
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                optionprice[0] = S;
                double cv = 0;
                for (int j = 1; j < Value.step + 1; j++)
                {
                    optionprice[j] = Price(optionprice[j - 1], r, vol, t, RandomList[i, j - 1]);
                    tl = t - (j - 1) * deltaT;
                    d1 = (Math.Log(optionprice[j - 1] / Value.K) + (r + Math.Pow(vol, 2) / 2) * tl) / (vol * Math.Sqrt(tl));
                    delta = normalCDF(d1);
                    cv = cv + delta * (optionprice[j] - optionprice[j - 1] * Math.Exp(r * deltaT));
                }
                PriceList.Add(Math.Max(optionprice.Max() - Value.K, 0) - cv);
            }
            double price = 0;
            price = Math.Exp(-r * t) * PriceList.Average();
            Program.increase(1);
            return price;
        }
        public double PutPrice(double S, double r, double vol, double t)
        {
            Initial();
            double tl;
            double d1;
            double delta;
            double deltaT = t / Convert.ToDouble(Value.step);
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                optionprice[0] = S;
                double cv = 0;
                for (int j = 1; j < Value.step + 1; j++)
                {
                    optionprice[j] = Price(optionprice[j - 1], r, vol, t, RandomList[i, j - 1]);
                    tl = t - (j - 1) * deltaT;
                    d1 = (Math.Log(optionprice[j - 1] / Value.K) + (r + Math.Pow(vol, 2) / 2) * tl) / (vol * Math.Sqrt(tl));
                    delta = normalCDF(d1)-1;
                    cv = cv + delta * (optionprice[j] - optionprice[j - 1] * Math.Exp(r * deltaT));
                }
                PriceList.Add(Math.Max(Value.K - optionprice.Min(), 0) - cv);
            }
            double price = 0;
            price = Math.Exp(-r * t) * PriceList.Average();
            Program.increase(1);
            return price;
        }
        public double SE()
        {
            double avg = PriceList.Average();
            double sum = PriceList.Sum(v => Math.Pow(v - avg, 2));
            return (Math.Sqrt(sum / (PriceList.Count - 1))) / (Math.Sqrt(PriceList.Count));
        }
        public double normalCDF(double value)
        {
            const double A1 = 0.254829592;
            const double A2 = -0.284496736;
            const double A3 = 1.421413741;
            const double A4 = -1.453152027;
            const double A5 = 1.061405429;
            const double P = 0.3275911;

            int sign;
            if (value < 0)
                sign = -1;
            else
                sign = 1;

            double x = Math.Abs(value) / Math.Sqrt(2);
            double t = 1 / (1 + P * x);
            double erf = 1 - (((((A5 * t + A4) * t + A3) * t + A2) * t + A1) * t * Math.Exp(-x * x));


            return 0.5 * (1 + sign * erf);
        }
        internal static class GaussianBoxMuller
        {
            public static double NextDouble()
            {
                double x, y, square;

                do
                {
                    x = 2 * RandomProvider.GetThreadRandom().NextDouble() - 1;
                    y = 2 * RandomProvider.GetThreadRandom().NextDouble() - 1;
                    square = (x * x) + (y * y);
                } while (square >= 1);

                return x * Math.Sqrt(-2 * Math.Log(square) / square);
            }
        }
        internal static class RandomProvider
        {
            private static ThreadLocal<Random> randomWrapper = new ThreadLocal<Random>(() =>
                new Random(Interlocked.Increment(ref seed))
            );

            private static int seed = Environment.TickCount * 10;

            public static Random GetThreadRandom()
            {
                return randomWrapper.Value;
            }
        }
        public double calldelta(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S + 0.0001, r, vol, t);
            double cp2 = CallPrice(S - 0.0001, r, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double callgamma(double S, double r, double vol, double t)
        {
            return (calldelta(S + 0.01, r, vol, t) - calldelta(S, r, vol, t)) / 0.01;
        }
        public double callvega(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r, vol + 0.0001, t);
            double cp2 = CallPrice(S, r, vol - 0.0001, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double calltheta(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r, vol, t + 0.0001);
            double cp2 = CallPrice(S, r, vol, t - 0.0001);
            return (cp1 - cp2) / 0.0002;
        }
        public double callrho(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r + 0.0001, vol, t);
            double cp2 = CallPrice(S, r - 0.0001, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double putdelta(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S + 0.0001, r, vol, t);
            double cp2 = PutPrice(S - 0.0001, r, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double putgamma(double S, double r, double vol, double t)
        {
            return (putdelta(S + 0.01, r, vol, t) - putdelta(S, r, vol, t)) / 0.01;
        }
        public double putvega(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r, vol + 0.0001, t);
            double cp2 = PutPrice(S, r, vol - 0.0001, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double puttheta(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r, vol, t + 0.0001);
            double cp2 = PutPrice(S, r, vol, t - 0.0001);
            return (cp1 - cp2) / 0.0002;
        }
        public double putrho(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r + 0.0001, vol, t);
            double cp2 = PutPrice(S, r - 0.0001, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
    }

    public class RangeCV: PreparationCV, CallPrice, StandardError, PutPrice, CallGreek, PutGreek
    {
        public List<double> PriceList;
        public static double[,] RandomList;
        public void Initial()
        {
            optionprice = new double[Value.step + 1];
            PriceList = new List<double>();
        }
        public void RandomGenerated()
        {
            RandomList = new double[Value.trials, Value.step];
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                for (int k = 0; k < Value.step; k++)
                {
                    RandomList[i, k] = GaussianBoxMuller.NextDouble();
                }
            }
        }
        public double CallPrice(double S, double r, double vol, double t)
        {
            Initial();
            double tl;
            double d1;
            double delta;
            double deltaT = t / Convert.ToDouble(Value.step);
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                optionprice[0] = S;
                double cv = 0;
                for (int j = 1; j < Value.step + 1; j++)
                {
                    optionprice[j] = Price(optionprice[j - 1], r, vol, t, RandomList[i, j - 1]);
                    tl = t - (j - 1) * deltaT;
                    d1 = (Math.Log(optionprice[j - 1] / Value.K) + (r + Math.Pow(vol, 2) / 2) * tl) / (vol * Math.Sqrt(tl));
                    delta = normalCDF(d1);
                    cv = cv + delta * (optionprice[j] - optionprice[j - 1] * Math.Exp(r * deltaT));
                }
                PriceList.Add(optionprice.Max() - optionprice.Min() - cv);
            }
            double price = 0;
            price = Math.Exp(-r * t) * PriceList.Average();
            Program.increase(1);
            return price;
        }
        public double PutPrice(double S, double r, double vol, double t)
        {
            Initial();
            double tl;
            double d1;
            double delta;
            double deltaT = t / Convert.ToDouble(Value.step);
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                optionprice[0] = S;
                double cv = 0;
                for (int j = 1; j < Value.step + 1; j++)
                {
                    optionprice[j] = Price(optionprice[j - 1], r, vol, t, RandomList[i, j - 1]);
                    tl = t - (j - 1) * deltaT;
                    d1 = (Math.Log(optionprice[j - 1] / Value.K) + (r + Math.Pow(vol, 2) / 2) * tl) / (vol * Math.Sqrt(tl));
                    delta = normalCDF(d1)-1;
                    cv = cv + delta * (optionprice[j] - optionprice[j - 1] * Math.Exp(r * deltaT));
                }
                PriceList.Add(optionprice.Max() - optionprice.Min() - cv);
            }
            double price = 0;
            price = Math.Exp(-r * t) * PriceList.Average();
            Program.increase(1);
            return price;
        }
        public double SE()
        {
            double avg = PriceList.Average();
            double sum = PriceList.Sum(v => Math.Pow(v - avg, 2));
            return (Math.Sqrt(sum / (PriceList.Count - 1))) / (Math.Sqrt(PriceList.Count));
        }
        public double normalCDF(double value)
        {
            const double A1 = 0.254829592;
            const double A2 = -0.284496736;
            const double A3 = 1.421413741;
            const double A4 = -1.453152027;
            const double A5 = 1.061405429;
            const double P = 0.3275911;

            int sign;
            if (value < 0)
                sign = -1;
            else
                sign = 1;

            double x = Math.Abs(value) / Math.Sqrt(2);
            double t = 1 / (1 + P * x);
            double erf = 1 - (((((A5 * t + A4) * t + A3) * t + A2) * t + A1) * t * Math.Exp(-x * x));


            return 0.5 * (1 + sign * erf);
        }
        internal static class GaussianBoxMuller
        {
            public static double NextDouble()
            {
                double x, y, square;

                do
                {
                    x = 2 * RandomProvider.GetThreadRandom().NextDouble() - 1;
                    y = 2 * RandomProvider.GetThreadRandom().NextDouble() - 1;
                    square = (x * x) + (y * y);
                } while (square >= 1);

                return x * Math.Sqrt(-2 * Math.Log(square) / square);
            }
        }
        internal static class RandomProvider
        {
            private static ThreadLocal<Random> randomWrapper = new ThreadLocal<Random>(() =>
                new Random(Interlocked.Increment(ref seed))
            );

            private static int seed = Environment.TickCount * 10;

            public static Random GetThreadRandom()
            {
                return randomWrapper.Value;
            }
        }
        public double calldelta(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S + 0.0001, r, vol, t);
            double cp2 = CallPrice(S - 0.0001, r, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double callgamma(double S, double r, double vol, double t)
        {
            return (calldelta(S + 0.01, r, vol, t) - calldelta(S, r, vol, t)) / 0.01;
        }
        public double callvega(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r, vol + 0.0001, t);
            double cp2 = CallPrice(S, r, vol - 0.0001, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double calltheta(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r, vol, t + 0.0001);
            double cp2 = CallPrice(S, r, vol, t - 0.0001);
            return (cp1 - cp2) / 0.0002;
        }
        public double callrho(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r + 0.0001, vol, t);
            double cp2 = CallPrice(S, r - 0.0001, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double putdelta(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S + 0.0001, r, vol, t);
            double cp2 = PutPrice(S - 0.0001, r, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double putgamma(double S, double r, double vol, double t)
        {
            return (putdelta(S + 0.01, r, vol, t) - putdelta(S, r, vol, t)) / 0.01;
        }
        public double putvega(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r, vol + 0.0001, t);
            double cp2 = PutPrice(S, r, vol - 0.0001, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double puttheta(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r, vol, t + 0.0001);
            double cp2 = PutPrice(S, r, vol, t - 0.0001);
            return (cp1 - cp2) / 0.0002;
        }
        public double putrho(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r + 0.0001, vol, t);
            double cp2 = PutPrice(S, r - 0.0001, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
    }

}
