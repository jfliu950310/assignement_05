﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ConsoleApp6
{
    static class Program
    {
        static void Main()
        {
            Thread A = new Thread(RunGUI);
            A.Start();
            MessageBox.Show("Attention: You should fill out all of the blanks in this GUI. Do not leave any textboxs empty." + "\n" +
                            "The Greeks warning: All Greeks are based on 0.0001 scale except gamma,which is based on 0.01 scale.");
            A.Join();
        }

        public static Gui gui1 = new Gui();
        static void RunGUI()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(gui1);
        }

        delegate void progresscheck(int amount);
        delegate void finish();

        public static void increase(int input)
        {
            if (Program.gui1.progressBar1.InvokeRequired)
            {
                progresscheck progressdelegate = new progresscheck(progress_check_method);
                gui1.progressBar1.BeginInvoke(progressdelegate, input);
            }
            else
            {
                progress_check_method(input);
            }
        }

        public static void finished()
        {
            if (Program.gui1.richTextBox1.InvokeRequired)
            {
                finish finishdelegate = new finish(finish_check_method);
                gui1.richTextBox1.BeginInvoke(finishdelegate);
            }
            else
            {
                finish_check_method();
            }
        }

        public static void progress_check_method(int amout)
        {
            gui1.progressBar1.Value += amout;
            gui1.progressBar1.Update();
        }

        public static void finish_check_method()
        {
            gui1.finish();

        }
    }
}
