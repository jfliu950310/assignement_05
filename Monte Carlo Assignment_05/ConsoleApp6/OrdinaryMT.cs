﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ConsoleApp6
{
    public class PreparationMT
    {
        public double[,] optionprice { get; set; }
        public double[,] sumup { get; set; }
        public List<double> Option { get; set; }
        public double Price(double S, double r, double vol, double t, double ep)
        {
            double price;
            double deltaT = t / Convert.ToDouble(Value.step);
            price = S * Math.Exp((r - Math.Pow(vol, 2) / 2) * deltaT + vol * Math.Sqrt(deltaT) * ep);
            return price;
        }
    }

    public class EuropeanMT : PreparationMT, CallPrice, StandardError, PutPrice,CallGreek,PutGreek
    {
        public static double[,] RandomList;
        public void Initial()
        {
            optionprice = new double[Value.trials, Value.step + 1];
            Option = new List<double>();
        }
        public void RandomGenerated()
        {
            RandomList = new double[Value.trials, Value.step];

            Parallel.ForEach(Ienum.Step(0, Value.trials, 1), new ParallelOptions { MaxDegreeOfParallelism = Value.Thread }, i =>
            {
                for (int k = 0; k < Value.step; k++)
                {
                    RandomList[i, k] = GaussianBoxMuller.NextDouble();
                }
            });
        }
        public double CallPrice(double S, double r, double vol, double t)
        {
            Initial();
            Parallel.ForEach(Ienum.Step(0, Value.trials, 1), new ParallelOptions { MaxDegreeOfParallelism = Value.Thread }, i =>
            {
                optionprice[i, 0] = S;
                for (uint j = 1; j < Value.step + 1; j++)
                {
                    optionprice[i, j] = Price(optionprice[i, j - 1], r, vol, t, RandomList[i, j - 1]);
                }
            });
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                Option.Add(Math.Max(optionprice[i, Value.step] - Value.K, 0));
            }
            double price = 0;
            price = Math.Exp(-r * t) * Option.Average();
            Program.increase(1);
            return price;
        }
        public double PutPrice(double S, double r, double vol, double t)
        {
            Initial();
            Parallel.ForEach(Ienum.Step(0, Value.trials, 1), new ParallelOptions { MaxDegreeOfParallelism = Value.Thread }, i =>
            {
                optionprice[i, 0] = S;
                for (uint j = 1; j < Value.step + 1; j++)
                {
                    optionprice[i, j] = Price(optionprice[i, j - 1], r, vol, t, RandomList[i, j - 1]);
                }
            });
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                Option.Add(Math.Max(Value.K - optionprice[i, Value.step], 0));
            }
            double price = 0;
            price = Math.Exp(-r * t) * Option.Average();
            Program.increase(1);
            return price;
        }
        public double SE()
        {
            double avg = Option.Average();
            double sum = Option.Sum(v => Math.Pow(v - avg, 2));
            return (Math.Sqrt(sum / (Option.Count - 1))) / (Math.Sqrt(Option.Count));
        }
        internal static class GaussianBoxMuller
        {
            public static double NextDouble()
            {
                double x, y, square;

                do
                {
                    x = 2 * RandomProvider.GetThreadRandom().NextDouble() - 1;
                    y = 2 * RandomProvider.GetThreadRandom().NextDouble() - 1;
                    square = (x * x) + (y * y);
                } while (square >= 1);

                return x * Math.Sqrt(-2 * Math.Log(square) / square);
            }
        }
        internal static class RandomProvider
        {
            private static ThreadLocal<Random> randomWrapper = new ThreadLocal<Random>(() =>
                new Random(Interlocked.Increment(ref seed))
            );

            private static int seed = Environment.TickCount * 10;

            public static Random GetThreadRandom()
            {
                return randomWrapper.Value;
            }
        }
        public double calldelta(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S + 0.0001, r, vol, t);
            double cp2 = CallPrice(S - 0.0001, r, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double callgamma(double S, double r, double vol, double t)
        {
            return (calldelta(S + 0.01, r, vol, t) - calldelta(S, r, vol, t)) / 0.01;
        }
        public double callvega(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r, vol + 0.0001, t);
            double cp2 = CallPrice(S, r, vol - 0.0001, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double calltheta(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r, vol, t + 0.0001);
            double cp2 = CallPrice(S, r, vol, t - 0.0001);
            return (cp1 - cp2) / 0.0002;
        }
        public double callrho(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r + 0.0001, vol, t);
            double cp2 = CallPrice(S, r - 0.0001, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double putdelta(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S + 0.0001, r, vol, t);
            double cp2 = PutPrice(S - 0.0001, r, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double putgamma(double S, double r, double vol, double t)
        {
            return (putdelta(S + 0.01, r, vol, t) - putdelta(S, r, vol, t)) / 0.01;
        }
        public double putvega(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r, vol + 0.0001, t);
            double cp2 = PutPrice(S, r, vol - 0.0001, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double puttheta(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r, vol, t + 0.0001);
            double cp2 = PutPrice(S, r, vol, t - 0.0001);
            return (cp1 - cp2) / 0.0002;
        }
        public double putrho(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r + 0.0001, vol, t);
            double cp2 = PutPrice(S, r - 0.0001, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
    }

    public class AsianMT : PreparationMT, CallPrice, StandardError, PutPrice, CallGreek, PutGreek
    {
        public static double[,] RandomList;
        public void Initial()
        {
            optionprice = new double[Value.trials, Value.step + 1];
            sumup = new double[Value.trials, Value.step + 1];
            Option = new List<double>();
        }
        public void RandomGenerated()
        {
            RandomList = new double[Value.trials, Value.step];

            Parallel.ForEach(Ienum.Step(0, Value.trials, 1), new ParallelOptions { MaxDegreeOfParallelism = Value.Thread }, i =>
            {
                for (int k = 0; k < Value.step; k++)
                {
                    RandomList[i, k] = GaussianBoxMuller.NextDouble();
                }
            });
        }
        public double CallPrice(double S, double r, double vol, double t)
        {
            Initial();
            Parallel.ForEach(Ienum.Step(0, Value.trials, 1), new ParallelOptions { MaxDegreeOfParallelism = Value.Thread }, i =>
            {
                optionprice[i, 0] = S;
                sumup[i, 0] = S;
                for (uint j = 1; j < Value.step + 1; j++)
                {
                    optionprice[i, j] = Price(optionprice[i, j - 1], r, vol, t, RandomList[i, j - 1]);
                    sumup[i, j] = sumup[i, j - 1] + optionprice[i, j];
                }
            });
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                Option.Add(Math.Max((sumup[i, Value.step]-sumup[i,0] )/(Value.step)- Value.K, 0));
            }
            double price = 0;
            price = Math.Exp(-r * t) * Option.Average();
            Program.increase(1);
            return price;
        }
        public double PutPrice(double S, double r, double vol, double t)
        {
            Initial();
            Parallel.ForEach(Ienum.Step(0, Value.trials, 1), new ParallelOptions { MaxDegreeOfParallelism = Value.Thread }, i =>
            {
                optionprice[i, 0] = S;
                sumup[i, 0] = S;
                for (uint j = 1; j < Value.step + 1; j++)
                {
                    optionprice[i, j] = Price(optionprice[i, j - 1], r, vol, t, RandomList[i, j - 1]);
                    sumup[i, j] = sumup[i, j - 1] + optionprice[i, j];
                }
            });
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                Option.Add(Math.Max(Value.K - (sumup[i, Value.step]-sumup[i,0])/(Value.step), 0));
            }
            double price = 0;
            price = Math.Exp(-r * t) * Option.Average();
            Program.increase(1);
            return price;
        }
        public double SE()
        {
            double avg = Option.Average();
            double sum = Option.Sum(v => Math.Pow(v - avg, 2));
            return (Math.Sqrt(sum / (Option.Count - 1))) / (Math.Sqrt(Option.Count));
        }
        internal static class GaussianBoxMuller
        {
            public static double NextDouble()
            {
                double x, y, square;

                do
                {
                    x = 2 * RandomProvider.GetThreadRandom().NextDouble() - 1;
                    y = 2 * RandomProvider.GetThreadRandom().NextDouble() - 1;
                    square = (x * x) + (y * y);
                } while (square >= 1);

                return x * Math.Sqrt(-2 * Math.Log(square) / square);
            }
        }
        internal static class RandomProvider
        {
            private static ThreadLocal<Random> randomWrapper = new ThreadLocal<Random>(() =>
                new Random(Interlocked.Increment(ref seed))
            );

            private static int seed = Environment.TickCount * 10;

            public static Random GetThreadRandom()
            {
                return randomWrapper.Value;
            }
        }
        public double calldelta(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S + 0.0001, r, vol, t);
            double cp2 = CallPrice(S - 0.0001, r, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double callgamma(double S, double r, double vol, double t)
        {
            return (calldelta(S + 0.01, r, vol, t) - calldelta(S, r, vol, t)) / 0.01;
        }
        public double callvega(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r, vol + 0.0001, t);
            double cp2 = CallPrice(S, r, vol - 0.0001, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double calltheta(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r, vol, t + 0.0001);
            double cp2 = CallPrice(S, r, vol, t - 0.0001);
            return (cp1 - cp2) / 0.0002;
        }
        public double callrho(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r + 0.0001, vol, t);
            double cp2 = CallPrice(S, r - 0.0001, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double putdelta(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S + 0.0001, r, vol, t);
            double cp2 = PutPrice(S - 0.0001, r, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double putgamma(double S, double r, double vol, double t)
        {
            return (putdelta(S + 0.01, r, vol, t) - putdelta(S, r, vol, t)) / 0.01;
        }
        public double putvega(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r, vol + 0.0001, t);
            double cp2 = PutPrice(S, r, vol - 0.0001, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double puttheta(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r, vol, t + 0.0001);
            double cp2 = PutPrice(S, r, vol, t - 0.0001);
            return (cp1 - cp2) / 0.0002;
        }
        public double putrho(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r + 0.0001, vol, t);
            double cp2 = PutPrice(S, r - 0.0001, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
    }

    public class DigitalMT : PreparationMT, CallPrice, StandardError, PutPrice, CallGreek, PutGreek
    {
        public static double[,] RandomList;
        public void Initial()
        {
            optionprice = new double[Value.trials, Value.step + 1];
            Option = new List<double>();
        }
        public void RandomGenerated()
        {
            RandomList = new double[Value.trials, Value.step];

            Parallel.ForEach(Ienum.Step(0, Value.trials, 1), new ParallelOptions { MaxDegreeOfParallelism = Value.Thread }, i =>
            {
                for (int k = 0; k < Value.step; k++)
                {
                    RandomList[i, k] = GaussianBoxMuller.NextDouble();
                }
            });
        }
        public double CallPrice(double S, double r, double vol, double t)
        {
            Initial();
            Parallel.ForEach(Ienum.Step(0, Value.trials, 1), new ParallelOptions { MaxDegreeOfParallelism = Value.Thread }, i =>
            {
                optionprice[i, 0] = S;
                for (uint j = 1; j < Value.step + 1; j++)
                {
                    optionprice[i, j] = Price(optionprice[i, j - 1], r, vol, t, RandomList[i, j - 1]);
                }
            });
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                if (optionprice[i, Value.step] - Value.K >= 0)
                { Option.Add(Value.rebate); }
                else { Option.Add(0); }
            }
            double price = 0;
            price = Math.Exp(-r * t) * Option.Average();
            Program.increase(1);
            return price;
        }
        public double PutPrice(double S, double r, double vol, double t)
        {
            Initial();
            Parallel.ForEach(Ienum.Step(0, Value.trials, 1), new ParallelOptions { MaxDegreeOfParallelism = Value.Thread }, i =>
            {
                optionprice[i, 0] = S;
                for (uint j = 1; j < Value.step + 1; j++)
                {
                    optionprice[i, j] = Price(optionprice[i, j - 1], r, vol, t, RandomList[i, j - 1]);
                }
            });
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                if (Value.K - optionprice[i, Value.step] >= 0)
                { Option.Add(Value.rebate); }
                else { Option.Add(0); }
            }
            double price = 0;
            price = Math.Exp(-r * t) * Option.Average();
            Program.increase(1);
            return price;
        }
        public double SE()
        {
            double avg = Option.Average();
            double sum = Option.Sum(v => Math.Pow(v - avg, 2));
            return (Math.Sqrt(sum / (Option.Count - 1))) / (Math.Sqrt(Option.Count));
        }
        internal static class GaussianBoxMuller
        {
            public static double NextDouble()
            {
                double x, y, square;

                do
                {
                    x = 2 * RandomProvider.GetThreadRandom().NextDouble() - 1;
                    y = 2 * RandomProvider.GetThreadRandom().NextDouble() - 1;
                    square = (x * x) + (y * y);
                } while (square >= 1);

                return x * Math.Sqrt(-2 * Math.Log(square) / square);
            }
        }
        internal static class RandomProvider
        {
            private static ThreadLocal<Random> randomWrapper = new ThreadLocal<Random>(() =>
                new Random(Interlocked.Increment(ref seed))
            );

            private static int seed = Environment.TickCount * 10;

            public static Random GetThreadRandom()
            {
                return randomWrapper.Value;
            }
        }
        public double calldelta(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S + 0.0001, r, vol, t);
            double cp2 = CallPrice(S - 0.0001, r, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double callgamma(double S, double r, double vol, double t)
        {
            return (calldelta(S + 0.01, r, vol, t) - calldelta(S, r, vol, t)) / 0.01;
        }
        public double callvega(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r, vol + 0.0001, t);
            double cp2 = CallPrice(S, r, vol - 0.0001, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double calltheta(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r, vol, t + 0.0001);
            double cp2 = CallPrice(S, r, vol, t - 0.0001);
            return (cp1 - cp2) / 0.0002;
        }
        public double callrho(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r + 0.0001, vol, t);
            double cp2 = CallPrice(S, r - 0.0001, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double putdelta(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S + 0.0001, r, vol, t);
            double cp2 = PutPrice(S - 0.0001, r, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double putgamma(double S, double r, double vol, double t)
        {
            return (putdelta(S + 0.01, r, vol, t) - putdelta(S, r, vol, t)) / 0.01;
        }
        public double putvega(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r, vol + 0.0001, t);
            double cp2 = PutPrice(S, r, vol - 0.0001, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double puttheta(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r, vol, t + 0.0001);
            double cp2 = PutPrice(S, r, vol, t - 0.0001);
            return (cp1 - cp2) / 0.0002;
        }
        public double putrho(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r + 0.0001, vol, t);
            double cp2 = PutPrice(S, r - 0.0001, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
    }

    public class BarrierDAOMT : PreparationMT, CallPrice, StandardError, PutPrice, CallGreek, PutGreek
    {
        public static double[,] RandomList;
        public void Initial()
        {
            optionprice = new double[Value.trials, Value.step + 1];
            Option = new List<double>();
        }
        public void RandomGenerated()
        {
            RandomList = new double[Value.trials, Value.step];

            Parallel.ForEach(Ienum.Step(0, Value.trials, 1), new ParallelOptions { MaxDegreeOfParallelism = Value.Thread }, i =>
            {
                for (int k = 0; k < Value.step; k++)
                {
                    RandomList[i, k] = GaussianBoxMuller.NextDouble();
                }
            });
        }
        public double CallPrice(double S, double r, double vol, double t)
        {
            Initial();
            Parallel.ForEach(Ienum.Step(0, Value.trials, 1), new ParallelOptions { MaxDegreeOfParallelism = Value.Thread }, i =>
            {
                optionprice[i, 0] = S;
                for (uint j = 1; j < Value.step + 1; j++)
                {
                    optionprice[i, j] = Price(optionprice[i, j - 1], r, vol, t, RandomList[i, j - 1]);
                }
            });
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                double[] m = new double[Value.step+1];
                for(uint j=0;j<Value.step+1;j++)
                {
                    m[j] = optionprice[i, j];
                }
                if (m.Min() >= Value.barrier)
                { Option.Add(Math.Max(optionprice[i, Value.step] - Value.K, 0)); }
                else { Option.Add(0); }
            }
            double price = 0;
            price = Math.Exp(-r * t) * Option.Average();
            Program.increase(1);
            return price;
        }
        public double PutPrice(double S, double r, double vol, double t)
        {
            Initial();
            Parallel.ForEach(Ienum.Step(0, Value.trials, 1), new ParallelOptions { MaxDegreeOfParallelism = Value.Thread }, i =>
            {
                optionprice[i, 0] = S;
                for (uint j = 1; j < Value.step + 1; j++)
                {
                    optionprice[i, j] = Price(optionprice[i, j - 1], r, vol, t, RandomList[i, j - 1]);
                }
            });
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                double[] m = new double[Value.step + 1];
                for (uint j = 0; j < Value.step + 1; j++)
                {
                    m[j] = optionprice[i, j];
                }
                if (m.Min() >= Value.barrier)
                { Option.Add(Math.Max(Value.K - optionprice[i, Value.step], 0)); }
                else { Option.Add(0); }
            }
            double price = 0;
            price = Math.Exp(-r * t) * Option.Average();
            Program.increase(1);
            return price;
        }
        public double SE()
        {
            double avg = Option.Average();
            double sum = Option.Sum(v => Math.Pow(v - avg, 2));
            return (Math.Sqrt(sum / (Option.Count - 1))) / (Math.Sqrt(Option.Count));
        }
        internal static class GaussianBoxMuller
        {
            public static double NextDouble()
            {
                double x, y, square;

                do
                {
                    x = 2 * RandomProvider.GetThreadRandom().NextDouble() - 1;
                    y = 2 * RandomProvider.GetThreadRandom().NextDouble() - 1;
                    square = (x * x) + (y * y);
                } while (square >= 1);

                return x * Math.Sqrt(-2 * Math.Log(square) / square);
            }
        }
        internal static class RandomProvider
        {
            private static ThreadLocal<Random> randomWrapper = new ThreadLocal<Random>(() =>
                new Random(Interlocked.Increment(ref seed))
            );

            private static int seed = Environment.TickCount * 10;

            public static Random GetThreadRandom()
            {
                return randomWrapper.Value;
            }
        }
        public double calldelta(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S + 0.0001, r, vol, t);
            double cp2 = CallPrice(S - 0.0001, r, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double callgamma(double S, double r, double vol, double t)
        {
            return (calldelta(S + 0.01, r, vol, t) - calldelta(S, r, vol, t)) / 0.01;
        }
        public double callvega(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r, vol + 0.0001, t);
            double cp2 = CallPrice(S, r, vol - 0.0001, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double calltheta(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r, vol, t + 0.0001);
            double cp2 = CallPrice(S, r, vol, t - 0.0001);
            return (cp1 - cp2) / 0.0002;
        }
        public double callrho(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r + 0.0001, vol, t);
            double cp2 = CallPrice(S, r - 0.0001, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double putdelta(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S + 0.0001, r, vol, t);
            double cp2 = PutPrice(S - 0.0001, r, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double putgamma(double S, double r, double vol, double t)
        {
            return (putdelta(S + 0.01, r, vol, t) - putdelta(S, r, vol, t)) / 0.01;
        }
        public double putvega(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r, vol + 0.0001, t);
            double cp2 = PutPrice(S, r, vol - 0.0001, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double puttheta(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r, vol, t + 0.0001);
            double cp2 = PutPrice(S, r, vol, t - 0.0001);
            return (cp1 - cp2) / 0.0002;
        }
        public double putrho(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r + 0.0001, vol, t);
            double cp2 = PutPrice(S, r - 0.0001, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
    }

    public class BarrierDAIMT : PreparationMT, CallPrice, StandardError, PutPrice, CallGreek, PutGreek
    {
        public static double[,] RandomList;
        public void Initial()
        {
            optionprice = new double[Value.trials, Value.step + 1];
            Option = new List<double>();
        }
        public void RandomGenerated()
        {
            RandomList = new double[Value.trials, Value.step];

            Parallel.ForEach(Ienum.Step(0, Value.trials, 1), new ParallelOptions { MaxDegreeOfParallelism = Value.Thread }, i =>
            {
                for (int k = 0; k < Value.step; k++)
                {
                    RandomList[i, k] = GaussianBoxMuller.NextDouble();
                }
            });
        }
        public double CallPrice(double S, double r, double vol, double t)
        {
            Initial();
            Parallel.ForEach(Ienum.Step(0, Value.trials, 1), new ParallelOptions { MaxDegreeOfParallelism = Value.Thread }, i =>
            {
                optionprice[i, 0] = S;
                for (uint j = 1; j < Value.step + 1; j++)
                {
                    optionprice[i, j] = Price(optionprice[i, j - 1], r, vol, t, RandomList[i, j - 1]);
                }
            });
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                double[] m = new double[Value.step + 1];
                for (uint j = 0; j < Value.step + 1; j++)
                {
                    m[j] = optionprice[i, j];
                }
                if (m.Min() < Value.barrier)
                { Option.Add(Math.Max(optionprice[i, Value.step] - Value.K, 0)); }
                else { Option.Add(0); }
            }
            double price = 0;
            price = Math.Exp(-r * t) * Option.Average();
            Program.increase(1);
            return price;
        }
        public double PutPrice(double S, double r, double vol, double t)
        {
            Initial();
            Parallel.ForEach(Ienum.Step(0, Value.trials, 1), new ParallelOptions { MaxDegreeOfParallelism = Value.Thread }, i =>
            {
                optionprice[i, 0] = S;
                for (uint j = 1; j < Value.step + 1; j++)
                {
                    optionprice[i, j] = Price(optionprice[i, j - 1], r, vol, t, RandomList[i, j - 1]);
                }
            });
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                double[] m = new double[Value.step + 1];
                for (uint j = 0; j < Value.step + 1; j++)
                {
                    m[j] = optionprice[i, j];
                }
                if (m.Min() < Value.barrier)
                { Option.Add(Math.Max(Value.K - optionprice[i, Value.step], 0)); }
                else { Option.Add(0); }
            }
            double price = 0;
            price = Math.Exp(-r * t) * Option.Average();
            Program.increase(1);
            return price;
        }
        public double SE()
        {
            double avg = Option.Average();
            double sum = Option.Sum(v => Math.Pow(v - avg, 2));
            return (Math.Sqrt(sum / (Option.Count - 1))) / (Math.Sqrt(Option.Count));
        }
        internal static class GaussianBoxMuller
        {
            public static double NextDouble()
            {
                double x, y, square;

                do
                {
                    x = 2 * RandomProvider.GetThreadRandom().NextDouble() - 1;
                    y = 2 * RandomProvider.GetThreadRandom().NextDouble() - 1;
                    square = (x * x) + (y * y);
                } while (square >= 1);

                return x * Math.Sqrt(-2 * Math.Log(square) / square);
            }
        }
        internal static class RandomProvider
        {
            private static ThreadLocal<Random> randomWrapper = new ThreadLocal<Random>(() =>
                new Random(Interlocked.Increment(ref seed))
            );

            private static int seed = Environment.TickCount * 10;

            public static Random GetThreadRandom()
            {
                return randomWrapper.Value;
            }
        }
        public double calldelta(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S + 0.0001, r, vol, t);
            double cp2 = CallPrice(S - 0.0001, r, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double callgamma(double S, double r, double vol, double t)
        {
            return (calldelta(S + 0.01, r, vol, t) - calldelta(S, r, vol, t)) / 0.01;
        }
        public double callvega(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r, vol + 0.0001, t);
            double cp2 = CallPrice(S, r, vol - 0.0001, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double calltheta(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r, vol, t + 0.0001);
            double cp2 = CallPrice(S, r, vol, t - 0.0001);
            return (cp1 - cp2) / 0.0002;
        }
        public double callrho(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r + 0.0001, vol, t);
            double cp2 = CallPrice(S, r - 0.0001, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double putdelta(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S + 0.0001, r, vol, t);
            double cp2 = PutPrice(S - 0.0001, r, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double putgamma(double S, double r, double vol, double t)
        {
            return (putdelta(S + 0.01, r, vol, t) - putdelta(S, r, vol, t)) / 0.01;
        }
        public double putvega(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r, vol + 0.0001, t);
            double cp2 = PutPrice(S, r, vol - 0.0001, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double puttheta(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r, vol, t + 0.0001);
            double cp2 = PutPrice(S, r, vol, t - 0.0001);
            return (cp1 - cp2) / 0.0002;
        }
        public double putrho(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r + 0.0001, vol, t);
            double cp2 = PutPrice(S, r - 0.0001, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
    }

    public class BarrierUAOMT : PreparationMT, CallPrice, StandardError, PutPrice, CallGreek, PutGreek
    {
        public static double[,] RandomList;
        public void Initial()
        {
            optionprice = new double[Value.trials, Value.step + 1];
            Option = new List<double>();
        }
        public void RandomGenerated()
        {
            RandomList = new double[Value.trials, Value.step];

            Parallel.ForEach(Ienum.Step(0, Value.trials, 1), new ParallelOptions { MaxDegreeOfParallelism = Value.Thread }, i =>
            {
                for (int k = 0; k < Value.step; k++)
                {
                    RandomList[i, k] = GaussianBoxMuller.NextDouble();
                }
            });
        }
        public double CallPrice(double S, double r, double vol, double t)
        {
            Initial();
            Parallel.ForEach(Ienum.Step(0, Value.trials, 1), new ParallelOptions { MaxDegreeOfParallelism = Value.Thread }, i =>
            {
                optionprice[i, 0] = S;
                for (uint j = 1; j < Value.step + 1; j++)
                {
                    optionprice[i, j] = Price(optionprice[i, j - 1], r, vol, t, RandomList[i, j - 1]);
                }
            });
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                double[] m = new double[Value.step + 1];
                for (uint j = 0; j < Value.step + 1; j++)
                {
                    m[j] = optionprice[i, j];
                }
                if (m.Max() <= Value.barrier)
                { Option.Add(Math.Max(optionprice[i, Value.step] - Value.K, 0)); }
                else { Option.Add(0); }
            }
            double price = 0;
            price = Math.Exp(-r * t) * Option.Average();
            Program.increase(1);
            return price;
        }
        public double PutPrice(double S, double r, double vol, double t)
        {
            Initial();
            Parallel.ForEach(Ienum.Step(0, Value.trials, 1), new ParallelOptions { MaxDegreeOfParallelism = Value.Thread }, i =>
            {
                optionprice[i, 0] = S;
                for (uint j = 1; j < Value.step + 1; j++)
                {
                    optionprice[i, j] = Price(optionprice[i, j - 1], r, vol, t, RandomList[i, j - 1]);
                }
            });
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                double[] m = new double[Value.step + 1];
                for (uint j = 0; j < Value.step + 1; j++)
                {
                    m[j] = optionprice[i, j];
                }
                if (m.Max() <= Value.barrier)
                { Option.Add(Math.Max(Value.K - optionprice[i, Value.step], 0)); }
                else { Option.Add(0); }
            }
            double price = 0;
            price = Math.Exp(-r * t) * Option.Average();
            Program.increase(1);
            return price;
        }
        public double SE()
        {
            double avg = Option.Average();
            double sum = Option.Sum(v => Math.Pow(v - avg, 2));
            return (Math.Sqrt(sum / (Option.Count - 1))) / (Math.Sqrt(Option.Count));
        }
        internal static class GaussianBoxMuller
        {
            public static double NextDouble()
            {
                double x, y, square;

                do
                {
                    x = 2 * RandomProvider.GetThreadRandom().NextDouble() - 1;
                    y = 2 * RandomProvider.GetThreadRandom().NextDouble() - 1;
                    square = (x * x) + (y * y);
                } while (square >= 1);

                return x * Math.Sqrt(-2 * Math.Log(square) / square);
            }
        }
        internal static class RandomProvider
        {
            private static ThreadLocal<Random> randomWrapper = new ThreadLocal<Random>(() =>
                new Random(Interlocked.Increment(ref seed))
            );

            private static int seed = Environment.TickCount * 10;

            public static Random GetThreadRandom()
            {
                return randomWrapper.Value;
            }
        }
        public double calldelta(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S + 0.0001, r, vol, t);
            double cp2 = CallPrice(S - 0.0001, r, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double callgamma(double S, double r, double vol, double t)
        {
            return (calldelta(S + 0.01, r, vol, t) - calldelta(S, r, vol, t)) / 0.01;
        }
        public double callvega(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r, vol + 0.0001, t);
            double cp2 = CallPrice(S, r, vol - 0.0001, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double calltheta(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r, vol, t + 0.0001);
            double cp2 = CallPrice(S, r, vol, t - 0.0001);
            return (cp1 - cp2) / 0.0002;
        }
        public double callrho(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r + 0.0001, vol, t);
            double cp2 = CallPrice(S, r - 0.0001, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double putdelta(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S + 0.0001, r, vol, t);
            double cp2 = PutPrice(S - 0.0001, r, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double putgamma(double S, double r, double vol, double t)
        {
            return (putdelta(S + 0.01, r, vol, t) - putdelta(S, r, vol, t)) / 0.01;
        }
        public double putvega(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r, vol + 0.0001, t);
            double cp2 = PutPrice(S, r, vol - 0.0001, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double puttheta(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r, vol, t + 0.0001);
            double cp2 = PutPrice(S, r, vol, t - 0.0001);
            return (cp1 - cp2) / 0.0002;
        }
        public double putrho(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r + 0.0001, vol, t);
            double cp2 = PutPrice(S, r - 0.0001, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
    }

    public class BarrierUAIMT : PreparationMT, CallPrice, StandardError, PutPrice, CallGreek, PutGreek
    {
        public static double[,] RandomList;
        public void Initial()
        {
            optionprice = new double[Value.trials, Value.step + 1];
            Option = new List<double>();
        }
        public void RandomGenerated()
        {
            RandomList = new double[Value.trials, Value.step];

            Parallel.ForEach(Ienum.Step(0, Value.trials, 1), new ParallelOptions { MaxDegreeOfParallelism = Value.Thread }, i =>
            {
                for (int k = 0; k < Value.step; k++)
                {
                    RandomList[i, k] = GaussianBoxMuller.NextDouble();
                }
            });
        }
        public double CallPrice(double S, double r, double vol, double t)
        {
            Initial();
            Parallel.ForEach(Ienum.Step(0, Value.trials, 1), new ParallelOptions { MaxDegreeOfParallelism = Value.Thread }, i =>
            {
                optionprice[i, 0] = S;
                for (uint j = 1; j < Value.step + 1; j++)
                {
                    optionprice[i, j] = Price(optionprice[i, j - 1], r, vol, t, RandomList[i, j - 1]);
                }
            });
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                double[] m = new double[Value.step + 1];
                for (uint j = 0; j < Value.step + 1; j++)
                {
                    m[j] = optionprice[i, j];
                }
                if (m.Max() > Value.barrier)
                { Option.Add(Math.Max(optionprice[i, Value.step] - Value.K, 0)); }
                else { Option.Add(0); }
            }
            double price = 0;
            price = Math.Exp(-r * t) * Option.Average();
            Program.increase(1);
            return price;
        }
        public double PutPrice(double S, double r, double vol, double t)
        {
            Initial();
            Parallel.ForEach(Ienum.Step(0, Value.trials, 1), new ParallelOptions { MaxDegreeOfParallelism = Value.Thread }, i =>
            {
                optionprice[i, 0] = S;
                for (uint j = 1; j < Value.step + 1; j++)
                {
                    optionprice[i, j] = Price(optionprice[i, j - 1], r, vol, t, RandomList[i, j - 1]);
                }
            });
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                double[] m = new double[Value.step + 1];
                for (uint j = 0; j < Value.step + 1; j++)
                {
                    m[j] = optionprice[i, j];
                }
                if (m.Max() > Value.barrier)
                { Option.Add(Math.Max(Value.K - optionprice[i, Value.step], 0)); }
                else { Option.Add(0); }
            }
            double price = 0;
            price = Math.Exp(-r * t) * Option.Average();
            Program.increase(1);
            return price;
        }
        public double SE()
        {
            double avg = Option.Average();
            double sum = Option.Sum(v => Math.Pow(v - avg, 2));
            return (Math.Sqrt(sum / (Option.Count - 1))) / (Math.Sqrt(Option.Count));
        }
        internal static class GaussianBoxMuller
        {
            public static double NextDouble()
            {
                double x, y, square;

                do
                {
                    x = 2 * RandomProvider.GetThreadRandom().NextDouble() - 1;
                    y = 2 * RandomProvider.GetThreadRandom().NextDouble() - 1;
                    square = (x * x) + (y * y);
                } while (square >= 1);

                return x * Math.Sqrt(-2 * Math.Log(square) / square);
            }
        }
        internal static class RandomProvider
        {
            private static ThreadLocal<Random> randomWrapper = new ThreadLocal<Random>(() =>
                new Random(Interlocked.Increment(ref seed))
            );

            private static int seed = Environment.TickCount * 10;

            public static Random GetThreadRandom()
            {
                return randomWrapper.Value;
            }
        }
        public double calldelta(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S + 0.0001, r, vol, t);
            double cp2 = CallPrice(S - 0.0001, r, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double callgamma(double S, double r, double vol, double t)
        {
            return (calldelta(S + 0.01, r, vol, t) - calldelta(S, r, vol, t)) / 0.01;
        }
        public double callvega(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r, vol + 0.0001, t);
            double cp2 = CallPrice(S, r, vol - 0.0001, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double calltheta(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r, vol, t + 0.0001);
            double cp2 = CallPrice(S, r, vol, t - 0.0001);
            return (cp1 - cp2) / 0.0002;
        }
        public double callrho(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r + 0.0001, vol, t);
            double cp2 = CallPrice(S, r - 0.0001, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double putdelta(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S + 0.0001, r, vol, t);
            double cp2 = PutPrice(S - 0.0001, r, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double putgamma(double S, double r, double vol, double t)
        {
            return (putdelta(S + 0.01, r, vol, t) - putdelta(S, r, vol, t)) / 0.01;
        }
        public double putvega(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r, vol + 0.0001, t);
            double cp2 = PutPrice(S, r, vol - 0.0001, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double puttheta(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r, vol, t + 0.0001);
            double cp2 = PutPrice(S, r, vol, t - 0.0001);
            return (cp1 - cp2) / 0.0002;
        }
        public double putrho(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r + 0.0001, vol, t);
            double cp2 = PutPrice(S, r - 0.0001, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
    }

    public class LookbackMT: PreparationMT, CallPrice, StandardError, PutPrice, CallGreek, PutGreek
    {
        public static double[,] RandomList;
        public void Initial()
        {
            optionprice = new double[Value.trials, Value.step + 1];
            Option = new List<double>();
        }
        public void RandomGenerated()
        {
            RandomList = new double[Value.trials, Value.step];

            Parallel.ForEach(Ienum.Step(0, Value.trials, 1), new ParallelOptions { MaxDegreeOfParallelism = Value.Thread }, i =>
            {
                for (int k = 0; k < Value.step; k++)
                {
                    RandomList[i, k] = GaussianBoxMuller.NextDouble();
                }
            });
        }
        public double CallPrice(double S, double r, double vol, double t)
        {
            Initial();
            Parallel.ForEach(Ienum.Step(0, Value.trials, 1), new ParallelOptions { MaxDegreeOfParallelism = Value.Thread }, i =>
            {
                optionprice[i, 0] = S;
                for (uint j = 1; j < Value.step + 1; j++)
                {
                    optionprice[i, j] = Price(optionprice[i, j - 1], r, vol, t, RandomList[i, j - 1]);
                }
            });
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                double[] m = new double[Value.step + 1];
                for (uint j = 0; j < Value.step + 1; j++)
                {
                    m[j] = optionprice[i, j];
                }
                Option.Add(Math.Max(m.Max() - Value.K, 0));
            }
            double price = 0;
            price = Math.Exp(-r * t) * Option.Average();
            Program.increase(1);
            return price;
        }
        public double PutPrice(double S, double r, double vol, double t)
        {
            Initial();
            Parallel.ForEach(Ienum.Step(0, Value.trials, 1), new ParallelOptions { MaxDegreeOfParallelism = Value.Thread }, i =>
            {
                optionprice[i, 0] = S;
                for (uint j = 1; j < Value.step + 1; j++)
                {
                    optionprice[i, j] = Price(optionprice[i, j - 1], r, vol, t, RandomList[i, j - 1]);
                }
            });
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                double[] m = new double[Value.step + 1];
                for (uint j = 0; j < Value.step + 1; j++)
                {
                    m[j] = optionprice[i, j];
                }
                Option.Add(Math.Max(Value.K - m.Min(), 0));
            }
            double price = 0;
            price = Math.Exp(-r * t) * Option.Average();
            Program.increase(1);
            return price;
        }
        public double SE()
        {
            double avg = Option.Average();
            double sum = Option.Sum(v => Math.Pow(v - avg, 2));
            return (Math.Sqrt(sum / (Option.Count - 1))) / (Math.Sqrt(Option.Count));
        }
        internal static class GaussianBoxMuller
        {
            public static double NextDouble()
            {
                double x, y, square;

                do
                {
                    x = 2 * RandomProvider.GetThreadRandom().NextDouble() - 1;
                    y = 2 * RandomProvider.GetThreadRandom().NextDouble() - 1;
                    square = (x * x) + (y * y);
                } while (square >= 1);

                return x * Math.Sqrt(-2 * Math.Log(square) / square);
            }
        }
        internal static class RandomProvider
        {
            private static ThreadLocal<Random> randomWrapper = new ThreadLocal<Random>(() =>
                new Random(Interlocked.Increment(ref seed))
            );

            private static int seed = Environment.TickCount * 10;

            public static Random GetThreadRandom()
            {
                return randomWrapper.Value;
            }
        }
        public double calldelta(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S + 0.0001, r, vol, t);
            double cp2 = CallPrice(S - 0.0001, r, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double callgamma(double S, double r, double vol, double t)
        {
            return (calldelta(S + 0.01, r, vol, t) - calldelta(S, r, vol, t)) / 0.01;
        }
        public double callvega(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r, vol + 0.0001, t);
            double cp2 = CallPrice(S, r, vol - 0.0001, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double calltheta(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r, vol, t + 0.0001);
            double cp2 = CallPrice(S, r, vol, t - 0.0001);
            return (cp1 - cp2) / 0.0002;
        }
        public double callrho(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r + 0.0001, vol, t);
            double cp2 = CallPrice(S, r - 0.0001, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double putdelta(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S + 0.0001, r, vol, t);
            double cp2 = PutPrice(S - 0.0001, r, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double putgamma(double S, double r, double vol, double t)
        {
            return (putdelta(S + 0.01, r, vol, t) - putdelta(S, r, vol, t)) / 0.01;
        }
        public double putvega(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r, vol + 0.0001, t);
            double cp2 = PutPrice(S, r, vol - 0.0001, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double puttheta(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r, vol, t + 0.0001);
            double cp2 = PutPrice(S, r, vol, t - 0.0001);
            return (cp1 - cp2) / 0.0002;
        }
        public double putrho(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r + 0.0001, vol, t);
            double cp2 = PutPrice(S, r - 0.0001, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
    }

    public class RangeMT : PreparationMT, CallPrice, StandardError, PutPrice, CallGreek, PutGreek
    {
        public static double[,] RandomList;
        public void Initial()
        {
            optionprice = new double[Value.trials, Value.step + 1];
            Option = new List<double>();
        }
        public void RandomGenerated()
        {
            RandomList = new double[Value.trials, Value.step];

            Parallel.ForEach(Ienum.Step(0, Value.trials, 1), new ParallelOptions { MaxDegreeOfParallelism = Value.Thread }, i =>
            {
                for (int k = 0; k < Value.step; k++)
                {
                    RandomList[i, k] = GaussianBoxMuller.NextDouble();
                }
            });
        }
        public double CallPrice(double S, double r, double vol, double t)
        {
            Initial();
            Parallel.ForEach(Ienum.Step(0, Value.trials, 1), new ParallelOptions { MaxDegreeOfParallelism = Value.Thread }, i =>
            {
                optionprice[i, 0] = S;
                for (uint j = 1; j < Value.step + 1; j++)
                {
                    optionprice[i, j] = Price(optionprice[i, j - 1], r, vol, t, RandomList[i, j - 1]);
                }
            });
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                double[] m = new double[Value.step + 1];
                for (uint j = 0; j < Value.step + 1; j++)
                {
                    m[j] = optionprice[i, j];
                }
                Option.Add(m.Max() - m.Min());
            }
            double price = 0;
            price = Math.Exp(-r * t) * Option.Average();
            Program.increase(1);
            return price;
        }
        public double PutPrice(double S, double r, double vol, double t)
        {
            Initial();
            Parallel.ForEach(Ienum.Step(0, Value.trials, 1), new ParallelOptions { MaxDegreeOfParallelism = Value.Thread }, i =>
            {
                optionprice[i, 0] = S;
                for (uint j = 1; j < Value.step + 1; j++)
                {
                    optionprice[i, j] = Price(optionprice[i, j - 1], r, vol, t, RandomList[i, j - 1]);
                }
            });
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                double[] m = new double[Value.step + 1];
                for (uint j = 0; j < Value.step + 1; j++)
                {
                    m[j] = optionprice[i, j];
                }
                Option.Add(m.Max() - m.Min());
            }
            double price = 0;
            price = Math.Exp(-r * t) * Option.Average();
            Program.increase(1);
            return price;
        }
        public double SE()
        {
            double avg = Option.Average();
            double sum = Option.Sum(v => Math.Pow(v - avg, 2));
            return (Math.Sqrt(sum / (Option.Count - 1))) / (Math.Sqrt(Option.Count));
        }
        internal static class GaussianBoxMuller
        {
            public static double NextDouble()
            {
                double x, y, square;

                do
                {
                    x = 2 * RandomProvider.GetThreadRandom().NextDouble() - 1;
                    y = 2 * RandomProvider.GetThreadRandom().NextDouble() - 1;
                    square = (x * x) + (y * y);
                } while (square >= 1);

                return x * Math.Sqrt(-2 * Math.Log(square) / square);
            }
        }
        internal static class RandomProvider
        {
            private static ThreadLocal<Random> randomWrapper = new ThreadLocal<Random>(() =>
                new Random(Interlocked.Increment(ref seed))
            );

            private static int seed = Environment.TickCount * 10;

            public static Random GetThreadRandom()
            {
                return randomWrapper.Value;
            }
        }
        public double calldelta(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S + 0.0001, r, vol, t);
            double cp2 = CallPrice(S - 0.0001, r, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double callgamma(double S, double r, double vol, double t)
        {
            return (calldelta(S + 0.01, r, vol, t) - calldelta(S, r, vol, t)) / 0.01;
        }
        public double callvega(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r, vol + 0.0001, t);
            double cp2 = CallPrice(S, r, vol - 0.0001, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double calltheta(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r, vol, t + 0.0001);
            double cp2 = CallPrice(S, r, vol, t - 0.0001);
            return (cp1 - cp2) / 0.0002;
        }
        public double callrho(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r + 0.0001, vol, t);
            double cp2 = CallPrice(S, r - 0.0001, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double putdelta(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S + 0.0001, r, vol, t);
            double cp2 = PutPrice(S - 0.0001, r, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double putgamma(double S, double r, double vol, double t)
        {
            return (putdelta(S + 0.01, r, vol, t) - putdelta(S, r, vol, t)) / 0.01;
        }
        public double putvega(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r, vol + 0.0001, t);
            double cp2 = PutPrice(S, r, vol - 0.0001, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double puttheta(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r, vol, t + 0.0001);
            double cp2 = PutPrice(S, r, vol, t - 0.0001);
            return (cp1 - cp2) / 0.0002;
        }
        public double putrho(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r + 0.0001, vol, t);
            double cp2 = PutPrice(S, r - 0.0001, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
    }
}
