﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Diagnostics;

namespace ConsoleApp6
{
    public class BarrierPresent
    {
        Stopwatch watch = new Stopwatch();

        public void BarrierDAOCall()
        {
            BarrierDAO euro = new BarrierDAO();
            watch.Start();
            euro.RandomGenerated();
            Value.Price = euro.CallPrice(Value.S, Value.r, Value.vol, Value.t);
            Value.SE = euro.SE();
            Value.delta = euro.calldelta(Value.S, Value.r, Value.vol, Value.t);
            Value.gamma = euro.callgamma(Value.S, Value.r, Value.vol, Value.t);
            Value.theta = euro.calltheta(Value.S, Value.r, Value.vol, Value.t);
            Value.vega = euro.callvega(Value.S, Value.r, Value.vol, Value.t);
            Value.rho = euro.callrho(Value.S, Value.r, Value.vol, Value.t);
            Value.Thread = 1;
            watch.Stop();
            Value.timer = watch.Elapsed.Seconds.ToString() + " : " + watch.Elapsed.Milliseconds.ToString();
            watch.Reset();
            Program.finished();
            GC.Collect();
        }
        public void BarrierDAOPut()
        {
            BarrierDAO euro = new BarrierDAO();
            watch.Start();
            euro.RandomGenerated();
            Value.Price = euro.PutPrice(Value.S, Value.r, Value.vol, Value.t);
            Value.SE = euro.SE();
            Value.delta = euro.putdelta(Value.S, Value.r, Value.vol, Value.t);
            Value.gamma = euro.putgamma(Value.S, Value.r, Value.vol, Value.t);
            Value.theta = euro.puttheta(Value.S, Value.r, Value.vol, Value.t);
            Value.vega = euro.putvega(Value.S, Value.r, Value.vol, Value.t);
            Value.rho = euro.putrho(Value.S, Value.r, Value.vol, Value.t);
            Value.Thread = 1;
            watch.Stop();
            Value.timer = watch.Elapsed.Seconds.ToString() + " : " + watch.Elapsed.Milliseconds.ToString();
            watch.Reset();
            Program.finished();
            GC.Collect();
        }
        public void BarrierDAOCallAnti()
        {
            BarrierDAOAntithetic euro = new BarrierDAOAntithetic();
            watch.Start();
            euro.RandomGenerated();
            Value.Price = euro.CallPrice(Value.S, Value.r, Value.vol, Value.t);
            Value.SE = euro.SE();
            Value.delta = euro.calldelta(Value.S, Value.r, Value.vol, Value.t);
            Value.gamma = euro.callgamma(Value.S, Value.r, Value.vol, Value.t);
            Value.theta = euro.calltheta(Value.S, Value.r, Value.vol, Value.t);
            Value.vega = euro.callvega(Value.S, Value.r, Value.vol, Value.t);
            Value.rho = euro.callrho(Value.S, Value.r, Value.vol, Value.t);
            Value.Thread = 1;
            watch.Stop();
            Value.timer = watch.Elapsed.Seconds.ToString() + " : " + watch.Elapsed.Milliseconds.ToString();
            watch.Reset();
            Program.finished();
            GC.Collect();
        }
        public void BarrierDAOPutAnti()
        {
            BarrierDAOAntithetic euro = new BarrierDAOAntithetic();
            watch.Start();
            euro.RandomGenerated();
            Value.Price = euro.PutPrice(Value.S, Value.r, Value.vol, Value.t);
            Value.SE = euro.SE();
            Value.delta = euro.putdelta(Value.S, Value.r, Value.vol, Value.t);
            Value.gamma = euro.putgamma(Value.S, Value.r, Value.vol, Value.t);
            Value.theta = euro.puttheta(Value.S, Value.r, Value.vol, Value.t);
            Value.vega = euro.putvega(Value.S, Value.r, Value.vol, Value.t);
            Value.rho = euro.putrho(Value.S, Value.r, Value.vol, Value.t);
            Value.Thread = 1;
            watch.Stop();
            Value.timer = watch.Elapsed.Seconds.ToString() + " : " + watch.Elapsed.Milliseconds.ToString();
            watch.Reset();
            Program.finished();
            GC.Collect();
        }
        public void BarrierDAOCallCV()
        {
            BarrierDAOCV euro = new BarrierDAOCV();
            watch.Start();
            euro.RandomGenerated();
            Value.Price = euro.CallPrice(Value.S, Value.r, Value.vol, Value.t);
            Value.SE = euro.SE();
            Value.delta = euro.calldelta(Value.S, Value.r, Value.vol, Value.t);
            Value.gamma = euro.callgamma(Value.S, Value.r, Value.vol, Value.t);
            Value.theta = euro.calltheta(Value.S, Value.r, Value.vol, Value.t);
            Value.vega = euro.callvega(Value.S, Value.r, Value.vol, Value.t);
            Value.rho = euro.callrho(Value.S, Value.r, Value.vol, Value.t);
            Value.Thread = 1;
            watch.Stop();
            Value.timer = watch.Elapsed.Seconds.ToString() + " : " + watch.Elapsed.Milliseconds.ToString();
            watch.Reset();
            Program.finished();
            GC.Collect();
        }
        public void BarrierDAOPutCV()
        {
            BarrierDAOCV euro = new BarrierDAOCV();
            watch.Start();
            euro.RandomGenerated();
            Value.Price = euro.PutPrice(Value.S, Value.r, Value.vol, Value.t);
            Value.SE = euro.SE();
            Value.delta = euro.putdelta(Value.S, Value.r, Value.vol, Value.t);
            Value.gamma = euro.putgamma(Value.S, Value.r, Value.vol, Value.t);
            Value.theta = euro.puttheta(Value.S, Value.r, Value.vol, Value.t);
            Value.vega = euro.putvega(Value.S, Value.r, Value.vol, Value.t);
            Value.rho = euro.putrho(Value.S, Value.r, Value.vol, Value.t);
            Value.Thread = 1;
            watch.Stop();
            Value.timer = watch.Elapsed.Seconds.ToString() + " : " + watch.Elapsed.Milliseconds.ToString();
            watch.Reset();
            Program.finished();
            GC.Collect();
        }
        public void BarrierDAOCallAntiCV()
        {
            BarrierDAOAntiCV euro = new BarrierDAOAntiCV();
            watch.Start();
            euro.RandomGenerated();
            Value.Price = euro.CallPrice(Value.S, Value.r, Value.vol, Value.t);
            Value.SE = euro.SE();
            Value.delta = euro.calldelta(Value.S, Value.r, Value.vol, Value.t);
            Value.gamma = euro.callgamma(Value.S, Value.r, Value.vol, Value.t);
            Value.theta = euro.calltheta(Value.S, Value.r, Value.vol, Value.t);
            Value.vega = euro.callvega(Value.S, Value.r, Value.vol, Value.t);
            Value.rho = euro.callrho(Value.S, Value.r, Value.vol, Value.t);
            Value.Thread = 1;
            watch.Stop();
            Value.timer = watch.Elapsed.Seconds.ToString() + " : " + watch.Elapsed.Milliseconds.ToString();
            watch.Reset();
            Program.finished();
            GC.Collect();
        }
        public void BarrierDAOPutAntiCV()
        {
            BarrierDAOAntiCV euro = new BarrierDAOAntiCV();
            watch.Start();
            euro.RandomGenerated();
            Value.Price = euro.PutPrice(Value.S, Value.r, Value.vol, Value.t);
            Value.SE = euro.SE();
            Value.delta = euro.putdelta(Value.S, Value.r, Value.vol, Value.t);
            Value.gamma = euro.putgamma(Value.S, Value.r, Value.vol, Value.t);
            Value.theta = euro.puttheta(Value.S, Value.r, Value.vol, Value.t);
            Value.vega = euro.putvega(Value.S, Value.r, Value.vol, Value.t);
            Value.rho = euro.putrho(Value.S, Value.r, Value.vol, Value.t);
            Value.Thread = 1;
            watch.Stop();
            Value.timer = watch.Elapsed.Seconds.ToString() + " : " + watch.Elapsed.Milliseconds.ToString();
            watch.Reset();
            Program.finished();
            GC.Collect();
        }
        public void BarrierDAOCallMT()
        {
            BarrierDAOMT euro = new BarrierDAOMT();
            watch.Start();
            euro.RandomGenerated();
            Value.Price = euro.CallPrice(Value.S, Value.r, Value.vol, Value.t);
            Value.SE = euro.SE();
            Value.delta = euro.calldelta(Value.S, Value.r, Value.vol, Value.t);
            Value.gamma = euro.callgamma(Value.S, Value.r, Value.vol, Value.t);
            Value.theta = euro.calltheta(Value.S, Value.r, Value.vol, Value.t);
            Value.vega = euro.callvega(Value.S, Value.r, Value.vol, Value.t);
            Value.rho = euro.callrho(Value.S, Value.r, Value.vol, Value.t);
            Value.Thread = 4;
            watch.Stop();
            Value.timer = watch.Elapsed.Seconds.ToString() + " : " + watch.Elapsed.Milliseconds.ToString();
            watch.Reset();
            Program.finished();
            GC.Collect();
        }
        public void BarrierDAOPutMT()
        {
            BarrierDAOMT euro = new BarrierDAOMT();
            watch.Start();
            euro.RandomGenerated();
            Value.Price = euro.PutPrice(Value.S, Value.r, Value.vol, Value.t);
            Value.SE = euro.SE();
            Value.delta = euro.putdelta(Value.S, Value.r, Value.vol, Value.t);
            Value.gamma = euro.putgamma(Value.S, Value.r, Value.vol, Value.t);
            Value.theta = euro.puttheta(Value.S, Value.r, Value.vol, Value.t);
            Value.vega = euro.putvega(Value.S, Value.r, Value.vol, Value.t);
            Value.rho = euro.putrho(Value.S, Value.r, Value.vol, Value.t);
            Value.Thread = 4;
            watch.Stop();
            Value.timer = watch.Elapsed.Seconds.ToString() + " : " + watch.Elapsed.Milliseconds.ToString();
            watch.Reset();
            Program.finished();
            GC.Collect();
        }
        public void BarrierDAOCallAntiMT()
        {
            BarrierDAOAntiMT euro = new BarrierDAOAntiMT();
            watch.Start();
            euro.RandomGenerated();
            Value.Price = euro.CallPrice(Value.S, Value.r, Value.vol, Value.t);
            Value.SE = euro.SE();
            Value.delta = euro.calldelta(Value.S, Value.r, Value.vol, Value.t);
            Value.gamma = euro.callgamma(Value.S, Value.r, Value.vol, Value.t);
            Value.theta = euro.calltheta(Value.S, Value.r, Value.vol, Value.t);
            Value.vega = euro.callvega(Value.S, Value.r, Value.vol, Value.t);
            Value.rho = euro.callrho(Value.S, Value.r, Value.vol, Value.t);
            Value.Thread = 4;
            watch.Stop();
            Value.timer = watch.Elapsed.Seconds.ToString() + " : " + watch.Elapsed.Milliseconds.ToString();
            watch.Reset();
            Program.finished();
            GC.Collect();
        }
        public void BarrierDAOPutAntiMT()
        {
            BarrierDAOAntiMT euro = new BarrierDAOAntiMT();
            watch.Start();
            euro.RandomGenerated();
            Value.Price = euro.PutPrice(Value.S, Value.r, Value.vol, Value.t);
            Value.SE = euro.SE();
            Value.delta = euro.putdelta(Value.S, Value.r, Value.vol, Value.t);
            Value.gamma = euro.putgamma(Value.S, Value.r, Value.vol, Value.t);
            Value.theta = euro.puttheta(Value.S, Value.r, Value.vol, Value.t);
            Value.vega = euro.putvega(Value.S, Value.r, Value.vol, Value.t);
            Value.rho = euro.putrho(Value.S, Value.r, Value.vol, Value.t);
            Value.Thread = 4;
            watch.Stop();
            Value.timer = watch.Elapsed.Seconds.ToString() + " : " + watch.Elapsed.Milliseconds.ToString();
            watch.Reset();
            Program.finished();
            GC.Collect();
        }
        public void BarrierDAOCallCVMT()
        {
            BarrierDAOCVMT euro = new BarrierDAOCVMT();
            watch.Start();
            euro.RandomGenerated();
            Value.Price = euro.CallPrice(Value.S, Value.r, Value.vol, Value.t);
            Value.SE = euro.SE();
            Value.delta = euro.calldelta(Value.S, Value.r, Value.vol, Value.t);
            Value.gamma = euro.callgamma(Value.S, Value.r, Value.vol, Value.t);
            Value.theta = euro.calltheta(Value.S, Value.r, Value.vol, Value.t);
            Value.vega = euro.callvega(Value.S, Value.r, Value.vol, Value.t);
            Value.rho = euro.callrho(Value.S, Value.r, Value.vol, Value.t);
            Value.Thread = 4;
            watch.Stop();
            Value.timer = watch.Elapsed.Seconds.ToString() + " : " + watch.Elapsed.Milliseconds.ToString();
            watch.Reset();
            Program.finished();
            GC.Collect();
        }
        public void BarrierDAOPutCVMT()
        {
            BarrierDAOCVMT euro = new BarrierDAOCVMT();
            watch.Start();
            euro.RandomGenerated();
            Value.Price = euro.PutPrice(Value.S, Value.r, Value.vol, Value.t);
            Value.SE = euro.SE();
            Value.delta = euro.putdelta(Value.S, Value.r, Value.vol, Value.t);
            Value.gamma = euro.putgamma(Value.S, Value.r, Value.vol, Value.t);
            Value.theta = euro.puttheta(Value.S, Value.r, Value.vol, Value.t);
            Value.vega = euro.putvega(Value.S, Value.r, Value.vol, Value.t);
            Value.rho = euro.putrho(Value.S, Value.r, Value.vol, Value.t);
            Value.Thread = 4;
            watch.Stop();
            Value.timer = watch.Elapsed.Seconds.ToString() + " : " + watch.Elapsed.Milliseconds.ToString();
            watch.Reset();
            Program.finished();
            GC.Collect();
        }
        public void BarrierDAOCallAntiCVMT()
        {
            BarrierDAOAntiCVMT euro = new BarrierDAOAntiCVMT();
            watch.Start();
            euro.RandomGenerated();
            Value.Price = euro.CallPrice(Value.S, Value.r, Value.vol, Value.t);
            Value.SE = euro.SE();
            Value.delta = euro.calldelta(Value.S, Value.r, Value.vol, Value.t);
            Value.gamma = euro.callgamma(Value.S, Value.r, Value.vol, Value.t);
            Value.theta = euro.calltheta(Value.S, Value.r, Value.vol, Value.t);
            Value.vega = euro.callvega(Value.S, Value.r, Value.vol, Value.t);
            Value.rho = euro.callrho(Value.S, Value.r, Value.vol, Value.t);
            Value.Thread = 4;
            watch.Stop();
            Value.timer = watch.Elapsed.Seconds.ToString() + " : " + watch.Elapsed.Milliseconds.ToString();
            watch.Reset();
            Program.finished();
            GC.Collect();
        }
        public void BarrierDAOPutAntiCVMT()
        {
            BarrierDAOAntiCVMT euro = new BarrierDAOAntiCVMT();
            watch.Start();
            euro.RandomGenerated();
            Value.Price = euro.PutPrice(Value.S, Value.r, Value.vol, Value.t);
            Value.SE = euro.SE();
            Value.delta = euro.putdelta(Value.S, Value.r, Value.vol, Value.t);
            Value.gamma = euro.putgamma(Value.S, Value.r, Value.vol, Value.t);
            Value.theta = euro.puttheta(Value.S, Value.r, Value.vol, Value.t);
            Value.vega = euro.putvega(Value.S, Value.r, Value.vol, Value.t);
            Value.rho = euro.putrho(Value.S, Value.r, Value.vol, Value.t);
            Value.Thread = 4;
            watch.Stop();
            Value.timer = watch.Elapsed.Seconds.ToString() + " : " + watch.Elapsed.Milliseconds.ToString();
            watch.Reset();
            Program.finished();
            GC.Collect();
        }

        public void BarrierDAICall()
        {
            BarrierDAI euro = new BarrierDAI();
            watch.Start();
            euro.RandomGenerated();
            Value.Price = euro.CallPrice(Value.S, Value.r, Value.vol, Value.t);
            Value.SE = euro.SE();
            Value.delta = euro.calldelta(Value.S, Value.r, Value.vol, Value.t);
            Value.gamma = euro.callgamma(Value.S, Value.r, Value.vol, Value.t);
            Value.theta = euro.calltheta(Value.S, Value.r, Value.vol, Value.t);
            Value.vega = euro.callvega(Value.S, Value.r, Value.vol, Value.t);
            Value.rho = euro.callrho(Value.S, Value.r, Value.vol, Value.t);
            Value.Thread = 1;
            watch.Stop();
            Value.timer = watch.Elapsed.Seconds.ToString() + " : " + watch.Elapsed.Milliseconds.ToString();
            watch.Reset();
            Program.finished();
            GC.Collect();
        }
        public void BarrierDAIPut()
        {
            BarrierDAI euro = new BarrierDAI();
            watch.Start();
            euro.RandomGenerated();
            Value.Price = euro.PutPrice(Value.S, Value.r, Value.vol, Value.t);
            Value.SE = euro.SE();
            Value.delta = euro.putdelta(Value.S, Value.r, Value.vol, Value.t);
            Value.gamma = euro.putgamma(Value.S, Value.r, Value.vol, Value.t);
            Value.theta = euro.puttheta(Value.S, Value.r, Value.vol, Value.t);
            Value.vega = euro.putvega(Value.S, Value.r, Value.vol, Value.t);
            Value.rho = euro.putrho(Value.S, Value.r, Value.vol, Value.t);
            Value.Thread = 1;
            watch.Stop();
            Value.timer = watch.Elapsed.Seconds.ToString() + " : " + watch.Elapsed.Milliseconds.ToString();
            watch.Reset();
            Program.finished();
            GC.Collect();
        }
        public void BarrierDAICallAnti()
        {
            BarrierDAIAntithetic euro = new BarrierDAIAntithetic();
            watch.Start();
            euro.RandomGenerated();
            Value.Price = euro.CallPrice(Value.S, Value.r, Value.vol, Value.t);
            Value.SE = euro.SE();
            Value.delta = euro.calldelta(Value.S, Value.r, Value.vol, Value.t);
            Value.gamma = euro.callgamma(Value.S, Value.r, Value.vol, Value.t);
            Value.theta = euro.calltheta(Value.S, Value.r, Value.vol, Value.t);
            Value.vega = euro.callvega(Value.S, Value.r, Value.vol, Value.t);
            Value.rho = euro.callrho(Value.S, Value.r, Value.vol, Value.t);
            Value.Thread = 1;
            watch.Stop();
            Value.timer = watch.Elapsed.Seconds.ToString() + " : " + watch.Elapsed.Milliseconds.ToString();
            watch.Reset();
            Program.finished();
            GC.Collect();
        }
        public void BarrierDAIPutAnti()
        {
            BarrierDAIAntithetic euro = new BarrierDAIAntithetic();
            watch.Start();
            euro.RandomGenerated();
            Value.Price = euro.PutPrice(Value.S, Value.r, Value.vol, Value.t);
            Value.SE = euro.SE();
            Value.delta = euro.putdelta(Value.S, Value.r, Value.vol, Value.t);
            Value.gamma = euro.putgamma(Value.S, Value.r, Value.vol, Value.t);
            Value.theta = euro.puttheta(Value.S, Value.r, Value.vol, Value.t);
            Value.vega = euro.putvega(Value.S, Value.r, Value.vol, Value.t);
            Value.rho = euro.putrho(Value.S, Value.r, Value.vol, Value.t);
            Value.Thread = 1;
            watch.Stop();
            Value.timer = watch.Elapsed.Seconds.ToString() + " : " + watch.Elapsed.Milliseconds.ToString();
            watch.Reset();
            Program.finished();
            GC.Collect();
        }
        public void BarrierDAICallCV()
        {
            BarrierDAICV euro = new BarrierDAICV();
            watch.Start();
            euro.RandomGenerated();
            Value.Price = euro.CallPrice(Value.S, Value.r, Value.vol, Value.t);
            Value.SE = euro.SE();
            Value.delta = euro.calldelta(Value.S, Value.r, Value.vol, Value.t);
            Value.gamma = euro.callgamma(Value.S, Value.r, Value.vol, Value.t);
            Value.theta = euro.calltheta(Value.S, Value.r, Value.vol, Value.t);
            Value.vega = euro.callvega(Value.S, Value.r, Value.vol, Value.t);
            Value.rho = euro.callrho(Value.S, Value.r, Value.vol, Value.t);
            Value.Thread = 1;
            watch.Stop();
            Value.timer = watch.Elapsed.Seconds.ToString() + " : " + watch.Elapsed.Milliseconds.ToString();
            watch.Reset();
            Program.finished();
            GC.Collect();
        }
        public void BarrierDAIPutCV()
        {
            BarrierDAICV euro = new BarrierDAICV();
            watch.Start();
            euro.RandomGenerated();
            Value.Price = euro.PutPrice(Value.S, Value.r, Value.vol, Value.t);
            Value.SE = euro.SE();
            Value.delta = euro.putdelta(Value.S, Value.r, Value.vol, Value.t);
            Value.gamma = euro.putgamma(Value.S, Value.r, Value.vol, Value.t);
            Value.theta = euro.puttheta(Value.S, Value.r, Value.vol, Value.t);
            Value.vega = euro.putvega(Value.S, Value.r, Value.vol, Value.t);
            Value.rho = euro.putrho(Value.S, Value.r, Value.vol, Value.t);
            Value.Thread = 1;
            watch.Stop();
            Value.timer = watch.Elapsed.Seconds.ToString() + " : " + watch.Elapsed.Milliseconds.ToString();
            watch.Reset();
            Program.finished();
            GC.Collect();
        }
        public void BarrierDAICallAntiCV()
        {
            BarrierDAIAntiCV euro = new BarrierDAIAntiCV();
            watch.Start();
            euro.RandomGenerated();
            Value.Price = euro.CallPrice(Value.S, Value.r, Value.vol, Value.t);
            Value.SE = euro.SE();
            Value.delta = euro.calldelta(Value.S, Value.r, Value.vol, Value.t);
            Value.gamma = euro.callgamma(Value.S, Value.r, Value.vol, Value.t);
            Value.theta = euro.calltheta(Value.S, Value.r, Value.vol, Value.t);
            Value.vega = euro.callvega(Value.S, Value.r, Value.vol, Value.t);
            Value.rho = euro.callrho(Value.S, Value.r, Value.vol, Value.t);
            Value.Thread = 1;
            watch.Stop();
            Value.timer = watch.Elapsed.Seconds.ToString() + " : " + watch.Elapsed.Milliseconds.ToString();
            watch.Reset();
            Program.finished();
            GC.Collect();
        }
        public void BarrierDAIPutAntiCV()
        {
            BarrierDAIAntiCV euro = new BarrierDAIAntiCV();
            watch.Start();
            euro.RandomGenerated();
            Value.Price = euro.PutPrice(Value.S, Value.r, Value.vol, Value.t);
            Value.SE = euro.SE();
            Value.delta = euro.putdelta(Value.S, Value.r, Value.vol, Value.t);
            Value.gamma = euro.putgamma(Value.S, Value.r, Value.vol, Value.t);
            Value.theta = euro.puttheta(Value.S, Value.r, Value.vol, Value.t);
            Value.vega = euro.putvega(Value.S, Value.r, Value.vol, Value.t);
            Value.rho = euro.putrho(Value.S, Value.r, Value.vol, Value.t);
            Value.Thread = 1;
            watch.Stop();
            Value.timer = watch.Elapsed.Seconds.ToString() + " : " + watch.Elapsed.Milliseconds.ToString();
            watch.Reset();
            Program.finished();
            GC.Collect();
        }
        public void BarrierDAICallMT()
        {
            BarrierDAIMT euro = new BarrierDAIMT();
            watch.Start();
            euro.RandomGenerated();
            Value.Price = euro.CallPrice(Value.S, Value.r, Value.vol, Value.t);
            Value.SE = euro.SE();
            Value.delta = euro.calldelta(Value.S, Value.r, Value.vol, Value.t);
            Value.gamma = euro.callgamma(Value.S, Value.r, Value.vol, Value.t);
            Value.theta = euro.calltheta(Value.S, Value.r, Value.vol, Value.t);
            Value.vega = euro.callvega(Value.S, Value.r, Value.vol, Value.t);
            Value.rho = euro.callrho(Value.S, Value.r, Value.vol, Value.t);
            Value.Thread = 4;
            watch.Stop();
            Value.timer = watch.Elapsed.Seconds.ToString() + " : " + watch.Elapsed.Milliseconds.ToString();
            watch.Reset();
            Program.finished();
            GC.Collect();
        }
        public void BarrierDAIPutMT()
        {
            BarrierDAIMT euro = new BarrierDAIMT();
            watch.Start();
            euro.RandomGenerated();
            Value.Price = euro.PutPrice(Value.S, Value.r, Value.vol, Value.t);
            Value.SE = euro.SE();
            Value.delta = euro.putdelta(Value.S, Value.r, Value.vol, Value.t);
            Value.gamma = euro.putgamma(Value.S, Value.r, Value.vol, Value.t);
            Value.theta = euro.puttheta(Value.S, Value.r, Value.vol, Value.t);
            Value.vega = euro.putvega(Value.S, Value.r, Value.vol, Value.t);
            Value.rho = euro.putrho(Value.S, Value.r, Value.vol, Value.t);
            Value.Thread = 4;
            watch.Stop();
            Value.timer = watch.Elapsed.Seconds.ToString() + " : " + watch.Elapsed.Milliseconds.ToString();
            watch.Reset();
            Program.finished();
            GC.Collect();
        }
        public void BarrierDAICallAntiMT()
        {
            BarrierDAIAntiMT euro = new BarrierDAIAntiMT();
            watch.Start();
            euro.RandomGenerated();
            Value.Price = euro.CallPrice(Value.S, Value.r, Value.vol, Value.t);
            Value.SE = euro.SE();
            Value.delta = euro.calldelta(Value.S, Value.r, Value.vol, Value.t);
            Value.gamma = euro.callgamma(Value.S, Value.r, Value.vol, Value.t);
            Value.theta = euro.calltheta(Value.S, Value.r, Value.vol, Value.t);
            Value.vega = euro.callvega(Value.S, Value.r, Value.vol, Value.t);
            Value.rho = euro.callrho(Value.S, Value.r, Value.vol, Value.t);
            Value.Thread = 4;
            watch.Stop();
            Value.timer = watch.Elapsed.Seconds.ToString() + " : " + watch.Elapsed.Milliseconds.ToString();
            watch.Reset();
            Program.finished();
            GC.Collect();
        }
        public void BarrierDAIPutAntiMT()
        {
            BarrierDAIAntiMT euro = new BarrierDAIAntiMT();
            watch.Start();
            euro.RandomGenerated();
            Value.Price = euro.PutPrice(Value.S, Value.r, Value.vol, Value.t);
            Value.SE = euro.SE();
            Value.delta = euro.putdelta(Value.S, Value.r, Value.vol, Value.t);
            Value.gamma = euro.putgamma(Value.S, Value.r, Value.vol, Value.t);
            Value.theta = euro.puttheta(Value.S, Value.r, Value.vol, Value.t);
            Value.vega = euro.putvega(Value.S, Value.r, Value.vol, Value.t);
            Value.rho = euro.putrho(Value.S, Value.r, Value.vol, Value.t);
            Value.Thread = 4;
            watch.Stop();
            Value.timer = watch.Elapsed.Seconds.ToString() + " : " + watch.Elapsed.Milliseconds.ToString();
            watch.Reset();
            Program.finished();
            GC.Collect();
        }
        public void BarrierDAICallCVMT()
        {
            BarrierDAICVMT euro = new BarrierDAICVMT();
            watch.Start();
            euro.RandomGenerated();
            Value.Price = euro.CallPrice(Value.S, Value.r, Value.vol, Value.t);
            Value.SE = euro.SE();
            Value.delta = euro.calldelta(Value.S, Value.r, Value.vol, Value.t);
            Value.gamma = euro.callgamma(Value.S, Value.r, Value.vol, Value.t);
            Value.theta = euro.calltheta(Value.S, Value.r, Value.vol, Value.t);
            Value.vega = euro.callvega(Value.S, Value.r, Value.vol, Value.t);
            Value.rho = euro.callrho(Value.S, Value.r, Value.vol, Value.t);
            Value.Thread = 4;
            watch.Stop();
            Value.timer = watch.Elapsed.Seconds.ToString() + " : " + watch.Elapsed.Milliseconds.ToString();
            watch.Reset();
            Program.finished();
            GC.Collect();
        }
        public void BarrierDAIPutCVMT()
        {
            BarrierDAICVMT euro = new BarrierDAICVMT();
            watch.Start();
            euro.RandomGenerated();
            Value.Price = euro.PutPrice(Value.S, Value.r, Value.vol, Value.t);
            Value.SE = euro.SE();
            Value.delta = euro.putdelta(Value.S, Value.r, Value.vol, Value.t);
            Value.gamma = euro.putgamma(Value.S, Value.r, Value.vol, Value.t);
            Value.theta = euro.puttheta(Value.S, Value.r, Value.vol, Value.t);
            Value.vega = euro.putvega(Value.S, Value.r, Value.vol, Value.t);
            Value.rho = euro.putrho(Value.S, Value.r, Value.vol, Value.t);
            Value.Thread = 4;
            watch.Stop();
            Value.timer = watch.Elapsed.Seconds.ToString() + " : " + watch.Elapsed.Milliseconds.ToString();
            watch.Reset();
            Program.finished();
            GC.Collect();
        }
        public void BarrierDAICallAntiCVMT()
        {
            BarrierDAIAntiCVMT euro = new BarrierDAIAntiCVMT();
            watch.Start();
            euro.RandomGenerated();
            Value.Price = euro.CallPrice(Value.S, Value.r, Value.vol, Value.t);
            Value.SE = euro.SE();
            Value.delta = euro.calldelta(Value.S, Value.r, Value.vol, Value.t);
            Value.gamma = euro.callgamma(Value.S, Value.r, Value.vol, Value.t);
            Value.theta = euro.calltheta(Value.S, Value.r, Value.vol, Value.t);
            Value.vega = euro.callvega(Value.S, Value.r, Value.vol, Value.t);
            Value.rho = euro.callrho(Value.S, Value.r, Value.vol, Value.t);
            Value.Thread = 4;
            watch.Stop();
            Value.timer = watch.Elapsed.Seconds.ToString() + " : " + watch.Elapsed.Milliseconds.ToString();
            watch.Reset();
            Program.finished();
            GC.Collect();
        }
        public void BarrierDAIPutAntiCVMT()
        {
            BarrierDAIAntiCVMT euro = new BarrierDAIAntiCVMT();
            watch.Start();
            euro.RandomGenerated();
            Value.Price = euro.PutPrice(Value.S, Value.r, Value.vol, Value.t);
            Value.SE = euro.SE();
            Value.delta = euro.putdelta(Value.S, Value.r, Value.vol, Value.t);
            Value.gamma = euro.putgamma(Value.S, Value.r, Value.vol, Value.t);
            Value.theta = euro.puttheta(Value.S, Value.r, Value.vol, Value.t);
            Value.vega = euro.putvega(Value.S, Value.r, Value.vol, Value.t);
            Value.rho = euro.putrho(Value.S, Value.r, Value.vol, Value.t);
            Value.Thread = 4;
            watch.Stop();
            Value.timer = watch.Elapsed.Seconds.ToString() + " : " + watch.Elapsed.Milliseconds.ToString();
            watch.Reset();
            Program.finished();
            GC.Collect();
        }

        public void BarrierUAOCall()
        {
            BarrierUAO euro = new BarrierUAO();
            watch.Start();
            euro.RandomGenerated();
            Value.Price = euro.CallPrice(Value.S, Value.r, Value.vol, Value.t);
            Value.SE = euro.SE();
            Value.delta = euro.calldelta(Value.S, Value.r, Value.vol, Value.t);
            Value.gamma = euro.callgamma(Value.S, Value.r, Value.vol, Value.t);
            Value.theta = euro.calltheta(Value.S, Value.r, Value.vol, Value.t);
            Value.vega = euro.callvega(Value.S, Value.r, Value.vol, Value.t);
            Value.rho = euro.callrho(Value.S, Value.r, Value.vol, Value.t);
            Value.Thread = 1;
            watch.Stop();
            Value.timer = watch.Elapsed.Seconds.ToString() + " : " + watch.Elapsed.Milliseconds.ToString();
            watch.Reset();
            Program.finished();
            GC.Collect();
        }
        public void BarrierUAOPut()
        {
            BarrierUAO euro = new BarrierUAO();
            watch.Start();
            euro.RandomGenerated();
            Value.Price = euro.PutPrice(Value.S, Value.r, Value.vol, Value.t);
            Value.SE = euro.SE();
            Value.delta = euro.putdelta(Value.S, Value.r, Value.vol, Value.t);
            Value.gamma = euro.putgamma(Value.S, Value.r, Value.vol, Value.t);
            Value.theta = euro.puttheta(Value.S, Value.r, Value.vol, Value.t);
            Value.vega = euro.putvega(Value.S, Value.r, Value.vol, Value.t);
            Value.rho = euro.putrho(Value.S, Value.r, Value.vol, Value.t);
            Value.Thread = 1;
            watch.Stop();
            Value.timer = watch.Elapsed.Seconds.ToString() + " : " + watch.Elapsed.Milliseconds.ToString();
            watch.Reset();
            Program.finished();
            GC.Collect();
        }
        public void BarrierUAOCallAnti()
        {
            BarrierUAOAntithetic euro = new BarrierUAOAntithetic();
            watch.Start();
            euro.RandomGenerated();
            Value.Price = euro.CallPrice(Value.S, Value.r, Value.vol, Value.t);
            Value.SE = euro.SE();
            Value.delta = euro.calldelta(Value.S, Value.r, Value.vol, Value.t);
            Value.gamma = euro.callgamma(Value.S, Value.r, Value.vol, Value.t);
            Value.theta = euro.calltheta(Value.S, Value.r, Value.vol, Value.t);
            Value.vega = euro.callvega(Value.S, Value.r, Value.vol, Value.t);
            Value.rho = euro.callrho(Value.S, Value.r, Value.vol, Value.t);
            Value.Thread = 1;
            watch.Stop();
            Value.timer = watch.Elapsed.Seconds.ToString() + " : " + watch.Elapsed.Milliseconds.ToString();
            watch.Reset();
            Program.finished();
            GC.Collect();
        }
        public void BarrierUAOPutAnti()
        {
            BarrierUAOAntithetic euro = new BarrierUAOAntithetic();
            watch.Start();
            euro.RandomGenerated();
            Value.Price = euro.PutPrice(Value.S, Value.r, Value.vol, Value.t);
            Value.SE = euro.SE();
            Value.delta = euro.putdelta(Value.S, Value.r, Value.vol, Value.t);
            Value.gamma = euro.putgamma(Value.S, Value.r, Value.vol, Value.t);
            Value.theta = euro.puttheta(Value.S, Value.r, Value.vol, Value.t);
            Value.vega = euro.putvega(Value.S, Value.r, Value.vol, Value.t);
            Value.rho = euro.putrho(Value.S, Value.r, Value.vol, Value.t);
            Value.Thread = 1;
            watch.Stop();
            Value.timer = watch.Elapsed.Seconds.ToString() + " : " + watch.Elapsed.Milliseconds.ToString();
            watch.Reset();
            Program.finished();
            GC.Collect();
        }
        public void BarrierUAOCallCV()
        {
            BarrierUAOCV euro = new BarrierUAOCV();
            watch.Start();
            euro.RandomGenerated();
            Value.Price = euro.CallPrice(Value.S, Value.r, Value.vol, Value.t);
            Value.SE = euro.SE();
            Value.delta = euro.calldelta(Value.S, Value.r, Value.vol, Value.t);
            Value.gamma = euro.callgamma(Value.S, Value.r, Value.vol, Value.t);
            Value.theta = euro.calltheta(Value.S, Value.r, Value.vol, Value.t);
            Value.vega = euro.callvega(Value.S, Value.r, Value.vol, Value.t);
            Value.rho = euro.callrho(Value.S, Value.r, Value.vol, Value.t);
            Value.Thread = 1;
            watch.Stop();
            Value.timer = watch.Elapsed.Seconds.ToString() + " : " + watch.Elapsed.Milliseconds.ToString();
            watch.Reset();
            Program.finished();
            GC.Collect();
        }
        public void BarrierUAOPutCV()
        {
            BarrierUAOCV euro = new BarrierUAOCV();
            watch.Start();
            euro.RandomGenerated();
            Value.Price = euro.PutPrice(Value.S, Value.r, Value.vol, Value.t);
            Value.SE = euro.SE();
            Value.delta = euro.putdelta(Value.S, Value.r, Value.vol, Value.t);
            Value.gamma = euro.putgamma(Value.S, Value.r, Value.vol, Value.t);
            Value.theta = euro.puttheta(Value.S, Value.r, Value.vol, Value.t);
            Value.vega = euro.putvega(Value.S, Value.r, Value.vol, Value.t);
            Value.rho = euro.putrho(Value.S, Value.r, Value.vol, Value.t);
            Value.Thread = 1;
            watch.Stop();
            Value.timer = watch.Elapsed.Seconds.ToString() + " : " + watch.Elapsed.Milliseconds.ToString();
            watch.Reset();
            Program.finished();
            GC.Collect();
        }
        public void BarrierUAOCallAntiCV()
        {
            BarrierUAOAntiCV euro = new BarrierUAOAntiCV();
            watch.Start();
            euro.RandomGenerated();
            Value.Price = euro.CallPrice(Value.S, Value.r, Value.vol, Value.t);
            Value.SE = euro.SE();
            Value.delta = euro.calldelta(Value.S, Value.r, Value.vol, Value.t);
            Value.gamma = euro.callgamma(Value.S, Value.r, Value.vol, Value.t);
            Value.theta = euro.calltheta(Value.S, Value.r, Value.vol, Value.t);
            Value.vega = euro.callvega(Value.S, Value.r, Value.vol, Value.t);
            Value.rho = euro.callrho(Value.S, Value.r, Value.vol, Value.t);
            Value.Thread = 1;
            watch.Stop();
            Value.timer = watch.Elapsed.Seconds.ToString() + " : " + watch.Elapsed.Milliseconds.ToString();
            watch.Reset();
            Program.finished();
            GC.Collect();
        }
        public void BarrierUAOPutAntiCV()
        {
            BarrierUAOAntiCV euro = new BarrierUAOAntiCV();
            watch.Start();
            euro.RandomGenerated();
            Value.Price = euro.PutPrice(Value.S, Value.r, Value.vol, Value.t);
            Value.SE = euro.SE();
            Value.delta = euro.putdelta(Value.S, Value.r, Value.vol, Value.t);
            Value.gamma = euro.putgamma(Value.S, Value.r, Value.vol, Value.t);
            Value.theta = euro.puttheta(Value.S, Value.r, Value.vol, Value.t);
            Value.vega = euro.putvega(Value.S, Value.r, Value.vol, Value.t);
            Value.rho = euro.putrho(Value.S, Value.r, Value.vol, Value.t);
            Value.Thread = 1;
            watch.Stop();
            Value.timer = watch.Elapsed.Seconds.ToString() + " : " + watch.Elapsed.Milliseconds.ToString();
            watch.Reset();
            Program.finished();
            GC.Collect();
        }
        public void BarrierUAOCallMT()
        {
            BarrierUAOMT euro = new BarrierUAOMT();
            watch.Start();
            euro.RandomGenerated();
            Value.Price = euro.CallPrice(Value.S, Value.r, Value.vol, Value.t);
            Value.SE = euro.SE();
            Value.delta = euro.calldelta(Value.S, Value.r, Value.vol, Value.t);
            Value.gamma = euro.callgamma(Value.S, Value.r, Value.vol, Value.t);
            Value.theta = euro.calltheta(Value.S, Value.r, Value.vol, Value.t);
            Value.vega = euro.callvega(Value.S, Value.r, Value.vol, Value.t);
            Value.rho = euro.callrho(Value.S, Value.r, Value.vol, Value.t);
            Value.Thread = 4;
            watch.Stop();
            Value.timer = watch.Elapsed.Seconds.ToString() + " : " + watch.Elapsed.Milliseconds.ToString();
            watch.Reset();
            Program.finished();
            GC.Collect();
        }
        public void BarrierUAOPutMT()
        {
            BarrierUAOMT euro = new BarrierUAOMT();
            watch.Start();
            euro.RandomGenerated();
            Value.Price = euro.PutPrice(Value.S, Value.r, Value.vol, Value.t);
            Value.SE = euro.SE();
            Value.delta = euro.putdelta(Value.S, Value.r, Value.vol, Value.t);
            Value.gamma = euro.putgamma(Value.S, Value.r, Value.vol, Value.t);
            Value.theta = euro.puttheta(Value.S, Value.r, Value.vol, Value.t);
            Value.vega = euro.putvega(Value.S, Value.r, Value.vol, Value.t);
            Value.rho = euro.putrho(Value.S, Value.r, Value.vol, Value.t);
            Value.Thread = 4;
            watch.Stop();
            Value.timer = watch.Elapsed.Seconds.ToString() + " : " + watch.Elapsed.Milliseconds.ToString();
            watch.Reset();
            Program.finished();
            GC.Collect();
        }
        public void BarrierUAOCallAntiMT()
        {
            BarrierUAOAntiMT euro = new BarrierUAOAntiMT();
            watch.Start();
            euro.RandomGenerated();
            Value.Price = euro.CallPrice(Value.S, Value.r, Value.vol, Value.t);
            Value.SE = euro.SE();
            Value.delta = euro.calldelta(Value.S, Value.r, Value.vol, Value.t);
            Value.gamma = euro.callgamma(Value.S, Value.r, Value.vol, Value.t);
            Value.theta = euro.calltheta(Value.S, Value.r, Value.vol, Value.t);
            Value.vega = euro.callvega(Value.S, Value.r, Value.vol, Value.t);
            Value.rho = euro.callrho(Value.S, Value.r, Value.vol, Value.t);
            Value.Thread = 4;
            watch.Stop();
            Value.timer = watch.Elapsed.Seconds.ToString() + " : " + watch.Elapsed.Milliseconds.ToString();
            watch.Reset();
            Program.finished();
            GC.Collect();
        }
        public void BarrierUAOPutAntiMT()
        {
            BarrierUAOAntiMT euro = new BarrierUAOAntiMT();
            watch.Start();
            euro.RandomGenerated();
            Value.Price = euro.PutPrice(Value.S, Value.r, Value.vol, Value.t);
            Value.SE = euro.SE();
            Value.delta = euro.putdelta(Value.S, Value.r, Value.vol, Value.t);
            Value.gamma = euro.putgamma(Value.S, Value.r, Value.vol, Value.t);
            Value.theta = euro.puttheta(Value.S, Value.r, Value.vol, Value.t);
            Value.vega = euro.putvega(Value.S, Value.r, Value.vol, Value.t);
            Value.rho = euro.putrho(Value.S, Value.r, Value.vol, Value.t);
            Value.Thread = 4;
            watch.Stop();
            Value.timer = watch.Elapsed.Seconds.ToString() + " : " + watch.Elapsed.Milliseconds.ToString();
            watch.Reset();
            Program.finished();
            GC.Collect();
        }
        public void BarrierUAOCallCVMT()
        {
            BarrierUAOCVMT euro = new BarrierUAOCVMT();
            watch.Start();
            euro.RandomGenerated();
            Value.Price = euro.CallPrice(Value.S, Value.r, Value.vol, Value.t);
            Value.SE = euro.SE();
            Value.delta = euro.calldelta(Value.S, Value.r, Value.vol, Value.t);
            Value.gamma = euro.callgamma(Value.S, Value.r, Value.vol, Value.t);
            Value.theta = euro.calltheta(Value.S, Value.r, Value.vol, Value.t);
            Value.vega = euro.callvega(Value.S, Value.r, Value.vol, Value.t);
            Value.rho = euro.callrho(Value.S, Value.r, Value.vol, Value.t);
            Value.Thread = 4;
            watch.Stop();
            Value.timer = watch.Elapsed.Seconds.ToString() + " : " + watch.Elapsed.Milliseconds.ToString();
            watch.Reset();
            Program.finished();
            GC.Collect();
        }
        public void BarrierUAOPutCVMT()
        {
            BarrierUAOCVMT euro = new BarrierUAOCVMT();
            watch.Start();
            euro.RandomGenerated();
            Value.Price = euro.PutPrice(Value.S, Value.r, Value.vol, Value.t);
            Value.SE = euro.SE();
            Value.delta = euro.putdelta(Value.S, Value.r, Value.vol, Value.t);
            Value.gamma = euro.putgamma(Value.S, Value.r, Value.vol, Value.t);
            Value.theta = euro.puttheta(Value.S, Value.r, Value.vol, Value.t);
            Value.vega = euro.putvega(Value.S, Value.r, Value.vol, Value.t);
            Value.rho = euro.putrho(Value.S, Value.r, Value.vol, Value.t);
            Value.Thread = 4;
            watch.Stop();
            Value.timer = watch.Elapsed.Seconds.ToString() + " : " + watch.Elapsed.Milliseconds.ToString();
            watch.Reset();
            Program.finished();
            GC.Collect();
        }
        public void BarrierUAOCallAntiCVMT()
        {
            BarrierUAOAntiCVMT euro = new BarrierUAOAntiCVMT();
            watch.Start();
            euro.RandomGenerated();
            Value.Price = euro.CallPrice(Value.S, Value.r, Value.vol, Value.t);
            Value.SE = euro.SE();
            Value.delta = euro.calldelta(Value.S, Value.r, Value.vol, Value.t);
            Value.gamma = euro.callgamma(Value.S, Value.r, Value.vol, Value.t);
            Value.theta = euro.calltheta(Value.S, Value.r, Value.vol, Value.t);
            Value.vega = euro.callvega(Value.S, Value.r, Value.vol, Value.t);
            Value.rho = euro.callrho(Value.S, Value.r, Value.vol, Value.t);
            Value.Thread = 4;
            watch.Stop();
            Value.timer = watch.Elapsed.Seconds.ToString() + " : " + watch.Elapsed.Milliseconds.ToString();
            watch.Reset();
            Program.finished();
            GC.Collect();
        }
        public void BarrierUAOPutAntiCVMT()
        {
            BarrierUAOAntiCVMT euro = new BarrierUAOAntiCVMT();
            watch.Start();
            euro.RandomGenerated();
            Value.Price = euro.PutPrice(Value.S, Value.r, Value.vol, Value.t);
            Value.SE = euro.SE();
            Value.delta = euro.putdelta(Value.S, Value.r, Value.vol, Value.t);
            Value.gamma = euro.putgamma(Value.S, Value.r, Value.vol, Value.t);
            Value.theta = euro.puttheta(Value.S, Value.r, Value.vol, Value.t);
            Value.vega = euro.putvega(Value.S, Value.r, Value.vol, Value.t);
            Value.rho = euro.putrho(Value.S, Value.r, Value.vol, Value.t);
            Value.Thread = 4;
            watch.Stop();
            Value.timer = watch.Elapsed.Seconds.ToString() + " : " + watch.Elapsed.Milliseconds.ToString();
            watch.Reset();
            Program.finished();
            GC.Collect();
        }

        public void BarrierUAICall()
        {
            BarrierUAI euro = new BarrierUAI();
            watch.Start();
            euro.RandomGenerated();
            Value.Price = euro.CallPrice(Value.S, Value.r, Value.vol, Value.t);
            Value.SE = euro.SE();
            Value.delta = euro.calldelta(Value.S, Value.r, Value.vol, Value.t);
            Value.gamma = euro.callgamma(Value.S, Value.r, Value.vol, Value.t);
            Value.theta = euro.calltheta(Value.S, Value.r, Value.vol, Value.t);
            Value.vega = euro.callvega(Value.S, Value.r, Value.vol, Value.t);
            Value.rho = euro.callrho(Value.S, Value.r, Value.vol, Value.t);
            Value.Thread = 1;
            watch.Stop();
            Value.timer = watch.Elapsed.Seconds.ToString() + " : " + watch.Elapsed.Milliseconds.ToString();
            watch.Reset();
            Program.finished();
            GC.Collect();
        }
        public void BarrierUAIPut()
        {
            BarrierUAI euro = new BarrierUAI();
            watch.Start();
            euro.RandomGenerated();
            Value.Price = euro.PutPrice(Value.S, Value.r, Value.vol, Value.t);
            Value.SE = euro.SE();
            Value.delta = euro.putdelta(Value.S, Value.r, Value.vol, Value.t);
            Value.gamma = euro.putgamma(Value.S, Value.r, Value.vol, Value.t);
            Value.theta = euro.puttheta(Value.S, Value.r, Value.vol, Value.t);
            Value.vega = euro.putvega(Value.S, Value.r, Value.vol, Value.t);
            Value.rho = euro.putrho(Value.S, Value.r, Value.vol, Value.t);
            Value.Thread = 1;
            watch.Stop();
            Value.timer = watch.Elapsed.Seconds.ToString() + " : " + watch.Elapsed.Milliseconds.ToString();
            watch.Reset();
            Program.finished();
            GC.Collect();
        }
        public void BarrierUAICallAnti()
        {
            BarrierUAIAntithetic euro = new BarrierUAIAntithetic();
            watch.Start();
            euro.RandomGenerated();
            Value.Price = euro.CallPrice(Value.S, Value.r, Value.vol, Value.t);
            Value.SE = euro.SE();
            Value.delta = euro.calldelta(Value.S, Value.r, Value.vol, Value.t);
            Value.gamma = euro.callgamma(Value.S, Value.r, Value.vol, Value.t);
            Value.theta = euro.calltheta(Value.S, Value.r, Value.vol, Value.t);
            Value.vega = euro.callvega(Value.S, Value.r, Value.vol, Value.t);
            Value.rho = euro.callrho(Value.S, Value.r, Value.vol, Value.t);
            Value.Thread = 1;
            watch.Stop();
            Value.timer = watch.Elapsed.Seconds.ToString() + " : " + watch.Elapsed.Milliseconds.ToString();
            watch.Reset();
            Program.finished();
            GC.Collect();
        }
        public void BarrierUAIPutAnti()
        {
            BarrierUAIAntithetic euro = new BarrierUAIAntithetic();
            watch.Start();
            euro.RandomGenerated();
            Value.Price = euro.PutPrice(Value.S, Value.r, Value.vol, Value.t);
            Value.SE = euro.SE();
            Value.delta = euro.putdelta(Value.S, Value.r, Value.vol, Value.t);
            Value.gamma = euro.putgamma(Value.S, Value.r, Value.vol, Value.t);
            Value.theta = euro.puttheta(Value.S, Value.r, Value.vol, Value.t);
            Value.vega = euro.putvega(Value.S, Value.r, Value.vol, Value.t);
            Value.rho = euro.putrho(Value.S, Value.r, Value.vol, Value.t);
            Value.Thread = 1;
            watch.Stop();
            Value.timer = watch.Elapsed.Seconds.ToString() + " : " + watch.Elapsed.Milliseconds.ToString();
            watch.Reset();
            Program.finished();
            GC.Collect();
        }
        public void BarrierUAICallCV()
        {
            BarrierUAICV euro = new BarrierUAICV();
            watch.Start();
            euro.RandomGenerated();
            Value.Price = euro.CallPrice(Value.S, Value.r, Value.vol, Value.t);
            Value.SE = euro.SE();
            Value.delta = euro.calldelta(Value.S, Value.r, Value.vol, Value.t);
            Value.gamma = euro.callgamma(Value.S, Value.r, Value.vol, Value.t);
            Value.theta = euro.calltheta(Value.S, Value.r, Value.vol, Value.t);
            Value.vega = euro.callvega(Value.S, Value.r, Value.vol, Value.t);
            Value.rho = euro.callrho(Value.S, Value.r, Value.vol, Value.t);
            Value.Thread = 1;
            watch.Stop();
            Value.timer = watch.Elapsed.Seconds.ToString() + " : " + watch.Elapsed.Milliseconds.ToString();
            watch.Reset();
            Program.finished();
            GC.Collect();
        }
        public void BarrierUAIPutCV()
        {
            BarrierUAICV euro = new BarrierUAICV();
            watch.Start();
            euro.RandomGenerated();
            Value.Price = euro.PutPrice(Value.S, Value.r, Value.vol, Value.t);
            Value.SE = euro.SE();
            Value.delta = euro.putdelta(Value.S, Value.r, Value.vol, Value.t);
            Value.gamma = euro.putgamma(Value.S, Value.r, Value.vol, Value.t);
            Value.theta = euro.puttheta(Value.S, Value.r, Value.vol, Value.t);
            Value.vega = euro.putvega(Value.S, Value.r, Value.vol, Value.t);
            Value.rho = euro.putrho(Value.S, Value.r, Value.vol, Value.t);
            Value.Thread = 1;
            watch.Stop();
            Value.timer = watch.Elapsed.Seconds.ToString() + " : " + watch.Elapsed.Milliseconds.ToString();
            watch.Reset();
            Program.finished();
            GC.Collect();
        }
        public void BarrierUAICallAntiCV()
        {
            BarrierUAIAntiCV euro = new BarrierUAIAntiCV();
            watch.Start();
            euro.RandomGenerated();
            Value.Price = euro.CallPrice(Value.S, Value.r, Value.vol, Value.t);
            Value.SE = euro.SE();
            Value.delta = euro.calldelta(Value.S, Value.r, Value.vol, Value.t);
            Value.gamma = euro.callgamma(Value.S, Value.r, Value.vol, Value.t);
            Value.theta = euro.calltheta(Value.S, Value.r, Value.vol, Value.t);
            Value.vega = euro.callvega(Value.S, Value.r, Value.vol, Value.t);
            Value.rho = euro.callrho(Value.S, Value.r, Value.vol, Value.t);
            Value.Thread = 1;
            watch.Stop();
            Value.timer = watch.Elapsed.Seconds.ToString() + " : " + watch.Elapsed.Milliseconds.ToString();
            watch.Reset();
            Program.finished();
            GC.Collect();
        }
        public void BarrierUAIPutAntiCV()
        {
            BarrierUAIAntiCV euro = new BarrierUAIAntiCV();
            watch.Start();
            euro.RandomGenerated();
            Value.Price = euro.PutPrice(Value.S, Value.r, Value.vol, Value.t);
            Value.SE = euro.SE();
            Value.delta = euro.putdelta(Value.S, Value.r, Value.vol, Value.t);
            Value.gamma = euro.putgamma(Value.S, Value.r, Value.vol, Value.t);
            Value.theta = euro.puttheta(Value.S, Value.r, Value.vol, Value.t);
            Value.vega = euro.putvega(Value.S, Value.r, Value.vol, Value.t);
            Value.rho = euro.putrho(Value.S, Value.r, Value.vol, Value.t);
            Value.Thread = 1;
            watch.Stop();
            Value.timer = watch.Elapsed.Seconds.ToString() + " : " + watch.Elapsed.Milliseconds.ToString();
            watch.Reset();
            Program.finished();
            GC.Collect();
        }
        public void BarrierUAICallMT()
        {
            BarrierUAIMT euro = new BarrierUAIMT();
            watch.Start();
            euro.RandomGenerated();
            Value.Price = euro.CallPrice(Value.S, Value.r, Value.vol, Value.t);
            Value.SE = euro.SE();
            Value.delta = euro.calldelta(Value.S, Value.r, Value.vol, Value.t);
            Value.gamma = euro.callgamma(Value.S, Value.r, Value.vol, Value.t);
            Value.theta = euro.calltheta(Value.S, Value.r, Value.vol, Value.t);
            Value.vega = euro.callvega(Value.S, Value.r, Value.vol, Value.t);
            Value.rho = euro.callrho(Value.S, Value.r, Value.vol, Value.t);
            Value.Thread = 4;
            watch.Stop();
            Value.timer = watch.Elapsed.Seconds.ToString() + " : " + watch.Elapsed.Milliseconds.ToString();
            watch.Reset();
            Program.finished();
            GC.Collect();
        }
        public void BarrierUAIPutMT()
        {
            BarrierUAIMT euro = new BarrierUAIMT();
            watch.Start();
            euro.RandomGenerated();
            Value.Price = euro.PutPrice(Value.S, Value.r, Value.vol, Value.t);
            Value.SE = euro.SE();
            Value.delta = euro.putdelta(Value.S, Value.r, Value.vol, Value.t);
            Value.gamma = euro.putgamma(Value.S, Value.r, Value.vol, Value.t);
            Value.theta = euro.puttheta(Value.S, Value.r, Value.vol, Value.t);
            Value.vega = euro.putvega(Value.S, Value.r, Value.vol, Value.t);
            Value.rho = euro.putrho(Value.S, Value.r, Value.vol, Value.t);
            Value.Thread = 4;
            watch.Stop();
            Value.timer = watch.Elapsed.Seconds.ToString() + " : " + watch.Elapsed.Milliseconds.ToString();
            watch.Reset();
            Program.finished();
            GC.Collect();
        }
        public void BarrierUAICallAntiMT()
        {
            BarrierUAIAntiMT euro = new BarrierUAIAntiMT();
            watch.Start();
            euro.RandomGenerated();
            Value.Price = euro.CallPrice(Value.S, Value.r, Value.vol, Value.t);
            Value.SE = euro.SE();
            Value.delta = euro.calldelta(Value.S, Value.r, Value.vol, Value.t);
            Value.gamma = euro.callgamma(Value.S, Value.r, Value.vol, Value.t);
            Value.theta = euro.calltheta(Value.S, Value.r, Value.vol, Value.t);
            Value.vega = euro.callvega(Value.S, Value.r, Value.vol, Value.t);
            Value.rho = euro.callrho(Value.S, Value.r, Value.vol, Value.t);
            Value.Thread = 4;
            watch.Stop();
            Value.timer = watch.Elapsed.Seconds.ToString() + " : " + watch.Elapsed.Milliseconds.ToString();
            watch.Reset();
            Program.finished();
            GC.Collect();
        }
        public void BarrierUAIPutAntiMT()
        {
            BarrierUAIAntiMT euro = new BarrierUAIAntiMT();
            watch.Start();
            euro.RandomGenerated();
            Value.Price = euro.PutPrice(Value.S, Value.r, Value.vol, Value.t);
            Value.SE = euro.SE();
            Value.delta = euro.putdelta(Value.S, Value.r, Value.vol, Value.t);
            Value.gamma = euro.putgamma(Value.S, Value.r, Value.vol, Value.t);
            Value.theta = euro.puttheta(Value.S, Value.r, Value.vol, Value.t);
            Value.vega = euro.putvega(Value.S, Value.r, Value.vol, Value.t);
            Value.rho = euro.putrho(Value.S, Value.r, Value.vol, Value.t);
            Value.Thread = 4;
            watch.Stop();
            Value.timer = watch.Elapsed.Seconds.ToString() + " : " + watch.Elapsed.Milliseconds.ToString();
            watch.Reset();
            Program.finished();
            GC.Collect();
        }
        public void BarrierUAICallCVMT()
        {
            BarrierUAICVMT euro = new BarrierUAICVMT();
            watch.Start();
            euro.RandomGenerated();
            Value.Price = euro.CallPrice(Value.S, Value.r, Value.vol, Value.t);
            Value.SE = euro.SE();
            Value.delta = euro.calldelta(Value.S, Value.r, Value.vol, Value.t);
            Value.gamma = euro.callgamma(Value.S, Value.r, Value.vol, Value.t);
            Value.theta = euro.calltheta(Value.S, Value.r, Value.vol, Value.t);
            Value.vega = euro.callvega(Value.S, Value.r, Value.vol, Value.t);
            Value.rho = euro.callrho(Value.S, Value.r, Value.vol, Value.t);
            Value.Thread = 4;
            watch.Stop();
            Value.timer = watch.Elapsed.Seconds.ToString() + " : " + watch.Elapsed.Milliseconds.ToString();
            watch.Reset();
            Program.finished();
            GC.Collect();
        }
        public void BarrierUAIPutCVMT()
        {
            BarrierUAICVMT euro = new BarrierUAICVMT();
            watch.Start();
            euro.RandomGenerated();
            Value.Price = euro.PutPrice(Value.S, Value.r, Value.vol, Value.t);
            Value.SE = euro.SE();
            Value.delta = euro.putdelta(Value.S, Value.r, Value.vol, Value.t);
            Value.gamma = euro.putgamma(Value.S, Value.r, Value.vol, Value.t);
            Value.theta = euro.puttheta(Value.S, Value.r, Value.vol, Value.t);
            Value.vega = euro.putvega(Value.S, Value.r, Value.vol, Value.t);
            Value.rho = euro.putrho(Value.S, Value.r, Value.vol, Value.t);
            Value.Thread = 4;
            watch.Stop();
            Value.timer = watch.Elapsed.Seconds.ToString() + " : " + watch.Elapsed.Milliseconds.ToString();
            watch.Reset();
            Program.finished();
            GC.Collect();
        }
        public void BarrierUAICallAntiCVMT()
        {
            BarrierUAIAntiCVMT euro = new BarrierUAIAntiCVMT();
            watch.Start();
            euro.RandomGenerated();
            Value.Price = euro.CallPrice(Value.S, Value.r, Value.vol, Value.t);
            Value.SE = euro.SE();
            Value.delta = euro.calldelta(Value.S, Value.r, Value.vol, Value.t);
            Value.gamma = euro.callgamma(Value.S, Value.r, Value.vol, Value.t);
            Value.theta = euro.calltheta(Value.S, Value.r, Value.vol, Value.t);
            Value.vega = euro.callvega(Value.S, Value.r, Value.vol, Value.t);
            Value.rho = euro.callrho(Value.S, Value.r, Value.vol, Value.t);
            Value.Thread = 4;
            watch.Stop();
            Value.timer = watch.Elapsed.Seconds.ToString() + " : " + watch.Elapsed.Milliseconds.ToString();
            watch.Reset();
            Program.finished();
            GC.Collect();
        }
        public void BarrierUAIPutAntiCVMT()
        {
            BarrierUAIAntiCVMT euro = new BarrierUAIAntiCVMT();
            watch.Start();
            euro.RandomGenerated();
            Value.Price = euro.PutPrice(Value.S, Value.r, Value.vol, Value.t);
            Value.SE = euro.SE();
            Value.delta = euro.putdelta(Value.S, Value.r, Value.vol, Value.t);
            Value.gamma = euro.putgamma(Value.S, Value.r, Value.vol, Value.t);
            Value.theta = euro.puttheta(Value.S, Value.r, Value.vol, Value.t);
            Value.vega = euro.putvega(Value.S, Value.r, Value.vol, Value.t);
            Value.rho = euro.putrho(Value.S, Value.r, Value.vol, Value.t);
            Value.Thread = 4;
            watch.Stop();
            Value.timer = watch.Elapsed.Seconds.ToString() + " : " + watch.Elapsed.Milliseconds.ToString();
            watch.Reset();
            Program.finished();
            GC.Collect();
        }
    }
}
