﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ConsoleApp6
{
    public interface CallPrice
    {
        double CallPrice(double S, double r, double vol, double t);
    }

    public interface StandardError
    {
        double SE();
    }

    public interface PutPrice
    {
        double PutPrice(double S, double r, double vol, double t);
    }

    public interface CallGreek
    {
        double calldelta(double S, double r, double vol, double t);
        double callgamma(double S, double r, double vol, double t);
        double calltheta(double S, double r, double vol, double t);
        double callvega(double S, double r, double vol, double t);
        double callrho(double S, double r, double vol, double t);
    }

    public interface PutGreek
    {
        double putdelta(double S, double r, double vol, double t);
        double putgamma(double S, double r, double vol, double t);
        double puttheta(double S, double r, double vol, double t);
        double putvega(double S, double r, double vol, double t);
        double putrho(double S, double r, double vol, double t);
    }

    public class Preparation
    {
        public double[] optionprice { get; set; }
        public double Price(double S, double r, double vol, double t, double ep)
        {
            double price;
            double deltaT = t / Convert.ToDouble(Value.step);
            price = S * Math.Exp((r - Math.Pow(vol, 2) / 2) * deltaT + vol * Math.Sqrt(deltaT) * ep);
            return price;
        }

    }

    public class European : Preparation, CallPrice, StandardError,PutPrice,CallGreek,PutGreek
    {
        public List<double> PriceList;
        public static double[,] RandomList;
        public void Initial()
        {
            optionprice = new double[Value.step + 1];
            PriceList = new List<double>();
        }
        public void RandomGenerated()
        {
            RandomList = new double[Value.trials, Value.step];
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                for (int k = 0; k < Value.step; k++)
                {
                    RandomList[i, k] = GaussianBoxMuller.NextDouble();
                }
            }
        }
        public double CallPrice(double S, double r, double vol, double t)
        {
            Initial();
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                optionprice[0] = S;
                for (int j = 1; j < Value.step + 1; j++)
                {
                    optionprice[j] = Price(optionprice[j - 1], r, vol, t, RandomList[i, j - 1]);
                }
                PriceList.Add(Math.Max(optionprice.Last() - Value.K,0));
            }
            double price = 0;
            price = Math.Exp(-r * t) * PriceList.Average();
            Program.increase(1);
            return price;
        }
        public double PutPrice(double S, double r, double vol, double t)
        {
            Initial();
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                optionprice[0] = S;
                for (uint j = 1; j < Value.step + 1; j++)
                {
                    optionprice[j] = Price(optionprice[j - 1], r, vol, t, RandomList[i, j - 1]);
                }
                PriceList.Add(Math.Max(Value.K - optionprice.Last(),0));
            }
            double price = 0;
            price = Math.Exp(-r * t) * PriceList.Average();
            Program.increase(1);
            return price;
        }
        public double SE()
        {
            double avg = PriceList.Average();
            double sum = PriceList.Sum(v => Math.Pow(v - avg, 2));
            return (Math.Sqrt(sum / (PriceList.Count - 1))) / (Math.Sqrt(PriceList.Count));
        }
        public double calldelta(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S + 0.0001, r, vol, t);
            double cp2 = CallPrice(S - 0.0001, r, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double callgamma(double S, double r, double vol, double t)
        {
            return (calldelta(S + 0.01, r, vol, t) - calldelta(S, r, vol, t)) / 0.01;
        }
        public double callvega(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r, vol + 0.0001, t);
            double cp2 = CallPrice(S, r, vol - 0.0001, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double calltheta(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r, vol, t + 0.0001);
            double cp2 = CallPrice(S, r, vol, t - 0.0001);
            return (cp1 - cp2) / 0.0002;
        }
        public double callrho(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r + 0.0001, vol, t);
            double cp2 = CallPrice(S, r - 0.0001, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double putdelta(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S + 0.0001, r, vol, t);
            double cp2 = PutPrice(S - 0.0001, r, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double putgamma(double S, double r, double vol, double t)
        {
            return (putdelta(S + 0.01, r, vol, t) - putdelta(S, r, vol, t)) / 0.01;
        }
        public double putvega(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r, vol + 0.0001, t);
            double cp2 = PutPrice(S, r, vol - 0.0001, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double puttheta(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r, vol, t + 0.0001);
            double cp2 = PutPrice(S, r, vol, t - 0.0001);
            return (cp1 - cp2) / 0.0002;
        }
        public double putrho(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r + 0.0001, vol, t);
            double cp2 = PutPrice(S, r - 0.0001, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        internal static class GaussianBoxMuller
        {
            public static double NextDouble()
            {
                double x, y, square;

                do
                {
                    x = 2 * RandomProvider.GetThreadRandom().NextDouble() - 1;
                    y = 2 * RandomProvider.GetThreadRandom().NextDouble() - 1;
                    square = (x * x) + (y * y);
                } while (square >= 1);

                return x * Math.Sqrt(-2 * Math.Log(square) / square);
            }
        }
        internal static class RandomProvider
        {
            private static ThreadLocal<Random> randomWrapper = new ThreadLocal<Random>(() =>
                new Random(Interlocked.Increment(ref seed))
            );

            private static int seed = Environment.TickCount * 10;

            public static Random GetThreadRandom()
            {
                return randomWrapper.Value;
            }
        }
    }

    public class Asian: Preparation,CallPrice, StandardError,PutPrice, CallGreek, PutGreek
    {
        public List<double> PriceList;
        public static double[,] RandomList;
        public void Initial()
        {
            optionprice = new double[Value.step + 1];
            PriceList = new List<double>();
        }
        public void RandomGenerated()
        {
            RandomList = new double[Value.trials, Value.step];
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                for (int k = 0; k < Value.step; k++)
                {
                    RandomList[i, k] = GaussianBoxMuller.NextDouble();
                }
            }
        }
        public double CallPrice(double S, double r, double vol, double t)
        {
            Initial();
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                optionprice[0] = S;
                for (int j = 1; j < Value.step + 1; j++)
                {
                    optionprice[j] = Price(optionprice[j - 1], r, vol, t, RandomList[i, j - 1]);
                }
                PriceList.Add(Math.Max((optionprice.Average()*(Value.step+1) - optionprice[0])/(Value.step)-Value.K,0));
            }
            double price = 0;
            price = Math.Exp(-r * t) * PriceList.Average();
            Program.increase(1);
            return price;
        }
        public double PutPrice(double S, double r, double vol, double t)
        {
            Initial();
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                optionprice[0] = S;
                for (uint j = 1; j < Value.step + 1; j++)
                {
                    optionprice[j] = Price(optionprice[j - 1], r, vol, t, RandomList[i, j - 1]);
                }
                PriceList.Add(Math.Max(Value.K - (optionprice.Average() * (Value.step + 1) - optionprice[0]) / (Value.step), 0));
            }
            double price = 0;
            price = Math.Exp(-r * t) * PriceList.Average();
            Program.increase(1);
            return price;
        }
        public double SE()
        {
            double avg = PriceList.Average();
            double sum = PriceList.Sum(v => Math.Pow(v - avg, 2));
            return (Math.Sqrt(sum / (PriceList.Count - 1))) / (Math.Sqrt(PriceList.Count));
        }
        internal static class GaussianBoxMuller
        {
            public static double NextDouble()
            {
                double x, y, square;

                do
                {
                    x = 2 * RandomProvider.GetThreadRandom().NextDouble() - 1;
                    y = 2 * RandomProvider.GetThreadRandom().NextDouble() - 1;
                    square = (x * x) + (y * y);
                } while (square >= 1);

                return x * Math.Sqrt(-2 * Math.Log(square) / square);
            }
        }
        internal static class RandomProvider
        {
            private static ThreadLocal<Random> randomWrapper = new ThreadLocal<Random>(() =>
                new Random(Interlocked.Increment(ref seed))
            );

            private static int seed = Environment.TickCount * 10;

            public static Random GetThreadRandom()
            {
                return randomWrapper.Value;
            }
        }
        public double calldelta(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S + 0.0001, r, vol, t);
            double cp2 = CallPrice(S - 0.0001, r, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double callgamma(double S, double r, double vol, double t)
        {
            return (calldelta(S + 0.01, r, vol, t) - calldelta(S, r, vol, t)) / 0.01;
        }
        public double callvega(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r, vol + 0.0001, t);
            double cp2 = CallPrice(S, r, vol - 0.0001, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double calltheta(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r, vol, t + 0.0001);
            double cp2 = CallPrice(S, r, vol, t - 0.0001);
            return (cp1 - cp2) / 0.0002;
        }
        public double callrho(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r + 0.0001, vol, t);
            double cp2 = CallPrice(S, r - 0.0001, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double putdelta(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S + 0.0001, r, vol, t);
            double cp2 = PutPrice(S - 0.0001, r, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double putgamma(double S, double r, double vol, double t)
        {
            return (putdelta(S + 0.01, r, vol, t) - putdelta(S, r, vol, t)) / 0.01;
        }
        public double putvega(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r, vol + 0.0001, t);
            double cp2 = PutPrice(S, r, vol - 0.0001, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double puttheta(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r, vol, t + 0.0001);
            double cp2 = PutPrice(S, r, vol, t - 0.0001);
            return (cp1 - cp2) / 0.0002;
        }
        public double putrho(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r + 0.0001, vol, t);
            double cp2 = PutPrice(S, r - 0.0001, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
    }

    public class Digital: Preparation,CallPrice, StandardError, PutPrice,CallGreek, PutGreek
    {
        public List<double> PriceList;
        public static double[,] RandomList;
        public void Initial()
        {
            optionprice = new double[Value.step + 1];
            PriceList = new List<double>();
        }
        public void RandomGenerated()
        {
            RandomList = new double[Value.trials, Value.step];
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                for (int k = 0; k < Value.step; k++)
                {
                    RandomList[i, k] = GaussianBoxMuller.NextDouble();
                }
            }
        }
        public double CallPrice(double S, double r, double vol, double t)
        {
            Initial();
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                optionprice[0] = S;
                for (int j = 1; j < Value.step + 1; j++)
                {
                    optionprice[j] = Price(optionprice[j - 1], r, vol, t, RandomList[i, j - 1]);
                }
                if(optionprice.Last()-Value.K>=0)
                { PriceList.Add(Value.rebate); }
                else
                { PriceList.Add(0); }
            }
            double price = 0;
            price = Math.Exp(-r * t) * PriceList.Average();
            Program.increase(1);
            return price;
        }
        public double PutPrice(double S, double r, double vol, double t)
        {
            Initial();
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                optionprice[0] = S;
                for (int j = 1; j < Value.step + 1; j++)
                {
                    optionprice[j] = Price(optionprice[j - 1], r, vol, t, RandomList[i, j - 1]);
                }
                if (Value.K - optionprice.Last() >= 0)
                { PriceList.Add(Value.rebate); }
                else
                { PriceList.Add(0); }
            }
            double price = 0;
            price = Math.Exp(-r * t) * PriceList.Average();
            Program.increase(1);
            return price;
        }
        public double SE()
        {
            double avg = PriceList.Average();
            double sum = PriceList.Sum(v => Math.Pow(v - avg, 2));
            return (Math.Sqrt(sum / (PriceList.Count - 1))) / (Math.Sqrt(PriceList.Count));
        }
        internal static class GaussianBoxMuller
        {
            public static double NextDouble()
            {
                double x, y, square;

                do
                {
                    x = 2 * RandomProvider.GetThreadRandom().NextDouble() - 1;
                    y = 2 * RandomProvider.GetThreadRandom().NextDouble() - 1;
                    square = (x * x) + (y * y);
                } while (square >= 1);

                return x * Math.Sqrt(-2 * Math.Log(square) / square);
            }
        }
        internal static class RandomProvider
        {
            private static ThreadLocal<Random> randomWrapper = new ThreadLocal<Random>(() =>
                new Random(Interlocked.Increment(ref seed))
            );

            private static int seed = Environment.TickCount * 10;

            public static Random GetThreadRandom()
            {
                return randomWrapper.Value;
            }
        }
        public double calldelta(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S + 0.0001, r, vol, t);
            double cp2 = CallPrice(S - 0.0001, r, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double callgamma(double S, double r, double vol, double t)
        {
            return (calldelta(S + 0.01, r, vol, t) - calldelta(S, r, vol, t)) / 0.01;
        }
        public double callvega(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r, vol + 0.0001, t);
            double cp2 = CallPrice(S, r, vol - 0.0001, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double calltheta(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r, vol, t + 0.0001);
            double cp2 = CallPrice(S, r, vol, t - 0.0001);
            return (cp1 - cp2) / 0.0002;
        }
        public double callrho(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r + 0.0001, vol, t);
            double cp2 = CallPrice(S, r - 0.0001, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double putdelta(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S + 0.0001, r, vol, t);
            double cp2 = PutPrice(S - 0.0001, r, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double putgamma(double S, double r, double vol, double t)
        {
            return (putdelta(S + 0.01, r, vol, t) - putdelta(S, r, vol, t)) / 0.01;
        }
        public double putvega(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r, vol + 0.0001, t);
            double cp2 = PutPrice(S, r, vol - 0.0001, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double puttheta(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r, vol, t + 0.0001);
            double cp2 = PutPrice(S, r, vol, t - 0.0001);
            return (cp1 - cp2) / 0.0002;
        }
        public double putrho(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r + 0.0001, vol, t);
            double cp2 = PutPrice(S, r - 0.0001, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
    }

    public class BarrierDAO: Preparation,CallPrice, StandardError, PutPrice, CallGreek, PutGreek
    {
        public List<double> PriceList;
        public static double[,] RandomList;
        public void Initial()
        {
            optionprice = new double[Value.step + 1];
            PriceList = new List<double>();
        }
        public void RandomGenerated()
        {
            RandomList = new double[Value.trials, Value.step];
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                for (int k = 0; k < Value.step; k++)
                {
                    RandomList[i, k] = GaussianBoxMuller.NextDouble();
                }
            }
        }
        public double CallPrice(double S, double r, double vol, double t)
        {
            Initial();
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                optionprice[0] = S;
                for (int j = 1; j < Value.step + 1; j++)
                {
                    optionprice[j] = Price(optionprice[j - 1], r, vol, t, RandomList[i, j - 1]);
                }
                if (optionprice.Min() >= Value.barrier)
                { PriceList.Add(Math.Max(optionprice.Last()-Value.K,0)); }
                else { PriceList.Add(0); }
            }
            double price = 0;
            price = Math.Exp(-r * t) * PriceList.Average();
            Program.increase(1);
            return price;
        }
        public double PutPrice(double S, double r, double vol, double t)
        {
            Initial();
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                optionprice[0] = S;
                for (uint j = 1; j < Value.step + 1; j++)
                {
                    optionprice[j] = Price(optionprice[j - 1], r, vol, t, RandomList[i, j - 1]);
                }
                if (optionprice.Min() >= Value.barrier)
                { PriceList.Add(Math.Max(Value.K - optionprice.Last(),0)); }
                else { PriceList.Add(0); }
            }
            double price = 0;
            price = Math.Exp(-r * t) * PriceList.Average();
            Program.increase(1);
            return price;
        }
        public double SE()
        {
            double avg = PriceList.Average();
            double sum = PriceList.Sum(v => Math.Pow(v - avg, 2));
            return (Math.Sqrt(sum / (PriceList.Count - 1))) / (Math.Sqrt(PriceList.Count));
        }
        internal static class GaussianBoxMuller
        {
            public static double NextDouble()
            {
                double x, y, square;

                do
                {
                    x = 2 * RandomProvider.GetThreadRandom().NextDouble() - 1;
                    y = 2 * RandomProvider.GetThreadRandom().NextDouble() - 1;
                    square = (x * x) + (y * y);
                } while (square >= 1);

                return x * Math.Sqrt(-2 * Math.Log(square) / square);
            }
        }
        internal static class RandomProvider
        {
            private static ThreadLocal<Random> randomWrapper = new ThreadLocal<Random>(() =>
                new Random(Interlocked.Increment(ref seed))
            );

            private static int seed = Environment.TickCount * 10;

            public static Random GetThreadRandom()
            {
                return randomWrapper.Value;
            }
        }
        public double calldelta(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S + 0.0001, r, vol, t);
            double cp2 = CallPrice(S - 0.0001, r, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double callgamma(double S, double r, double vol, double t)
        {
            return (calldelta(S + 0.01, r, vol, t) - calldelta(S, r, vol, t)) / 0.01;
        }
        public double callvega(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r, vol + 0.0001, t);
            double cp2 = CallPrice(S, r, vol - 0.0001, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double calltheta(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r, vol, t + 0.0001);
            double cp2 = CallPrice(S, r, vol, t - 0.0001);
            return (cp1 - cp2) / 0.0002;
        }
        public double callrho(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r + 0.0001, vol, t);
            double cp2 = CallPrice(S, r - 0.0001, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double putdelta(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S + 0.0001, r, vol, t);
            double cp2 = PutPrice(S - 0.0001, r, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double putgamma(double S, double r, double vol, double t)
        {
            return (putdelta(S + 0.01, r, vol, t) - putdelta(S, r, vol, t)) / 0.01;
        }
        public double putvega(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r, vol + 0.0001, t);
            double cp2 = PutPrice(S, r, vol - 0.0001, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double puttheta(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r, vol, t + 0.0001);
            double cp2 = PutPrice(S, r, vol, t - 0.0001);
            return (cp1 - cp2) / 0.0002;
        }
        public double putrho(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r + 0.0001, vol, t);
            double cp2 = PutPrice(S, r - 0.0001, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
    }

    public class BarrierDAI: Preparation,CallPrice, StandardError, PutPrice, CallGreek, PutGreek
    {
        public List<double> PriceList;
        public static double[,] RandomList;
        public void Initial()
        {
            optionprice = new double[Value.step + 1];
            PriceList = new List<double>();
        }
        public void RandomGenerated()
        {
            RandomList = new double[Value.trials, Value.step];
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                for (int k = 0; k < Value.step; k++)
                {
                    RandomList[i, k] = GaussianBoxMuller.NextDouble();
                }
            }
        }
        public double CallPrice(double S, double r, double vol, double t)
        {
            Initial();
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                optionprice[0] = S;
                for (int j = 1; j < Value.step + 1; j++)
                {
                    optionprice[j] = Price(optionprice[j - 1], r, vol, t, RandomList[i, j - 1]);
                }
                if (optionprice.Min() < Value.barrier)
                { PriceList.Add(Math.Max(optionprice.Last() - Value.K, 0)); }
                else { PriceList.Add(0); }
            }
            double price = 0;
            price = Math.Exp(-r * t) * PriceList.Average();
            Program.increase(1);
            return price;
        }
        public double PutPrice(double S, double r, double vol, double t)
        {
            Initial();
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                optionprice[0] = S;
                for (uint j = 1; j < Value.step + 1; j++)
                {
                    optionprice[j] = Price(optionprice[j - 1], r, vol, t, RandomList[i, j - 1]);
                }
                if (optionprice.Min() < Value.barrier)
                { PriceList.Add(Math.Max(Value.K - optionprice.Last(), 0)); }
                else { PriceList.Add(0); }
            }
            double price = 0;
            price = Math.Exp(-r * t) * PriceList.Average();
            Program.increase(1);
            return price;
        }
        public double SE()
        {
            double avg = PriceList.Average();
            double sum = PriceList.Sum(v => Math.Pow(v - avg, 2));
            return (Math.Sqrt(sum / (PriceList.Count - 1))) / (Math.Sqrt(PriceList.Count));
        }
        internal static class GaussianBoxMuller
        {
            public static double NextDouble()
            {
                double x, y, square;

                do
                {
                    x = 2 * RandomProvider.GetThreadRandom().NextDouble() - 1;
                    y = 2 * RandomProvider.GetThreadRandom().NextDouble() - 1;
                    square = (x * x) + (y * y);
                } while (square >= 1);

                return x * Math.Sqrt(-2 * Math.Log(square) / square);
            }
        }
        internal static class RandomProvider
        {
            private static ThreadLocal<Random> randomWrapper = new ThreadLocal<Random>(() =>
                new Random(Interlocked.Increment(ref seed))
            );

            private static int seed = Environment.TickCount * 10;

            public static Random GetThreadRandom()
            {
                return randomWrapper.Value;
            }
        }
        public double calldelta(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S + 0.0001, r, vol, t);
            double cp2 = CallPrice(S - 0.0001, r, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double callgamma(double S, double r, double vol, double t)
        {
            return (calldelta(S + 0.01, r, vol, t) - calldelta(S, r, vol, t)) / 0.01;
        }
        public double callvega(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r, vol + 0.0001, t);
            double cp2 = CallPrice(S, r, vol - 0.0001, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double calltheta(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r, vol, t + 0.0001);
            double cp2 = CallPrice(S, r, vol, t - 0.0001);
            return (cp1 - cp2) / 0.0002;
        }
        public double callrho(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r + 0.0001, vol, t);
            double cp2 = CallPrice(S, r - 0.0001, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double putdelta(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S + 0.0001, r, vol, t);
            double cp2 = PutPrice(S - 0.0001, r, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double putgamma(double S, double r, double vol, double t)
        {
            return (putdelta(S + 0.01, r, vol, t) - putdelta(S, r, vol, t)) / 0.01;
        }
        public double putvega(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r, vol + 0.0001, t);
            double cp2 = PutPrice(S, r, vol - 0.0001, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double puttheta(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r, vol, t + 0.0001);
            double cp2 = PutPrice(S, r, vol, t - 0.0001);
            return (cp1 - cp2) / 0.0002;
        }
        public double putrho(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r + 0.0001, vol, t);
            double cp2 = PutPrice(S, r - 0.0001, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
    }

    public class BarrierUAO: Preparation,CallPrice, StandardError, PutPrice, CallGreek, PutGreek
    {
        public List<double> PriceList;
        public static double[,] RandomList;
        public void Initial()
        {
           optionprice = new double[Value.step + 1];
            PriceList = new List<double>();
        }
        public void RandomGenerated()
        {
            RandomList = new double[Value.trials, Value.step];
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                for (int k = 0; k < Value.step; k++)
                {
                    RandomList[i, k] = GaussianBoxMuller.NextDouble();
                }
            }
        }
        public double CallPrice(double S, double r, double vol, double t)
        {
            Initial();
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                optionprice[0] = S;
                for (int j = 1; j < Value.step + 1; j++)
                {
                    optionprice[j] = Price(optionprice[j - 1], r, vol, t, RandomList[i, j - 1]);
                }
                if (optionprice.Max() <= Value.barrier)
                { PriceList.Add(Math.Max(optionprice.Last() - Value.K, 0)); }
                else { PriceList.Add(0); }
            }
            double price = 0;
            price = Math.Exp(-r * t) * PriceList.Average();
            Program.increase(1);
            return price;
        }
        public double PutPrice(double S, double r, double vol, double t)
        {
            Initial();
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                optionprice[0] = S;
                for (uint j = 1; j < Value.step + 1; j++)
                {
                    optionprice[j] = Price(optionprice[j - 1], r, vol, t, RandomList[i, j - 1]);
                }
                if (optionprice.Max() <= Value.barrier)
                { PriceList.Add(Math.Max(Value.K - optionprice.Last(), 0)); }
                else { PriceList.Add(0); }
            }
            double price = 0;
            price = Math.Exp(-r * t) * PriceList.Average();
            Program.increase(1);
            return price;
        }
        public double SE()
        {
            double avg = PriceList.Average();
            double sum = PriceList.Sum(v => Math.Pow(v - avg, 2));
            return (Math.Sqrt(sum / (PriceList.Count - 1))) / (Math.Sqrt(PriceList.Count));
        }
        internal static class GaussianBoxMuller
        {
            public static double NextDouble()
            {
                double x, y, square;

                do
                {
                    x = 2 * RandomProvider.GetThreadRandom().NextDouble() - 1;
                    y = 2 * RandomProvider.GetThreadRandom().NextDouble() - 1;
                    square = (x * x) + (y * y);
                } while (square >= 1);

                return x * Math.Sqrt(-2 * Math.Log(square) / square);
            }
        }
        internal static class RandomProvider
        {
            private static ThreadLocal<Random> randomWrapper = new ThreadLocal<Random>(() =>
                new Random(Interlocked.Increment(ref seed))
            );

            private static int seed = Environment.TickCount * 10;

            public static Random GetThreadRandom()
            {
                return randomWrapper.Value;
            }
        }
        public double calldelta(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S + 0.0001, r, vol, t);
            double cp2 = CallPrice(S - 0.0001, r, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double callgamma(double S, double r, double vol, double t)
        {
            return (calldelta(S + 0.01, r, vol, t) - calldelta(S, r, vol, t)) / 0.01;
        }
        public double callvega(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r, vol + 0.0001, t);
            double cp2 = CallPrice(S, r, vol - 0.0001, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double calltheta(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r, vol, t + 0.0001);
            double cp2 = CallPrice(S, r, vol, t - 0.0001);
            return (cp1 - cp2) / 0.0002;
        }
        public double callrho(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r + 0.0001, vol, t);
            double cp2 = CallPrice(S, r - 0.0001, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double putdelta(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S + 0.0001, r, vol, t);
            double cp2 = PutPrice(S - 0.0001, r, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double putgamma(double S, double r, double vol, double t)
        {
            return (putdelta(S + 0.01, r, vol, t) - putdelta(S, r, vol, t)) / 0.01;
        }
        public double putvega(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r, vol + 0.0001, t);
            double cp2 = PutPrice(S, r, vol - 0.0001, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double puttheta(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r, vol, t + 0.0001);
            double cp2 = PutPrice(S, r, vol, t - 0.0001);
            return (cp1 - cp2) / 0.0002;
        }
        public double putrho(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r + 0.0001, vol, t);
            double cp2 = PutPrice(S, r - 0.0001, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
    }

    public class BarrierUAI: Preparation,CallPrice, StandardError, PutPrice, CallGreek, PutGreek
    {
        public List<double> PriceList;
        public static double[,] RandomList;
        public void Initial()
        {
            optionprice = new double[Value.step + 1];
            PriceList = new List<double>();
        }
        public void RandomGenerated()
        {
            RandomList = new double[Value.trials, Value.step];
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                for (int k = 0; k < Value.step; k++)
                {
                    RandomList[i, k] = GaussianBoxMuller.NextDouble();
                }
            }
        }
        public double CallPrice(double S, double r, double vol, double t)
        {
            Initial();
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                optionprice[0] = S;
                for (int j = 1; j < Value.step + 1; j++)
                {
                    optionprice[j] = Price(optionprice[j - 1], r, vol, t, RandomList[i, j - 1]);
                }
                if (optionprice.Max() > Value.barrier)
                { PriceList.Add(Math.Max(optionprice.Last() - Value.K, 0)); }
                else { PriceList.Add(0); }
            }
            double price = 0;
            price = Math.Exp(-r * t) * PriceList.Average();
            Program.increase(1);
            return price;
        }
        public double PutPrice(double S, double r, double vol, double t)
        {
            Initial();
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                optionprice[0] = S;
                for (uint j = 1; j < Value.step + 1; j++)
                {
                    optionprice[j] = Price(optionprice[j - 1], r, vol, t, RandomList[i, j - 1]);
                }
                if (optionprice.Max() > Value.barrier)
                { PriceList.Add(Math.Max(Value.K - optionprice.Last(), 0)); }
                else { PriceList.Add(0); }
            }
            double price = 0;
            price = Math.Exp(-r * t) * PriceList.Average();
            Program.increase(1);
            return price;
        }
        public double SE()
        {
            double avg = PriceList.Average();
            double sum = PriceList.Sum(v => Math.Pow(v - avg, 2));
            return (Math.Sqrt(sum / (PriceList.Count - 1))) / (Math.Sqrt(PriceList.Count));
        }
        internal static class GaussianBoxMuller
        {
            public static double NextDouble()
            {
                double x, y, square;

                do
                {
                    x = 2 * RandomProvider.GetThreadRandom().NextDouble() - 1;
                    y = 2 * RandomProvider.GetThreadRandom().NextDouble() - 1;
                    square = (x * x) + (y * y);
                } while (square >= 1);

                return x * Math.Sqrt(-2 * Math.Log(square) / square);
            }
        }
        internal static class RandomProvider
        {
            private static ThreadLocal<Random> randomWrapper = new ThreadLocal<Random>(() =>
                new Random(Interlocked.Increment(ref seed))
            );

            private static int seed = Environment.TickCount * 10;

            public static Random GetThreadRandom()
            {
                return randomWrapper.Value;
            }
        }
        public double calldelta(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S + 0.0001, r, vol, t);
            double cp2 = CallPrice(S - 0.0001, r, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double callgamma(double S, double r, double vol, double t)
        {
            return (calldelta(S + 0.01, r, vol, t) - calldelta(S, r, vol, t)) / 0.01;
        }
        public double callvega(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r, vol + 0.0001, t);
            double cp2 = CallPrice(S, r, vol - 0.0001, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double calltheta(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r, vol, t + 0.0001);
            double cp2 = CallPrice(S, r, vol, t - 0.0001);
            return (cp1 - cp2) / 0.0002;
        }
        public double callrho(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r + 0.0001, vol, t);
            double cp2 = CallPrice(S, r - 0.0001, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double putdelta(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S + 0.0001, r, vol, t);
            double cp2 = PutPrice(S - 0.0001, r, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double putgamma(double S, double r, double vol, double t)
        {
            return (putdelta(S + 0.01, r, vol, t) - putdelta(S, r, vol, t)) / 0.01;
        }
        public double putvega(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r, vol + 0.0001, t);
            double cp2 = PutPrice(S, r, vol - 0.0001, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double puttheta(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r, vol, t + 0.0001);
            double cp2 = PutPrice(S, r, vol, t - 0.0001);
            return (cp1 - cp2) / 0.0002;
        }
        public double putrho(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r + 0.0001, vol, t);
            double cp2 = PutPrice(S, r - 0.0001, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
    }

    public class Lookback: Preparation,CallPrice, StandardError, PutPrice, CallGreek, PutGreek
    {
        public List<double> PriceList;
        public static double[,] RandomList;
        public void Initial()
        {
           optionprice = new double[Value.step + 1];
            PriceList = new List<double>();
        }
        public void RandomGenerated()
        {
            RandomList = new double[Value.trials, Value.step];
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                for (int k = 0; k < Value.step; k++)
                {
                    RandomList[i, k] = GaussianBoxMuller.NextDouble();
                }
            }
        }
        public double CallPrice(double S, double r, double vol, double t)
        {
            Initial();
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                optionprice[0] = S;
                for (int j = 1; j < Value.step + 1; j++)
                {
                    optionprice[j] = Price(optionprice[j - 1], r, vol, t, RandomList[i, j - 1]);
                }
                PriceList.Add(Math.Max(optionprice.Max()-Value.K,0));
            }
            double price = 0;
            price = Math.Exp(-r * t) * PriceList.Average();
            Program.increase(1);
            return price;
        }
        public double PutPrice(double S, double r, double vol, double t)
        {
            Initial();
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                optionprice[0] = S;
                for (int j = 1; j < Value.step + 1; j++)
                {
                    optionprice[j] = Price(optionprice[j - 1], r, vol, t, RandomList[i, j - 1]);
                }
                PriceList.Add(Math.Max(Value.K - optionprice.Min(), 0));
            }
            double price = 0;
            price = Math.Exp(-r * t) * PriceList.Average();
            Program.increase(1);
            return price;
        }
        public double SE()
        {
            double avg = PriceList.Average();
            double sum = PriceList.Sum(v => Math.Pow(v - avg, 2));
            return (Math.Sqrt(sum / (PriceList.Count - 1))) / (Math.Sqrt(PriceList.Count));
        }
        internal static class GaussianBoxMuller
        {
            public static double NextDouble()
            {
                double x, y, square;

                do
                {
                    x = 2 * RandomProvider.GetThreadRandom().NextDouble() - 1;
                    y = 2 * RandomProvider.GetThreadRandom().NextDouble() - 1;
                    square = (x * x) + (y * y);
                } while (square >= 1);

                return x * Math.Sqrt(-2 * Math.Log(square) / square);
            }
        }
        internal static class RandomProvider
        {
            private static ThreadLocal<Random> randomWrapper = new ThreadLocal<Random>(() =>
                new Random(Interlocked.Increment(ref seed))
            );

            private static int seed = Environment.TickCount * 10;

            public static Random GetThreadRandom()
            {
                return randomWrapper.Value;
            }
        }
        public double calldelta(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S + 0.0001, r, vol, t);
            double cp2 = CallPrice(S - 0.0001, r, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double callgamma(double S, double r, double vol, double t)
        {
            return (calldelta(S + 0.01, r, vol, t) - calldelta(S, r, vol, t)) / 0.01;
        }
        public double callvega(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r, vol + 0.0001, t);
            double cp2 = CallPrice(S, r, vol - 0.0001, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double calltheta(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r, vol, t + 0.0001);
            double cp2 = CallPrice(S, r, vol, t - 0.0001);
            return (cp1 - cp2) / 0.0002;
        }
        public double callrho(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r + 0.0001, vol, t);
            double cp2 = CallPrice(S, r - 0.0001, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double putdelta(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S + 0.0001, r, vol, t);
            double cp2 = PutPrice(S - 0.0001, r, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double putgamma(double S, double r, double vol, double t)
        {
            return (putdelta(S + 0.01, r, vol, t) - putdelta(S, r, vol, t)) / 0.01;
        }
        public double putvega(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r, vol + 0.0001, t);
            double cp2 = PutPrice(S, r, vol - 0.0001, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double puttheta(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r, vol, t + 0.0001);
            double cp2 = PutPrice(S, r, vol, t - 0.0001);
            return (cp1 - cp2) / 0.0002;
        }
        public double putrho(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r + 0.0001, vol, t);
            double cp2 = PutPrice(S, r - 0.0001, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
    }

    public class Range: Preparation,CallPrice, StandardError, PutPrice, CallGreek, PutGreek
    {
        public List<double> PriceList;
        public static double[,] RandomList;
        public void Initial()
        {
            optionprice = new double[Value.step + 1];
            PriceList = new List<double>();
        }
        public void RandomGenerated()
        {
            RandomList = new double[Value.trials, Value.step];
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                for (int k = 0; k < Value.step; k++)
                {
                    RandomList[i, k] = GaussianBoxMuller.NextDouble();
                }
            }
        }
        public double CallPrice(double S, double r, double vol, double t)
        {
            Initial();
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                optionprice[0] = S;
                for (int j = 1; j < Value.step + 1; j++)
                {
                    optionprice[j] =Price(optionprice[j - 1], r, vol, t, RandomList[i, j - 1]);
                }
                PriceList.Add(optionprice.Max() - optionprice.Min());
            }
            double price = 0;
            price = Math.Exp(-r * t) * PriceList.Average();
            Program.increase(1);
            return price;
        }
        public double PutPrice(double S, double r, double vol, double t)
        {
            Initial();
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                optionprice[0] = S;
                for (int j = 1; j < Value.step + 1; j++)
                {
                    optionprice[j] = Price(optionprice[j - 1], r, vol, t, RandomList[i, j - 1]);
                }
                PriceList.Add(optionprice.Max() - optionprice.Min());
            }
            double price = 0;
            price = Math.Exp(-r * t) * PriceList.Average();
            Program.increase(1);
            return price;
        }
        public double SE()
        {
            double avg = PriceList.Average();
            double sum = PriceList.Sum(v => Math.Pow(v - avg, 2));
            return (Math.Sqrt(sum / (PriceList.Count - 1))) / (Math.Sqrt(PriceList.Count));
        }
        internal static class GaussianBoxMuller
        {
            public static double NextDouble()
            {
                double x, y, square;

                do
                {
                    x = 2 * RandomProvider.GetThreadRandom().NextDouble() - 1;
                    y = 2 * RandomProvider.GetThreadRandom().NextDouble() - 1;
                    square = (x * x) + (y * y);
                } while (square >= 1);

                return x * Math.Sqrt(-2 * Math.Log(square) / square);
            }
        }
        internal static class RandomProvider
        {
            private static ThreadLocal<Random> randomWrapper = new ThreadLocal<Random>(() =>
                new Random(Interlocked.Increment(ref seed))
            );

            private static int seed = Environment.TickCount * 10;

            public static Random GetThreadRandom()
            {
                return randomWrapper.Value;
            }
        }
        public double calldelta(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S + 0.0001, r, vol, t);
            double cp2 = CallPrice(S - 0.0001, r, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double callgamma(double S, double r, double vol, double t)
        {
            return (calldelta(S + 0.01, r, vol, t) - calldelta(S, r, vol, t)) / 0.01;
        }
        public double callvega(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r, vol + 0.0001, t);
            double cp2 = CallPrice(S, r, vol - 0.0001, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double calltheta(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r, vol, t + 0.0001);
            double cp2 = CallPrice(S, r, vol, t - 0.0001);
            return (cp1 - cp2) / 0.0002;
        }
        public double callrho(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r + 0.0001, vol, t);
            double cp2 = CallPrice(S, r - 0.0001, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double putdelta(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S + 0.0001, r, vol, t);
            double cp2 = PutPrice(S - 0.0001, r, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double putgamma(double S, double r, double vol, double t)
        {
            return (putdelta(S + 0.01, r, vol, t) - putdelta(S, r, vol, t)) / 0.01;
        }
        public double putvega(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r, vol + 0.0001, t);
            double cp2 = PutPrice(S, r, vol - 0.0001, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double puttheta(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r, vol, t + 0.0001);
            double cp2 = PutPrice(S, r, vol, t - 0.0001);
            return (cp1 - cp2) / 0.0002;
        }
        public double putrho(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r + 0.0001, vol, t);
            double cp2 = PutPrice(S, r - 0.0001, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
    }
}
