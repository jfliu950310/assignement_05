﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ConsoleApp6
{
    public class PreparationAntiCVMT
    {
        public double[,] optionprice { get; set; }
        public double[,] optioninverse { get; set; }
        public double[] Option { get; set; }
        public double[] Optioninverse { get; set; }
        public double Price(double S, double r, double vol, double t, double ep)
        {
            double price;
            double deltaT = t / Convert.ToDouble(Value.step);
            price = S * Math.Exp((r - Math.Pow(vol, 2) / 2) * deltaT + vol * Math.Sqrt(deltaT) * ep);
            return price;
        }
    }

    public class EuropeanAntiCVMT:PreparationAntiCVMT,CallPrice,StandardError,PutPrice,CallGreek,PutGreek
    {
        public static double[,] RandomList1;
        public static double[,] RandomList2;
        public void Initial()
        {
            optionprice = new double[Value.trials, Value.step + 1];
            optioninverse = new double[Value.trials, Value.step + 1];
            Option = new double[Value.trials];
            Optioninverse = new double[Value.trials];
        }
        public void RandomGenerated()
        {
            RandomList1 = new double[Value.trials, Value.step];
            Parallel.ForEach(Ienum.Step(0, Value.trials, 1), new ParallelOptions { MaxDegreeOfParallelism = Value.Thread }, i =>
            {
                for (int k = 0; k < Value.step; k++)
                {
                    RandomList1[i, k] = GaussianBoxMuller.NextDouble();
                }
            });
        }
        public void RandomAntithetic()
        {
            RandomList2 = new double[Value.trials, Value.step];
            Parallel.ForEach(Ienum.Step(0, Value.trials, 1), new ParallelOptions { MaxDegreeOfParallelism = Value.Thread }, i =>
            {
                for (int k = 0; k < Value.step; k++)
                {
                    RandomList2[i, k] = -RandomList1[i, k];
                }
            });
        }
        public double CallPrice(double S, double r, double vol, double t)
        {
            Initial();
            double deltaT = t / Convert.ToDouble(Value.step);
            RandomAntithetic();
            Parallel.ForEach(Ienum.Step(0, Value.trials, 1), new ParallelOptions { MaxDegreeOfParallelism = Value.Thread }, i =>
            {
                double tl1;
                double d1;
                double delta1;
                double tl2;
                double d2;
                double delta2;
                optionprice[i, 0] = S;
                optioninverse[i, 0] = S;
                double cv1 = 0;
                double cv2 = 0;
                for (int j = 1; j < Value.step + 1; j++)
                {
                    optionprice[i, j] = Price(optionprice[i, j - 1], r, vol, t, RandomList1[i, j - 1]);
                    tl1 = t - (j - 1) * deltaT;
                    d1 = (Math.Log(optionprice[i, j - 1] / Value.K) + (r + Math.Pow(vol, 2) / 2) * tl1) / (vol * Math.Sqrt(tl1));
                    delta1 = normalCDF(d1);
                    cv1 = cv1 + delta1 * (optionprice[i, j] - optionprice[i, j - 1] * Math.Exp(r * deltaT));
                    optioninverse[i, j] = Price(optioninverse[i, j - 1], r, vol, t, RandomList2[i, j - 1]);
                    tl2 = t - (j - 1) * deltaT;
                    d2 = (Math.Log(optioninverse[i, j - 1] / Value.K) + (r + Math.Pow(vol, 2) / 2) * tl2) / (vol * Math.Sqrt(tl2));
                    delta2 = normalCDF(d2);
                    cv2 = cv2 + delta2 * (optioninverse[i, j] - optioninverse[i, j - 1] * Math.Exp(r * deltaT));
                }
                Option[i] = Math.Max(optionprice[i, Value.step] - Value.K, 0) - cv1;
                Optioninverse[i] = Math.Max(optioninverse[i, Value.step] - Value.K, 0) - cv2;
            });
            double price1 = 0;
            price1 = Math.Exp(-r * t) * Option.Average();
            double price2 = 0;
            price2 = Math.Exp(-r * t) * Optioninverse.Average();
            Program.increase(1);
            return (price1 + price2) / 2;
        }
        public double PutPrice(double S, double r, double vol, double t)
        {
            Initial();
            RandomAntithetic();
            double deltaT = t / Convert.ToDouble(Value.step);
            Parallel.ForEach(Ienum.Step(0, Value.trials, 1), new ParallelOptions { MaxDegreeOfParallelism = Value.Thread }, i =>
            {
                double tl1;
                double tl2;
                double d1;
                double d2;
                double delta1;
                double delta2;
                optioninverse[i, 0] = S;
                optionprice[i, 0] = S;
                double cv1 = 0;
                double cv2 = 0;
                for (int j = 1; j < Value.step + 1; j++)
                {
                    optionprice[i, j] = Price(optionprice[i, j - 1], r, vol, t, RandomList1[i, j - 1]);
                    tl1 = t - (j - 1) * deltaT;
                    d1 = (Math.Log(optionprice[i, j - 1] / Value.K) + (r + Math.Pow(vol, 2) / 2) * tl1) / (vol * Math.Sqrt(tl1));
                    delta1 = normalCDF(d1) - 1;
                    cv1 = cv1 + delta1 * (optionprice[i, j] - optionprice[i, j - 1] * Math.Exp(r * deltaT));
                    optioninverse[i, j] = Price(optioninverse[i, j - 1], r, vol, t, RandomList2[i, j - 1]);
                    tl2 = t - (j - 1) * deltaT;
                    d2 = (Math.Log(optioninverse[i, j - 1] / Value.K) + (r + Math.Pow(vol, 2) / 2) * tl2) / (vol * Math.Sqrt(tl2));
                    delta2 = normalCDF(d2) - 1;
                    cv2 = cv2 + delta2 * (optioninverse[i, j] - optioninverse[i, j - 1] * Math.Exp(r * deltaT));
                }
                Option[i] = Math.Max(Value.K - optionprice[i, Value.step], 0) - cv1;
                Optioninverse[i] = Math.Max(Value.K - optioninverse[i, Value.step], 0) - cv2;
            });
            double price1 = 0;
            price1 = Math.Exp(-r * t) * Option.Average();
            double price2 = 0;
            price2 = Math.Exp(-r * t) * Optioninverse.Average();
            Program.increase(1);
            return (price1 + price2) / 2;
        }
        public double SE()
        {
            List<double> Option1 = new List<double>();
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                Option1.Add((Option[i] + Optioninverse[i]) / 2);
            }
            double avg = Option1.Average();
            double sum = Option1.Sum(v => Math.Pow(v - avg, 2));
            return (Math.Sqrt(sum / (Option1.Count - 1))) / (Math.Sqrt(Option1.Count));
        }
        public double normalCDF(double value)
        {
            const double A1 = 0.254829592;
            const double A2 = -0.284496736;
            const double A3 = 1.421413741;
            const double A4 = -1.453152027;
            const double A5 = 1.061405429;
            const double P = 0.3275911;

            int sign;
            if (value < 0)
                sign = -1;
            else
                sign = 1;

            double x = Math.Abs(value) / Math.Sqrt(2);
            double t = 1 / (1 + P * x);
            double erf = 1 - (((((A5 * t + A4) * t + A3) * t + A2) * t + A1) * t * Math.Exp(-x * x));


            return 0.5 * (1 + sign * erf);
        }
        internal static class GaussianBoxMuller
        {
            public static double NextDouble()
            {
                double x, y, square;

                do
                {
                    x = 2 * RandomProvider.GetThreadRandom().NextDouble() - 1;
                    y = 2 * RandomProvider.GetThreadRandom().NextDouble() - 1;
                    square = (x * x) + (y * y);
                } while (square >= 1);

                return x * Math.Sqrt(-2 * Math.Log(square) / square);
            }
        }
        internal static class RandomProvider
        {
            private static ThreadLocal<Random> randomWrapper = new ThreadLocal<Random>(() =>
                new Random(Interlocked.Increment(ref seed))
            );

            private static int seed = Environment.TickCount;

            public static Random GetThreadRandom()
            {
                return randomWrapper.Value;
            }
        }
        public double calldelta(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S + 0.0001, r, vol, t);
            double cp2 = CallPrice(S - 0.0001, r, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double callgamma(double S, double r, double vol, double t)
        {
            return (calldelta(S + 0.01, r, vol, t) - calldelta(S, r, vol, t)) / 0.01;
        }
        public double callvega(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r, vol + 0.0001, t);
            double cp2 = CallPrice(S, r, vol - 0.0001, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double calltheta(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r, vol, t + 0.0001);
            double cp2 = CallPrice(S, r, vol, t - 0.0001);
            return (cp1 - cp2) / 0.0002;
        }
        public double callrho(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r + 0.0001, vol, t);
            double cp2 = CallPrice(S, r - 0.0001, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double putdelta(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S + 0.0001, r, vol, t);
            double cp2 = PutPrice(S - 0.0001, r, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double putgamma(double S, double r, double vol, double t)
        {
            return (putdelta(S + 0.01, r, vol, t) - putdelta(S, r, vol, t)) / 0.01;
        }
        public double putvega(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r, vol + 0.0001, t);
            double cp2 = PutPrice(S, r, vol - 0.0001, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double puttheta(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r, vol, t + 0.0001);
            double cp2 = PutPrice(S, r, vol, t - 0.0001);
            return (cp1 - cp2) / 0.0002;
        }
        public double putrho(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r + 0.0001, vol, t);
            double cp2 = PutPrice(S, r - 0.0001, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
    }

    public class AsianAntiCVMT : PreparationAntiCVMT, CallPrice, StandardError, PutPrice, CallGreek, PutGreek
    {
        public static double[,] RandomList1;
        public static double[,] RandomList2;
        public void Initial()
        {
            optionprice = new double[Value.trials, Value.step + 1];
            optioninverse = new double[Value.trials, Value.step + 1];
            Option = new double[Value.trials];
            Optioninverse = new double[Value.trials];
        }
        public void RandomGenerated()
        {
            RandomList1 = new double[Value.trials, Value.step];
            Parallel.ForEach(Ienum.Step(0, Value.trials, 1), new ParallelOptions { MaxDegreeOfParallelism = Value.Thread }, i =>
            {
                for (int k = 0; k < Value.step; k++)
                {
                    RandomList1[i, k] = GaussianBoxMuller.NextDouble();
                }
            });
        }
        public void RandomAntithetic()
        {
            RandomList2 = new double[Value.trials, Value.step];
            Parallel.ForEach(Ienum.Step(0, Value.trials, 1), new ParallelOptions { MaxDegreeOfParallelism = Value.Thread }, i =>
            {
                for (int k = 0; k < Value.step; k++)
                {
                    RandomList2[i, k] = -RandomList1[i, k];
                }
            });
        }
        public double CallPrice(double S, double r, double vol, double t)
        {
            Initial();
            double deltaT = t / Convert.ToDouble(Value.step);
            RandomAntithetic();
            Parallel.ForEach(Ienum.Step(0, Value.trials, 1), new ParallelOptions { MaxDegreeOfParallelism = Value.Thread }, i =>
            {
                double tl1;
                double d1;
                double delta1;
                double tl2;
                double d2;
                double delta2;
                optionprice[i, 0] = S;
                optioninverse[i, 0] = S;
                double cv1 = 0;
                double cv2 = 0;
                for (int j = 1; j < Value.step + 1; j++)
                {
                    optionprice[i, j] = Price(optionprice[i, j - 1], r, vol, t, RandomList1[i, j - 1]);
                    tl1 = t - (j - 1) * deltaT;
                    d1 = (Math.Log(optionprice[i, j - 1] / Value.K) + (r + Math.Pow(vol, 2) / 2) * tl1) / (vol * Math.Sqrt(tl1));
                    delta1 = normalCDF(d1);
                    cv1 = cv1 + delta1 * (optionprice[i, j] - optionprice[i, j - 1] * Math.Exp(r * deltaT));
                    optioninverse[i, j] = Price(optioninverse[i, j - 1], r, vol, t, RandomList2[i, j - 1]);
                    tl2 = t - (j - 1) * deltaT;
                    d2 = (Math.Log(optioninverse[i, j - 1] / Value.K) + (r + Math.Pow(vol, 2) / 2) * tl2) / (vol * Math.Sqrt(tl2));
                    delta2 = normalCDF(d2);
                    cv2 = cv2 + delta2 * (optioninverse[i, j] - optioninverse[i, j - 1] * Math.Exp(r * deltaT));
                }
                double[] m = new double[Value.step + 1];
                double[] n = new double[Value.step + 1];
                for(int j = 0;j<Value.step+1;j++)
                {
                    m[j] = optionprice[i, j];
                    n[j] = optioninverse[i, j];
                }
                Option[i] = Math.Max((m.Average()*(Value.step+1)-m[0])/(Value.step) - Value.K, 0) - cv1;
                Optioninverse[i] = Math.Max((n.Average() * (Value.step + 1) - n[0]) / (Value.step) - Value.K, 0) - cv2;
            });
            double price1 = 0;
            price1 = Math.Exp(-r * t) * Option.Average();
            double price2 = 0;
            price2 = Math.Exp(-r * t) * Optioninverse.Average();
            Program.increase(1);
            return (price1 + price2) / 2;
        }
        public double PutPrice(double S, double r, double vol, double t)
        {
            Initial();
            RandomAntithetic();
            double deltaT = t / Convert.ToDouble(Value.step);
            Parallel.ForEach(Ienum.Step(0, Value.trials, 1), new ParallelOptions { MaxDegreeOfParallelism = Value.Thread }, i =>
            {
                double tl1;
                double tl2;
                double d1;
                double d2;
                double delta1;
                double delta2;
                optioninverse[i, 0] = S;
                optionprice[i, 0] = S;
                double cv1 = 0;
                double cv2 = 0;
                for (int j = 1; j < Value.step + 1; j++)
                {
                    optionprice[i, j] = Price(optionprice[i, j - 1], r, vol, t, RandomList1[i, j - 1]);
                    tl1 = t - (j - 1) * deltaT;
                    d1 = (Math.Log(optionprice[i, j - 1] / Value.K) + (r + Math.Pow(vol, 2) / 2) * tl1) / (vol * Math.Sqrt(tl1));
                    delta1 = normalCDF(d1) - 1;
                    cv1 = cv1 + delta1 * (optionprice[i, j] - optionprice[i, j - 1] * Math.Exp(r * deltaT));
                    optioninverse[i, j] = Price(optioninverse[i, j - 1], r, vol, t, RandomList2[i, j - 1]);
                    tl2 = t - (j - 1) * deltaT;
                    d2 = (Math.Log(optioninverse[i, j - 1] / Value.K) + (r + Math.Pow(vol, 2) / 2) * tl2) / (vol * Math.Sqrt(tl2));
                    delta2 = normalCDF(d2) - 1;
                    cv2 = cv2 + delta2 * (optioninverse[i, j] - optioninverse[i, j - 1] * Math.Exp(r * deltaT));
                }
                double[] m = new double[Value.step + 1];
                double[] n = new double[Value.step + 1];
                for (int j = 0; j < Value.step + 1; j++)
                {
                    m[j] = optionprice[i, j];
                    n[j] = optioninverse[i, j];
                }
                Option[i] = Math.Max(Value.K - (m.Average() * (Value.step + 1) - m[0]) / (Value.step), 0) - cv1;
                Optioninverse[i] = Math.Max(Value.K - (n.Average() * (Value.step + 1) - n[0]) / (Value.step), 0) - cv2;
            });
            double price1 = 0;
            price1 = Math.Exp(-r * t) * Option.Average();
            double price2 = 0;
            price2 = Math.Exp(-r * t) * Optioninverse.Average();
            Program.increase(1);
            return (price1 + price2) / 2;
        }
        public double SE()
        {
            List<double> Option1 = new List<double>();
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                Option1.Add((Option[i] + Optioninverse[i]) / 2);
            }
            double avg = Option1.Average();
            double sum = Option1.Sum(v => Math.Pow(v - avg, 2));
            return (Math.Sqrt(sum / (Option1.Count - 1))) / (Math.Sqrt(Option1.Count));
        }
        public double normalCDF(double value)
        {
            const double A1 = 0.254829592;
            const double A2 = -0.284496736;
            const double A3 = 1.421413741;
            const double A4 = -1.453152027;
            const double A5 = 1.061405429;
            const double P = 0.3275911;

            int sign;
            if (value < 0)
                sign = -1;
            else
                sign = 1;

            double x = Math.Abs(value) / Math.Sqrt(2);
            double t = 1 / (1 + P * x);
            double erf = 1 - (((((A5 * t + A4) * t + A3) * t + A2) * t + A1) * t * Math.Exp(-x * x));


            return 0.5 * (1 + sign * erf);
        }
        internal static class GaussianBoxMuller
        {
            public static double NextDouble()
            {
                double x, y, square;

                do
                {
                    x = 2 * RandomProvider.GetThreadRandom().NextDouble() - 1;
                    y = 2 * RandomProvider.GetThreadRandom().NextDouble() - 1;
                    square = (x * x) + (y * y);
                } while (square >= 1);

                return x * Math.Sqrt(-2 * Math.Log(square) / square);
            }
        }
        internal static class RandomProvider
        {
            private static ThreadLocal<Random> randomWrapper = new ThreadLocal<Random>(() =>
                new Random(Interlocked.Increment(ref seed))
            );

            private static int seed = Environment.TickCount;

            public static Random GetThreadRandom()
            {
                return randomWrapper.Value;
            }
        }
        public double calldelta(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S + 0.0001, r, vol, t);
            double cp2 = CallPrice(S - 0.0001, r, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double callgamma(double S, double r, double vol, double t)
        {
            return (calldelta(S + 0.01, r, vol, t) - calldelta(S, r, vol, t)) / 0.01;
        }
        public double callvega(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r, vol + 0.0001, t);
            double cp2 = CallPrice(S, r, vol - 0.0001, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double calltheta(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r, vol, t + 0.0001);
            double cp2 = CallPrice(S, r, vol, t - 0.0001);
            return (cp1 - cp2) / 0.0002;
        }
        public double callrho(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r + 0.0001, vol, t);
            double cp2 = CallPrice(S, r - 0.0001, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double putdelta(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S + 0.0001, r, vol, t);
            double cp2 = PutPrice(S - 0.0001, r, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double putgamma(double S, double r, double vol, double t)
        {
            return (putdelta(S + 0.01, r, vol, t) - putdelta(S, r, vol, t)) / 0.01;
        }
        public double putvega(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r, vol + 0.0001, t);
            double cp2 = PutPrice(S, r, vol - 0.0001, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double puttheta(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r, vol, t + 0.0001);
            double cp2 = PutPrice(S, r, vol, t - 0.0001);
            return (cp1 - cp2) / 0.0002;
        }
        public double putrho(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r + 0.0001, vol, t);
            double cp2 = PutPrice(S, r - 0.0001, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
    }

    public class DigitalAntiCVMT : PreparationAntiCVMT, CallPrice, StandardError, PutPrice, CallGreek, PutGreek
    {
        public static double[,] RandomList1;
        public static double[,] RandomList2;
        public void Initial()
        {
            optionprice = new double[Value.trials, Value.step + 1];
            optioninverse = new double[Value.trials, Value.step + 1];
            Option = new double[Value.trials];
            Optioninverse = new double[Value.trials];
        }
        public void RandomGenerated()
        {
            RandomList1 = new double[Value.trials, Value.step];
            Parallel.ForEach(Ienum.Step(0, Value.trials, 1), new ParallelOptions { MaxDegreeOfParallelism = Value.Thread }, i =>
            {
                for (int k = 0; k < Value.step; k++)
                {
                    RandomList1[i, k] = GaussianBoxMuller.NextDouble();
                }
            });
        }
        public void RandomAntithetic()
        {
            RandomList2 = new double[Value.trials, Value.step];
            Parallel.ForEach(Ienum.Step(0, Value.trials, 1), new ParallelOptions { MaxDegreeOfParallelism = Value.Thread }, i =>
            {
                for (int k = 0; k < Value.step; k++)
                {
                    RandomList2[i, k] = -RandomList1[i, k];
                }
            });
        }
        public double CallPrice(double S, double r, double vol, double t)
        {
            Initial();
            double deltaT = t / Convert.ToDouble(Value.step);
            RandomAntithetic();
            Parallel.ForEach(Ienum.Step(0, Value.trials, 1), new ParallelOptions { MaxDegreeOfParallelism = Value.Thread }, i =>
            {
                double tl1;
                double d1;
                double delta1;
                double tl2;
                double d2;
                double delta2;
                optionprice[i, 0] = S;
                optioninverse[i, 0] = S;
                double cv1 = 0;
                double cv2 = 0;
                for (int j = 1; j < Value.step + 1; j++)
                {
                    optionprice[i, j] = Price(optionprice[i, j - 1], r, vol, t, RandomList1[i, j - 1]);
                    tl1 = t - (j - 1) * deltaT;
                    d1 = (Math.Log(optionprice[i, j - 1] / Value.K) + (r + Math.Pow(vol, 2) / 2) * tl1) / (vol * Math.Sqrt(tl1));
                    delta1 = normalCDF(d1);
                    cv1 = cv1 + delta1 * (optionprice[i, j] - optionprice[i, j - 1] * Math.Exp(r * deltaT));
                    optioninverse[i, j] = Price(optioninverse[i, j - 1], r, vol, t, RandomList2[i, j - 1]);
                    tl2 = t - (j - 1) * deltaT;
                    d2 = (Math.Log(optioninverse[i, j - 1] / Value.K) + (r + Math.Pow(vol, 2) / 2) * tl2) / (vol * Math.Sqrt(tl2));
                    delta2 = normalCDF(d2);
                    cv2 = cv2 + delta2 * (optioninverse[i, j] - optioninverse[i, j - 1] * Math.Exp(r * deltaT));
                }
                if (optionprice[i, Value.step] - Value.K >= 0)
                { Option[i] = Value.rebate - cv1; }
                else { Option[i] = 0 - cv1;}
                if (optioninverse[i, Value.step] - Value.K >= 0)
                { Optioninverse[i] = Value.rebate - cv2; }
                else { Optioninverse[i] = 0 - cv2; }
            });
            double price1 = 0;
            price1 = Math.Exp(-r * t) * Option.Average();
            double price2 = 0;
            price2 = Math.Exp(-r * t) * Optioninverse.Average();
            Program.increase(1);
            return (price1 + price2) / 2;
        }
        public double PutPrice(double S, double r, double vol, double t)
        {
            Initial();
            RandomAntithetic();
            double deltaT = t / Convert.ToDouble(Value.step);
            Parallel.ForEach(Ienum.Step(0, Value.trials, 1), new ParallelOptions { MaxDegreeOfParallelism = Value.Thread }, i =>
            {
                double tl1;
                double tl2;
                double d1;
                double d2;
                double delta1;
                double delta2;
                optioninverse[i, 0] = S;
                optionprice[i, 0] = S;
                double cv1 = 0;
                double cv2 = 0;
                for (int j = 1; j < Value.step + 1; j++)
                {
                    optionprice[i, j] = Price(optionprice[i, j - 1], r, vol, t, RandomList1[i, j - 1]);
                    tl1 = t - (j - 1) * deltaT;
                    d1 = (Math.Log(optionprice[i, j - 1] / Value.K) + (r + Math.Pow(vol, 2) / 2) * tl1) / (vol * Math.Sqrt(tl1));
                    delta1 = normalCDF(d1) - 1;
                    cv1 = cv1 + delta1 * (optionprice[i, j] - optionprice[i, j - 1] * Math.Exp(r * deltaT));
                    optioninverse[i, j] = Price(optioninverse[i, j - 1], r, vol, t, RandomList2[i, j - 1]);
                    tl2 = t - (j - 1) * deltaT;
                    d2 = (Math.Log(optioninverse[i, j - 1] / Value.K) + (r + Math.Pow(vol, 2) / 2) * tl2) / (vol * Math.Sqrt(tl2));
                    delta2 = normalCDF(d2) - 1;
                    cv2 = cv2 + delta2 * (optioninverse[i, j] - optioninverse[i, j - 1] * Math.Exp(r * deltaT));
                }
                if (Value.K - optionprice[i, Value.step] >= 0)
                { Option[i] = Value.rebate - cv1; }
                else { Option[i] = 0 - cv1; }
                if (Value.K - optioninverse[i, Value.step] >= 0)
                { Optioninverse[i] = Value.rebate - cv2; }
                else { Optioninverse[i] = 0 - cv2; }
            });
            double price1 = 0;
            price1 = Math.Exp(-r * t) * Option.Average();
            double price2 = 0;
            price2 = Math.Exp(-r * t) * Optioninverse.Average();
            Program.increase(1);
            return (price1 + price2) / 2;
        }
        public double SE()
        {
            List<double> Option1 = new List<double>();
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                Option1.Add((Option[i] + Optioninverse[i]) / 2);
            }
            double avg = Option1.Average();
            double sum = Option1.Sum(v => Math.Pow(v - avg, 2));
            return (Math.Sqrt(sum / (Option1.Count - 1))) / (Math.Sqrt(Option1.Count));
        }
        public double normalCDF(double value)
        {
            const double A1 = 0.254829592;
            const double A2 = -0.284496736;
            const double A3 = 1.421413741;
            const double A4 = -1.453152027;
            const double A5 = 1.061405429;
            const double P = 0.3275911;

            int sign;
            if (value < 0)
                sign = -1;
            else
                sign = 1;

            double x = Math.Abs(value) / Math.Sqrt(2);
            double t = 1 / (1 + P * x);
            double erf = 1 - (((((A5 * t + A4) * t + A3) * t + A2) * t + A1) * t * Math.Exp(-x * x));


            return 0.5 * (1 + sign * erf);
        }
        internal static class GaussianBoxMuller
        {
            public static double NextDouble()
            {
                double x, y, square;

                do
                {
                    x = 2 * RandomProvider.GetThreadRandom().NextDouble() - 1;
                    y = 2 * RandomProvider.GetThreadRandom().NextDouble() - 1;
                    square = (x * x) + (y * y);
                } while (square >= 1);

                return x * Math.Sqrt(-2 * Math.Log(square) / square);
            }
        }
        internal static class RandomProvider
        {
            private static ThreadLocal<Random> randomWrapper = new ThreadLocal<Random>(() =>
                new Random(Interlocked.Increment(ref seed))
            );

            private static int seed = Environment.TickCount;

            public static Random GetThreadRandom()
            {
                return randomWrapper.Value;
            }
        }
        public double calldelta(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S + 0.0001, r, vol, t);
            double cp2 = CallPrice(S - 0.0001, r, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double callgamma(double S, double r, double vol, double t)
        {
            return (calldelta(S + 0.01, r, vol, t) - calldelta(S, r, vol, t)) / 0.01;
        }
        public double callvega(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r, vol + 0.0001, t);
            double cp2 = CallPrice(S, r, vol - 0.0001, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double calltheta(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r, vol, t + 0.0001);
            double cp2 = CallPrice(S, r, vol, t - 0.0001);
            return (cp1 - cp2) / 0.0002;
        }
        public double callrho(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r + 0.0001, vol, t);
            double cp2 = CallPrice(S, r - 0.0001, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double putdelta(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S + 0.0001, r, vol, t);
            double cp2 = PutPrice(S - 0.0001, r, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double putgamma(double S, double r, double vol, double t)
        {
            return (putdelta(S + 0.01, r, vol, t) - putdelta(S, r, vol, t)) / 0.01;
        }
        public double putvega(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r, vol + 0.0001, t);
            double cp2 = PutPrice(S, r, vol - 0.0001, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double puttheta(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r, vol, t + 0.0001);
            double cp2 = PutPrice(S, r, vol, t - 0.0001);
            return (cp1 - cp2) / 0.0002;
        }
        public double putrho(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r + 0.0001, vol, t);
            double cp2 = PutPrice(S, r - 0.0001, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
    }

    public class BarrierDAOAntiCVMT : PreparationAntiCVMT, CallPrice, StandardError, PutPrice, CallGreek, PutGreek
    {
        public static double[,] RandomList1;
        public static double[,] RandomList2;
        public void Initial()
        {
            optionprice = new double[Value.trials, Value.step + 1];
            optioninverse = new double[Value.trials, Value.step + 1];
            Option = new double[Value.trials];
            Optioninverse = new double[Value.trials];
        }
        public void RandomGenerated()
        {
            RandomList1 = new double[Value.trials, Value.step];
            Parallel.ForEach(Ienum.Step(0, Value.trials, 1), new ParallelOptions { MaxDegreeOfParallelism = Value.Thread }, i =>
            {
                for (int k = 0; k < Value.step; k++)
                {
                    RandomList1[i, k] = GaussianBoxMuller.NextDouble();
                }
            });
        }
        public void RandomAntithetic()
        {
            RandomList2 = new double[Value.trials, Value.step];
            Parallel.ForEach(Ienum.Step(0, Value.trials, 1), new ParallelOptions { MaxDegreeOfParallelism = Value.Thread }, i =>
            {
                for (int k = 0; k < Value.step; k++)
                {
                    RandomList2[i, k] = -RandomList1[i, k];
                }
            });
        }
        public double CallPrice(double S, double r, double vol, double t)
        {
            Initial();
            double deltaT = t / Convert.ToDouble(Value.step);
            RandomAntithetic();
            Parallel.ForEach(Ienum.Step(0, Value.trials, 1), new ParallelOptions { MaxDegreeOfParallelism = Value.Thread }, i =>
            {
                double tl1;
                double d1;
                double delta1;
                double tl2;
                double d2;
                double delta2;
                optionprice[i, 0] = S;
                optioninverse[i, 0] = S;
                double cv1 = 0;
                double cv2 = 0;
                for (int j = 1; j < Value.step + 1; j++)
                {
                    optionprice[i, j] = Price(optionprice[i, j - 1], r, vol, t, RandomList1[i, j - 1]);
                    tl1 = t - (j - 1) * deltaT;
                    d1 = (Math.Log(optionprice[i, j - 1] / Value.K) + (r + Math.Pow(vol, 2) / 2) * tl1) / (vol * Math.Sqrt(tl1));
                    delta1 = normalCDF(d1);
                    cv1 = cv1 + delta1 * (optionprice[i, j] - optionprice[i, j - 1] * Math.Exp(r * deltaT));
                    optioninverse[i, j] = Price(optioninverse[i, j - 1], r, vol, t, RandomList2[i, j - 1]);
                    tl2 = t - (j - 1) * deltaT;
                    d2 = (Math.Log(optioninverse[i, j - 1] / Value.K) + (r + Math.Pow(vol, 2) / 2) * tl2) / (vol * Math.Sqrt(tl2));
                    delta2 = normalCDF(d2);
                    cv2 = cv2 + delta2 * (optioninverse[i, j] - optioninverse[i, j - 1] * Math.Exp(r * deltaT));
                }
                double[] m = new double[Value.step + 1];
                double[] n = new double[Value.step + 1];
                for (int j = 0; j < Value.step + 1; j++)
                {
                    m[j] = optionprice[i, j];
                    n[j] = optioninverse[i, j];
                }
                if (m.Min() >= Value.barrier)
                { Option[i] = Math.Max(optionprice[i, Value.step] - Value.K, 0) - cv1; }
                else { Option[i] = 0; }
                if (n.Min() >= Value.barrier)
                { Optioninverse[i] = Math.Max(optioninverse[i, Value.step] - Value.K, 0) - cv2; }
                else { Optioninverse[i] = 0; }
            });
            double price1 = 0;
            price1 = Math.Exp(-r * t) * Option.Average();
            double price2 = 0;
            price2 = Math.Exp(-r * t) * Optioninverse.Average();
            Program.increase(1);
            return (price1 + price2) / 2;
        }
        public double PutPrice(double S, double r, double vol, double t)
        {
            Initial();
            RandomAntithetic();
            double deltaT = t / Convert.ToDouble(Value.step);
            Parallel.ForEach(Ienum.Step(0, Value.trials, 1), new ParallelOptions { MaxDegreeOfParallelism = Value.Thread }, i =>
            {
                double tl1;
                double tl2;
                double d1;
                double d2;
                double delta1;
                double delta2;
                optioninverse[i, 0] = S;
                optionprice[i, 0] = S;
                double cv1 = 0;
                double cv2 = 0;
                for (int j = 1; j < Value.step + 1; j++)
                {
                    optionprice[i, j] = Price(optionprice[i, j - 1], r, vol, t, RandomList1[i, j - 1]);
                    tl1 = t - (j - 1) * deltaT;
                    d1 = (Math.Log(optionprice[i, j - 1] / Value.K) + (r + Math.Pow(vol, 2) / 2) * tl1) / (vol * Math.Sqrt(tl1));
                    delta1 = normalCDF(d1) - 1;
                    cv1 = cv1 + delta1 * (optionprice[i, j] - optionprice[i, j - 1] * Math.Exp(r * deltaT));
                    optioninverse[i, j] = Price(optioninverse[i, j - 1], r, vol, t, RandomList2[i, j - 1]);
                    tl2 = t - (j - 1) * deltaT;
                    d2 = (Math.Log(optioninverse[i, j - 1] / Value.K) + (r + Math.Pow(vol, 2) / 2) * tl2) / (vol * Math.Sqrt(tl2));
                    delta2 = normalCDF(d2) - 1;
                    cv2 = cv2 + delta2 * (optioninverse[i, j] - optioninverse[i, j - 1] * Math.Exp(r * deltaT));
                }
                double[] m = new double[Value.step + 1];
                double[] n = new double[Value.step + 1];
                for (int j = 0; j < Value.step + 1; j++)
                {
                    m[j] = optionprice[i, j];
                    n[j] = optioninverse[i, j];
                }
                if (m.Min() >= Value.barrier)
                { Option[i] = Math.Max(Value.K - optionprice[i, Value.step], 0) - cv1; }
                else { Option[i] = 0; }
                if (n.Min() >= Value.barrier)
                { Optioninverse[i] = Math.Max(Value.K - optioninverse[i, Value.step], 0) - cv2; }
                else { Optioninverse[i] = 0; }
            });
            double price1 = 0;
            price1 = Math.Exp(-r * t) * Option.Average();
            double price2 = 0;
            price2 = Math.Exp(-r * t) * Optioninverse.Average();
            Program.increase(1);
            return (price1 + price2) / 2;
        }
        public double SE()
        {
            List<double> Option1 = new List<double>();
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                Option1.Add((Option[i] + Optioninverse[i]) / 2);
            }
            double avg = Option1.Average();
            double sum = Option1.Sum(v => Math.Pow(v - avg, 2));
            return (Math.Sqrt(sum / (Option1.Count - 1))) / (Math.Sqrt(Option1.Count));
        }
        public double normalCDF(double value)
        {
            const double A1 = 0.254829592;
            const double A2 = -0.284496736;
            const double A3 = 1.421413741;
            const double A4 = -1.453152027;
            const double A5 = 1.061405429;
            const double P = 0.3275911;

            int sign;
            if (value < 0)
                sign = -1;
            else
                sign = 1;

            double x = Math.Abs(value) / Math.Sqrt(2);
            double t = 1 / (1 + P * x);
            double erf = 1 - (((((A5 * t + A4) * t + A3) * t + A2) * t + A1) * t * Math.Exp(-x * x));


            return 0.5 * (1 + sign * erf);
        }
        internal static class GaussianBoxMuller
        {
            public static double NextDouble()
            {
                double x, y, square;

                do
                {
                    x = 2 * RandomProvider.GetThreadRandom().NextDouble() - 1;
                    y = 2 * RandomProvider.GetThreadRandom().NextDouble() - 1;
                    square = (x * x) + (y * y);
                } while (square >= 1);

                return x * Math.Sqrt(-2 * Math.Log(square) / square);
            }
        }
        internal static class RandomProvider
        {
            private static ThreadLocal<Random> randomWrapper = new ThreadLocal<Random>(() =>
                new Random(Interlocked.Increment(ref seed))
            );

            private static int seed = Environment.TickCount;

            public static Random GetThreadRandom()
            {
                return randomWrapper.Value;
            }
        }
        public double calldelta(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S + 0.0001, r, vol, t);
            double cp2 = CallPrice(S - 0.0001, r, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double callgamma(double S, double r, double vol, double t)
        {
            return (calldelta(S + 0.01, r, vol, t) - calldelta(S, r, vol, t)) / 0.01;
        }
        public double callvega(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r, vol + 0.0001, t);
            double cp2 = CallPrice(S, r, vol - 0.0001, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double calltheta(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r, vol, t + 0.0001);
            double cp2 = CallPrice(S, r, vol, t - 0.0001);
            return (cp1 - cp2) / 0.0002;
        }
        public double callrho(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r + 0.0001, vol, t);
            double cp2 = CallPrice(S, r - 0.0001, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double putdelta(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S + 0.0001, r, vol, t);
            double cp2 = PutPrice(S - 0.0001, r, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double putgamma(double S, double r, double vol, double t)
        {
            return (putdelta(S + 0.01, r, vol, t) - putdelta(S, r, vol, t)) / 0.01;
        }
        public double putvega(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r, vol + 0.0001, t);
            double cp2 = PutPrice(S, r, vol - 0.0001, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double puttheta(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r, vol, t + 0.0001);
            double cp2 = PutPrice(S, r, vol, t - 0.0001);
            return (cp1 - cp2) / 0.0002;
        }
        public double putrho(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r + 0.0001, vol, t);
            double cp2 = PutPrice(S, r - 0.0001, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
    }

    public class BarrierDAIAntiCVMT : PreparationAntiCVMT, CallPrice, StandardError, PutPrice, CallGreek, PutGreek
    {
        public static double[,] RandomList1;
        public static double[,] RandomList2;
        public void Initial()
        {
            optionprice = new double[Value.trials, Value.step + 1];
            optioninverse = new double[Value.trials, Value.step + 1];
            Option = new double[Value.trials];
            Optioninverse = new double[Value.trials];
        }
        public void RandomGenerated()
        {
            RandomList1 = new double[Value.trials, Value.step];
            Parallel.ForEach(Ienum.Step(0, Value.trials, 1), new ParallelOptions { MaxDegreeOfParallelism = Value.Thread }, i =>
            {
                for (int k = 0; k < Value.step; k++)
                {
                    RandomList1[i, k] = GaussianBoxMuller.NextDouble();
                }
            });
        }
        public void RandomAntithetic()
        {
            RandomList2 = new double[Value.trials, Value.step];
            Parallel.ForEach(Ienum.Step(0, Value.trials, 1), new ParallelOptions { MaxDegreeOfParallelism = Value.Thread }, i =>
            {
                for (int k = 0; k < Value.step; k++)
                {
                    RandomList2[i, k] = -RandomList1[i, k];
                }
            });
        }
        public double CallPrice(double S, double r, double vol, double t)
        {
            Initial();
            double deltaT = t / Convert.ToDouble(Value.step);
            RandomAntithetic();
            Parallel.ForEach(Ienum.Step(0, Value.trials, 1), new ParallelOptions { MaxDegreeOfParallelism = Value.Thread }, i =>
            {
                double tl1;
                double d1;
                double delta1;
                double tl2;
                double d2;
                double delta2;
                optionprice[i, 0] = S;
                optioninverse[i, 0] = S;
                double cv1 = 0;
                double cv2 = 0;
                for (int j = 1; j < Value.step + 1; j++)
                {
                    optionprice[i, j] = Price(optionprice[i, j - 1], r, vol, t, RandomList1[i, j - 1]);
                    tl1 = t - (j - 1) * deltaT;
                    d1 = (Math.Log(optionprice[i, j - 1] / Value.K) + (r + Math.Pow(vol, 2) / 2) * tl1) / (vol * Math.Sqrt(tl1));
                    delta1 = normalCDF(d1);
                    cv1 = cv1 + delta1 * (optionprice[i, j] - optionprice[i, j - 1] * Math.Exp(r * deltaT));
                    optioninverse[i, j] = Price(optioninverse[i, j - 1], r, vol, t, RandomList2[i, j - 1]);
                    tl2 = t - (j - 1) * deltaT;
                    d2 = (Math.Log(optioninverse[i, j - 1] / Value.K) + (r + Math.Pow(vol, 2) / 2) * tl2) / (vol * Math.Sqrt(tl2));
                    delta2 = normalCDF(d2);
                    cv2 = cv2 + delta2 * (optioninverse[i, j] - optioninverse[i, j - 1] * Math.Exp(r * deltaT));
                }
                double[] m = new double[Value.step + 1];
                double[] n = new double[Value.step + 1];
                for (int j = 0; j < Value.step + 1; j++)
                {
                    m[j] = optionprice[i, j];
                    n[j] = optioninverse[i, j];
                }
                if (m.Min() < Value.barrier)
                { Option[i] = Math.Max(optionprice[i, Value.step] - Value.K, 0) - cv1; }
                else { Option[i] = 0; }
                if (n.Min() < Value.barrier)
                { Optioninverse[i] = Math.Max(optioninverse[i, Value.step] - Value.K, 0) - cv2; }
                else { Optioninverse[i] = 0; }
            });
            double price1 = 0;
            price1 = Math.Exp(-r * t) * Option.Average();
            double price2 = 0;
            price2 = Math.Exp(-r * t) * Optioninverse.Average();
            Program.increase(1);
            return (price1 + price2) / 2;
        }
        public double PutPrice(double S, double r, double vol, double t)
        {
            Initial();
            RandomAntithetic();
            double deltaT = t / Convert.ToDouble(Value.step);
            Parallel.ForEach(Ienum.Step(0, Value.trials, 1), new ParallelOptions { MaxDegreeOfParallelism = Value.Thread }, i =>
            {
                double tl1;
                double tl2;
                double d1;
                double d2;
                double delta1;
                double delta2;
                optioninverse[i, 0] = S;
                optionprice[i, 0] = S;
                double cv1 = 0;
                double cv2 = 0;
                for (int j = 1; j < Value.step + 1; j++)
                {
                    optionprice[i, j] = Price(optionprice[i, j - 1], r, vol, t, RandomList1[i, j - 1]);
                    tl1 = t - (j - 1) * deltaT;
                    d1 = (Math.Log(optionprice[i, j - 1] / Value.K) + (r + Math.Pow(vol, 2) / 2) * tl1) / (vol * Math.Sqrt(tl1));
                    delta1 = normalCDF(d1) - 1;
                    cv1 = cv1 + delta1 * (optionprice[i, j] - optionprice[i, j - 1] * Math.Exp(r * deltaT));
                    optioninverse[i, j] = Price(optioninverse[i, j - 1], r, vol, t, RandomList2[i, j - 1]);
                    tl2 = t - (j - 1) * deltaT;
                    d2 = (Math.Log(optioninverse[i, j - 1] / Value.K) + (r + Math.Pow(vol, 2) / 2) * tl2) / (vol * Math.Sqrt(tl2));
                    delta2 = normalCDF(d2) - 1;
                    cv2 = cv2 + delta2 * (optioninverse[i, j] - optioninverse[i, j - 1] * Math.Exp(r * deltaT));
                }
                double[] m = new double[Value.step + 1];
                double[] n = new double[Value.step + 1];
                for (int j = 0; j < Value.step + 1; j++)
                {
                    m[j] = optionprice[i, j];
                    n[j] = optioninverse[i, j];
                }
                if (m.Min() < Value.barrier)
                { Option[i] = Math.Max(Value.K - optionprice[i, Value.step], 0) - cv1; }
                else { Option[i] = 0; }
                if (n.Min() < Value.barrier)
                { Optioninverse[i] = Math.Max(Value.K - optioninverse[i, Value.step], 0) - cv2; }
                else { Optioninverse[i] = 0; }
            });
            double price1 = 0;
            price1 = Math.Exp(-r * t) * Option.Average();
            double price2 = 0;
            price2 = Math.Exp(-r * t) * Optioninverse.Average();
            Program.increase(1);
            return (price1 + price2) / 2;
        }
        public double SE()
        {
            List<double> Option1 = new List<double>();
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                Option1.Add((Option[i] + Optioninverse[i]) / 2);
            }
            double avg = Option1.Average();
            double sum = Option1.Sum(v => Math.Pow(v - avg, 2));
            return (Math.Sqrt(sum / (Option1.Count - 1))) / (Math.Sqrt(Option1.Count));
        }
        public double normalCDF(double value)
        {
            const double A1 = 0.254829592;
            const double A2 = -0.284496736;
            const double A3 = 1.421413741;
            const double A4 = -1.453152027;
            const double A5 = 1.061405429;
            const double P = 0.3275911;

            int sign;
            if (value < 0)
                sign = -1;
            else
                sign = 1;

            double x = Math.Abs(value) / Math.Sqrt(2);
            double t = 1 / (1 + P * x);
            double erf = 1 - (((((A5 * t + A4) * t + A3) * t + A2) * t + A1) * t * Math.Exp(-x * x));


            return 0.5 * (1 + sign * erf);
        }
        internal static class GaussianBoxMuller
        {
            public static double NextDouble()
            {
                double x, y, square;

                do
                {
                    x = 2 * RandomProvider.GetThreadRandom().NextDouble() - 1;
                    y = 2 * RandomProvider.GetThreadRandom().NextDouble() - 1;
                    square = (x * x) + (y * y);
                } while (square >= 1);

                return x * Math.Sqrt(-2 * Math.Log(square) / square);
            }
        }
        internal static class RandomProvider
        {
            private static ThreadLocal<Random> randomWrapper = new ThreadLocal<Random>(() =>
                new Random(Interlocked.Increment(ref seed))
            );

            private static int seed = Environment.TickCount;

            public static Random GetThreadRandom()
            {
                return randomWrapper.Value;
            }
        }
        public double calldelta(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S + 0.0001, r, vol, t);
            double cp2 = CallPrice(S - 0.0001, r, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double callgamma(double S, double r, double vol, double t)
        {
            return (calldelta(S + 0.01, r, vol, t) - calldelta(S, r, vol, t)) / 0.01;
        }
        public double callvega(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r, vol + 0.0001, t);
            double cp2 = CallPrice(S, r, vol - 0.0001, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double calltheta(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r, vol, t + 0.0001);
            double cp2 = CallPrice(S, r, vol, t - 0.0001);
            return (cp1 - cp2) / 0.0002;
        }
        public double callrho(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r + 0.0001, vol, t);
            double cp2 = CallPrice(S, r - 0.0001, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double putdelta(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S + 0.0001, r, vol, t);
            double cp2 = PutPrice(S - 0.0001, r, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double putgamma(double S, double r, double vol, double t)
        {
            return (putdelta(S + 0.01, r, vol, t) - putdelta(S, r, vol, t)) / 0.01;
        }
        public double putvega(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r, vol + 0.0001, t);
            double cp2 = PutPrice(S, r, vol - 0.0001, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double puttheta(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r, vol, t + 0.0001);
            double cp2 = PutPrice(S, r, vol, t - 0.0001);
            return (cp1 - cp2) / 0.0002;
        }
        public double putrho(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r + 0.0001, vol, t);
            double cp2 = PutPrice(S, r - 0.0001, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
    }

    public class BarrierUAOAntiCVMT : PreparationAntiCVMT, CallPrice, StandardError, PutPrice, CallGreek, PutGreek
    {
        public static double[,] RandomList1;
        public static double[,] RandomList2;
        public void Initial()
        {
            optionprice = new double[Value.trials, Value.step + 1];
            optioninverse = new double[Value.trials, Value.step + 1];
            Option = new double[Value.trials];
            Optioninverse = new double[Value.trials];
        }
        public void RandomGenerated()
        {
            RandomList1 = new double[Value.trials, Value.step];
            Parallel.ForEach(Ienum.Step(0, Value.trials, 1), new ParallelOptions { MaxDegreeOfParallelism = Value.Thread }, i =>
            {
                for (int k = 0; k < Value.step; k++)
                {
                    RandomList1[i, k] = GaussianBoxMuller.NextDouble();
                }
            });
        }
        public void RandomAntithetic()
        {
            RandomList2 = new double[Value.trials, Value.step];
            Parallel.ForEach(Ienum.Step(0, Value.trials, 1), new ParallelOptions { MaxDegreeOfParallelism = Value.Thread }, i =>
            {
                for (int k = 0; k < Value.step; k++)
                {
                    RandomList2[i, k] = -RandomList1[i, k];
                }
            });
        }
        public double CallPrice(double S, double r, double vol, double t)
        {
            Initial();
            double deltaT = t / Convert.ToDouble(Value.step);
            RandomAntithetic();
            Parallel.ForEach(Ienum.Step(0, Value.trials, 1), new ParallelOptions { MaxDegreeOfParallelism = Value.Thread }, i =>
            {
                double tl1;
                double d1;
                double delta1;
                double tl2;
                double d2;
                double delta2;
                optionprice[i, 0] = S;
                optioninverse[i, 0] = S;
                double cv1 = 0;
                double cv2 = 0;
                for (int j = 1; j < Value.step + 1; j++)
                {
                    optionprice[i, j] = Price(optionprice[i, j - 1], r, vol, t, RandomList1[i, j - 1]);
                    tl1 = t - (j - 1) * deltaT;
                    d1 = (Math.Log(optionprice[i, j - 1] / Value.K) + (r + Math.Pow(vol, 2) / 2) * tl1) / (vol * Math.Sqrt(tl1));
                    delta1 = normalCDF(d1);
                    cv1 = cv1 + delta1 * (optionprice[i, j] - optionprice[i, j - 1] * Math.Exp(r * deltaT));
                    optioninverse[i, j] = Price(optioninverse[i, j - 1], r, vol, t, RandomList2[i, j - 1]);
                    tl2 = t - (j - 1) * deltaT;
                    d2 = (Math.Log(optioninverse[i, j - 1] / Value.K) + (r + Math.Pow(vol, 2) / 2) * tl2) / (vol * Math.Sqrt(tl2));
                    delta2 = normalCDF(d2);
                    cv2 = cv2 + delta2 * (optioninverse[i, j] - optioninverse[i, j - 1] * Math.Exp(r * deltaT));
                }
                double[] m = new double[Value.step + 1];
                double[] n = new double[Value.step + 1];
                for (int j = 0; j < Value.step + 1; j++)
                {
                    m[j] = optionprice[i, j];
                    n[j] = optioninverse[i, j];
                }
                if (m.Max() <= Value.barrier)
                { Option[i] = Math.Max(optionprice[i, Value.step] - Value.K, 0) - cv1; }
                else { Option[i] = 0; }
                if (n.Max() <= Value.barrier)
                { Optioninverse[i] = Math.Max(optioninverse[i, Value.step] - Value.K, 0) - cv2; }
                else { Optioninverse[i] = 0; }
            });
            double price1 = 0;
            price1 = Math.Exp(-r * t) * Option.Average();
            double price2 = 0;
            price2 = Math.Exp(-r * t) * Optioninverse.Average();
            Program.increase(1);
            return (price1 + price2) / 2;
        }
        public double PutPrice(double S, double r, double vol, double t)
        {
            Initial();
            RandomAntithetic();
            double deltaT = t / Convert.ToDouble(Value.step);
            Parallel.ForEach(Ienum.Step(0, Value.trials, 1), new ParallelOptions { MaxDegreeOfParallelism = Value.Thread }, i =>
            {
                double tl1;
                double tl2;
                double d1;
                double d2;
                double delta1;
                double delta2;
                optioninverse[i, 0] = S;
                optionprice[i, 0] = S;
                double cv1 = 0;
                double cv2 = 0;
                for (int j = 1; j < Value.step + 1; j++)
                {
                    optionprice[i, j] = Price(optionprice[i, j - 1], r, vol, t, RandomList1[i, j - 1]);
                    tl1 = t - (j - 1) * deltaT;
                    d1 = (Math.Log(optionprice[i, j - 1] / Value.K) + (r + Math.Pow(vol, 2) / 2) * tl1) / (vol * Math.Sqrt(tl1));
                    delta1 = normalCDF(d1) - 1;
                    cv1 = cv1 + delta1 * (optionprice[i, j] - optionprice[i, j - 1] * Math.Exp(r * deltaT));
                    optioninverse[i, j] = Price(optioninverse[i, j - 1], r, vol, t, RandomList2[i, j - 1]);
                    tl2 = t - (j - 1) * deltaT;
                    d2 = (Math.Log(optioninverse[i, j - 1] / Value.K) + (r + Math.Pow(vol, 2) / 2) * tl2) / (vol * Math.Sqrt(tl2));
                    delta2 = normalCDF(d2) - 1;
                    cv2 = cv2 + delta2 * (optioninverse[i, j] - optioninverse[i, j - 1] * Math.Exp(r * deltaT));
                }
                double[] m = new double[Value.step + 1];
                double[] n = new double[Value.step + 1];
                for (int j = 0; j < Value.step + 1; j++)
                {
                    m[j] = optionprice[i, j];
                    n[j] = optioninverse[i, j];
                }
                if (m.Max() <= Value.barrier)
                { Option[i] = Math.Max(Value.K - optionprice[i, Value.step], 0) - cv1; }
                else { Option[i] = 0; }
                if (n.Max() <= Value.barrier)
                { Optioninverse[i] = Math.Max(Value.K - optioninverse[i, Value.step], 0) - cv2; }
                else { Optioninverse[i] = 0; }
            });
            double price1 = 0;
            price1 = Math.Exp(-r * t) * Option.Average();
            double price2 = 0;
            price2 = Math.Exp(-r * t) * Optioninverse.Average();
            Program.increase(1);
            return (price1 + price2) / 2;
        }
        public double SE()
        {
            List<double> Option1 = new List<double>();
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                Option1.Add((Option[i] + Optioninverse[i]) / 2);
            }
            double avg = Option1.Average();
            double sum = Option1.Sum(v => Math.Pow(v - avg, 2));
            return (Math.Sqrt(sum / (Option1.Count - 1))) / (Math.Sqrt(Option1.Count));
        }
        public double normalCDF(double value)
        {
            const double A1 = 0.254829592;
            const double A2 = -0.284496736;
            const double A3 = 1.421413741;
            const double A4 = -1.453152027;
            const double A5 = 1.061405429;
            const double P = 0.3275911;

            int sign;
            if (value < 0)
                sign = -1;
            else
                sign = 1;

            double x = Math.Abs(value) / Math.Sqrt(2);
            double t = 1 / (1 + P * x);
            double erf = 1 - (((((A5 * t + A4) * t + A3) * t + A2) * t + A1) * t * Math.Exp(-x * x));


            return 0.5 * (1 + sign * erf);
        }
        internal static class GaussianBoxMuller
        {
            public static double NextDouble()
            {
                double x, y, square;

                do
                {
                    x = 2 * RandomProvider.GetThreadRandom().NextDouble() - 1;
                    y = 2 * RandomProvider.GetThreadRandom().NextDouble() - 1;
                    square = (x * x) + (y * y);
                } while (square >= 1);

                return x * Math.Sqrt(-2 * Math.Log(square) / square);
            }
        }
        internal static class RandomProvider
        {
            private static ThreadLocal<Random> randomWrapper = new ThreadLocal<Random>(() =>
                new Random(Interlocked.Increment(ref seed))
            );

            private static int seed = Environment.TickCount;

            public static Random GetThreadRandom()
            {
                return randomWrapper.Value;
            }
        }
        public double calldelta(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S + 0.0001, r, vol, t);
            double cp2 = CallPrice(S - 0.0001, r, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double callgamma(double S, double r, double vol, double t)
        {
            return (calldelta(S + 0.01, r, vol, t) - calldelta(S, r, vol, t)) / 0.01;
        }
        public double callvega(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r, vol + 0.0001, t);
            double cp2 = CallPrice(S, r, vol - 0.0001, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double calltheta(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r, vol, t + 0.0001);
            double cp2 = CallPrice(S, r, vol, t - 0.0001);
            return (cp1 - cp2) / 0.0002;
        }
        public double callrho(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r + 0.0001, vol, t);
            double cp2 = CallPrice(S, r - 0.0001, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double putdelta(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S + 0.0001, r, vol, t);
            double cp2 = PutPrice(S - 0.0001, r, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double putgamma(double S, double r, double vol, double t)
        {
            return (putdelta(S + 0.01, r, vol, t) - putdelta(S, r, vol, t)) / 0.01;
        }
        public double putvega(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r, vol + 0.0001, t);
            double cp2 = PutPrice(S, r, vol - 0.0001, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double puttheta(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r, vol, t + 0.0001);
            double cp2 = PutPrice(S, r, vol, t - 0.0001);
            return (cp1 - cp2) / 0.0002;
        }
        public double putrho(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r + 0.0001, vol, t);
            double cp2 = PutPrice(S, r - 0.0001, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
    }

    public class BarrierUAIAntiCVMT : PreparationAntiCVMT, CallPrice, StandardError, PutPrice, CallGreek, PutGreek
    {
        public static double[,] RandomList1;
        public static double[,] RandomList2;
        public void Initial()
        {
            optionprice = new double[Value.trials, Value.step + 1];
            optioninverse = new double[Value.trials, Value.step + 1];
            Option = new double[Value.trials];
            Optioninverse = new double[Value.trials];
        }
        public void RandomGenerated()
        {
            RandomList1 = new double[Value.trials, Value.step];
            Parallel.ForEach(Ienum.Step(0, Value.trials, 1), new ParallelOptions { MaxDegreeOfParallelism = Value.Thread }, i =>
            {
                for (int k = 0; k < Value.step; k++)
                {
                    RandomList1[i, k] = GaussianBoxMuller.NextDouble();
                }
            });
        }
        public void RandomAntithetic()
        {
            RandomList2 = new double[Value.trials, Value.step];
            Parallel.ForEach(Ienum.Step(0, Value.trials, 1), new ParallelOptions { MaxDegreeOfParallelism = Value.Thread }, i =>
            {
                for (int k = 0; k < Value.step; k++)
                {
                    RandomList2[i, k] = -RandomList1[i, k];
                }
            });
        }
        public double CallPrice(double S, double r, double vol, double t)
        {
            Initial();
            double deltaT = t / Convert.ToDouble(Value.step);
            RandomAntithetic();
            Parallel.ForEach(Ienum.Step(0, Value.trials, 1), new ParallelOptions { MaxDegreeOfParallelism = Value.Thread }, i =>
            {
                double tl1;
                double d1;
                double delta1;
                double tl2;
                double d2;
                double delta2;
                optionprice[i, 0] = S;
                optioninverse[i, 0] = S;
                double cv1 = 0;
                double cv2 = 0;
                for (int j = 1; j < Value.step + 1; j++)
                {
                    optionprice[i, j] = Price(optionprice[i, j - 1], r, vol, t, RandomList1[i, j - 1]);
                    tl1 = t - (j - 1) * deltaT;
                    d1 = (Math.Log(optionprice[i, j - 1] / Value.K) + (r + Math.Pow(vol, 2) / 2) * tl1) / (vol * Math.Sqrt(tl1));
                    delta1 = normalCDF(d1);
                    cv1 = cv1 + delta1 * (optionprice[i, j] - optionprice[i, j - 1] * Math.Exp(r * deltaT));
                    optioninverse[i, j] = Price(optioninverse[i, j - 1], r, vol, t, RandomList2[i, j - 1]);
                    tl2 = t - (j - 1) * deltaT;
                    d2 = (Math.Log(optioninverse[i, j - 1] / Value.K) + (r + Math.Pow(vol, 2) / 2) * tl2) / (vol * Math.Sqrt(tl2));
                    delta2 = normalCDF(d2);
                    cv2 = cv2 + delta2 * (optioninverse[i, j] - optioninverse[i, j - 1] * Math.Exp(r * deltaT));
                }
                double[] m = new double[Value.step + 1];
                double[] n = new double[Value.step + 1];
                for (int j = 0; j < Value.step + 1; j++)
                {
                    m[j] = optionprice[i, j];
                    n[j] = optioninverse[i, j];
                }
                if (m.Max() > Value.barrier)
                { Option[i] = Math.Max(optionprice[i, Value.step] - Value.K, 0) - cv1; }
                else { Option[i] = 0; }
                if (n.Max() > Value.barrier)
                { Optioninverse[i] = Math.Max(optioninverse[i, Value.step] - Value.K, 0) - cv2; }
                else { Optioninverse[i] = 0; }
            });
            double price1 = 0;
            price1 = Math.Exp(-r * t) * Option.Average();
            double price2 = 0;
            price2 = Math.Exp(-r * t) * Optioninverse.Average();
            Program.increase(1);
            return (price1 + price2) / 2;
        }
        public double PutPrice(double S, double r, double vol, double t)
        {
            Initial();
            RandomAntithetic();
            double deltaT = t / Convert.ToDouble(Value.step);
            Parallel.ForEach(Ienum.Step(0, Value.trials, 1), new ParallelOptions { MaxDegreeOfParallelism = Value.Thread }, i =>
            {
                double tl1;
                double tl2;
                double d1;
                double d2;
                double delta1;
                double delta2;
                optioninverse[i, 0] = S;
                optionprice[i, 0] = S;
                double cv1 = 0;
                double cv2 = 0;
                for (int j = 1; j < Value.step + 1; j++)
                {
                    optionprice[i, j] = Price(optionprice[i, j - 1], r, vol, t, RandomList1[i, j - 1]);
                    tl1 = t - (j - 1) * deltaT;
                    d1 = (Math.Log(optionprice[i, j - 1] / Value.K) + (r + Math.Pow(vol, 2) / 2) * tl1) / (vol * Math.Sqrt(tl1));
                    delta1 = normalCDF(d1) - 1;
                    cv1 = cv1 + delta1 * (optionprice[i, j] - optionprice[i, j - 1] * Math.Exp(r * deltaT));
                    optioninverse[i, j] = Price(optioninverse[i, j - 1], r, vol, t, RandomList2[i, j - 1]);
                    tl2 = t - (j - 1) * deltaT;
                    d2 = (Math.Log(optioninverse[i, j - 1] / Value.K) + (r + Math.Pow(vol, 2) / 2) * tl2) / (vol * Math.Sqrt(tl2));
                    delta2 = normalCDF(d2) - 1;
                    cv2 = cv2 + delta2 * (optioninverse[i, j] - optioninverse[i, j - 1] * Math.Exp(r * deltaT));
                }
                double[] m = new double[Value.step + 1];
                double[] n = new double[Value.step + 1];
                for (int j = 0; j < Value.step + 1; j++)
                {
                    m[j] = optionprice[i, j];
                    n[j] = optioninverse[i, j];
                }
                if (m.Max() > Value.barrier)
                { Option[i] = Math.Max(Value.K - optionprice[i, Value.step], 0) - cv1; }
                else { Option[i] = 0; }
                if (n.Max() > Value.barrier)
                { Optioninverse[i] = Math.Max(Value.K - optioninverse[i, Value.step], 0) - cv2; }
                else { Optioninverse[i] = 0; }
            });
            double price1 = 0;
            price1 = Math.Exp(-r * t) * Option.Average();
            double price2 = 0;
            price2 = Math.Exp(-r * t) * Optioninverse.Average();
            Program.increase(1);
            return (price1 + price2) / 2;
        }
        public double SE()
        {
            List<double> Option1 = new List<double>();
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                Option1.Add((Option[i] + Optioninverse[i]) / 2);
            }
            double avg = Option1.Average();
            double sum = Option1.Sum(v => Math.Pow(v - avg, 2));
            return (Math.Sqrt(sum / (Option1.Count - 1))) / (Math.Sqrt(Option1.Count));
        }
        public double normalCDF(double value)
        {
            const double A1 = 0.254829592;
            const double A2 = -0.284496736;
            const double A3 = 1.421413741;
            const double A4 = -1.453152027;
            const double A5 = 1.061405429;
            const double P = 0.3275911;

            int sign;
            if (value < 0)
                sign = -1;
            else
                sign = 1;

            double x = Math.Abs(value) / Math.Sqrt(2);
            double t = 1 / (1 + P * x);
            double erf = 1 - (((((A5 * t + A4) * t + A3) * t + A2) * t + A1) * t * Math.Exp(-x * x));


            return 0.5 * (1 + sign * erf);
        }
        internal static class GaussianBoxMuller
        {
            public static double NextDouble()
            {
                double x, y, square;

                do
                {
                    x = 2 * RandomProvider.GetThreadRandom().NextDouble() - 1;
                    y = 2 * RandomProvider.GetThreadRandom().NextDouble() - 1;
                    square = (x * x) + (y * y);
                } while (square >= 1);

                return x * Math.Sqrt(-2 * Math.Log(square) / square);
            }
        }
        internal static class RandomProvider
        {
            private static ThreadLocal<Random> randomWrapper = new ThreadLocal<Random>(() =>
                new Random(Interlocked.Increment(ref seed))
            );

            private static int seed = Environment.TickCount;

            public static Random GetThreadRandom()
            {
                return randomWrapper.Value;
            }
        }
        public double calldelta(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S + 0.0001, r, vol, t);
            double cp2 = CallPrice(S - 0.0001, r, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double callgamma(double S, double r, double vol, double t)
        {
            return (calldelta(S + 0.01, r, vol, t) - calldelta(S, r, vol, t)) / 0.01;
        }
        public double callvega(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r, vol + 0.0001, t);
            double cp2 = CallPrice(S, r, vol - 0.0001, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double calltheta(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r, vol, t + 0.0001);
            double cp2 = CallPrice(S, r, vol, t - 0.0001);
            return (cp1 - cp2) / 0.0002;
        }
        public double callrho(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r + 0.0001, vol, t);
            double cp2 = CallPrice(S, r - 0.0001, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double putdelta(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S + 0.0001, r, vol, t);
            double cp2 = PutPrice(S - 0.0001, r, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double putgamma(double S, double r, double vol, double t)
        {
            return (putdelta(S + 0.01, r, vol, t) - putdelta(S, r, vol, t)) / 0.01;
        }
        public double putvega(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r, vol + 0.0001, t);
            double cp2 = PutPrice(S, r, vol - 0.0001, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double puttheta(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r, vol, t + 0.0001);
            double cp2 = PutPrice(S, r, vol, t - 0.0001);
            return (cp1 - cp2) / 0.0002;
        }
        public double putrho(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r + 0.0001, vol, t);
            double cp2 = PutPrice(S, r - 0.0001, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
    }

    public class LookbackAntiCVMT: PreparationAntiCVMT, CallPrice, StandardError, PutPrice, CallGreek, PutGreek
    {
        public static double[,] RandomList1;
        public static double[,] RandomList2;
        public void Initial()
        {
            optionprice = new double[Value.trials, Value.step + 1];
            optioninverse = new double[Value.trials, Value.step + 1];
            Option = new double[Value.trials];
            Optioninverse = new double[Value.trials];
        }
        public void RandomGenerated()
        {
            RandomList1 = new double[Value.trials, Value.step];
            Parallel.ForEach(Ienum.Step(0, Value.trials, 1), new ParallelOptions { MaxDegreeOfParallelism = Value.Thread }, i =>
            {
                for (int k = 0; k < Value.step; k++)
                {
                    RandomList1[i, k] = GaussianBoxMuller.NextDouble();
                }
            });
        }
        public void RandomAntithetic()
        {
            RandomList2 = new double[Value.trials, Value.step];
            Parallel.ForEach(Ienum.Step(0, Value.trials, 1), new ParallelOptions { MaxDegreeOfParallelism = Value.Thread }, i =>
            {
                for (int k = 0; k < Value.step; k++)
                {
                    RandomList2[i, k] = -RandomList1[i, k];
                }
            });
        }
        public double CallPrice(double S, double r, double vol, double t)
        {
            Initial();
            double deltaT = t / Convert.ToDouble(Value.step);
            RandomAntithetic();
            Parallel.ForEach(Ienum.Step(0, Value.trials, 1), new ParallelOptions { MaxDegreeOfParallelism = Value.Thread }, i =>
            {
                double tl1;
                double d1;
                double delta1;
                double tl2;
                double d2;
                double delta2;
                optionprice[i, 0] = S;
                optioninverse[i, 0] = S;
                double cv1 = 0;
                double cv2 = 0;
                for (int j = 1; j < Value.step + 1; j++)
                {
                    optionprice[i, j] = Price(optionprice[i, j - 1], r, vol, t, RandomList1[i, j - 1]);
                    tl1 = t - (j - 1) * deltaT;
                    d1 = (Math.Log(optionprice[i, j - 1] / Value.K) + (r + Math.Pow(vol, 2) / 2) * tl1) / (vol * Math.Sqrt(tl1));
                    delta1 = normalCDF(d1);
                    cv1 = cv1 + delta1 * (optionprice[i, j] - optionprice[i, j - 1] * Math.Exp(r * deltaT));
                    optioninverse[i, j] = Price(optioninverse[i, j - 1], r, vol, t, RandomList2[i, j - 1]);
                    tl2 = t - (j - 1) * deltaT;
                    d2 = (Math.Log(optioninverse[i, j - 1] / Value.K) + (r + Math.Pow(vol, 2) / 2) * tl2) / (vol * Math.Sqrt(tl2));
                    delta2 = normalCDF(d2);
                    cv2 = cv2 + delta2 * (optioninverse[i, j] - optioninverse[i, j - 1] * Math.Exp(r * deltaT));
                }
                double[] m = new double[Value.step + 1];
                double[] n = new double[Value.step + 1];
                for (int j = 0; j < Value.step + 1; j++)
                {
                    m[j] = optionprice[i, j];
                    n[j] = optioninverse[i, j];
                }
                Option[i] = Math.Max(m.Max() - Value.K, 0) - cv1;
                Optioninverse[i] = Math.Max(n.Max() - Value.K, 0) - cv2;
            });
            double price1 = 0;
            price1 = Math.Exp(-r * t) * Option.Average();
            double price2 = 0;
            price2 = Math.Exp(-r * t) * Optioninverse.Average();
            Program.increase(1);
            return (price1 + price2) / 2;
        }
        public double PutPrice(double S, double r, double vol, double t)
        {
            Initial();
            RandomAntithetic();
            double deltaT = t / Convert.ToDouble(Value.step);
            Parallel.ForEach(Ienum.Step(0, Value.trials, 1), new ParallelOptions { MaxDegreeOfParallelism = Value.Thread }, i =>
            {
                double tl1;
                double tl2;
                double d1;
                double d2;
                double delta1;
                double delta2;
                optioninverse[i, 0] = S;
                optionprice[i, 0] = S;
                double cv1 = 0;
                double cv2 = 0;
                for (int j = 1; j < Value.step + 1; j++)
                {
                    optionprice[i, j] = Price(optionprice[i, j - 1], r, vol, t, RandomList1[i, j - 1]);
                    tl1 = t - (j - 1) * deltaT;
                    d1 = (Math.Log(optionprice[i, j - 1] / Value.K) + (r + Math.Pow(vol, 2) / 2) * tl1) / (vol * Math.Sqrt(tl1));
                    delta1 = normalCDF(d1) - 1;
                    cv1 = cv1 + delta1 * (optionprice[i, j] - optionprice[i, j - 1] * Math.Exp(r * deltaT));
                    optioninverse[i, j] = Price(optioninverse[i, j - 1], r, vol, t, RandomList2[i, j - 1]);
                    tl2 = t - (j - 1) * deltaT;
                    d2 = (Math.Log(optioninverse[i, j - 1] / Value.K) + (r + Math.Pow(vol, 2) / 2) * tl2) / (vol * Math.Sqrt(tl2));
                    delta2 = normalCDF(d2) - 1;
                    cv2 = cv2 + delta2 * (optioninverse[i, j] - optioninverse[i, j - 1] * Math.Exp(r * deltaT));
                }
                double[] m = new double[Value.step + 1];
                double[] n = new double[Value.step + 1];
                for (int j = 0; j < Value.step + 1; j++)
                {
                    m[j] = optionprice[i, j];
                    n[j] = optioninverse[i, j];
                }
                Option[i] = Math.Max(Value.K - m.Min(), 0) - cv1;
                Optioninverse[i] = Math.Max(Value.K - n.Min(), 0) - cv2;
            });
            double price1 = 0;
            price1 = Math.Exp(-r * t) * Option.Average();
            double price2 = 0;
            price2 = Math.Exp(-r * t) * Optioninverse.Average();
            Program.increase(1);
            return (price1 + price2) / 2;
        }
        public double SE()
        {
            List<double> Option1 = new List<double>();
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                Option1.Add((Option[i] + Optioninverse[i]) / 2);
            }
            double avg = Option1.Average();
            double sum = Option1.Sum(v => Math.Pow(v - avg, 2));
            return (Math.Sqrt(sum / (Option1.Count - 1))) / (Math.Sqrt(Option1.Count));
        }
        public double normalCDF(double value)
        {
            const double A1 = 0.254829592;
            const double A2 = -0.284496736;
            const double A3 = 1.421413741;
            const double A4 = -1.453152027;
            const double A5 = 1.061405429;
            const double P = 0.3275911;

            int sign;
            if (value < 0)
                sign = -1;
            else
                sign = 1;

            double x = Math.Abs(value) / Math.Sqrt(2);
            double t = 1 / (1 + P * x);
            double erf = 1 - (((((A5 * t + A4) * t + A3) * t + A2) * t + A1) * t * Math.Exp(-x * x));


            return 0.5 * (1 + sign * erf);
        }
        internal static class GaussianBoxMuller
        {
            public static double NextDouble()
            {
                double x, y, square;

                do
                {
                    x = 2 * RandomProvider.GetThreadRandom().NextDouble() - 1;
                    y = 2 * RandomProvider.GetThreadRandom().NextDouble() - 1;
                    square = (x * x) + (y * y);
                } while (square >= 1);

                return x * Math.Sqrt(-2 * Math.Log(square) / square);
            }
        }
        internal static class RandomProvider
        {
            private static ThreadLocal<Random> randomWrapper = new ThreadLocal<Random>(() =>
                new Random(Interlocked.Increment(ref seed))
            );

            private static int seed = Environment.TickCount;

            public static Random GetThreadRandom()
            {
                return randomWrapper.Value;
            }
        }
        public double calldelta(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S + 0.0001, r, vol, t);
            double cp2 = CallPrice(S - 0.0001, r, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double callgamma(double S, double r, double vol, double t)
        {
            return (calldelta(S + 0.01, r, vol, t) - calldelta(S, r, vol, t)) / 0.01;
        }
        public double callvega(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r, vol + 0.0001, t);
            double cp2 = CallPrice(S, r, vol - 0.0001, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double calltheta(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r, vol, t + 0.0001);
            double cp2 = CallPrice(S, r, vol, t - 0.0001);
            return (cp1 - cp2) / 0.0002;
        }
        public double callrho(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r + 0.0001, vol, t);
            double cp2 = CallPrice(S, r - 0.0001, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double putdelta(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S + 0.0001, r, vol, t);
            double cp2 = PutPrice(S - 0.0001, r, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double putgamma(double S, double r, double vol, double t)
        {
            return (putdelta(S + 0.01, r, vol, t) - putdelta(S, r, vol, t)) / 0.01;
        }
        public double putvega(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r, vol + 0.0001, t);
            double cp2 = PutPrice(S, r, vol - 0.0001, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double puttheta(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r, vol, t + 0.0001);
            double cp2 = PutPrice(S, r, vol, t - 0.0001);
            return (cp1 - cp2) / 0.0002;
        }
        public double putrho(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r + 0.0001, vol, t);
            double cp2 = PutPrice(S, r - 0.0001, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
    }

    public class RangeAntiCVMT : PreparationAntiCVMT, CallPrice, StandardError, PutPrice, CallGreek, PutGreek
    {
        public static double[,] RandomList1;
        public static double[,] RandomList2;
        public void Initial()
        {
            optionprice = new double[Value.trials, Value.step + 1];
            optioninverse = new double[Value.trials, Value.step + 1];
            Option = new double[Value.trials];
            Optioninverse = new double[Value.trials];
        }
        public void RandomGenerated()
        {
            RandomList1 = new double[Value.trials, Value.step];
            Parallel.ForEach(Ienum.Step(0, Value.trials, 1), new ParallelOptions { MaxDegreeOfParallelism = Value.Thread }, i =>
            {
                for (int k = 0; k < Value.step; k++)
                {
                    RandomList1[i, k] = GaussianBoxMuller.NextDouble();
                }
            });
        }
        public void RandomAntithetic()
        {
            RandomList2 = new double[Value.trials, Value.step];
            Parallel.ForEach(Ienum.Step(0, Value.trials, 1), new ParallelOptions { MaxDegreeOfParallelism = Value.Thread }, i =>
            {
                for (int k = 0; k < Value.step; k++)
                {
                    RandomList2[i, k] = -RandomList1[i, k];
                }
            });
        }
        public double CallPrice(double S, double r, double vol, double t)
        {
            Initial();
            double deltaT = t / Convert.ToDouble(Value.step);
            RandomAntithetic();
            Parallel.ForEach(Ienum.Step(0, Value.trials, 1), new ParallelOptions { MaxDegreeOfParallelism = Value.Thread }, i =>
            {
                double tl1;
                double d1;
                double delta1;
                double tl2;
                double d2;
                double delta2;
                optionprice[i, 0] = S;
                optioninverse[i, 0] = S;
                double cv1 = 0;
                double cv2 = 0;
                for (int j = 1; j < Value.step + 1; j++)
                {
                    optionprice[i, j] = Price(optionprice[i, j - 1], r, vol, t, RandomList1[i, j - 1]);
                    tl1 = t - (j - 1) * deltaT;
                    d1 = (Math.Log(optionprice[i, j - 1] / Value.K) + (r + Math.Pow(vol, 2) / 2) * tl1) / (vol * Math.Sqrt(tl1));
                    delta1 = normalCDF(d1);
                    cv1 = cv1 + delta1 * (optionprice[i, j] - optionprice[i, j - 1] * Math.Exp(r * deltaT));
                    optioninverse[i, j] = Price(optioninverse[i, j - 1], r, vol, t, RandomList2[i, j - 1]);
                    tl2 = t - (j - 1) * deltaT;
                    d2 = (Math.Log(optioninverse[i, j - 1] / Value.K) + (r + Math.Pow(vol, 2) / 2) * tl2) / (vol * Math.Sqrt(tl2));
                    delta2 = normalCDF(d2);
                    cv2 = cv2 + delta2 * (optioninverse[i, j] - optioninverse[i, j - 1] * Math.Exp(r * deltaT));
                }
                double[] m = new double[Value.step + 1];
                double[] n = new double[Value.step + 1];
                for (int j = 0; j < Value.step + 1; j++)
                {
                    m[j] = optionprice[i, j];
                    n[j] = optioninverse[i, j];
                }
                Option[i] = m.Max() - m.Min() - cv1;
                Optioninverse[i] = n.Max() - n.Min() - cv2;
            });
            double price1 = 0;
            price1 = Math.Exp(-r * t) * Option.Average();
            double price2 = 0;
            price2 = Math.Exp(-r * t) * Optioninverse.Average();
            Program.increase(1);
            return (price1 + price2) / 2;
        }
        public double PutPrice(double S, double r, double vol, double t)
        {
            Initial();
            RandomAntithetic();
            double deltaT = t / Convert.ToDouble(Value.step);
            Parallel.ForEach(Ienum.Step(0, Value.trials, 1), new ParallelOptions { MaxDegreeOfParallelism = Value.Thread }, i =>
            {
                double tl1;
                double tl2;
                double d1;
                double d2;
                double delta1;
                double delta2;
                optioninverse[i, 0] = S;
                optionprice[i, 0] = S;
                double cv1 = 0;
                double cv2 = 0;
                for (int j = 1; j < Value.step + 1; j++)
                {
                    optionprice[i, j] = Price(optionprice[i, j - 1], r, vol, t, RandomList1[i, j - 1]);
                    tl1 = t - (j - 1) * deltaT;
                    d1 = (Math.Log(optionprice[i, j - 1] / Value.K) + (r + Math.Pow(vol, 2) / 2) * tl1) / (vol * Math.Sqrt(tl1));
                    delta1 = normalCDF(d1) - 1;
                    cv1 = cv1 + delta1 * (optionprice[i, j] - optionprice[i, j - 1] * Math.Exp(r * deltaT));
                    optioninverse[i, j] = Price(optioninverse[i, j - 1], r, vol, t, RandomList2[i, j - 1]);
                    tl2 = t - (j - 1) * deltaT;
                    d2 = (Math.Log(optioninverse[i, j - 1] / Value.K) + (r + Math.Pow(vol, 2) / 2) * tl2) / (vol * Math.Sqrt(tl2));
                    delta2 = normalCDF(d2) - 1;
                    cv2 = cv2 + delta2 * (optioninverse[i, j] - optioninverse[i, j - 1] * Math.Exp(r * deltaT));
                }
                double[] m = new double[Value.step + 1];
                double[] n = new double[Value.step + 1];
                for (int j = 0; j < Value.step + 1; j++)
                {
                    m[j] = optionprice[i, j];
                    n[j] = optioninverse[i, j];
                }
                Option[i] = m.Max() - m.Min() - cv1;
                Optioninverse[i] = n.Max() - n.Min() - cv2;
            });
            double price1 = 0;
            price1 = Math.Exp(-r * t) * Option.Average();
            double price2 = 0;
            price2 = Math.Exp(-r * t) * Optioninverse.Average();
            Program.increase(1);
            return (price1 + price2) / 2;
        }
        public double SE()
        {
            List<double> Option1 = new List<double>();
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                Option1.Add((Option[i] + Optioninverse[i]) / 2);
            }
            double avg = Option1.Average();
            double sum = Option1.Sum(v => Math.Pow(v - avg, 2));
            return (Math.Sqrt(sum / (Option1.Count - 1))) / (Math.Sqrt(Option1.Count));
        }
        public double normalCDF(double value)
        {
            const double A1 = 0.254829592;
            const double A2 = -0.284496736;
            const double A3 = 1.421413741;
            const double A4 = -1.453152027;
            const double A5 = 1.061405429;
            const double P = 0.3275911;

            int sign;
            if (value < 0)
                sign = -1;
            else
                sign = 1;

            double x = Math.Abs(value) / Math.Sqrt(2);
            double t = 1 / (1 + P * x);
            double erf = 1 - (((((A5 * t + A4) * t + A3) * t + A2) * t + A1) * t * Math.Exp(-x * x));


            return 0.5 * (1 + sign * erf);
        }
        internal static class GaussianBoxMuller
        {
            public static double NextDouble()
            {
                double x, y, square;

                do
                {
                    x = 2 * RandomProvider.GetThreadRandom().NextDouble() - 1;
                    y = 2 * RandomProvider.GetThreadRandom().NextDouble() - 1;
                    square = (x * x) + (y * y);
                } while (square >= 1);

                return x * Math.Sqrt(-2 * Math.Log(square) / square);
            }
        }
        internal static class RandomProvider
        {
            private static ThreadLocal<Random> randomWrapper = new ThreadLocal<Random>(() =>
                new Random(Interlocked.Increment(ref seed))
            );

            private static int seed = Environment.TickCount;

            public static Random GetThreadRandom()
            {
                return randomWrapper.Value;
            }
        }
        public double calldelta(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S + 0.0001, r, vol, t);
            double cp2 = CallPrice(S - 0.0001, r, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double callgamma(double S, double r, double vol, double t)
        {
            return (calldelta(S + 0.01, r, vol, t) - calldelta(S, r, vol, t)) / 0.01;
        }
        public double callvega(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r, vol + 0.0001, t);
            double cp2 = CallPrice(S, r, vol - 0.0001, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double calltheta(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r, vol, t + 0.0001);
            double cp2 = CallPrice(S, r, vol, t - 0.0001);
            return (cp1 - cp2) / 0.0002;
        }
        public double callrho(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r + 0.0001, vol, t);
            double cp2 = CallPrice(S, r - 0.0001, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double putdelta(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S + 0.0001, r, vol, t);
            double cp2 = PutPrice(S - 0.0001, r, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double putgamma(double S, double r, double vol, double t)
        {
            return (putdelta(S + 0.01, r, vol, t) - putdelta(S, r, vol, t)) / 0.01;
        }
        public double putvega(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r, vol + 0.0001, t);
            double cp2 = PutPrice(S, r, vol - 0.0001, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double puttheta(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r, vol, t + 0.0001);
            double cp2 = PutPrice(S, r, vol, t - 0.0001);
            return (cp1 - cp2) / 0.0002;
        }
        public double putrho(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r + 0.0001, vol, t);
            double cp2 = PutPrice(S, r - 0.0001, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
    }
}
