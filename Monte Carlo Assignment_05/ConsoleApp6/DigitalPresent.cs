﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Diagnostics;

namespace ConsoleApp6
{
    public class DigitalPresent
    {
        Stopwatch watch = new Stopwatch();

        public void DigitalCall()
        {
            Digital euro = new Digital();
            watch.Start();
            euro.RandomGenerated();
            Value.Price = euro.CallPrice(Value.S, Value.r, Value.vol, Value.t);
            Value.SE = euro.SE();
            Value.delta = euro.calldelta(Value.S, Value.r, Value.vol, Value.t);
            Value.gamma = euro.callgamma(Value.S, Value.r, Value.vol, Value.t);
            Value.theta = euro.calltheta(Value.S, Value.r, Value.vol, Value.t);
            Value.vega = euro.callvega(Value.S, Value.r, Value.vol, Value.t);
            Value.rho = euro.callrho(Value.S, Value.r, Value.vol, Value.t);
            Value.Thread = 1;
            watch.Stop();
            Value.timer = watch.Elapsed.Seconds.ToString() + " : " + watch.Elapsed.Milliseconds.ToString();
            watch.Reset();
            Program.finished();
            GC.Collect();
        }

        public void DigitalPut()
        {
            Digital euro = new Digital();
            watch.Start();
            euro.RandomGenerated();
            Value.Price = euro.PutPrice(Value.S, Value.r, Value.vol, Value.t);
            Value.SE = euro.SE();
            Value.delta = euro.putdelta(Value.S, Value.r, Value.vol, Value.t);
            Value.gamma = euro.putgamma(Value.S, Value.r, Value.vol, Value.t);
            Value.theta = euro.puttheta(Value.S, Value.r, Value.vol, Value.t);
            Value.vega = euro.putvega(Value.S, Value.r, Value.vol, Value.t);
            Value.rho = euro.putrho(Value.S, Value.r, Value.vol, Value.t);
            Value.Thread = 1;
            watch.Stop();
            Value.timer = watch.Elapsed.Seconds.ToString() + " : " + watch.Elapsed.Milliseconds.ToString();
            watch.Reset();
            Program.finished();
            GC.Collect();
        }

        public void DigitalCallAnti()
        {
            DigitalAntithetic euro = new DigitalAntithetic();
            watch.Start();
            euro.RandomGenerated();
            Value.Price = euro.CallPrice(Value.S, Value.r, Value.vol, Value.t);
            Value.SE = euro.SE();
            Value.delta = euro.calldelta(Value.S, Value.r, Value.vol, Value.t);
            Value.gamma = euro.callgamma(Value.S, Value.r, Value.vol, Value.t);
            Value.theta = euro.calltheta(Value.S, Value.r, Value.vol, Value.t);
            Value.vega = euro.callvega(Value.S, Value.r, Value.vol, Value.t);
            Value.rho = euro.callrho(Value.S, Value.r, Value.vol, Value.t);
            Value.Thread = 1;
            watch.Stop();
            Value.timer = watch.Elapsed.Seconds.ToString() + " : " + watch.Elapsed.Milliseconds.ToString();
            watch.Reset();
            Program.finished();
            GC.Collect();
        }

        public void DigitalPutAnti()
        {
            DigitalAntithetic euro = new DigitalAntithetic();
            watch.Start();
            euro.RandomGenerated();
            Value.Price = euro.PutPrice(Value.S, Value.r, Value.vol, Value.t);
            Value.SE = euro.SE();
            Value.delta = euro.putdelta(Value.S, Value.r, Value.vol, Value.t);
            Value.gamma = euro.putgamma(Value.S, Value.r, Value.vol, Value.t);
            Value.theta = euro.puttheta(Value.S, Value.r, Value.vol, Value.t);
            Value.vega = euro.putvega(Value.S, Value.r, Value.vol, Value.t);
            Value.rho = euro.putrho(Value.S, Value.r, Value.vol, Value.t);
            Value.Thread = 1;
            watch.Stop();
            Value.timer = watch.Elapsed.Seconds.ToString() + " : " + watch.Elapsed.Milliseconds.ToString();
            watch.Reset();
            Program.finished();
            GC.Collect();
        }

        public void DigitalCallCV()
        {
            DigitalCV euro = new DigitalCV();
            watch.Start();
            euro.RandomGenerated();
            Value.Price = euro.CallPrice(Value.S, Value.r, Value.vol, Value.t);
            Value.SE = euro.SE();
            Value.delta = euro.calldelta(Value.S, Value.r, Value.vol, Value.t);
            Value.gamma = euro.callgamma(Value.S, Value.r, Value.vol, Value.t);
            Value.theta = euro.calltheta(Value.S, Value.r, Value.vol, Value.t);
            Value.vega = euro.callvega(Value.S, Value.r, Value.vol, Value.t);
            Value.rho = euro.callrho(Value.S, Value.r, Value.vol, Value.t);
            Value.Thread = 1;
            watch.Stop();
            Value.timer = watch.Elapsed.Seconds.ToString() + " : " + watch.Elapsed.Milliseconds.ToString();
            watch.Reset();
            Program.finished();
            GC.Collect();
        }

        public void DigitalPutCV()
        {
            DigitalCV euro = new DigitalCV();
            watch.Start();
            euro.RandomGenerated();
            Value.Price = euro.PutPrice(Value.S, Value.r, Value.vol, Value.t);
            Value.SE = euro.SE();
            Value.delta = euro.putdelta(Value.S, Value.r, Value.vol, Value.t);
            Value.gamma = euro.putgamma(Value.S, Value.r, Value.vol, Value.t);
            Value.theta = euro.puttheta(Value.S, Value.r, Value.vol, Value.t);
            Value.vega = euro.putvega(Value.S, Value.r, Value.vol, Value.t);
            Value.rho = euro.putrho(Value.S, Value.r, Value.vol, Value.t);
            Value.Thread = 1;
            watch.Stop();
            Value.timer = watch.Elapsed.Seconds.ToString() + " : " + watch.Elapsed.Milliseconds.ToString();
            watch.Reset();
            Program.finished();
            GC.Collect();
        }

        public void DigitalCallAntiCV()
        {
            DigitalAntiCV euro = new DigitalAntiCV();
            watch.Start();
            euro.RandomGenerated();
            Value.Price = euro.CallPrice(Value.S, Value.r, Value.vol, Value.t);
            Value.SE = euro.SE();
            Value.delta = euro.calldelta(Value.S, Value.r, Value.vol, Value.t);
            Value.gamma = euro.callgamma(Value.S, Value.r, Value.vol, Value.t);
            Value.theta = euro.calltheta(Value.S, Value.r, Value.vol, Value.t);
            Value.vega = euro.callvega(Value.S, Value.r, Value.vol, Value.t);
            Value.rho = euro.callrho(Value.S, Value.r, Value.vol, Value.t);
            Value.Thread = 1;
            watch.Stop();
            Value.timer = watch.Elapsed.Seconds.ToString() + " : " + watch.Elapsed.Milliseconds.ToString();
            watch.Reset();
            Program.finished();
            GC.Collect();
        }

        public void DigitalPutAntiCV()
        {
            DigitalAntiCV euro = new DigitalAntiCV();
            watch.Start();
            euro.RandomGenerated();
            Value.Price = euro.PutPrice(Value.S, Value.r, Value.vol, Value.t);
            Value.SE = euro.SE();
            Value.delta = euro.putdelta(Value.S, Value.r, Value.vol, Value.t);
            Value.gamma = euro.putgamma(Value.S, Value.r, Value.vol, Value.t);
            Value.theta = euro.puttheta(Value.S, Value.r, Value.vol, Value.t);
            Value.vega = euro.putvega(Value.S, Value.r, Value.vol, Value.t);
            Value.rho = euro.putrho(Value.S, Value.r, Value.vol, Value.t);
            Value.Thread = 1;
            watch.Stop();
            Value.timer = watch.Elapsed.Seconds.ToString() + " : " + watch.Elapsed.Milliseconds.ToString();
            watch.Reset();
            Program.finished();
            GC.Collect();
        }

        public void DigitalCallMT()
        {
            DigitalMT euro = new DigitalMT();
            watch.Start();
            euro.RandomGenerated();
            Value.Price = euro.CallPrice(Value.S, Value.r, Value.vol, Value.t);
            Value.SE = euro.SE();
            Value.delta = euro.calldelta(Value.S, Value.r, Value.vol, Value.t);
            Value.gamma = euro.callgamma(Value.S, Value.r, Value.vol, Value.t);
            Value.theta = euro.calltheta(Value.S, Value.r, Value.vol, Value.t);
            Value.vega = euro.callvega(Value.S, Value.r, Value.vol, Value.t);
            Value.rho = euro.callrho(Value.S, Value.r, Value.vol, Value.t);
            Value.Thread = 4;
            watch.Stop();
            Value.timer = watch.Elapsed.Seconds.ToString() + " : " + watch.Elapsed.Milliseconds.ToString();
            watch.Reset();
            Program.finished();
            GC.Collect();
        }

        public void DigitalPutMT()
        {
            DigitalMT euro = new DigitalMT();
            watch.Start();
            euro.RandomGenerated();
            Value.Price = euro.PutPrice(Value.S, Value.r, Value.vol, Value.t);
            Value.SE = euro.SE();
            Value.delta = euro.putdelta(Value.S, Value.r, Value.vol, Value.t);
            Value.gamma = euro.putgamma(Value.S, Value.r, Value.vol, Value.t);
            Value.theta = euro.puttheta(Value.S, Value.r, Value.vol, Value.t);
            Value.vega = euro.putvega(Value.S, Value.r, Value.vol, Value.t);
            Value.rho = euro.putrho(Value.S, Value.r, Value.vol, Value.t);
            Value.Thread = 4;
            watch.Stop();
            Value.timer = watch.Elapsed.Seconds.ToString() + " : " + watch.Elapsed.Milliseconds.ToString();
            watch.Reset();
            Program.finished();
            GC.Collect();
        }

        public void DigitalCallAntiMT()
        {
            DigitalAntiMT euro = new DigitalAntiMT();
            watch.Start();
            euro.RandomGenerated();
            Value.Price = euro.CallPrice(Value.S, Value.r, Value.vol, Value.t);
            Value.SE = euro.SE();
            Value.delta = euro.calldelta(Value.S, Value.r, Value.vol, Value.t);
            Value.gamma = euro.callgamma(Value.S, Value.r, Value.vol, Value.t);
            Value.theta = euro.calltheta(Value.S, Value.r, Value.vol, Value.t);
            Value.vega = euro.callvega(Value.S, Value.r, Value.vol, Value.t);
            Value.rho = euro.callrho(Value.S, Value.r, Value.vol, Value.t);
            Value.Thread = 4;
            watch.Stop();
            Value.timer = watch.Elapsed.Seconds.ToString() + " : " + watch.Elapsed.Milliseconds.ToString();
            watch.Reset();
            Program.finished();
            GC.Collect();
        }

        public void DigitalPutAntiMT()
        {
            DigitalAntiMT euro = new DigitalAntiMT();
            watch.Start();
            euro.RandomGenerated();
            Value.Price = euro.PutPrice(Value.S, Value.r, Value.vol, Value.t);
            Value.SE = euro.SE();
            Value.delta = euro.putdelta(Value.S, Value.r, Value.vol, Value.t);
            Value.gamma = euro.putgamma(Value.S, Value.r, Value.vol, Value.t);
            Value.theta = euro.puttheta(Value.S, Value.r, Value.vol, Value.t);
            Value.vega = euro.putvega(Value.S, Value.r, Value.vol, Value.t);
            Value.rho = euro.putrho(Value.S, Value.r, Value.vol, Value.t);
            Value.Thread = 4;
            watch.Stop();
            Value.timer = watch.Elapsed.Seconds.ToString() + " : " + watch.Elapsed.Milliseconds.ToString();
            watch.Reset();
            Program.finished();
            GC.Collect();
        }

        public void DigitalCallCVMT()
        {
            DigitalCVMT euro = new DigitalCVMT();
            watch.Start();
            euro.RandomGenerated();
            Value.Price = euro.CallPrice(Value.S, Value.r, Value.vol, Value.t);
            Value.SE = euro.SE();
            Value.delta = euro.calldelta(Value.S, Value.r, Value.vol, Value.t);
            Value.gamma = euro.callgamma(Value.S, Value.r, Value.vol, Value.t);
            Value.theta = euro.calltheta(Value.S, Value.r, Value.vol, Value.t);
            Value.vega = euro.callvega(Value.S, Value.r, Value.vol, Value.t);
            Value.rho = euro.callrho(Value.S, Value.r, Value.vol, Value.t);
            Value.Thread = 4;
            watch.Stop();
            Value.timer = watch.Elapsed.Seconds.ToString() + " : " + watch.Elapsed.Milliseconds.ToString();
            watch.Reset();
            Program.finished();
            GC.Collect();
        }

        public void DigitalPutCVMT()
        {
            DigitalCVMT euro = new DigitalCVMT();
            watch.Start();
            euro.RandomGenerated();
            Value.Price = euro.PutPrice(Value.S, Value.r, Value.vol, Value.t);
            Value.SE = euro.SE();
            Value.delta = euro.putdelta(Value.S, Value.r, Value.vol, Value.t);
            Value.gamma = euro.putgamma(Value.S, Value.r, Value.vol, Value.t);
            Value.theta = euro.puttheta(Value.S, Value.r, Value.vol, Value.t);
            Value.vega = euro.putvega(Value.S, Value.r, Value.vol, Value.t);
            Value.rho = euro.putrho(Value.S, Value.r, Value.vol, Value.t);
            Value.Thread = 4;
            watch.Stop();
            Value.timer = watch.Elapsed.Seconds.ToString() + " : " + watch.Elapsed.Milliseconds.ToString();
            watch.Reset();
            Program.finished();
            GC.Collect();
        }

        public void DigitalCallAntiCVMT()
        {
            DigitalAntiCVMT euro = new DigitalAntiCVMT();
            watch.Start();
            euro.RandomGenerated();
            Value.Price = euro.CallPrice(Value.S, Value.r, Value.vol, Value.t);
            Value.SE = euro.SE();
            Value.delta = euro.calldelta(Value.S, Value.r, Value.vol, Value.t);
            Value.gamma = euro.callgamma(Value.S, Value.r, Value.vol, Value.t);
            Value.theta = euro.calltheta(Value.S, Value.r, Value.vol, Value.t);
            Value.vega = euro.callvega(Value.S, Value.r, Value.vol, Value.t);
            Value.rho = euro.callrho(Value.S, Value.r, Value.vol, Value.t);
            Value.Thread = 4;
            watch.Stop();
            Value.timer = watch.Elapsed.Seconds.ToString() + " : " + watch.Elapsed.Milliseconds.ToString();
            watch.Reset();
            Program.finished();
            GC.Collect();
        }

        public void DigitalPutAntiCVMT()
        {
            DigitalAntiCVMT euro = new DigitalAntiCVMT();
            watch.Start();
            euro.RandomGenerated();
            Value.Price = euro.PutPrice(Value.S, Value.r, Value.vol, Value.t);
            Value.SE = euro.SE();
            Value.delta = euro.putdelta(Value.S, Value.r, Value.vol, Value.t);
            Value.gamma = euro.putgamma(Value.S, Value.r, Value.vol, Value.t);
            Value.theta = euro.puttheta(Value.S, Value.r, Value.vol, Value.t);
            Value.vega = euro.putvega(Value.S, Value.r, Value.vol, Value.t);
            Value.rho = euro.putrho(Value.S, Value.r, Value.vol, Value.t);
            Value.Thread = 4;
            watch.Stop();
            Value.timer = watch.Elapsed.Seconds.ToString() + " : " + watch.Elapsed.Milliseconds.ToString();
            watch.Reset();
            Program.finished();
            GC.Collect();
        }
    }
}
