﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ConsoleApp6
{
    public class PreparationAntithetic
    {
        public double[] optionprice { get; set; }
        public double[] optioninverse { get; set; }
        public double Price(double S, double r, double vol, double t, double ep)
        {
            double price;
            double deltaT = t / Convert.ToDouble(Value.step);
            price = S * Math.Exp((r - Math.Pow(vol, 2) / 2) * deltaT + vol * Math.Sqrt(deltaT) * ep);
            return price;
        }
    }

    public class EuropeanAntithetic : PreparationAntithetic, CallPrice, StandardError, PutPrice, CallGreek, PutGreek
    {
        public List<double> PriceList;
        public List<double> PriceInverse;
        public static double[,] RandomList1;
        public static double[,] RandomList2;
        public void Initial()
        {
            optionprice = new double[Value.step + 1];
            optioninverse = new double[Value.step + 1];
            PriceList = new List<double>();
            PriceInverse = new List<double>();
        }
        public void RandomGenerated()
        {
            RandomList1 = new double[Value.trials, Value.step];
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                for (int k = 0; k < Value.step; k++)
                {
                    RandomList1[i, k] = GaussianBoxMuller.NextDouble();
                }
            }
        }
        public void RandomAntithetic()
        {
            RandomList2 = new double[Value.trials, Value.step];
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                for (int k = 0; k < Value.step; k++)
                {
                    RandomList2[i, k] = -RandomList1[i, k];
                }
            }
        }
        public double CallPrice(double S, double r, double vol, double t)
        {
            Initial();
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                optionprice[0] = S;
                for (int j = 1; j < Value.step + 1; j++)
                {
                    optionprice[j] = Price(optionprice[j - 1], r, vol, t, RandomList1[i, j - 1]);
                }
                PriceList.Add(Math.Max(optionprice.Last() - Value.K, 0));
            }
            RandomAntithetic();
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                optioninverse[0] = S;
                for (int j = 1; j < Value.step + 1; j++)
                {
                    optioninverse[j] = Price(optioninverse[j - 1], r, vol, t, RandomList2[i, j - 1]);
                }
                PriceInverse.Add(Math.Max(optioninverse.Last()-Value.K,0));
            }
            double price1 = 0;
            price1 = Math.Exp(-r * t) * PriceList.Average();
            double price2 = 0;
            price2= Math.Exp(-r * t) * PriceInverse.Average();
            Program.increase(1);
            return (price1+price2)/2;
        }
        public double PutPrice(double S, double r, double vol, double t)
        {
            Initial();
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                optionprice[0] = S;
                for (uint j = 1; j < Value.step + 1; j++)
                {
                    optionprice[j] = Price(optionprice[j - 1], r, vol, t, RandomList1[i, j - 1]);
                }
                PriceList.Add(Math.Max(Value.K - optionprice.Last(), 0));
            }
            RandomAntithetic();
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                optioninverse[0] = S;
                for (int j = 1; j < Value.step + 1; j++)
                {
                    optioninverse[j] = Price(optioninverse[j - 1], r, vol, t, RandomList2[i, j - 1]);
                }
                PriceInverse.Add(Math.Max(Value.K - optioninverse.Last(),0));
            }
            double price1 = 0;
            price1 = Math.Exp(-r * t) * PriceList.Average();
            double price2 = 0;
            price2 = Math.Exp(-r * t) * PriceInverse.Average();
            Program.increase(1);
            return (price1+price2)/2;
        }
        public double SE()
        {
            List<double> Option = new List<double>();
            for (UInt32 i = 0; i < PriceList.Count; i++)
            {
                Option.Add((PriceList[(int)i] + PriceInverse[(int)i]) / 2);
            }
            double avg = Option.Average();
            double sum = Option.Sum(v => Math.Pow(v - avg, 2));
            return (Math.Sqrt(sum / (Option.Count - 1))) / (Math.Sqrt(Option.Count));
        }
        internal static class GaussianBoxMuller
        {
            public static double NextDouble()
            {
                double x, y, square;

                do
                {
                    x = 2 * RandomProvider.GetThreadRandom().NextDouble() - 1;
                    y = 2 * RandomProvider.GetThreadRandom().NextDouble() - 1;
                    square = (x * x) + (y * y);
                } while (square >= 1);

                return x * Math.Sqrt(-2 * Math.Log(square) / square);
            }
        }
        internal static class RandomProvider
        {
            private static ThreadLocal<Random> randomWrapper = new ThreadLocal<Random>(() =>
                new Random(Interlocked.Increment(ref seed))
            );

            private static int seed = Environment.TickCount * 10;

            public static Random GetThreadRandom()
            {
                return randomWrapper.Value;
            }
        }
        public double calldelta(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S + 0.0001, r, vol, t);
            double cp2 = CallPrice(S - 0.0001, r, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double callgamma(double S, double r, double vol, double t)
        {
            return (calldelta(S + 0.01, r, vol, t) - calldelta(S, r, vol, t)) / 0.01;
        }
        public double callvega(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r, vol + 0.0001, t);
            double cp2 = CallPrice(S, r, vol - 0.0001, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double calltheta(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r, vol, t + 0.0001);
            double cp2 = CallPrice(S, r, vol, t - 0.0001);
            return (cp1 - cp2) / 0.0002;
        }
        public double callrho(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r + 0.0001, vol, t);
            double cp2 = CallPrice(S, r - 0.0001, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double putdelta(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S + 0.0001, r, vol, t);
            double cp2 = PutPrice(S - 0.0001, r, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double putgamma(double S, double r, double vol, double t)
        {
            return (putdelta(S + 0.01, r, vol, t) - putdelta(S, r, vol, t)) / 0.01;
        }
        public double putvega(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r, vol + 0.0001, t);
            double cp2 = PutPrice(S, r, vol - 0.0001, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double puttheta(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r, vol, t + 0.0001);
            double cp2 = PutPrice(S, r, vol, t - 0.0001);
            return (cp1 - cp2) / 0.0002;
        }
        public double putrho(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r + 0.0001, vol, t);
            double cp2 = PutPrice(S, r - 0.0001, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
    }

    public class AsianAntithetic : PreparationAntithetic, CallPrice, StandardError, PutPrice, CallGreek, PutGreek
    {
        public List<double> PriceList;
        public List<double> PriceInverse;
        public static double[,] RandomList1;
        public static double[,] RandomList2;
        public void Initial()
        {
            optionprice = new double[Value.step + 1];
            optioninverse = new double[Value.step + 1];
            PriceList = new List<double>();
            PriceInverse = new List<double>();
        }
        public void RandomGenerated()
        {
            RandomList1 = new double[Value.trials, Value.step];
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                for (int k = 0; k < Value.step; k++)
                {
                    RandomList1[i, k] = GaussianBoxMuller.NextDouble();
                }
            }
        }
        public void RandomAntithetic()
        {
            RandomList2 = new double[Value.trials, Value.step];
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                for (int k = 0; k < Value.step; k++)
                {
                    RandomList2[i, k] = -RandomList1[i, k];
                }
            }
        }
        public double CallPrice(double S, double r, double vol, double t)
        {
            Initial();
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                optionprice[0] = S;
                for (int j = 1; j < Value.step + 1; j++)
                {
                    optionprice[j] = Price(optionprice[j - 1], r, vol, t, RandomList1[i, j - 1]);
                }
                PriceList.Add(Math.Max((optionprice.Average() * (Value.step + 1) - optionprice[0]) / (Value.step) - Value.K, 0));
            }
            RandomAntithetic();
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                optioninverse[0] = S;
                for (int j = 1; j < Value.step + 1; j++)
                {
                    optioninverse[j] = Price(optioninverse[j - 1], r, vol, t, RandomList2[i, j - 1]);
                }
                PriceInverse.Add(Math.Max((optioninverse.Average() * (Value.step + 1) - optioninverse[0]) / (Value.step) - Value.K, 0));
            }
            double price1 = 0;
            price1 = Math.Exp(-r * t) * PriceList.Average();
            double price2 = 0;
            price2 = Math.Exp(-r * t) * PriceInverse.Average();
            Program.increase(1);
            return (price1 + price2) / 2;
        }
        public double PutPrice(double S, double r, double vol, double t)
        {
            Initial();
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                optionprice[0] = S;
                for (uint j = 1; j < Value.step + 1; j++)
                {
                    optionprice[j] = Price(optionprice[j - 1], r, vol, t, RandomList1[i, j - 1]);
                }
                PriceList.Add(Math.Max(Value.K - (optionprice.Average() * (Value.step + 1) - optionprice[0]) / (Value.step), 0));
            }
            RandomAntithetic();
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                optioninverse[0] = S;
                for (int j = 1; j < Value.step + 1; j++)
                {
                    optioninverse[j] = Price(optioninverse[j - 1], r, vol, t, RandomList2[i, j - 1]);
                }
                PriceInverse.Add(Math.Max(Value.K - (optioninverse.Average() * (Value.step + 1) - optioninverse[0]) / (Value.step), 0));
            }
            double price1 = 0;
            price1 = Math.Exp(-r * t) * PriceList.Average();
            double price2 = 0;
            price2 = Math.Exp(-r * t) * PriceInverse.Average();
            Program.increase(1);
            return (price1 + price2) / 2;
        }
        public double SE()
        {
            List<double> Option = new List<double>();
            for (UInt32 i = 0; i < PriceList.Count; i++)
            {
                Option.Add((PriceList[(int)i] + PriceInverse[(int)i]) / 2);
            }
            double avg = Option.Average();
            double sum = Option.Sum(v => Math.Pow(v - avg, 2));
            return (Math.Sqrt(sum / (Option.Count - 1))) / (Math.Sqrt(Option.Count));
        }
        internal static class GaussianBoxMuller
        {
            public static double NextDouble()
            {
                double x, y, square;

                do
                {
                    x = 2 * RandomProvider.GetThreadRandom().NextDouble() - 1;
                    y = 2 * RandomProvider.GetThreadRandom().NextDouble() - 1;
                    square = (x * x) + (y * y);
                } while (square >= 1);

                return x * Math.Sqrt(-2 * Math.Log(square) / square);
            }
        }
        internal static class RandomProvider
        {
            private static ThreadLocal<Random> randomWrapper = new ThreadLocal<Random>(() =>
                new Random(Interlocked.Increment(ref seed))
            );

            private static int seed = Environment.TickCount * 10;

            public static Random GetThreadRandom()
            {
                return randomWrapper.Value;
            }
        }
        public double calldelta(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S + 0.0001, r, vol, t);
            double cp2 = CallPrice(S - 0.0001, r, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double callgamma(double S, double r, double vol, double t)
        {
            return (calldelta(S + 0.01, r, vol, t) - calldelta(S, r, vol, t)) / 0.01;
        }
        public double callvega(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r, vol + 0.0001, t);
            double cp2 = CallPrice(S, r, vol - 0.0001, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double calltheta(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r, vol, t + 0.0001);
            double cp2 = CallPrice(S, r, vol, t - 0.0001);
            return (cp1 - cp2) / 0.0002;
        }
        public double callrho(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r + 0.0001, vol, t);
            double cp2 = CallPrice(S, r - 0.0001, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double putdelta(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S + 0.0001, r, vol, t);
            double cp2 = PutPrice(S - 0.0001, r, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double putgamma(double S, double r, double vol, double t)
        {
            return (putdelta(S + 0.01, r, vol, t) - putdelta(S, r, vol, t)) / 0.01;
        }
        public double putvega(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r, vol + 0.0001, t);
            double cp2 = PutPrice(S, r, vol - 0.0001, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double puttheta(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r, vol, t + 0.0001);
            double cp2 = PutPrice(S, r, vol, t - 0.0001);
            return (cp1 - cp2) / 0.0002;
        }
        public double putrho(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r + 0.0001, vol, t);
            double cp2 = PutPrice(S, r - 0.0001, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
    }

    public class DigitalAntithetic : PreparationAntithetic, CallPrice, StandardError, PutPrice, CallGreek, PutGreek
    {
        public List<double> PriceList;
        public List<double> PriceInverse;
        public static double[,] RandomList1;
        public static double[,] RandomList2;
        public void Initial()
        {
            optionprice = new double[Value.step + 1];
            optioninverse = new double[Value.step + 1];
            PriceList = new List<double>();
            PriceInverse = new List<double>();
        }
        public void RandomGenerated()
        {
            RandomList1 = new double[Value.trials, Value.step];
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                for (int k = 0; k < Value.step; k++)
                {
                    RandomList1[i, k] = GaussianBoxMuller.NextDouble();
                }
            }
        }
        public void RandomAntithetic()
        {
            RandomList2 = new double[Value.trials, Value.step];
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                for (int k = 0; k < Value.step; k++)
                {
                    RandomList2[i, k] = -RandomList1[i, k];
                }
            }
        }
        public double CallPrice(double S, double r, double vol, double t)
        {
            Initial();
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                optionprice[0] = S;
                for (int j = 1; j < Value.step + 1; j++)
                {
                    optionprice[j] = Price(optionprice[j - 1], r, vol, t, RandomList1[i, j - 1]);
                }
                if (optionprice.Last() - Value.K >= 0)
                { PriceList.Add(Value.rebate); }
                else
                { PriceList.Add(0); }
            }
            RandomAntithetic();
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                optioninverse[0] = S;
                for (int j = 1; j < Value.step + 1; j++)
                {
                    optioninverse[j] = Price(optioninverse[j - 1], r, vol, t, RandomList2[i, j - 1]);
                }
                if (optioninverse.Last() - Value.K >= 0)
                { PriceInverse.Add(Value.rebate); }
                else
                { PriceInverse.Add(0); }
            }
            double price1 = 0;
            price1 = Math.Exp(-r * t) * PriceList.Average();
            double price2 = 0;
            price2 = Math.Exp(-r * t) * PriceInverse.Average();
            Program.increase(1);
            return (price1 + price2) / 2;
        }
        public double PutPrice(double S, double r, double vol, double t)
        {
            Initial();
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                optionprice[0] = S;
                for (int j = 1; j < Value.step + 1; j++)
                {
                    optionprice[j] = Price(optionprice[j - 1], r, vol, t, RandomList1[i, j - 1]);
                }
                if (Value.K - optionprice.Last() >= 0)
                { PriceList.Add(Value.rebate); }
                else
                { PriceList.Add(0); }
            }
            RandomAntithetic();
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                optioninverse[0] = S;
                for (int j = 1; j < Value.step + 1; j++)
                {
                    optioninverse[j] = Price(optioninverse[j - 1], r, vol, t, RandomList2[i, j - 1]);
                }
                if (Value.K - optioninverse.Last() >= 0)
                { PriceInverse.Add(Value.rebate); }
                else
                { PriceInverse.Add(0); }
            }
            double price1 = 0;
            price1 = Math.Exp(-r * t) * PriceList.Average();
            double price2 = 0;
            price2 = Math.Exp(-r * t) * PriceInverse.Average();
            Program.increase(1);
            return (price1 + price2) / 2;
        }
        public double SE()
        {
            List<double> Option = new List<double>();
            for (UInt32 i = 0; i < PriceList.Count; i++)
            {
                Option.Add((PriceList[(int)i] + PriceInverse[(int)i]) / 2);
            }
            double avg = Option.Average();
            double sum = Option.Sum(v => Math.Pow(v - avg, 2));
            return (Math.Sqrt(sum / (Option.Count - 1))) / (Math.Sqrt(Option.Count));
        }
        internal static class GaussianBoxMuller
        {
            public static double NextDouble()
            {
                double x, y, square;

                do
                {
                    x = 2 * RandomProvider.GetThreadRandom().NextDouble() - 1;
                    y = 2 * RandomProvider.GetThreadRandom().NextDouble() - 1;
                    square = (x * x) + (y * y);
                } while (square >= 1);

                return x * Math.Sqrt(-2 * Math.Log(square) / square);
            }
        }
        internal static class RandomProvider
        {
            private static ThreadLocal<Random> randomWrapper = new ThreadLocal<Random>(() =>
                new Random(Interlocked.Increment(ref seed))
            );

            private static int seed = Environment.TickCount * 10;

            public static Random GetThreadRandom()
            {
                return randomWrapper.Value;
            }
        }
        public double calldelta(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S + 0.0001, r, vol, t);
            double cp2 = CallPrice(S - 0.0001, r, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double callgamma(double S, double r, double vol, double t)
        {
            return (calldelta(S + 0.01, r, vol, t) - calldelta(S, r, vol, t)) / 0.01;
        }
        public double callvega(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r, vol + 0.0001, t);
            double cp2 = CallPrice(S, r, vol - 0.0001, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double calltheta(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r, vol, t + 0.0001);
            double cp2 = CallPrice(S, r, vol, t - 0.0001);
            return (cp1 - cp2) / 0.0002;
        }
        public double callrho(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r + 0.0001, vol, t);
            double cp2 = CallPrice(S, r - 0.0001, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double putdelta(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S + 0.0001, r, vol, t);
            double cp2 = PutPrice(S - 0.0001, r, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double putgamma(double S, double r, double vol, double t)
        {
            return (putdelta(S + 0.01, r, vol, t) - putdelta(S, r, vol, t)) / 0.01;
        }
        public double putvega(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r, vol + 0.0001, t);
            double cp2 = PutPrice(S, r, vol - 0.0001, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double puttheta(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r, vol, t + 0.0001);
            double cp2 = PutPrice(S, r, vol, t - 0.0001);
            return (cp1 - cp2) / 0.0002;
        }
        public double putrho(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r + 0.0001, vol, t);
            double cp2 = PutPrice(S, r - 0.0001, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
    }

    public class BarrierDAOAntithetic : PreparationAntithetic, CallPrice, StandardError, PutPrice, CallGreek, PutGreek
    {
        public List<double> PriceList;
        public List<double> PriceInverse;
        public static double[,] RandomList1;
        public static double[,] RandomList2;
        public void Initial()
        {
            optionprice = new double[Value.step + 1];
            optioninverse = new double[Value.step + 1];
            PriceList = new List<double>();
            PriceInverse = new List<double>();
        }
        public void RandomGenerated()
        {
            RandomList1 = new double[Value.trials, Value.step];
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                for (int k = 0; k < Value.step; k++)
                {
                    RandomList1[i, k] = GaussianBoxMuller.NextDouble();
                }
            }
        }
        public void RandomAntithetic()
        {
            RandomList2 = new double[Value.trials, Value.step];
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                for (int k = 0; k < Value.step; k++)
                {
                    RandomList2[i, k] = -RandomList1[i, k];
                }
            }
        }
        public double CallPrice(double S, double r, double vol, double t)
        {
            Initial();
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                optionprice[0] = S;
                for (int j = 1; j < Value.step + 1; j++)
                {
                    optionprice[j] = Price(optionprice[j - 1], r, vol, t, RandomList1[i, j - 1]);
                }
                if (optionprice.Min() >= Value.barrier)
                { PriceList.Add(Math.Max(optionprice.Last() - Value.K, 0)); }
                else { PriceList.Add(0); }
            }
            RandomAntithetic();
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                optioninverse[0] = S;
                for (int j = 1; j < Value.step + 1; j++)
                {
                    optioninverse[j] = Price(optioninverse[j - 1], r, vol, t, RandomList2[i, j - 1]);
                }
                if (optioninverse.Min() >= Value.barrier)
                { PriceInverse.Add(Math.Max(optioninverse.Last() - Value.K, 0)); }
                else { PriceInverse.Add(0); }
            }
            double price1 = 0;
            price1 = Math.Exp(-r * t) * PriceList.Average();
            double price2 = 0;
            price2 = Math.Exp(-r * t) * PriceInverse.Average();
            Program.increase(1);
            return (price1 + price2) / 2;
        }
        public double PutPrice(double S, double r, double vol, double t)
        {
            Initial();
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                optionprice[0] = S;
                for (uint j = 1; j < Value.step + 1; j++)
                {
                    optionprice[j] = Price(optionprice[j - 1], r, vol, t, RandomList1[i, j - 1]);
                }
                if (optionprice.Min() >= Value.barrier)
                { PriceList.Add(Math.Max(Value.K - optionprice.Last(), 0)); }
                else { PriceList.Add(0); }
            }
            RandomAntithetic();
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                optioninverse[0] = S;
                for (uint j = 1; j < Value.step + 1; j++)
                {
                    optioninverse[j] = Price(optioninverse[j - 1], r, vol, t, RandomList2[i, j - 1]);
                }
                if (optionprice.Min() >= Value.barrier)
                { PriceInverse.Add(Math.Max(Value.K - optioninverse.Last(), 0)); }
                else { PriceInverse.Add(0); }
            }
            double price1 = 0;
            price1 = Math.Exp(-r * t) * PriceList.Average();
            double price2 = 0;
            price2 = Math.Exp(-r * t) * PriceInverse.Average();
            Program.increase(1);
            return (price1 + price2) / 2;
        }
        public double SE()
        {
            List<double> Option = new List<double>();
            for (UInt32 i = 0; i < PriceList.Count; i++)
            {
                Option.Add((PriceList[(int)i] + PriceInverse[(int)i]) / 2);
            }
            double avg = Option.Average();
            double sum = Option.Sum(v => Math.Pow(v - avg, 2));
            return (Math.Sqrt(sum / (Option.Count - 1))) / (Math.Sqrt(Option.Count));
        }
        internal static class GaussianBoxMuller
        {
            public static double NextDouble()
            {
                double x, y, square;

                do
                {
                    x = 2 * RandomProvider.GetThreadRandom().NextDouble() - 1;
                    y = 2 * RandomProvider.GetThreadRandom().NextDouble() - 1;
                    square = (x * x) + (y * y);
                } while (square >= 1);

                return x * Math.Sqrt(-2 * Math.Log(square) / square);
            }
        }
        internal static class RandomProvider
        {
            private static ThreadLocal<Random> randomWrapper = new ThreadLocal<Random>(() =>
                new Random(Interlocked.Increment(ref seed))
            );

            private static int seed = Environment.TickCount * 10;

            public static Random GetThreadRandom()
            {
                return randomWrapper.Value;
            }
        }
        public double calldelta(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S + 0.0001, r, vol, t);
            double cp2 = CallPrice(S - 0.0001, r, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double callgamma(double S, double r, double vol, double t)
        {
            return (calldelta(S + 0.01, r, vol, t) - calldelta(S, r, vol, t)) / 0.01;
        }
        public double callvega(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r, vol + 0.0001, t);
            double cp2 = CallPrice(S, r, vol - 0.0001, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double calltheta(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r, vol, t + 0.0001);
            double cp2 = CallPrice(S, r, vol, t - 0.0001);
            return (cp1 - cp2) / 0.0002;
        }
        public double callrho(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r + 0.0001, vol, t);
            double cp2 = CallPrice(S, r - 0.0001, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double putdelta(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S + 0.0001, r, vol, t);
            double cp2 = PutPrice(S - 0.0001, r, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double putgamma(double S, double r, double vol, double t)
        {
            return (putdelta(S + 0.01, r, vol, t) - putdelta(S, r, vol, t)) / 0.01;
        }
        public double putvega(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r, vol + 0.0001, t);
            double cp2 = PutPrice(S, r, vol - 0.0001, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double puttheta(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r, vol, t + 0.0001);
            double cp2 = PutPrice(S, r, vol, t - 0.0001);
            return (cp1 - cp2) / 0.0002;
        }
        public double putrho(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r + 0.0001, vol, t);
            double cp2 = PutPrice(S, r - 0.0001, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
    }

    public class BarrierDAIAntithetic : PreparationAntithetic, CallPrice, StandardError, PutPrice, CallGreek, PutGreek
    {
        public List<double> PriceList;
        public List<double> PriceInverse;
        public static double[,] RandomList1;
        public static double[,] RandomList2;
        public void Initial()
        {
            optionprice = new double[Value.step + 1];
            optioninverse = new double[Value.step + 1];
            PriceList = new List<double>();
            PriceInverse = new List<double>();
        }
        public void RandomGenerated()
        {
            RandomList1 = new double[Value.trials, Value.step];
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                for (int k = 0; k < Value.step; k++)
                {
                    RandomList1[i, k] = GaussianBoxMuller.NextDouble();
                }
            }
        }
        public void RandomAntithetic()
        {
            RandomList2 = new double[Value.trials, Value.step];
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                for (int k = 0; k < Value.step; k++)
                {
                    RandomList2[i, k] = -RandomList1[i, k];
                }
            }
        }
        public double CallPrice(double S, double r, double vol, double t)
        {
            Initial();
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                optionprice[0] = S;
                for (int j = 1; j < Value.step + 1; j++)
                {
                    optionprice[j] = Price(optionprice[j - 1], r, vol, t, RandomList1[i, j - 1]);
                }
                if (optionprice.Min() < Value.barrier)
                { PriceList.Add(Math.Max(optionprice.Last() - Value.K, 0)); }
                else { PriceList.Add(0); }
            }
            RandomAntithetic();
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                optioninverse[0] = S;
                for (int j = 1; j < Value.step + 1; j++)
                {
                    optioninverse[j] = Price(optioninverse[j - 1], r, vol, t, RandomList2[i, j - 1]);
                }
                if (optioninverse.Min() < Value.barrier)
                { PriceInverse.Add(Math.Max(optioninverse.Last() - Value.K, 0)); }
                else { PriceInverse.Add(0); }
            }
            double price1 = 0;
            price1 = Math.Exp(-r * t) * PriceList.Average();
            double price2 = 0;
            price2 = Math.Exp(-r * t) * PriceInverse.Average();
            Program.increase(1);
            return (price1 + price2) / 2;
        }
        public double PutPrice(double S, double r, double vol, double t)
        {
            Initial();
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                optionprice[0] = S;
                for (uint j = 1; j < Value.step + 1; j++)
                {
                    optionprice[j] = Price(optionprice[j - 1], r, vol, t, RandomList1[i, j - 1]);
                }
                if (optionprice.Min() < Value.barrier)
                { PriceList.Add(Math.Max(Value.K - optionprice.Last(), 0)); }
                else { PriceList.Add(0); }
            }
            RandomAntithetic();
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                optioninverse[0] = S;
                for (uint j = 1; j < Value.step + 1; j++)
                {
                    optioninverse[j] = Price(optioninverse[j - 1], r, vol, t, RandomList2[i, j - 1]);
                }
                if (optionprice.Min() < Value.barrier)
                { PriceInverse.Add(Math.Max(Value.K - optioninverse.Last(), 0)); }
                else { PriceInverse.Add(0); }
            }
            double price1 = 0;
            price1 = Math.Exp(-r * t) * PriceList.Average();
            double price2 = 0;
            price2 = Math.Exp(-r * t) * PriceInverse.Average();
            Program.increase(1);
            return (price1 + price2) / 2;
        }
        public double SE()
        {
            List<double> Option = new List<double>();
            for (UInt32 i = 0; i < PriceList.Count; i++)
            {
                Option.Add((PriceList[(int)i] + PriceInverse[(int)i]) / 2);
            }
            double avg = Option.Average();
            double sum = Option.Sum(v => Math.Pow(v - avg, 2));
            return (Math.Sqrt(sum / (Option.Count - 1))) / (Math.Sqrt(Option.Count));
        }
        internal static class GaussianBoxMuller
        {
            public static double NextDouble()
            {
                double x, y, square;

                do
                {
                    x = 2 * RandomProvider.GetThreadRandom().NextDouble() - 1;
                    y = 2 * RandomProvider.GetThreadRandom().NextDouble() - 1;
                    square = (x * x) + (y * y);
                } while (square >= 1);

                return x * Math.Sqrt(-2 * Math.Log(square) / square);
            }
        }
        internal static class RandomProvider
        {
            private static ThreadLocal<Random> randomWrapper = new ThreadLocal<Random>(() =>
                new Random(Interlocked.Increment(ref seed))
            );

            private static int seed = Environment.TickCount * 10;

            public static Random GetThreadRandom()
            {
                return randomWrapper.Value;
            }
        }
        public double calldelta(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S + 0.0001, r, vol, t);
            double cp2 = CallPrice(S - 0.0001, r, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double callgamma(double S, double r, double vol, double t)
        {
            return (calldelta(S + 0.01, r, vol, t) - calldelta(S, r, vol, t)) / 0.01;
        }
        public double callvega(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r, vol + 0.0001, t);
            double cp2 = CallPrice(S, r, vol - 0.0001, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double calltheta(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r, vol, t + 0.0001);
            double cp2 = CallPrice(S, r, vol, t - 0.0001);
            return (cp1 - cp2) / 0.0002;
        }
        public double callrho(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r + 0.0001, vol, t);
            double cp2 = CallPrice(S, r - 0.0001, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double putdelta(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S + 0.0001, r, vol, t);
            double cp2 = PutPrice(S - 0.0001, r, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double putgamma(double S, double r, double vol, double t)
        {
            return (putdelta(S + 0.01, r, vol, t) - putdelta(S, r, vol, t)) / 0.01;
        }
        public double putvega(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r, vol + 0.0001, t);
            double cp2 = PutPrice(S, r, vol - 0.0001, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double puttheta(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r, vol, t + 0.0001);
            double cp2 = PutPrice(S, r, vol, t - 0.0001);
            return (cp1 - cp2) / 0.0002;
        }
        public double putrho(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r + 0.0001, vol, t);
            double cp2 = PutPrice(S, r - 0.0001, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
    }

    public class BarrierUAOAntithetic : PreparationAntithetic, CallPrice, StandardError, PutPrice, CallGreek, PutGreek
    {
        public List<double> PriceList;
        public List<double> PriceInverse;
        public static double[,] RandomList1;
        public static double[,] RandomList2;
        public void Initial()
        {
            optionprice = new double[Value.step + 1];
            optioninverse = new double[Value.step + 1];
            PriceList = new List<double>();
            PriceInverse = new List<double>();
        }
        public void RandomGenerated()
        {
            RandomList1 = new double[Value.trials, Value.step];
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                for (int k = 0; k < Value.step; k++)
                {
                    RandomList1[i, k] = GaussianBoxMuller.NextDouble();
                }
            }
        }
        public void RandomAntithetic()
        {
            RandomList2 = new double[Value.trials, Value.step];
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                for (int k = 0; k < Value.step; k++)
                {
                    RandomList2[i, k] = -RandomList1[i, k];
                }
            }
        }
        public double CallPrice(double S, double r, double vol, double t)
        {
            Initial();
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                optionprice[0] = S;
                for (int j = 1; j < Value.step + 1; j++)
                {
                    optionprice[j] = Price(optionprice[j - 1], r, vol, t, RandomList1[i, j - 1]);
                }
                if (optionprice.Max() <= Value.barrier)
                { PriceList.Add(Math.Max(optionprice.Last() - Value.K, 0)); }
                else { PriceList.Add(0); }
            }
            RandomAntithetic();
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                optioninverse[0] = S;
                for (int j = 1; j < Value.step + 1; j++)
                {
                    optioninverse[j] = Price(optioninverse[j - 1], r, vol, t, RandomList2[i, j - 1]);
                }
                if (optioninverse.Max() <= Value.barrier)
                { PriceInverse.Add(Math.Max(optioninverse.Last() - Value.K, 0)); }
                else { PriceInverse.Add(0); }
            }
            double price1 = 0;
            price1 = Math.Exp(-r * t) * PriceList.Average();
            double price2 = 0;
            price2 = Math.Exp(-r * t) * PriceInverse.Average();
            Program.increase(1);
            return (price1 + price2) / 2;
        }
        public double PutPrice(double S, double r, double vol, double t)
        {
            Initial();
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                optionprice[0] = S;
                for (uint j = 1; j < Value.step + 1; j++)
                {
                    optionprice[j] = Price(optionprice[j - 1], r, vol, t, RandomList1[i, j - 1]);
                }
                if (optionprice.Max() <= Value.barrier)
                { PriceList.Add(Math.Max(Value.K - optionprice.Last(), 0)); }
                else { PriceList.Add(0); }
            }
            RandomAntithetic();
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                optioninverse[0] = S;
                for (uint j = 1; j < Value.step + 1; j++)
                {
                    optioninverse[j] = Price(optioninverse[j - 1], r, vol, t, RandomList2[i, j - 1]);
                }
                if (optionprice.Max() <= Value.barrier)
                { PriceInverse.Add(Math.Max(Value.K - optioninverse.Last(), 0)); }
                else { PriceInverse.Add(0); }
            }
            double price1 = 0;
            price1 = Math.Exp(-r * t) * PriceList.Average();
            double price2 = 0;
            price2 = Math.Exp(-r * t) * PriceInverse.Average();
            Program.increase(1);
            return (price1 + price2) / 2;
        }
        public double SE()
        {
            List<double> Option = new List<double>();
            for (UInt32 i = 0; i < PriceList.Count; i++)
            {
                Option.Add((PriceList[(int)i] + PriceInverse[(int)i]) / 2);
            }
            double avg = Option.Average();
            double sum = Option.Sum(v => Math.Pow(v - avg, 2));
            return (Math.Sqrt(sum / (Option.Count - 1))) / (Math.Sqrt(Option.Count));
        }
        internal static class GaussianBoxMuller
        {
            public static double NextDouble()
            {
                double x, y, square;

                do
                {
                    x = 2 * RandomProvider.GetThreadRandom().NextDouble() - 1;
                    y = 2 * RandomProvider.GetThreadRandom().NextDouble() - 1;
                    square = (x * x) + (y * y);
                } while (square >= 1);

                return x * Math.Sqrt(-2 * Math.Log(square) / square);
            }
        }
        internal static class RandomProvider
        {
            private static ThreadLocal<Random> randomWrapper = new ThreadLocal<Random>(() =>
                new Random(Interlocked.Increment(ref seed))
            );

            private static int seed = Environment.TickCount * 10;

            public static Random GetThreadRandom()
            {
                return randomWrapper.Value;
            }
        }
        public double calldelta(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S + 0.0001, r, vol, t);
            double cp2 = CallPrice(S - 0.0001, r, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double callgamma(double S, double r, double vol, double t)
        {
            return (calldelta(S + 0.01, r, vol, t) - calldelta(S, r, vol, t)) / 0.01;
        }
        public double callvega(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r, vol + 0.0001, t);
            double cp2 = CallPrice(S, r, vol - 0.0001, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double calltheta(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r, vol, t + 0.0001);
            double cp2 = CallPrice(S, r, vol, t - 0.0001);
            return (cp1 - cp2) / 0.0002;
        }
        public double callrho(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r + 0.0001, vol, t);
            double cp2 = CallPrice(S, r - 0.0001, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double putdelta(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S + 0.0001, r, vol, t);
            double cp2 = PutPrice(S - 0.0001, r, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double putgamma(double S, double r, double vol, double t)
        {
            return (putdelta(S + 0.01, r, vol, t) - putdelta(S, r, vol, t)) / 0.01;
        }
        public double putvega(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r, vol + 0.0001, t);
            double cp2 = PutPrice(S, r, vol - 0.0001, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double puttheta(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r, vol, t + 0.0001);
            double cp2 = PutPrice(S, r, vol, t - 0.0001);
            return (cp1 - cp2) / 0.0002;
        }
        public double putrho(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r + 0.0001, vol, t);
            double cp2 = PutPrice(S, r - 0.0001, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
    }

    public class BarrierUAIAntithetic : PreparationAntithetic, CallPrice, StandardError, PutPrice, CallGreek, PutGreek
    {
        public List<double> PriceList;
        public List<double> PriceInverse;
        public static double[,] RandomList1;
        public static double[,] RandomList2;
        public void Initial()
        {
            optionprice = new double[Value.step + 1];
            optioninverse = new double[Value.step + 1];
            PriceList = new List<double>();
            PriceInverse = new List<double>();
        }
        public void RandomGenerated()
        {
            RandomList1 = new double[Value.trials, Value.step];
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                for (int k = 0; k < Value.step; k++)
                {
                    RandomList1[i, k] = GaussianBoxMuller.NextDouble();
                }
            }
        }
        public void RandomAntithetic()
        {
            RandomList2 = new double[Value.trials, Value.step];
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                for (int k = 0; k < Value.step; k++)
                {
                    RandomList2[i, k] = -RandomList1[i, k];
                }
            }
        }
        public double CallPrice(double S, double r, double vol, double t)
        {
            Initial();
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                optionprice[0] = S;
                for (int j = 1; j < Value.step + 1; j++)
                {
                    optionprice[j] = Price(optionprice[j - 1], r, vol, t, RandomList1[i, j - 1]);
                }
                if (optionprice.Max() > Value.barrier)
                { PriceList.Add(Math.Max(optionprice.Last() - Value.K, 0)); }
                else { PriceList.Add(0); }
            }
            RandomAntithetic();
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                optioninverse[0] = S;
                for (int j = 1; j < Value.step + 1; j++)
                {
                    optioninverse[j] = Price(optioninverse[j - 1], r, vol, t, RandomList2[i, j - 1]);
                }
                if (optioninverse.Max() > Value.barrier)
                { PriceInverse.Add(Math.Max(optioninverse.Last() - Value.K, 0)); }
                else { PriceInverse.Add(0); }
            }
            double price1 = 0;
            price1 = Math.Exp(-r * t) * PriceList.Average();
            double price2 = 0;
            price2 = Math.Exp(-r * t) * PriceInverse.Average();
            Program.increase(1);
            return (price1 + price2) / 2;
        }
        public double PutPrice(double S, double r, double vol, double t)
        {
            Initial();
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                optionprice[0] = S;
                for (uint j = 1; j < Value.step + 1; j++)
                {
                    optionprice[j] = Price(optionprice[j - 1], r, vol, t, RandomList1[i, j - 1]);
                }
                if (optionprice.Max() > Value.barrier)
                { PriceList.Add(Math.Max(Value.K - optionprice.Last(), 0)); }
                else { PriceList.Add(0); }
            }
            RandomAntithetic();
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                optioninverse[0] = S;
                for (uint j = 1; j < Value.step + 1; j++)
                {
                    optioninverse[j] = Price(optioninverse[j - 1], r, vol, t, RandomList2[i, j - 1]);
                }
                if (optionprice.Max() > Value.barrier)
                { PriceInverse.Add(Math.Max(Value.K - optioninverse.Last(), 0)); }
                else { PriceInverse.Add(0); }
            }
            double price1 = 0;
            price1 = Math.Exp(-r * t) * PriceList.Average();
            double price2 = 0;
            price2 = Math.Exp(-r * t) * PriceInverse.Average();
            Program.increase(1);
            return (price1 + price2) / 2;
        }
        public double SE()
        {
            List<double> Option = new List<double>();
            for (UInt32 i = 0; i < PriceList.Count; i++)
            {
                Option.Add((PriceList[(int)i] + PriceInverse[(int)i]) / 2);
            }
            double avg = Option.Average();
            double sum = Option.Sum(v => Math.Pow(v - avg, 2));
            return (Math.Sqrt(sum / (Option.Count - 1))) / (Math.Sqrt(Option.Count));
        }
        internal static class GaussianBoxMuller
        {
            public static double NextDouble()
            {
                double x, y, square;

                do
                {
                    x = 2 * RandomProvider.GetThreadRandom().NextDouble() - 1;
                    y = 2 * RandomProvider.GetThreadRandom().NextDouble() - 1;
                    square = (x * x) + (y * y);
                } while (square >= 1);

                return x * Math.Sqrt(-2 * Math.Log(square) / square);
            }
        }
        internal static class RandomProvider
        {
            private static ThreadLocal<Random> randomWrapper = new ThreadLocal<Random>(() =>
                new Random(Interlocked.Increment(ref seed))
            );

            private static int seed = Environment.TickCount * 10;

            public static Random GetThreadRandom()
            {
                return randomWrapper.Value;
            }
        }
        public double calldelta(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S + 0.0001, r, vol, t);
            double cp2 = CallPrice(S - 0.0001, r, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double callgamma(double S, double r, double vol, double t)
        {
            return (calldelta(S + 0.01, r, vol, t) - calldelta(S, r, vol, t)) / 0.01;
        }
        public double callvega(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r, vol + 0.0001, t);
            double cp2 = CallPrice(S, r, vol - 0.0001, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double calltheta(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r, vol, t + 0.0001);
            double cp2 = CallPrice(S, r, vol, t - 0.0001);
            return (cp1 - cp2) / 0.0002;
        }
        public double callrho(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r + 0.0001, vol, t);
            double cp2 = CallPrice(S, r - 0.0001, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double putdelta(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S + 0.0001, r, vol, t);
            double cp2 = PutPrice(S - 0.0001, r, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double putgamma(double S, double r, double vol, double t)
        {
            return (putdelta(S + 0.01, r, vol, t) - putdelta(S, r, vol, t)) / 0.01;
        }
        public double putvega(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r, vol + 0.0001, t);
            double cp2 = PutPrice(S, r, vol - 0.0001, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double puttheta(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r, vol, t + 0.0001);
            double cp2 = PutPrice(S, r, vol, t - 0.0001);
            return (cp1 - cp2) / 0.0002;
        }
        public double putrho(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r + 0.0001, vol, t);
            double cp2 = PutPrice(S, r - 0.0001, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
    }

    public class LookbackAntithetic : PreparationAntithetic, CallPrice, StandardError, PutPrice, CallGreek, PutGreek
    {
        public List<double> PriceList;
        public List<double> PriceInverse;
        public static double[,] RandomList1;
        public static double[,] RandomList2;
        public void Initial()
        {
            optionprice = new double[Value.step + 1];
            optioninverse = new double[Value.step + 1];
            PriceList = new List<double>();
            PriceInverse = new List<double>();
        }
        public void RandomGenerated()
        {
            RandomList1 = new double[Value.trials, Value.step];
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                for (int k = 0; k < Value.step; k++)
                {
                    RandomList1[i, k] = GaussianBoxMuller.NextDouble();
                }
            }
        }
        public void RandomAntithetic()
        {
            RandomList2 = new double[Value.trials, Value.step];
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                for (int k = 0; k < Value.step; k++)
                {
                    RandomList2[i, k] = -RandomList1[i, k];
                }
            }
        }
        public double CallPrice(double S, double r, double vol, double t)
        {
            Initial();
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                optionprice[0] = S;
                for (int j = 1; j < Value.step + 1; j++)
                {
                    optionprice[j] = Price(optionprice[j - 1], r, vol, t, RandomList1[i, j - 1]);
                }
                PriceList.Add(Math.Max(optionprice.Max() - Value.K, 0));
            }
            RandomAntithetic();
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                optioninverse[0] = S;
                for (int j = 1; j < Value.step + 1; j++)
                {
                    optioninverse[j] = Price(optioninverse[j - 1], r, vol, t, RandomList2[i, j - 1]);
                }
                PriceInverse.Add(Math.Max(optioninverse.Max() - Value.K, 0));
            }
            double price1 = 0;
            price1 = Math.Exp(-r * t) * PriceList.Average();
            double price2 = 0;
            price2 = Math.Exp(-r * t) * PriceInverse.Average();
            Program.increase(1);
            return (price1 + price2) / 2;
        }
        public double PutPrice(double S, double r, double vol, double t)
        {
            Initial();
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                optionprice[0] = S;
                for (int j = 1; j < Value.step + 1; j++)
                {
                    optionprice[j] = Price(optionprice[j - 1], r, vol, t, RandomList1[i, j - 1]);
                }
                PriceList.Add(Math.Max(Value.K - optionprice.Min(), 0));
            }
            RandomAntithetic();
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                optioninverse[0] = S;
                for (int j = 1; j < Value.step + 1; j++)
                {
                    optioninverse[j] = Price(optioninverse[j - 1], r, vol, t, RandomList2[i, j - 1]);
                }
                PriceInverse.Add(Math.Max(Value.K - optioninverse.Min(), 0));
            }
            double price1 = 0;
            price1 = Math.Exp(-r * t) * PriceList.Average();
            double price2 = 0;
            price2 = Math.Exp(-r * t) * PriceInverse.Average();
            Program.increase(1);
            return (price1 + price2) / 2;
        }
        public double SE()
        {
            List<double> Option = new List<double>();
            for (UInt32 i = 0; i < PriceList.Count; i++)
            {
                Option.Add((PriceList[(int)i] + PriceInverse[(int)i]) / 2);
            }
            double avg = Option.Average();
            double sum = Option.Sum(v => Math.Pow(v - avg, 2));
            return (Math.Sqrt(sum / (Option.Count - 1))) / (Math.Sqrt(Option.Count));
        }
        internal static class GaussianBoxMuller
        {
            public static double NextDouble()
            {
                double x, y, square;

                do
                {
                    x = 2 * RandomProvider.GetThreadRandom().NextDouble() - 1;
                    y = 2 * RandomProvider.GetThreadRandom().NextDouble() - 1;
                    square = (x * x) + (y * y);
                } while (square >= 1);

                return x * Math.Sqrt(-2 * Math.Log(square) / square);
            }
        }
        internal static class RandomProvider
        {
            private static ThreadLocal<Random> randomWrapper = new ThreadLocal<Random>(() =>
                new Random(Interlocked.Increment(ref seed))
            );

            private static int seed = Environment.TickCount * 10;

            public static Random GetThreadRandom()
            {
                return randomWrapper.Value;
            }
        }
        public double calldelta(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S + 0.0001, r, vol, t);
            double cp2 = CallPrice(S - 0.0001, r, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double callgamma(double S, double r, double vol, double t)
        {
            return (calldelta(S + 0.01, r, vol, t) - calldelta(S, r, vol, t)) / 0.01;
        }
        public double callvega(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r, vol + 0.0001, t);
            double cp2 = CallPrice(S, r, vol - 0.0001, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double calltheta(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r, vol, t + 0.0001);
            double cp2 = CallPrice(S, r, vol, t - 0.0001);
            return (cp1 - cp2) / 0.0002;
        }
        public double callrho(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r + 0.0001, vol, t);
            double cp2 = CallPrice(S, r - 0.0001, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double putdelta(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S + 0.0001, r, vol, t);
            double cp2 = PutPrice(S - 0.0001, r, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double putgamma(double S, double r, double vol, double t)
        {
            return (putdelta(S + 0.01, r, vol, t) - putdelta(S, r, vol, t)) / 0.01;
        }
        public double putvega(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r, vol + 0.0001, t);
            double cp2 = PutPrice(S, r, vol - 0.0001, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double puttheta(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r, vol, t + 0.0001);
            double cp2 = PutPrice(S, r, vol, t - 0.0001);
            return (cp1 - cp2) / 0.0002;
        }
        public double putrho(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r + 0.0001, vol, t);
            double cp2 = PutPrice(S, r - 0.0001, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
    }

    public class RangeAntithetic : PreparationAntithetic, CallPrice, StandardError, PutPrice, CallGreek, PutGreek
    {
        public List<double> PriceList;
        public List<double> PriceInverse;
        public static double[,] RandomList1;
        public static double[,] RandomList2;
        public void Initial()
        {
            optionprice = new double[Value.step + 1];
            optioninverse = new double[Value.step + 1];
            PriceList = new List<double>();
            PriceInverse = new List<double>();
        }
        public void RandomGenerated()
        {
            RandomList1 = new double[Value.trials, Value.step];
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                for (int k = 0; k < Value.step; k++)
                {
                    RandomList1[i, k] = GaussianBoxMuller.NextDouble();
                }
            }
        }
        public void RandomAntithetic()
        {
            RandomList2 = new double[Value.trials, Value.step];
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                for (int k = 0; k < Value.step; k++)
                {
                    RandomList2[i, k] = -RandomList1[i, k];
                }
            }
        }
        public double CallPrice(double S, double r, double vol, double t)
        {
            Initial();
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                optionprice[0] = S;
                for (int j = 1; j < Value.step + 1; j++)
                {
                    optionprice[j] = Price(optionprice[j - 1], r, vol, t, RandomList1[i, j - 1]);
                }
                PriceList.Add(optionprice.Max() - optionprice.Min());
            }
            RandomAntithetic();
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                optioninverse[0] = S;
                for (int j = 1; j < Value.step + 1; j++)
                {
                    optioninverse[j] = Price(optioninverse[j - 1], r, vol, t, RandomList2[i, j - 1]);
                }
                PriceInverse.Add(optioninverse.Max() - optioninverse.Min());
            }
            double price1 = 0;
            price1 = Math.Exp(-r * t) * PriceList.Average();
            double price2 = 0;
            price2 = Math.Exp(-r * t) * PriceInverse.Average();
            Program.increase(1);
            return (price1 + price2) / 2;
        }
        public double PutPrice(double S, double r, double vol, double t)
        {
            Initial();
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                optionprice[0] = S;
                for (int j = 1; j < Value.step + 1; j++)
                {
                    optionprice[j] = Price(optionprice[j - 1], r, vol, t, RandomList1[i, j - 1]);
                }
                PriceList.Add(optionprice.Max() - optionprice.Min());
            }
            RandomAntithetic();
            for (UInt32 i = 0; i < Value.trials; i++)
            {
                optioninverse[0] = S;
                for (int j = 1; j < Value.step + 1; j++)
                {
                    optioninverse[j] = Price(optioninverse[j - 1], r, vol, t, RandomList2[i, j - 1]);
                }
                PriceInverse.Add(optioninverse.Max() - optioninverse.Min());
            }
            double price1 = 0;
            price1 = Math.Exp(-r * t) * PriceList.Average();
            double price2 = 0;
            price2 = Math.Exp(-r * t) * PriceInverse.Average();
            Program.increase(1);
            return (price1 + price2) / 2;
        }
        public double SE()
        {
            List<double> Option = new List<double>();
            for (UInt32 i = 0; i < PriceList.Count; i++)
            {
                Option.Add((PriceList[(int)i] + PriceInverse[(int)i]) / 2);
            }
            double avg = Option.Average();
            double sum = Option.Sum(v => Math.Pow(v - avg, 2));
            return (Math.Sqrt(sum / (Option.Count - 1))) / (Math.Sqrt(Option.Count));
        }
        internal static class GaussianBoxMuller
        {
            public static double NextDouble()
            {
                double x, y, square;

                do
                {
                    x = 2 * RandomProvider.GetThreadRandom().NextDouble() - 1;
                    y = 2 * RandomProvider.GetThreadRandom().NextDouble() - 1;
                    square = (x * x) + (y * y);
                } while (square >= 1);

                return x * Math.Sqrt(-2 * Math.Log(square) / square);
            }
        }
        internal static class RandomProvider
        {
            private static ThreadLocal<Random> randomWrapper = new ThreadLocal<Random>(() =>
                new Random(Interlocked.Increment(ref seed))
            );

            private static int seed = Environment.TickCount * 10;

            public static Random GetThreadRandom()
            {
                return randomWrapper.Value;
            }
        }
        public double calldelta(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S + 0.0001, r, vol, t);
            double cp2 = CallPrice(S - 0.0001, r, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double callgamma(double S, double r, double vol, double t)
        {
            return (calldelta(S + 0.01, r, vol, t) - calldelta(S, r, vol, t)) / 0.01;
        }
        public double callvega(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r, vol + 0.0001, t);
            double cp2 = CallPrice(S, r, vol - 0.0001, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double calltheta(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r, vol, t + 0.0001);
            double cp2 = CallPrice(S, r, vol, t - 0.0001);
            return (cp1 - cp2) / 0.0002;
        }
        public double callrho(double S, double r, double vol, double t)
        {
            double cp1 = CallPrice(S, r + 0.0001, vol, t);
            double cp2 = CallPrice(S, r - 0.0001, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double putdelta(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S + 0.0001, r, vol, t);
            double cp2 = PutPrice(S - 0.0001, r, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double putgamma(double S, double r, double vol, double t)
        {
            return (putdelta(S + 0.01, r, vol, t) - putdelta(S, r, vol, t)) / 0.01;
        }
        public double putvega(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r, vol + 0.0001, t);
            double cp2 = PutPrice(S, r, vol - 0.0001, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double puttheta(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r, vol, t + 0.0001);
            double cp2 = PutPrice(S, r, vol, t - 0.0001);
            return (cp1 - cp2) / 0.0002;
        }
        public double putrho(double S, double r, double vol, double t)
        {
            double cp1 = PutPrice(S, r + 0.0001, vol, t);
            double cp2 = PutPrice(S, r - 0.0001, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
    }
}
