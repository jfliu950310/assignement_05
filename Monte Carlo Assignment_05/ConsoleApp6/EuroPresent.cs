﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;

namespace ConsoleApp6
{
    public class EuroPresent
    {
        Stopwatch watch = new Stopwatch();

        public void EuroCall()
        {
            European euro = new European();
            watch.Start();
            euro.RandomGenerated();
            Value.Price = euro.CallPrice(Value.S, Value.r, Value.vol, Value.t);
            Value.SE = euro.SE();
            Value.delta = euro.calldelta(Value.S, Value.r, Value.vol, Value.t);
            Value.gamma = euro.callgamma(Value.S, Value.r, Value.vol, Value.t);
            Value.theta = euro.calltheta(Value.S, Value.r, Value.vol, Value.t);
            Value.vega = euro.callvega(Value.S, Value.r, Value.vol, Value.t);
            Value.rho = euro.callrho(Value.S, Value.r, Value.vol, Value.t);
            Value.Thread = 1;
            watch.Stop();
            Value.timer = watch.Elapsed.Seconds.ToString() + " : " + watch.Elapsed.Milliseconds.ToString();
            watch.Reset();
            Program.finished();
            GC.Collect();
        }

        public void EuroPut()
        {
            European euro = new European();
            watch.Start();
            euro.RandomGenerated();
            Value.Price = euro.PutPrice(Value.S, Value.r, Value.vol, Value.t);
            Value.SE = euro.SE();
            Value.delta = euro.putdelta(Value.S, Value.r, Value.vol, Value.t);
            Value.gamma = euro.putgamma(Value.S, Value.r, Value.vol, Value.t);
            Value.theta = euro.puttheta(Value.S, Value.r, Value.vol, Value.t);
            Value.vega = euro.putvega(Value.S, Value.r, Value.vol, Value.t);
            Value.rho = euro.putrho(Value.S, Value.r, Value.vol, Value.t);
            Value.Thread = 1;
            watch.Stop();
            Value.timer = watch.Elapsed.Seconds.ToString() + " : " + watch.Elapsed.Milliseconds.ToString();
            watch.Reset();
            Program.finished();
            GC.Collect();
        }

        public void EuroCallAnti()
        {
            EuropeanAntithetic euro = new EuropeanAntithetic(); 
            watch.Start();
            euro.RandomGenerated();
            Value.Price = euro.CallPrice(Value.S, Value.r, Value.vol, Value.t);
            Value.SE = euro.SE();
            Value.delta = euro.calldelta(Value.S, Value.r, Value.vol, Value.t);
            Value.gamma = euro.callgamma(Value.S, Value.r, Value.vol, Value.t);
            Value.theta = euro.calltheta(Value.S, Value.r, Value.vol, Value.t);
            Value.vega = euro.callvega(Value.S, Value.r, Value.vol, Value.t);
            Value.rho = euro.callrho(Value.S, Value.r, Value.vol, Value.t);
            Value.Thread = 1;
            watch.Stop();
            Value.timer = watch.Elapsed.Seconds.ToString() + " : " + watch.Elapsed.Milliseconds.ToString();
            watch.Reset();
            Program.finished();
            GC.Collect();
        }

        public void EuroPutAnti()
        {
            EuropeanAntithetic euro = new EuropeanAntithetic();
            watch.Start();
            euro.RandomGenerated();
            Value.Price = euro.PutPrice(Value.S, Value.r, Value.vol, Value.t);
            Value.SE = euro.SE();
            Value.delta = euro.putdelta(Value.S, Value.r, Value.vol, Value.t);
            Value.gamma = euro.putgamma(Value.S, Value.r, Value.vol, Value.t);
            Value.theta = euro.puttheta(Value.S, Value.r, Value.vol, Value.t);
            Value.vega = euro.putvega(Value.S, Value.r, Value.vol, Value.t);
            Value.rho = euro.putrho(Value.S, Value.r, Value.vol, Value.t);
            Value.Thread = 1;
            watch.Stop();
            Value.timer = watch.Elapsed.Seconds.ToString() + " : " + watch.Elapsed.Milliseconds.ToString();
            watch.Reset();
            Program.finished();
            GC.Collect();
        }

        public void EuroCallCV()
        {
            EuropeanCV euro = new EuropeanCV();
            watch.Start();
            euro.RandomGenerated();
            Value.Price = euro.CallPrice(Value.S, Value.r, Value.vol, Value.t);
            Value.SE = euro.SE();
            Value.delta = euro.calldelta(Value.S, Value.r, Value.vol, Value.t);
            Value.gamma = euro.callgamma(Value.S, Value.r, Value.vol, Value.t);
            Value.theta = euro.calltheta(Value.S, Value.r, Value.vol, Value.t);
            Value.vega = euro.callvega(Value.S, Value.r, Value.vol, Value.t);
            Value.rho = euro.callrho(Value.S, Value.r, Value.vol, Value.t);
            Value.Thread = 1;
            watch.Stop();
            Value.timer = watch.Elapsed.Seconds.ToString() + " : " + watch.Elapsed.Milliseconds.ToString();
            watch.Reset();
            Program.finished();
            GC.Collect();
        }

        public void EuroPutCV()
        {
            EuropeanCV euro = new EuropeanCV();
            watch.Start();
            euro.RandomGenerated();
            Value.Price = euro.PutPrice(Value.S, Value.r, Value.vol, Value.t);
            Value.SE = euro.SE();
            Value.delta = euro.putdelta(Value.S, Value.r, Value.vol, Value.t);
            Value.gamma = euro.putgamma(Value.S, Value.r, Value.vol, Value.t);
            Value.theta = euro.puttheta(Value.S, Value.r, Value.vol, Value.t);
            Value.vega = euro.putvega(Value.S, Value.r, Value.vol, Value.t);
            Value.rho = euro.putrho(Value.S, Value.r, Value.vol, Value.t);
            Value.Thread = 1;
            watch.Stop();
            Value.timer = watch.Elapsed.Seconds.ToString() + " : " + watch.Elapsed.Milliseconds.ToString();
            watch.Reset();
            Program.finished();
            GC.Collect();
        }

        public void EuroCallAntiCV()
        {
            EuropeanAntiCV euro = new EuropeanAntiCV();
            watch.Start();
            euro.RandomGenerated();
            Value.Price = euro.CallPrice(Value.S, Value.r, Value.vol, Value.t);
            Value.SE = euro.SE();
            Value.delta = euro.calldelta(Value.S, Value.r, Value.vol, Value.t);
            Value.gamma = euro.callgamma(Value.S, Value.r, Value.vol, Value.t);
            Value.theta = euro.calltheta(Value.S, Value.r, Value.vol, Value.t);
            Value.vega = euro.callvega(Value.S, Value.r, Value.vol, Value.t);
            Value.rho = euro.callrho(Value.S, Value.r, Value.vol, Value.t);
            Value.Thread = 1;
            watch.Stop();
            Value.timer = watch.Elapsed.Seconds.ToString() + " : " + watch.Elapsed.Milliseconds.ToString();
            watch.Reset();
            Program.finished();
            GC.Collect();
        }

        public void EuroPutAntiCV()
        {
            EuropeanAntiCV euro = new EuropeanAntiCV();
            watch.Start();
            euro.RandomGenerated();
            Value.Price = euro.PutPrice(Value.S, Value.r, Value.vol, Value.t);
            Value.SE = euro.SE();
            Value.delta = euro.putdelta(Value.S, Value.r, Value.vol, Value.t);
            Value.gamma = euro.putgamma(Value.S, Value.r, Value.vol, Value.t);
            Value.theta = euro.puttheta(Value.S, Value.r, Value.vol, Value.t);
            Value.vega = euro.putvega(Value.S, Value.r, Value.vol, Value.t);
            Value.rho = euro.putrho(Value.S, Value.r, Value.vol, Value.t);
            Value.Thread = 1;
            watch.Stop();
            Value.timer = watch.Elapsed.Seconds.ToString() + " : " + watch.Elapsed.Milliseconds.ToString();
            watch.Reset();
            Program.finished();
            GC.Collect();
        }

        public void EuroCallMT()
        {
            EuropeanMT euro = new EuropeanMT();
            watch.Start();
            euro.RandomGenerated();
            Value.Price = euro.CallPrice(Value.S, Value.r, Value.vol, Value.t);
            Value.SE = euro.SE();
            Value.delta = euro.calldelta(Value.S, Value.r, Value.vol, Value.t);
            Value.gamma = euro.callgamma(Value.S, Value.r, Value.vol, Value.t);
            Value.theta = euro.calltheta(Value.S, Value.r, Value.vol, Value.t);
            Value.vega = euro.callvega(Value.S, Value.r, Value.vol, Value.t);
            Value.rho = euro.callrho(Value.S, Value.r, Value.vol, Value.t);
            Value.Thread = 4;
            watch.Stop();
            Value.timer = watch.Elapsed.Seconds.ToString() + " : " + watch.Elapsed.Milliseconds.ToString();
            watch.Reset();
            Program.finished();
            GC.Collect();
        }

        public void EuroPutMT()
        {
            EuropeanMT euro = new EuropeanMT();
            watch.Start();
            euro.RandomGenerated();
            Value.Price = euro.PutPrice(Value.S, Value.r, Value.vol, Value.t);
            Value.SE = euro.SE();
            Value.delta = euro.putdelta(Value.S, Value.r, Value.vol, Value.t);
            Value.gamma = euro.putgamma(Value.S, Value.r, Value.vol, Value.t);
            Value.theta = euro.puttheta(Value.S, Value.r, Value.vol, Value.t);
            Value.vega = euro.putvega(Value.S, Value.r, Value.vol, Value.t);
            Value.rho = euro.putrho(Value.S, Value.r, Value.vol, Value.t);
            Value.Thread = 4;
            watch.Stop();
            Value.timer = watch.Elapsed.Seconds.ToString() + " : " + watch.Elapsed.Milliseconds.ToString();
            watch.Reset();
            Program.finished();
            GC.Collect();
        }

        public void EuroCallAntiMT()
        {
            EuropeanAntiMT euro = new EuropeanAntiMT();
            watch.Start();
            euro.RandomGenerated();
            Value.Price = euro.CallPrice(Value.S, Value.r, Value.vol, Value.t);
            Value.SE = euro.SE();
            Value.delta = euro.calldelta(Value.S, Value.r, Value.vol, Value.t);
            Value.gamma = euro.callgamma(Value.S, Value.r, Value.vol, Value.t);
            Value.theta = euro.calltheta(Value.S, Value.r, Value.vol, Value.t);
            Value.vega = euro.callvega(Value.S, Value.r, Value.vol, Value.t);
            Value.rho = euro.callrho(Value.S, Value.r, Value.vol, Value.t);
            Value.Thread = 4;
            watch.Stop();
            Value.timer = watch.Elapsed.Seconds.ToString() + " : " + watch.Elapsed.Milliseconds.ToString();
            watch.Reset();
            Program.finished();
            GC.Collect();
        }

        public void EuroPutAntiMT()
        {
            EuropeanAntiMT euro = new EuropeanAntiMT();
            watch.Start();
            euro.RandomGenerated();
            Value.Price = euro.PutPrice(Value.S, Value.r, Value.vol, Value.t);
            Value.SE = euro.SE();
            Value.delta = euro.putdelta(Value.S, Value.r, Value.vol, Value.t);
            Value.gamma = euro.putgamma(Value.S, Value.r, Value.vol, Value.t);
            Value.theta = euro.puttheta(Value.S, Value.r, Value.vol, Value.t);
            Value.vega = euro.putvega(Value.S, Value.r, Value.vol, Value.t);
            Value.rho = euro.putrho(Value.S, Value.r, Value.vol, Value.t);
            Value.Thread = 4;
            watch.Stop();
            Value.timer = watch.Elapsed.Seconds.ToString() + " : " + watch.Elapsed.Milliseconds.ToString();
            watch.Reset();
            Program.finished();
            GC.Collect();
        }

        public void EuroCallCVMT()
        {
            EuropeanCVMT euro = new EuropeanCVMT();
            watch.Start();
            euro.RandomGenerated();
            Value.Price = euro.CallPrice(Value.S, Value.r, Value.vol, Value.t);
            Value.SE = euro.SE();
            Value.delta = euro.calldelta(Value.S, Value.r, Value.vol, Value.t);
            Value.gamma = euro.callgamma(Value.S, Value.r, Value.vol, Value.t);
            Value.theta = euro.calltheta(Value.S, Value.r, Value.vol, Value.t);
            Value.vega = euro.callvega(Value.S, Value.r, Value.vol, Value.t);
            Value.rho = euro.callrho(Value.S, Value.r, Value.vol, Value.t);
            Value.Thread = 4;
            watch.Stop();
            Value.timer = watch.Elapsed.Seconds.ToString() + " : " + watch.Elapsed.Milliseconds.ToString();
            watch.Reset();
            Program.finished();
            GC.Collect();
        }

        public void EuroPutCVMT()
        {
            EuropeanCVMT euro = new EuropeanCVMT();
            watch.Start();
            euro.RandomGenerated();
            Value.Price = euro.PutPrice(Value.S, Value.r, Value.vol, Value.t);
            Value.SE = euro.SE();
            Value.delta = euro.putdelta(Value.S, Value.r, Value.vol, Value.t);
            Value.gamma = euro.putgamma(Value.S, Value.r, Value.vol, Value.t);
            Value.theta = euro.puttheta(Value.S, Value.r, Value.vol, Value.t);
            Value.vega = euro.putvega(Value.S, Value.r, Value.vol, Value.t);
            Value.rho = euro.putrho(Value.S, Value.r, Value.vol, Value.t);
            Value.Thread = 4;
            watch.Stop();
            Value.timer = watch.Elapsed.Seconds.ToString() + " : " + watch.Elapsed.Milliseconds.ToString();
            watch.Reset();
            Program.finished();
            GC.Collect();
        }

        public void EuroCallAntiCVMT()
        {
            EuropeanAntiCVMT euro = new EuropeanAntiCVMT();
            watch.Start();
            euro.RandomGenerated();
            Value.Price = euro.CallPrice(Value.S, Value.r, Value.vol, Value.t);
            Value.SE = euro.SE();
            Value.delta = euro.calldelta(Value.S, Value.r, Value.vol, Value.t);
            Value.gamma = euro.callgamma(Value.S, Value.r, Value.vol, Value.t);
            Value.theta = euro.calltheta(Value.S, Value.r, Value.vol, Value.t);
            Value.vega = euro.callvega(Value.S, Value.r, Value.vol, Value.t);
            Value.rho = euro.callrho(Value.S, Value.r, Value.vol, Value.t);
            Value.Thread = 4;
            watch.Stop();
            Value.timer = watch.Elapsed.Seconds.ToString() + " : " + watch.Elapsed.Milliseconds.ToString();
            watch.Reset();
            Program.finished();
            GC.Collect();
        }

        public void EuroPutAntiCVMT()
        {
            EuropeanAntiCVMT euro = new EuropeanAntiCVMT();
            watch.Start();
            euro.RandomGenerated();
            Value.Price = euro.PutPrice(Value.S, Value.r, Value.vol, Value.t);
            Value.SE = euro.SE();
            Value.delta = euro.putdelta(Value.S, Value.r, Value.vol, Value.t);
            Value.gamma = euro.putgamma(Value.S, Value.r, Value.vol, Value.t);
            Value.theta = euro.puttheta(Value.S, Value.r, Value.vol, Value.t);
            Value.vega = euro.putvega(Value.S, Value.r, Value.vol, Value.t);
            Value.rho = euro.putrho(Value.S, Value.r, Value.vol, Value.t);
            Value.Thread = 4;
            watch.Stop();
            Value.timer = watch.Elapsed.Seconds.ToString() + " : " + watch.Elapsed.Milliseconds.ToString();
            watch.Reset();
            Program.finished();
            GC.Collect();
        }
    }

}


