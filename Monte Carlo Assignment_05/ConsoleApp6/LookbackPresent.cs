﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Diagnostics;

namespace ConsoleApp6
{
    public class LookbackPresent
    {
        Stopwatch watch = new Stopwatch();

        public void LookbackCall()
        {
            Lookback euro = new Lookback();
            watch.Start();
            euro.RandomGenerated();
            Value.Price = euro.CallPrice(Value.S, Value.r, Value.vol, Value.t);
            Value.SE = euro.SE();
            Value.delta = euro.calldelta(Value.S, Value.r, Value.vol, Value.t);
            Value.gamma = euro.callgamma(Value.S, Value.r, Value.vol, Value.t);
            Value.theta = euro.calltheta(Value.S, Value.r, Value.vol, Value.t);
            Value.vega = euro.callvega(Value.S, Value.r, Value.vol, Value.t);
            Value.rho = euro.callrho(Value.S, Value.r, Value.vol, Value.t);
            Value.Thread = 1;
            watch.Stop();
            Value.timer = watch.Elapsed.Seconds.ToString() + " : " + watch.Elapsed.Milliseconds.ToString();
            watch.Reset();
            Program.finished();
            GC.Collect();
        }

        public void LookbackPut()
        {
            Lookback euro = new Lookback();
            watch.Start();
            euro.RandomGenerated();
            Value.Price = euro.PutPrice(Value.S, Value.r, Value.vol, Value.t);
            Value.SE = euro.SE();
            Value.delta = euro.putdelta(Value.S, Value.r, Value.vol, Value.t);
            Value.gamma = euro.putgamma(Value.S, Value.r, Value.vol, Value.t);
            Value.theta = euro.puttheta(Value.S, Value.r, Value.vol, Value.t);
            Value.vega = euro.putvega(Value.S, Value.r, Value.vol, Value.t);
            Value.rho = euro.putrho(Value.S, Value.r, Value.vol, Value.t);
            Value.Thread = 1;
            watch.Stop();
            Value.timer = watch.Elapsed.Seconds.ToString() + " : " + watch.Elapsed.Milliseconds.ToString();
            watch.Reset();
            Program.finished();
            GC.Collect();
        }

        public void LookbackCallAnti()
        {
            LookbackAntithetic euro = new LookbackAntithetic();
            watch.Start();
            euro.RandomGenerated();
            Value.Price = euro.CallPrice(Value.S, Value.r, Value.vol, Value.t);
            Value.SE = euro.SE();
            Value.delta = euro.calldelta(Value.S, Value.r, Value.vol, Value.t);
            Value.gamma = euro.callgamma(Value.S, Value.r, Value.vol, Value.t);
            Value.theta = euro.calltheta(Value.S, Value.r, Value.vol, Value.t);
            Value.vega = euro.callvega(Value.S, Value.r, Value.vol, Value.t);
            Value.rho = euro.callrho(Value.S, Value.r, Value.vol, Value.t);
            Value.Thread = 1;
            watch.Stop();
            Value.timer = watch.Elapsed.Seconds.ToString() + " : " + watch.Elapsed.Milliseconds.ToString();
            watch.Reset();
            Program.finished();
            GC.Collect();
        }

        public void LookbackPutAnti()
        {
            LookbackAntithetic euro = new LookbackAntithetic();
            watch.Start();
            euro.RandomGenerated();
            Value.Price = euro.PutPrice(Value.S, Value.r, Value.vol, Value.t);
            Value.SE = euro.SE();
            Value.delta = euro.putdelta(Value.S, Value.r, Value.vol, Value.t);
            Value.gamma = euro.putgamma(Value.S, Value.r, Value.vol, Value.t);
            Value.theta = euro.puttheta(Value.S, Value.r, Value.vol, Value.t);
            Value.vega = euro.putvega(Value.S, Value.r, Value.vol, Value.t);
            Value.rho = euro.putrho(Value.S, Value.r, Value.vol, Value.t);
            Value.Thread = 1;
            watch.Stop();
            Value.timer = watch.Elapsed.Seconds.ToString() + " : " + watch.Elapsed.Milliseconds.ToString();
            watch.Reset();
            Program.finished();
            GC.Collect();
        }

        public void LookbackCallCV()
        {
            LookbackCV euro = new LookbackCV();
            watch.Start();
            euro.RandomGenerated();
            Value.Price = euro.CallPrice(Value.S, Value.r, Value.vol, Value.t);
            Value.SE = euro.SE();
            Value.delta = euro.calldelta(Value.S, Value.r, Value.vol, Value.t);
            Value.gamma = euro.callgamma(Value.S, Value.r, Value.vol, Value.t);
            Value.theta = euro.calltheta(Value.S, Value.r, Value.vol, Value.t);
            Value.vega = euro.callvega(Value.S, Value.r, Value.vol, Value.t);
            Value.rho = euro.callrho(Value.S, Value.r, Value.vol, Value.t);
            Value.Thread = 1;
            watch.Stop();
            Value.timer = watch.Elapsed.Seconds.ToString() + " : " + watch.Elapsed.Milliseconds.ToString();
            watch.Reset();
            Program.finished();
            GC.Collect();
        }

        public void LookbackPutCV()
        {
            LookbackCV euro = new LookbackCV();
            watch.Start();
            euro.RandomGenerated();
            Value.Price = euro.PutPrice(Value.S, Value.r, Value.vol, Value.t);
            Value.SE = euro.SE();
            Value.delta = euro.putdelta(Value.S, Value.r, Value.vol, Value.t);
            Value.gamma = euro.putgamma(Value.S, Value.r, Value.vol, Value.t);
            Value.theta = euro.puttheta(Value.S, Value.r, Value.vol, Value.t);
            Value.vega = euro.putvega(Value.S, Value.r, Value.vol, Value.t);
            Value.rho = euro.putrho(Value.S, Value.r, Value.vol, Value.t);
            Value.Thread = 1;
            watch.Stop();
            Value.timer = watch.Elapsed.Seconds.ToString() + " : " + watch.Elapsed.Milliseconds.ToString();
            watch.Reset();
            Program.finished();
            GC.Collect();
        }

        public void LookbackCallAntiCV()
        {
            LookbackAntiCV euro = new LookbackAntiCV();
            watch.Start();
            euro.RandomGenerated();
            Value.Price = euro.CallPrice(Value.S, Value.r, Value.vol, Value.t);
            Value.SE = euro.SE();
            Value.delta = euro.calldelta(Value.S, Value.r, Value.vol, Value.t);
            Value.gamma = euro.callgamma(Value.S, Value.r, Value.vol, Value.t);
            Value.theta = euro.calltheta(Value.S, Value.r, Value.vol, Value.t);
            Value.vega = euro.callvega(Value.S, Value.r, Value.vol, Value.t);
            Value.rho = euro.callrho(Value.S, Value.r, Value.vol, Value.t);
            Value.Thread = 1;
            watch.Stop();
            Value.timer = watch.Elapsed.Seconds.ToString() + " : " + watch.Elapsed.Milliseconds.ToString();
            watch.Reset();
            Program.finished();
            GC.Collect();
        }

        public void LookbackPutAntiCV()
        {
            LookbackAntiCV euro = new LookbackAntiCV();
            watch.Start();
            euro.RandomGenerated();
            Value.Price = euro.PutPrice(Value.S, Value.r, Value.vol, Value.t);
            Value.SE = euro.SE();
            Value.delta = euro.putdelta(Value.S, Value.r, Value.vol, Value.t);
            Value.gamma = euro.putgamma(Value.S, Value.r, Value.vol, Value.t);
            Value.theta = euro.puttheta(Value.S, Value.r, Value.vol, Value.t);
            Value.vega = euro.putvega(Value.S, Value.r, Value.vol, Value.t);
            Value.rho = euro.putrho(Value.S, Value.r, Value.vol, Value.t);
            Value.Thread = 1;
            watch.Stop();
            Value.timer = watch.Elapsed.Seconds.ToString() + " : " + watch.Elapsed.Milliseconds.ToString();
            watch.Reset();
            Program.finished();
            GC.Collect();
        }

        public void LookbackCallMT()
        {
            LookbackMT euro = new LookbackMT();
            watch.Start();
            euro.RandomGenerated();
            Value.Price = euro.CallPrice(Value.S, Value.r, Value.vol, Value.t);
            Value.SE = euro.SE();
            Value.delta = euro.calldelta(Value.S, Value.r, Value.vol, Value.t);
            Value.gamma = euro.callgamma(Value.S, Value.r, Value.vol, Value.t);
            Value.theta = euro.calltheta(Value.S, Value.r, Value.vol, Value.t);
            Value.vega = euro.callvega(Value.S, Value.r, Value.vol, Value.t);
            Value.rho = euro.callrho(Value.S, Value.r, Value.vol, Value.t);
            Value.Thread = 4;
            watch.Stop();
            Value.timer = watch.Elapsed.Seconds.ToString() + " : " + watch.Elapsed.Milliseconds.ToString();
            watch.Reset();
            Program.finished();
            GC.Collect();
        }

        public void LookbackPutMT()
        {
            LookbackMT euro = new LookbackMT();
            watch.Start();
            euro.RandomGenerated();
            Value.Price = euro.PutPrice(Value.S, Value.r, Value.vol, Value.t);
            Value.SE = euro.SE();
            Value.delta = euro.putdelta(Value.S, Value.r, Value.vol, Value.t);
            Value.gamma = euro.putgamma(Value.S, Value.r, Value.vol, Value.t);
            Value.theta = euro.puttheta(Value.S, Value.r, Value.vol, Value.t);
            Value.vega = euro.putvega(Value.S, Value.r, Value.vol, Value.t);
            Value.rho = euro.putrho(Value.S, Value.r, Value.vol, Value.t);
            Value.Thread = 4;
            watch.Stop();
            Value.timer = watch.Elapsed.Seconds.ToString() + " : " + watch.Elapsed.Milliseconds.ToString();
            watch.Reset();
            Program.finished();
            GC.Collect();
        }

        public void LookbackCallAntiMT()
        {
            LookbackAntiMT euro = new LookbackAntiMT();
            watch.Start();
            euro.RandomGenerated();
            Value.Price = euro.CallPrice(Value.S, Value.r, Value.vol, Value.t);
            Value.SE = euro.SE();
            Value.delta = euro.calldelta(Value.S, Value.r, Value.vol, Value.t);
            Value.gamma = euro.callgamma(Value.S, Value.r, Value.vol, Value.t);
            Value.theta = euro.calltheta(Value.S, Value.r, Value.vol, Value.t);
            Value.vega = euro.callvega(Value.S, Value.r, Value.vol, Value.t);
            Value.rho = euro.callrho(Value.S, Value.r, Value.vol, Value.t);
            Value.Thread = 4;
            watch.Stop();
            Value.timer = watch.Elapsed.Seconds.ToString() + " : " + watch.Elapsed.Milliseconds.ToString();
            watch.Reset();
            Program.finished();
            GC.Collect();
        }

        public void LookbackPutAntiMT()
        {
            LookbackAntiMT euro = new LookbackAntiMT();
            watch.Start();
            euro.RandomGenerated();
            Value.Price = euro.PutPrice(Value.S, Value.r, Value.vol, Value.t);
            Value.SE = euro.SE();
            Value.delta = euro.putdelta(Value.S, Value.r, Value.vol, Value.t);
            Value.gamma = euro.putgamma(Value.S, Value.r, Value.vol, Value.t);
            Value.theta = euro.puttheta(Value.S, Value.r, Value.vol, Value.t);
            Value.vega = euro.putvega(Value.S, Value.r, Value.vol, Value.t);
            Value.rho = euro.putrho(Value.S, Value.r, Value.vol, Value.t);
            Value.Thread = 4;
            watch.Stop();
            Value.timer = watch.Elapsed.Seconds.ToString() + " : " + watch.Elapsed.Milliseconds.ToString();
            watch.Reset();
            Program.finished();
            GC.Collect();
        }

        public void LookbackCallCVMT()
        {
            LookbackCVMT euro = new LookbackCVMT();
            watch.Start();
            euro.RandomGenerated();
            Value.Price = euro.CallPrice(Value.S, Value.r, Value.vol, Value.t);
            Value.SE = euro.SE();
            Value.delta = euro.calldelta(Value.S, Value.r, Value.vol, Value.t);
            Value.gamma = euro.callgamma(Value.S, Value.r, Value.vol, Value.t);
            Value.theta = euro.calltheta(Value.S, Value.r, Value.vol, Value.t);
            Value.vega = euro.callvega(Value.S, Value.r, Value.vol, Value.t);
            Value.rho = euro.callrho(Value.S, Value.r, Value.vol, Value.t);
            Value.Thread = 4;
            watch.Stop();
            Value.timer = watch.Elapsed.Seconds.ToString() + " : " + watch.Elapsed.Milliseconds.ToString();
            watch.Reset();
            Program.finished();
            GC.Collect();
        }

        public void LookbackPutCVMT()
        {
            LookbackCVMT euro = new LookbackCVMT();
            watch.Start();
            euro.RandomGenerated();
            Value.Price = euro.PutPrice(Value.S, Value.r, Value.vol, Value.t);
            Value.SE = euro.SE();
            Value.delta = euro.putdelta(Value.S, Value.r, Value.vol, Value.t);
            Value.gamma = euro.putgamma(Value.S, Value.r, Value.vol, Value.t);
            Value.theta = euro.puttheta(Value.S, Value.r, Value.vol, Value.t);
            Value.vega = euro.putvega(Value.S, Value.r, Value.vol, Value.t);
            Value.rho = euro.putrho(Value.S, Value.r, Value.vol, Value.t);
            Value.Thread = 4;
            watch.Stop();
            Value.timer = watch.Elapsed.Seconds.ToString() + " : " + watch.Elapsed.Milliseconds.ToString();
            watch.Reset();
            Program.finished();
            GC.Collect();
        }

        public void LookbackCallAntiCVMT()
        {
            LookbackAntiCVMT euro = new LookbackAntiCVMT();
            watch.Start();
            euro.RandomGenerated();
            Value.Price = euro.CallPrice(Value.S, Value.r, Value.vol, Value.t);
            Value.SE = euro.SE();
            Value.delta = euro.calldelta(Value.S, Value.r, Value.vol, Value.t);
            Value.gamma = euro.callgamma(Value.S, Value.r, Value.vol, Value.t);
            Value.theta = euro.calltheta(Value.S, Value.r, Value.vol, Value.t);
            Value.vega = euro.callvega(Value.S, Value.r, Value.vol, Value.t);
            Value.rho = euro.callrho(Value.S, Value.r, Value.vol, Value.t);
            Value.Thread = 4;
            watch.Stop();
            Value.timer = watch.Elapsed.Seconds.ToString() + " : " + watch.Elapsed.Milliseconds.ToString();
            watch.Reset();
            Program.finished();
            GC.Collect();
        }

        public void LookbackPutAntiCVMT()
        {
            LookbackAntiCVMT euro = new LookbackAntiCVMT();
            watch.Start();
            euro.RandomGenerated();
            Value.Price = euro.PutPrice(Value.S, Value.r, Value.vol, Value.t);
            Value.SE = euro.SE();
            Value.delta = euro.putdelta(Value.S, Value.r, Value.vol, Value.t);
            Value.gamma = euro.putgamma(Value.S, Value.r, Value.vol, Value.t);
            Value.theta = euro.puttheta(Value.S, Value.r, Value.vol, Value.t);
            Value.vega = euro.putvega(Value.S, Value.r, Value.vol, Value.t);
            Value.rho = euro.putrho(Value.S, Value.r, Value.vol, Value.t);
            Value.Thread = 4;
            watch.Stop();
            Value.timer = watch.Elapsed.Seconds.ToString() + " : " + watch.Elapsed.Milliseconds.ToString();
            watch.Reset();
            Program.finished();
            GC.Collect();
        }
    }
}
